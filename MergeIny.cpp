/*****************************************************************************
 * 
 * Commands:
 *    -U    Monthly update
 *    -L    Lien update
 *
 * Revision:
 * 05/24/2006 1.0.0     First version by Tung Bui
 * 08/01/2006 1.8.2     Remove sale update from Pamsale.txt since all
 *                      needed sale data are on roll file already - spn
 * 10/30/2006 1.8.3.3   Fix bug that causes infinit loop in Iny_MergeChar().
 * 09/14/2007 1.8.18    Add -Xc and -Xl option to extract sale and lien.
 * 12/03/2007 1.8.23    Fix Iny_LoadRoll() to update CHAR. - spn
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 07/16/2008 8.1.2     Add function Iny_Load_LDR() to load LDR data only, no update.
 *                      Modify Iny_FmtRecDoc() to capture sale price more accurately.
 *                      Create function Iny_ExtrSale() to extract sales from Pamsales.txt.
 *                      But this function is not currently needed. 
 * 07/10/2009 9.1.2     Reactivate Iny_ExtrSale() to extract sale from Pamsale.txt
 * 08/04/2009 9.1.3     Populate other values.
 * 02/23/2010 9.2.1     Fix CARE_OF bug and modify Iny_MergeOwner() to keep original 
 *                      owner name when possible.
 * 07/17/2011 11.0.1    Add S_HSENO
 * 07/22/2011 11.1.2    Clear out old situs before applying new one.
 * 10/21/2011 11.2.8    Modify Iny_Fmt1Doc() to format DocNum from roll file.  Add
 *                      Iny_FmtPamDoc() to format DocNum from PamSales.txt.  Modify
 *                      Iny_ExtrSale() to output in SCSAL_REC format and correctly
 *                      identify sale price and DocType. Add Iny_UpdSale1() to update
 *                      cum sale with sale1 from roll file. Add -Xsi option and 
 *                      use ApplyCumSale() to update sales in R01 file.
 * 10/26/2011 11.2.8.1  Modify Iny_UpdSale1() and Iny_ExtrSale() to better identify sale price.
 * 07/17/2012 12.1.3    Fix OTHER_VAL in Iny_CreateR01() to get data from FixtVal instead
 *                      of PersFixtVal, LeaseVal, and MobileVal which were not populated by INY.
 *                      Remove obsolete function Iny_LoadLien() and Iny_LoadUpdt().
 * 09/23/2013 13.2.7    Fix MergeOwner() to keep names as is if they are a corp name.
 *                      Update Vesting if found. Set ETAL flag.
 * 09/27/2013 13.3.0    Modify Iny_MergeOwner() and Iny_CleanName() to update Vesting.
 *                      Use removeNames() to clear owner names
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 10/22/2013 13.4.1    Add Iny_ConvStdChar() to convert Building.txt to STDCHAR format.
 * 11/09/2013 13.4.2.1  Fix bug in Iny_MergeChar() iQBaths is not initialized.
 * 01/30/2014 13.5.0    Modify Iny_ExtrSale() to add SaleCode and NoneSale_Flg based on 
 *                      known DocType. Fix APN length bug that trims trailing digits.
 *                      Fix Iny_UpdSale1() to append roll sale to Iny_Sale.rol instead of Iny_Sale.sls
 *                      Change sort seq for output file Iny_Sale.rol. Modify sale extract process.
 *                      Set SaleCode to 'F' if DocCode is 1 and to 'P' if DocCode is 3 or 70.
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 02/13/2016 15.6.0    Clean up Iny_MergeOwner().
 * 04/14/2016 15.9.1    Add -T to support loading tax file.  Still need detail data for Items & Agency table.
 * 07/20/2016 16.1.1    Modify Iny_MergeAdr() to correct situs city from misspelling when possible.
 * 08/30/2016 16.2.2    Use doTaxPrep() to create empty table for Tax_Items & Tax_Agency
 * 10/01/2016 16.3.2    Fix bug in loadIny() by using GetIniString() instead of GetPrivateProfileString()
 *                      since GetIniString() translates all relative path and the other does not.
 * 01/18/2017 16.8.1    Remove call to load TaxOwner.
 * 02/07/2017 16.9.0    Process GFGIS to populate Agency & Items tables.
 * 07/28/2017 17.0.1    Modify Iny_CreateR01() to ignore retired parcels.
 * 08/08/2017 17.0.2    Fix bug in Iny_LoadRoll() that enters infinite loop.
 * 11/17/2017 17.1.0    Call updateDelqFlag() to update Delq info in Tax_Base table
 * 05/04/2018 17.6.0    Use Cres_Load_Cortac() to load TaxBase.
 * 05/10/2018 17.8.0    Turn off option to load TaxDelq in Cres_Load_Cortac()
 * 06/25/2018 17.9.0    Call LoadTaxCodeTable() to load tax code table.
 * 06/14/2020 19.6.1    Add -Xn & -Mn
 * 07/18/2020 20.1.1    Modify Iny_MergeAdr() to verify direction before assign to S_DIR.
 * 08/09/2020 20.1.4    Add Iny_Load_LDR_Csv(), Iny_ExtrLienCsv(), Iny_MergeRollChar(), Iny_MergeMAdr(),
 *                      Iny_MergeSAdr(), Iny_MergeLienCsv() to support new LDR layout.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "doSort.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "PQ.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "CharRec.h"
#include "MergeIny.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "Tax.h"
#include "NdcExtr.h"

static FILE *fdVal;
static long lRollMatch, lRollSkip;

/****************************** Iny_ParseSitus1 *****************************
 *
 * Prefill "0" to make APN
 *
 ****************************************************************************/

void Iny_ParseSitus1(ADR_REC *pAdrRec, char *pAdr, char *pApn)
{
   char  acAdr[128], acStr[64], acTmp[32], *apItems[16], *pTmp;
   int   iCnt, iTmp, iIdx, iSfxCode;
   bool  bRet;

   // Clear output buffer
   memset((void *)pAdrRec, 0, sizeof(ADR_REC));

   strcpy(acAdr, pAdr);
   iCnt = ParseString(acAdr, ' ', 16, apItems);
   if (iCnt < 2)
   {
      strcpy(pAdrRec->strName, pAdr);
      pAdrRec->lStrNum = 0;
      return;
   }

#ifdef _DEBUG
   if (!memcmp(pApn, "0111101800", 10))
      iTmp = 0;
#endif

   // If strNum is 0, do not parse address
   iIdx = 0;
   //if (isNumber(apItems[0]))
   if (isdigit(*apItems[0]))
   {
      iIdx++;
      iTmp = atoi(apItems[0]);
      pAdrRec->lStrNum = iTmp;

      if (pTmp = strchr(apItems[0], '-'))
      {
         strcpy(pAdrRec->strSub, pTmp+1);
         *pTmp = 0;
      }
      strcpy(pAdrRec->strNum, apItems[0]);

   }

   // Check for sub hseno
   if (strchr(apItems[iIdx], '/'))
   {
      strcpy(pAdrRec->strSub, apItems[iIdx]);
      iIdx++;
   }

   // Dir
   bool bDir;
   bDir = isDir(apItems[iIdx]);

   // Possible cases E ST or W RD
   // Check for suffix
   bRet = false;
   acStr[0] = 0;
   if (bDir && iCnt > 2)
   {
      // If next item is suffix, then this one is not direction
      iSfxCode = GetSfxCode(apItems[iIdx+1]);
      if (iSfxCode)
      {
         strcpy(acStr, apItems[iIdx]);
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->SfxName, apItems[iIdx+1]);
         bRet = true;         // Done
      } else
      {
         // This is direction
         strcpy(pAdrRec->strDir, apItems[iIdx]);
         iIdx++;
      }
   }

   if (!bRet)
   {
      // Not direction, token is str name
      acTmp[0] = 0;
      iSfxCode = GetSfxCode(apItems[iCnt-1]);
      if (iSfxCode)
      {
         // Multi-word street name with suffix
         sprintf(pAdrRec->strSfx, "%d", iSfxCode);
         strcpy(pAdrRec->SfxName, apItems[iCnt-1]);
         while (iIdx < iCnt-1)
         {
            strcat(acStr, apItems[iIdx++]);
            if (iIdx < iCnt)
               strcat(acStr, " ");
         }
      } else
      {
         iSfxCode = GetSfxCode(apItems[iCnt-2]);
         if (iSfxCode)
         {
            // W LINE ST A
            sprintf(pAdrRec->strSfx, "%d", iSfxCode);
            strcpy(pAdrRec->SfxName, apItems[iCnt-2]);
            while (iIdx < iCnt-2)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt-2)
                  strcat(acStr, " ");
            }

            if (*apItems[iCnt-1] != '(')
               strcpy(pAdrRec->Unit, apItems[iCnt-1]);
         } else
         {
            // Multi-word street name without suffix
            while (iIdx < iCnt)
            {
               strcat(acStr, apItems[iIdx++]);
               if (iIdx < iCnt)
                  strcat(acStr, " ");
            }
         }
      }
   }

   acStr[SIZ_M_STREET] = 0;
   strcpy(pAdrRec->strName, acStr);
}

/******************************* Iny_FormatApn ******************************
 *
 * Prefill "0" to make APN
 *
 ****************************************************************************/

int Iny_FormatApn(char *pApn, char *pFmtApn, bool bChkCsv=false)
{
   int   iTmp, iRet=0;
   char  sApn[32], *pTmp;

   if (bChkCsv)
   {
      pTmp = strchr(pApn, cDelim);
      *pTmp = 0;
      strcpy(sApn, pApn);
      *pTmp = ',';
   } else
      strcpy(sApn, pApn);

   // Format APN
   iTmp = strlen(sApn);
   if (iTmp > 6 && iTmp < iApnLen)
      iRet = sprintf(pFmtApn, "%.*s%s", iApnLen-iTmp, "000000", sApn);
   else if (iTmp == iApnLen)
   {
      strcpy(pFmtApn, sApn);
      iRet = iApnLen;
   }

   return iRet;
}

/********************************* Iny_FmtRecDoc *****************************
 *
 *  1) 0477003219761118
 *  2) 1641007219991206
 *  3) 0501021920050519
 *
 *****************************************************************************/

void formatIny(char *pApn, char *pFmtApn)
{
   if (pApn[10] == ' ')
      sprintf(pFmtApn, "%.3s%.3s%.2s00", pApn, pApn+4, pApn+8);
   else
      sprintf(pFmtApn, "%.3s%.3s%.2s%.2s", pApn, pApn+4, pApn+8, pApn+11);
}

/*
 *    2006/0533-0208 (005-066-01)
 *    2005/0942-0318   ==> 20050318 942
 *    2002/01110032202 ==> 20020322 1110
 *    1999/01259 32399 ==> 19990323 1259
 *    0236/123 021679  ==> 19790216 236/123
 *    225/775 081677   ==> 19770816 225/775
 *    94/00371012694   ==> 19940126 371
 *    93/04515090393   ==> 19930903 4515
 *    01/04158101901   ==> 20011019 4158
 *            071901   ==> 20010719
 *    22/053  022431   ==> 19310224 022/053
 */

void Iny_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   char  acDate[16], acDoc[64], *pTmp;
   int   iMonth, iDay, iYear, iCurYear, iDoc;


   acDate[SIZ_SALE1_DT] = 0;
   acDoc[SIZ_SALE1_DOC] = 0;
   memset(acDate, ' ', SIZ_SALE1_DT);
   memset(acDoc, ' ', SIZ_SALE1_DOC);

   iMonth=iDay=iYear=iDoc = 0;
   pTmp = pRecDoc;
   if (*(pTmp+4) == '/' && *pTmp > '0')
   {  // 2006/0533-0208
      if (*(pTmp+9) == '-')
         iDoc = atoin(pTmp+5, 4);
      else
         iDoc = atoin(pTmp+5, 5);
      iYear = atoin(pTmp, 4);
      iDay = atoin(pTmp+12, 2);
      iMonth = atoin(pTmp+10, 2);
   } else if (*(pTmp+4) == '/' && *(pTmp+8) == ' ')
   {  // 0236/123 021679
      sprintf(acDoc, "%.7s", pTmp+1);
      iYear = atoin(pTmp+13, 2);
      iYear += 1900;
      iDay = atoin(pTmp+11, 2);
      iMonth = atoin(pTmp+9, 2);
   } else  
   {
      iYear = atoin(pTmp+12, 2);
      iDay = atoin(pTmp+10, 2);
      iMonth = atoin(pTmp+8, 2);
      
      if (*(pTmp+2) == '/')
      {
         // 22/053  022431
         if (*(pTmp+5) == ' ' || *(pTmp+6) == ' ')
         {
            sprintf(acDoc, "%.3d/%.3d", atoi(pTmp), atoi(pTmp+3));
            iYear += 1900;
         } else
         {
            if (!memcmp(pTmp, pTmp+12, 2) || *pTmp > '9')
            {  // 01/04158101901
               iDoc = atoin(pTmp+3, 5);
            } else
            {  // This case never occurs
               sprintf(acDoc, "0%.2s/%.3d", pTmp, atoin(pTmp+3, 3));
               iYear += 1900;
            }
         }
      } else if (*(pTmp+3) == '/')
      {  // This case never occurs
         sprintf(acDoc, "%.7s", pTmp);
      }
   }

   if (iMonth > 0)
   {
      if (iYear < 100)
      {
         iCurYear = getCurYear();
         if (iYear > iCurYear-2000)
            iYear += 1900;
         else
            iYear += 2000;
      }
      sprintf(acDate, "%.4d%.2d%.2d", iYear, iMonth, iDay);
   }

   if (iDoc > 0)
      sprintf(acDoc, "%.4d/%.4d", iYear, iDoc);

   blankPad(acDoc, SIZ_SALE1_DOC);
   memcpy(pOutDoc, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutDate, acDate, SIZ_SALE1_DT);
}

void Iny_FmtPamDoc(char *pOutDoc, char *pRecDoc, char *pRecDate)
{
   char  acDoc[64], *pTmp;
   int   iDoc;

   // Ignore bad record
   if (*(pRecDoc+2) == '/' && *(pRecDoc+5) == '/')
   {
      *pOutDoc = 0;
      return;
   }

   memset(acDoc, ' ', SIZ_SALE1_DOC);
   acDoc[SIZ_SALE1_DOC] = 0;
   iDoc = 0;
   pTmp = pRecDoc;
   if (*(pTmp+4) == '/' && *pTmp > '0')
   {
      if (*(pTmp+9) == '-')
         iDoc = atoin(pTmp+5, 4);         // never occurs
      else
         iDoc = atoin(pTmp+5, 5);
   } else if (*(pTmp+4) == '/' && *(pTmp+8) == ' ')
   {  // 123/456
      sprintf(acDoc, "%.7s", pTmp+1);     // never occurs
   } else  
   {
      if (*(pTmp+2) == '/')
      {
         if (*(pTmp+5) == ' ' || *(pTmp+6) == ' ')
            sprintf(acDoc, "%.3d/%.3d", atoi(pTmp), atoi(pTmp+3));
         else
         {
            if (!memcmp(pTmp, pRecDate+2, 2) || *pTmp > '9')
               iDoc = atoin(pTmp+3, 5);
            else
               sprintf(acDoc, "0%.2s/%.3d", pTmp, atoi(pTmp+3));
         }
      } else if (isdigit(*pTmp) && *(pTmp+3) == '/')
         sprintf(acDoc, "%.7s", pTmp);
      else
         acDoc[0] = 0;
   }

   if (iDoc > 0)
      sprintf(acDoc, "%.4s/%.4d", pRecDate, iDoc);
   strcpy(pOutDoc, acDoc);
}

void Iny_FmtRecDoc(char *pOutbuf, char *pRollRec)
{
   long     lPrice, lDate;
   double   dTmp, dTmp1, dPrice;
   char     acTmp[32], acDate[16], acDoc[64], *pTmp;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   pTmp = pRec->RecBook1;
   Iny_Fmt1Doc(acDoc, acDate, pTmp);
   lDate = atol(acDate);
   if (lDate > 19000101 && lDate < lToday)
   {
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

      // Update last recording date
      if (lDate > lLastRecDate && lDate < lToday)
         lLastRecDate = lDate;

      dTmp = atofn(pRec->StampAmt, CRESIZ_STMP_AMT);
      if (dTmp > 1.0)
      {
         if (pRec->StampAmt[8] == '.')
         {  // DocTax ?
            lPrice = (long)((dTmp * SALE_FACTOR)*100.0);
            dPrice = lPrice/100.0;
            lPrice = (long)(dTmp * SALE_FACTOR);
            dTmp1 = lPrice;
            if (dTmp > 1000.00 && dPrice > dTmp1)
            {
               lPrice = (long)dTmp;
               if (lPrice < 5000)
                  LogMsg0("*** 1. Questionable sale price on APN=%.10s, Amt=%.2f", pOutbuf, dTmp);
            } else if (dTmp > 9999.0)
            {
               lPrice = (long)dTmp;
               if (lPrice < 500)
                  LogMsg0("*** 2. Questionable sale price on APN=%.10s, Amt=%.2f", pOutbuf, dTmp);
            } else if (dTmp > 5000.00)
                  LogMsg0("*** 3. Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.2f", pOutbuf, lPrice, dTmp);

            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         } else
         {  // Sale price
            lPrice = atoin(pRec->StampAmt, 8);
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            if (lPrice < 500)
               LogMsg0("??? Suspected sale price for APN: %.10s, Amt=%d", pOutbuf, lPrice);
         }
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }
   } else
      return;

   // Advance to Rec #2
   pTmp += 16;
   Iny_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDate[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE1_DOC);
   }

   // Advance to Rec #3
   pTmp += 16;
   Iny_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDate[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE1_DOC);
   }
}

/******************************** Iny_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Iny_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;
   int   iCnt;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   if ((pTmp=strstr(acTmp, " 1/")) || (pTmp=strstr(acTmp, " 2/3")) )
   {
      if (*(pTmp-1) == ',')
         pTmp--;
      *pTmp = 0;
   } else if (pTmp=strstr(acTmp, "/100"))
   {
      *(pTmp-3) = 0;
   }

   blankRem((char *)&acTmp[0]);

   iCnt = 0;
   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Iny_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Iny_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  *pName2, acName1[64], acVesting[8], acTmp[64];
   OWNER myOwners;

   // Clear output buffer if needed
   removeNames(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0091200205", 10))
   //   iTmp = 0;
#endif

   // Save name1
   memcpy(acName1, pNames, CRESIZ_NAME_1*2);
   acName1[CRESIZ_NAME_1*2] = 0;
   
   // Point to name2
   pName2 = &acName1[CRESIZ_NAME_1];

   if (!memcmp(pName2, "C/O", 3))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   }

   // Update Owner1
   memcpy(pOutbuf+OFF_NAME1, acName1, CRESIZ_NAME_1);

   // Cleanup Name1 
   iTmp = Iny_CleanName(acName1, acTmp, acVesting, CRESIZ_NAME_1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acName1, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // Now parse owners
   splitOwner(acName1, &myOwners, 3);
   if (acVesting[0] > ' ' && isVestChk(acVesting))
   {
      memcpy(myOwners.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwners.acSwapName[SIZ_NAME1] = 0;
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwners.acSwapName, SIZ_NAME_SWAP);

}

void Iny_MergeOwner(char *pOutbuf, char *pOwner, char *pOwnerX)
{
   int   iTmp;
   char  acName1[256], acVesting[8], acTmp[64];
   OWNER myOwners;

   // Clear output buffer if needed
   removeNames(pOutbuf);

//#ifdef _DEBUG
//   if (!memcmp(pOutbuf, "0263400200", 10))
//      iTmp = 0;
//#endif

   // Save name1
   _strupr(pOwner);
   _strupr(pOwnerX);
   if (*pOwnerX > ' ')
   {
      if (strstr(pOwnerX, " ETUX ") || strstr(pOwnerX, " JR ") || 
         strstr(pOwnerX, " III ") || strchr(pOwnerX, '/') )
      {
         vmemcpy(pOutbuf+OFF_NAME_SWAP, pOwner, SIZ_NAME_SWAP);
         vmemcpy(pOutbuf+OFF_NAME1, pOwner, SIZ_NAME1);
         return;
      } else
         strcpy(acName1, pOwnerX);
   } else
      strcpy(acName1, pOwner);
   
   // Update Owner1
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1);

   // Cleanup Name1 
   iTmp = Iny_CleanName(acName1, acTmp, acVesting);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acName1, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // Now parse owners
   splitOwner(acName1, &myOwners, 3);
   if (acVesting[0] > ' ' && isVestChk(acVesting))
   {
      memcpy(myOwners.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwners.acSwapName[SIZ_NAME1] = 0;
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwners.acSwapName, SIZ_NAME_SWAP);
}

/********************************* Iny_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Iny_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     *pTmp, *pAddr1, acTmp[256], acStrNo[8], acSub[8], acUnit[8], 
            acCode[16], acAddr1[64], acAddr2[64], acChar;
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pRec->M_Addr1, "     ", 5))
      return;

   memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
   blankRem(acAddr1, CRESIZ_ADDR_1);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ')
            pAddr1++;
         
         updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
      }
   }

   // Start processing
   memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
   iTmp = atoin(pRec->M_Zip, 5);
   if (iTmp > 400)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   } else
   {
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
   }

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sMailAdr, pAddr1);

   memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
   acAddr2[CRESIZ_ADDR_2] = 0;
   parseAdr2(&sMailAdr, myTrim(acAddr2));

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      } else
      {
         acChar = isChar(sMailAdr.strNum, SIZ_M_STRNUM);
         if (acChar > ' ')
         {
            if (pTmp = strchr(sMailAdr.strNum, acChar))
               memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, strlen(pTmp));
         }
      }

      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));

   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   // Mail City & State
   vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   if (sMailAdr.State[0] >= 'A' && sMailAdr.State[1] >= 'A')
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

   // Situs
   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      ADR_REC sSitusAdr;
      int   iIdx=0;

      removeSitus(pOutbuf);

      iTmp = sprintf(acTmp, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);

      // Find strsub
      memcpy(acStrNo, pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
      myTrim(acStrNo, CRESIZ_SITUS_STREETNO);
      memcpy(pOutbuf+OFF_S_HSENO, acStrNo, strlen(acStrNo));

      pTmp = strchr(acStrNo, ' ');
      acSub[0] = 0;
      if (pTmp)
      {
         pTmp++;
         vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp, SIZ_S_STR_SUB);
         strcpy(acSub, pTmp);
      } else
      {
         pTmp = acStrNo;
         iTmp = 0;
         while (*pTmp)
         {
            if (isalpha(*pTmp))
               acSub[iTmp++] = *pTmp;
            pTmp++;
         }
         if (iTmp > 0)
         {
            acSub[iTmp] = 0;
            vmemcpy(pOutbuf+OFF_S_STR_SUB, acSub, SIZ_S_STR_SUB, iTmp);
         }
      }

      memcpy(acTmp, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
      blankRem(acTmp, CRESIZ_SITUS_STREETNAME);
      if (pTmp = strchr(acTmp, '*'))
         *pTmp = ' ';
      if (pTmp = strchr(acTmp, '('))
      {
         *pTmp++ = 0;
         if (!memcmp(pTmp, "REAR", 4))
            strcpy(acUnit, "REAR");
      }

      // Prepare display field
      iTmp = sprintf(acAddr1, "%s %s", acStrNo, acTmp);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // Parse street name
      parseAdr1S(&sSitusAdr, acTmp);

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ' && isDir(sSitusAdr.strDir))
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
      
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "048523120", 9))
      //   iTmp = 0;
#endif

      // Situs city
      acCode[0] = acAddr2[0] = 0;
      if (pRec->S_City[0] >= 'A')
      {
         memcpy(acAddr2, pRec->S_City, CRESIZ_SITUS_CITY);
         acAddr2[CRESIZ_SITUS_CITY] = 0;
         if (pTmp = strstr(acAddr2, " CA "))
            *pTmp = 0;
         blankRem(acAddr2, CRESIZ_SITUS_CITY);
         City2CodeEx(acAddr2, acCode, sSitusAdr.City, pOutbuf);
         if (acCode[0] > ' ')
         {
            strcpy(acAddr2, sSitusAdr.City);
            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
            // Turn ON this 2 lines when zipcode is in Iny_City.N2CX
            //if (City2Zip(acAddr2, acTmp))
            //   memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
         }
      }
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
      
      if (acCode[0] < '1')
      {
         if (!strcmp(myTrim(sMailAdr.strName), sSitusAdr.strName) && sMailAdr.City[0] > ' ')
         {
            // Assuming that no situs city and we have to use mail city
            City2Code(sMailAdr.City, acTmp, pOutbuf);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ST, "CA", 2);
               strcpy(acAddr2, sMailAdr.City);
            }
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
         } else
         {
            // Condition: Valid city name, Ex_Val is 7000,
            // Use_Code is N?,B1,A1,A2,C1,T1,R1,S1
            long lVal;
            lVal = atoin(pRec->ExVal, CRESIZ_EX_VAL);

            if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && (sMailAdr.lStrNum > 0) &&
                (iStrNo == sMailAdr.lStrNum) && (*(pOutbuf+OFF_HO_FL) == '1') )
            {
               // Get city code
               City2Code(sMailAdr.City, acTmp, pOutbuf);
               if (acTmp[0] > ' ')
               {
                  memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
                  memcpy(pOutbuf+OFF_S_ST, "CA", 2);
                  memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
                  strcpy(acAddr2, sMailAdr.City);
               }
            }

         }
      }

      if (acAddr2[0])
      {
         strcat(myTrim(acAddr2), " CA");
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      }
   }
}

void Iny_MergeMAdr(char *pOutbuf, char *pMAddr1, char *pMAddr2)
{
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pMAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pMAddr1);
   strcpy(acAddr2, pMAddr2);
   blankRem(acAddr1);
   blankRem(acAddr2);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ')
            pAddr1++;
         
         updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0263400200", 10))
   //   iTmp = 0;
#endif
   // Start processing
   vmemcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, SIZ_M_ADDR_D);

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sMailAdr, pAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }

      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D);
   parseAdr2(&sMailAdr, acAddr2);
   if (sMailAdr.City[0] >= 'A')
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   if (sMailAdr.State[0] >= 'A' && sMailAdr.State[1] >= 'A')
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   if (sMailAdr.Zip[0] >= '0')
      vmemcpy(pOutbuf+OFF_M_ZIP, sMailAdr.Zip, SIZ_M_ZIP);
}

void Iny_MergeMAdr(char *pOutbuf, char *pMAddr1, char *pCity, char *pState, char *pZip)
{
   char     *pAddr1, acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pMAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pMAddr1);
   blankRem(acAddr1);
   pAddr1 = acAddr1;

   iTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0111101400", 10))
   //   iTmp = 0;
#endif
   // Start processing
   vmemcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, SIZ_M_ADDR_D);

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sMailAdr, pAddr1);

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }

      if (sMailAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, strlen(sMailAdr.Unit));
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));

   if (*pCity >= 'A')
   {
      vmemcpy(pOutbuf+OFF_M_CITY, pCity, SIZ_M_CITY);
      if (*pState > ' ')
      {
         vmemcpy(pOutbuf+OFF_M_ST, pState, SIZ_M_ST);
         sprintf(acAddr2, "%s %s %s", pCity, pState, pZip);
      } else
         sprintf(acAddr2, "%s ", pCity);
      iTmp = blankRem(acAddr2);
      vmemcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, SIZ_M_CTY_ST_D, iTmp);
   }

   if (*pZip >= '0')
      vmemcpy(pOutbuf+OFF_M_ZIP, pZip, SIZ_M_ZIP);
}

void Iny_MergeSAdr(char *pOutbuf, char *pSAddr1, char *pCity, char *pZip)
{
   char     *pTmp, *pAddr1, acTmp[256], acUnit[8], 
            acCode[16], acAddr1[64], acAddr2[64];
   int      iTmp;
   ADR_REC  sSitusAdr;

   // Clear old Mailing
   removeSitus(pOutbuf);

   // Check for blank address
   if (!memcmp(pSAddr1, "     ", 5))
      return;

   strcpy(acAddr1, pSAddr1);
   blankRem(acAddr1);
   pAddr1 = acAddr1;

   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sSitusAdr, acAddr1);

   // Situs
   if (sSitusAdr.lStrNum > 0 && sSitusAdr.strName[0] >= ' ')
   {
      int   iIdx=0;

      removeSitus(pOutbuf);

      vmemcpy(pOutbuf+OFF_S_STRNUM, sSitusAdr.strNum, SIZ_S_STRNUM);
      vmemcpy(pOutbuf+OFF_S_HSENO, sSitusAdr.HseNo, SIZ_S_HSENO);
      vmemcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, SIZ_S_STR_SUB);
      strcpy(acTmp, sSitusAdr.strName);
      if (pTmp = strchr(acTmp, '*'))
         *pTmp = ' ';
      if (pTmp = strchr(acTmp, '('))
      {
         *pTmp++ = 0;
         if (!memcmp(pTmp, "REAR", 4))
            strcpy(acUnit, "REAR");
      }

      // Prepare display field
      vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D);

      if (sSitusAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
      if (sSitusAdr.strDir[0] > ' ' && isDir(sSitusAdr.strDir))
         vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
      if (sSitusAdr.SfxCode[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.SfxCode, SIZ_S_SUFF);
      if (sSitusAdr.Unit[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "048523120", 9))
      //   iTmp = 0;
#endif

      // Situs city
      acCode[0] = acAddr2[0] = 0;
      if (*pCity >= 'A')
      {
         strcpy(acAddr2, pCity);
         City2CodeEx(acAddr2, acCode, sSitusAdr.City, pOutbuf);
         if (acCode[0] > ' ')
         {
            strcpy(acAddr2, sSitusAdr.City);
            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
            // Turn ON this 2 lines when zipcode is in Iny_City.N2CX
            //if (City2Zip(acAddr2, acTmp))
            //   memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
         }
      }

      if (*pZip > ' ')
         vmemcpy(pOutbuf+OFF_S_ZIP, pZip, SIZ_S_ZIP);

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);     
      if (sSitusAdr.City[0] > ' ')
      {
         sprintf(acAddr2, "%s CA %s", sSitusAdr.City, pZip);
         iTmp = blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

void Iny_MergeSAdr(char *pOutbuf, char *pStrNum, char *pStrSub, char *pStrName, char *pCity)
{
   char     *pTmp, acTmp[256], acCode[16], acAddr1[64], acAddr2[64];
   int      iTmp;
   ADR_REC  sSitusAdr;

   // Check for blank address
   if (!memcmp(pStrName, "     ", 5))
      return;

   // Clear old Mailing
   removeSitus(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0111101800", 9))
   //   iTmp = 0;
#endif

   // Remove '*' in strName
   if (pTmp = strchr(pStrName, '*'))
      *pTmp = ' ';
   iTmp = sprintf(acAddr1, "%s%s %s", pStrNum, pStrSub, pStrName);
   vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);
   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   Iny_ParseSitus1(&sSitusAdr, pStrName, pOutbuf);
   sSitusAdr.lStrNum = atol(pStrNum);

   // Situs
   if (sSitusAdr.lStrNum > 0 && sSitusAdr.strName[0] >= ' ')
   {
      vmemcpy(pOutbuf+OFF_S_STRNUM, pStrNum, SIZ_S_STRNUM);
      vmemcpy(pOutbuf+OFF_S_HSENO, pStrNum, SIZ_S_HSENO);
      vmemcpy(pOutbuf+OFF_S_STR_SUB, pStrSub, SIZ_S_STR_SUB);
      strcpy(acTmp, sSitusAdr.strName);
      if (pTmp = strchr(acTmp, '('))
      {
         *pTmp++ = 0;
         if (!memcmp(pTmp, "REAR", 4))
            strcpy(sSitusAdr.Unit, "REAR");
      }

      if (sSitusAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, SIZ_S_STREET);
      if (sSitusAdr.strDir[0] > ' ' && isDir(sSitusAdr.strDir))
         vmemcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, SIZ_S_DIR);
      if (sSitusAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, SIZ_S_SUFF);
      if (sSitusAdr.Unit[0] > ' ')
         vmemcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, SIZ_S_UNITNO);
      
      // Situs city
      acCode[0] = acAddr2[0] = 0;
      if (*pCity >= 'A')
      {
         strcpy(acAddr2, pCity);
         City2CodeEx(acAddr2, acCode, sSitusAdr.City, pOutbuf);
         if (acCode[0] > ' ')
         {
            strcpy(acAddr2, sSitusAdr.City);
            memcpy(pOutbuf+OFF_S_CITY, acCode, 3);
            // Turn ON this 2 lines when zipcode is in Iny_City.N2CX
            //if (City2Zip(acAddr2, acTmp))
            //   memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
         }
      }

      memcpy(pOutbuf+OFF_S_ST, "CA", 2);     
      if (sSitusAdr.City[0] > ' ')
      {
         sprintf(acAddr2, "%s CA ", sSitusAdr.City);
         iTmp = blankRem(acAddr2);
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D, iTmp);
      }
   }
}

/********************************** Iny_MergeSale *****************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * Return value > 0 if sale rec is older than current
 *
 ******************************************************************************/

int Iny_MergeSale(char *pOutbuf, char *pRec)
{
   long     lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   double   dTax;
   char     acTmp[32];

   INY_SALE *pSaleRec = (INY_SALE *)pRec;

   lCurSaleDt = atoin(pSaleRec->DocDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   if (lCurSaleDt < lLstSaleDt)
      return -1;

   dTax = atofn(pSaleRec->DocTax, SSIZ_DOCTAX);
   lPrice = (long)(dTax * SALE_FACTOR); 
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

   //  Format DocNum - 2000/0960, 199/723, OBIT, OBIT/AFF, OBIT/HLTH, 


   // Update current sale
   memcpy(pOutbuf+OFF_SALE1_DOC, pSaleRec->DocNum, SSIZ_DOCNUM);
   memcpy(pOutbuf+OFF_SALE1_DT, pSaleRec->DocDate, SSIZ_DOCDATE);

   // Seller
   if (pSaleRec->Seller[0] > ' ')
      memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller, SIZ_SELLER);

   // Check for questionable sale amt
   if (lPrice > 1000)
   {
      sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
   } else
      memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, pSaleRec->DocNum, SSIZ_DOCNUM);
      memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleRec->DocDate, SSIZ_DOCDATE);
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}

/******************************** Iny_UpdateSale ******************************
 *
 *
 ******************************************************************************/

int Iny_UpdateSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop, iSaleCnt=0;
   char acTmp[64];

   INY_SALE *pSale;

   // Get first Sale rec for first call
   if (!pRec && !lSaleMatch)
   {
      //iRet = fread(acRec, 1, iSaleLen, fdSale);
      pRec = fgets(acRec, 1024, fdSale);
   }

   pSale = (INY_SALE *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      formatIny(pSale->Apn, acTmp);
      // Compare Apn
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);
         //iRet = fread(acRec, 1, iSaleLen, fdSale);
         pRec = fgets(acRec, 1024, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
      iRet = Iny_MergeSale(pOutbuf, pRec);

      // Get next sale record
      //iRet = fread(acRec, 1, iSaleLen, fdSale);
      pRec = fgets(acRec, 1024, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }
      formatIny(pSale->Apn, acTmp);

   } while (!memcmp(pOutbuf, acTmp, iApnLen));

   lSaleMatch++;

   return 0;
}

/********************************** CreateR01Rec *****************************
 *
 * Secured Roll Types
 * Type	Description
 * 10	   typical assessment
 * 11	   Section 11 assessments (City of Los Angeles land)
 * 20	   utility roll assessments (assessed by the state)
 * 30	   Possessory interest (interests in non-taxable land & imps)
 * 35	   Tenant Improvements
 * 40	   mines
 * 99	   assessments to be deleted
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Iny_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acTmp1[32];
   long     lTmp;
   int      iTmp;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Ignore retired parcels
   if (!memcmp(pRec->ParcType, "99", 2))
      return 1;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      strncpy(acTmp, pRec->APN, CRESIZ_APN);
      iTmp = replChar(acTmp, ' ', '0', CRESIZ_APN);

      memcpy(pOutbuf+OFF_APN_S, acTmp, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "14INY", 5);

      // Create APN_D
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value - 07/17/2012
      long lFixtVal = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers = atoin(pRec->PPVal, CRESIZ_PP_VAL);
      lTmp = lPers + lFixtVal;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lFixtVal > 0)
         {
            sprintf(acTmp, "%d         ", lFixtVal);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
      }

      /* 07/17/2012
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixtVal = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }
      */
      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"INY");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, 3);   // CRESIZ_USE_CODE);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, 3, pOutbuf);

   // Base year
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // HO Exempt
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (!memcmp(pRec->ExeCode1, "10", 2) || lTmp == 7000)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // LotSize
   long lLotSqft = atoin(pRec->LotSqft, CRESIZ_LOT_SQFT);
   dTmp = atofn(pRec->Acres, CRESIZ_ACRES);
   if (dTmp > 0.0)
   {
      lLotSqft = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
   } else if (lLotSqft > 0)
      dTmp = (double)lLotSqft/SQFT_FACTOR_1000;

   if (lLotSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0 && isdigit(pRec->Beds[0]))
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0' && isdigit(pRec->Baths[0]))
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5')
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // #Units
   iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // BldgCls
   if (pRec->BldgCls[0] >= 'A')
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];

   // Legal Desc
   //memset(pOutbuf+OFF_LEGAL2, ' ', SIZ_LEGAL2);    // One time only - remove this line before LDR
   if (pRec->LglDesc1[0] > ' ')
   {
      memcpy(acTmp, pRec->LglDesc1, CRESIZ_LGL_DESC*2);
      acTmp[CRESIZ_LGL_DESC*2] = 0;
      updateLegal(pOutbuf, acTmp);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0041300100", 10))
   //   iTmp = 0;
#endif   
   
   // Update Owners and CareOf
   Iny_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address, CareOf if not appears in Name2
   Iny_MergeAdr(pOutbuf, pRollRec);

   return 0;
}

/********************************* Iny_MergeChar ****************************
 *
 * Need to get LotSqft, Usecode from Redifile
 *
 ****************************************************************************/

int Iny_MergeChar(char *pOutbuf)
{
   static   char  acRec[2048], *pRec=NULL;
   int      iLoop, iTmp, iBeds, iFBaths, iHBaths, iQBaths, iRooms, iAttGar, iDetGar;
   long     lBldgSqft, lGarSqft, lYrBlt;
   char     acTmp[64], acCode[32];

   INY_CHAR  *pChar;

   // Get first Char rec for first call
   if (!pRec && !lCharMatch)
      pRec = fgets(acRec, 2048, fdChar);

   pChar = (INY_CHAR *)&acRec[0];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00106311", 8))
   //   iTmp = 0;
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdChar);
         fdChar = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      formatIny(pChar->Apn, acTmp);
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Char rec  %.*s", CSIZ_APN, pChar->Apn);
         pRec = fgets(acRec, 2048, fdChar);
         lCharSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   acTmp[0] = 0;
   iAttGar=iDetGar=iRooms=iBeds=iFBaths=iHBaths=iQBaths=iTmp = 0;
   lYrBlt=lBldgSqft=lGarSqft = 0;
   while (!iLoop)
   {
      // BldgQual
      if (*(pOutbuf+OFF_BLDG_QUAL) == ' ' && pChar->Bldg_Qual[0] > '0')
      {
         memcpy(acTmp, pChar->Bldg_Qual, 3);
         acTmp[3] = 0;

         iTmp = Value2Code(acTmp, acCode, NULL);
         if (iTmp >= 0)
            *(pOutbuf+OFF_BLDG_QUAL) = acCode[0];
      }

      // Impr Condition
      if (pChar->Bldg_Cond_Class[0] > ' ')
      {
         switch (pChar->Bldg_Cond_Class[0])
         {
            case '1':
               acCode[0] = 'E';
               break;
            case '2':
               acCode[0] = 'G';
               break;
            case '3':
               acCode[0] = 'A';
               break;
            case '4':
               acCode[0] = 'F';
               break;
            case '5':
               acCode[0] = 'P';
               break;
            default:
               acCode[0] = pChar->Bldg_Cond_Class[0];
               break;
         }
         *(pOutbuf+OFF_IMPR_COND) = acCode[0];
      }

      // Bldg Sqft
      lBldgSqft += atoin(pChar->Usable_Bldg_Sqft, CSIZ_USABLE_BLDG_SQFT);
      if (lBldgSqft > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BLDG_SF, lBldgSqft);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      }

      // Garage
      lGarSqft += atoin(pChar->Detached_Garages_Sqft, CSIZ_DETACHED_GARAGES_SQFT);
      lGarSqft += atoin(pChar->Garage_Sqft, CSIZ_GARAGE_SQFT);
      if (pChar->Detached_Garage[0] == 'T')
      {
         iDetGar += atoin(pChar->Detached_Garages_Nbr, CSIZ_DETACHED_GARAGES_NBR);
         *(pOutbuf+OFF_PARK_TYPE) = 'L';
      }
      
      if (pChar->Dif_Detached_Garage[0] == 'T')
      {
         iDetGar += atoin(pChar->Cars_Detached_Garages, CSIZ_CARS_DETACHED_GARAGES);
         *(pOutbuf+OFF_PARK_TYPE) = 'L';
      } 

      if (pChar->Car_Garage1[0] == 'T')
         iAttGar += 1;
      else if (pChar->Car_Garage2[0] == 'T')
         iAttGar += 2;
      else if (pChar->Car_Garage3[0] == 'T')
         iAttGar += 3;
      else if (pChar->Car_Garage4[0] == 'T')
         iAttGar += 4;
      if (iAttGar > 0)
         *(pOutbuf+OFF_PARK_TYPE) = 'I';
      
      iTmp = iAttGar + iDetGar;
      if (iTmp > 0)
      {
         sprintf(acTmp, "%u      ", iTmp);
         memcpy(pOutbuf+OFF_PARK_SPACE, acTmp, SIZ_PARK_SPACE);
      }

      // Garage Sqft
      if (lGarSqft > 0)
      {
         sprintf(acTmp, "%*u", SIZ_GAR_SQFT, lGarSqft);
         memcpy(pOutbuf+OFF_GAR_SQFT, acTmp, SIZ_GAR_SQFT);
      }

      // Fireplace
      iTmp = atoin(pChar->Fireplace_Nbr, CSIZ_FIREPLACE_NBR);
      iTmp += atoin(pChar->Fireplace2_Nbr, CSIZ_FIREPLACE_NBR);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_FIRE_PL, iTmp);
         memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SIZ_FIRE_PL);
      }

      // Pool
      if (pChar->In_Ground_Pool[0] == 'T' && pChar->Spa[0] == 'T')
         *(pOutbuf+OFF_POOL) = 'C';
      else if (pChar->In_Ground_Pool[0] == 'T')
         *(pOutbuf+OFF_POOL) = 'B';
      else if (pChar->Spa[0] == 'T')
         *(pOutbuf+OFF_POOL) = 'S';

      // Rooms
      iTmp = atoin(pChar->Rooms, CSIZ_ROOMS);
      if (iTmp > 0)
      {
         iRooms += iTmp;
         sprintf(acTmp, "%*u", SIZ_ROOMS, iRooms);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      }

      // Beds
      iTmp = atoin(pChar->Bedrooms, CSIZ_BEDROOMS);
      if (iTmp > 0)
      {
         iBeds += iTmp;
         sprintf(acTmp, "%*u", SIZ_BEDS, iBeds);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      }

      // Full Baths
      if (pChar->Bath1[0] == 'T')
      {
         iFBaths += atoin(pChar->Nbr_Bathr1, CSIZ_NBR_BATHR1);
         sprintf(acTmp, "%*u", SIZ_BATH_F, iFBaths);
         memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
      }

      // Half baths
      if (pChar->Bath2[0] == 'T')
      {
         iHBaths += atoin(pChar->Nbr_Bathr2, CSIZ_NBR_BATHR1);
         sprintf(acTmp, "%*u", SIZ_BATH_H, iHBaths);
         memcpy(pOutbuf+OFF_BATH_H, acTmp, SIZ_BATH_H);
      }

      // Quarter bath
      if (pChar->Bath3[0] == 'T')
      {
         iQBaths += atoin(pChar->Nbr_Bathr3, CSIZ_NBR_BATHR1);
         sprintf(acTmp, "%*u", SIZ_BATH_H, iQBaths);
         memcpy(pOutbuf+OFF_BATH_1Q, acTmp, SIZ_BATH_H);
      }

      // Roof
      /*
      if (pChar->Roof_Type_Canopy[0] == 'T')
         *(pOutbuf+OFF_ROOF_TYPE) = 'I';
      else if (pChar->Roof_Type_Flat[0] == 'T')
         *(pOutbuf+OFF_ROOF_TYPE) = 'F';
      else if (pChar->Roof_Type_Gable[0] == 'T')
         *(pOutbuf+OFF_ROOF_TYPE) = 'G';
      else if (pChar->Roof_Type_Gambrel[0] == 'T')
         *(pOutbuf+OFF_ROOF_TYPE) = 'N';
      else if (pChar->Roof_Type_Hip[0] == 'T')
         *(pOutbuf+OFF_ROOF_TYPE) = 'H';
      else if (pChar->Roof_Type_Mansard[0] == 'T')
         *(pOutbuf+OFF_ROOF_TYPE) = 'M';
      else if (pChar->Roof_Type_Other[0] == 'T')
         *(pOutbuf+OFF_ROOF_TYPE) = 'J';

      if (pChar->Roof_Material_Asphalt[0] == 'T')
         *(pOutbuf+OFF_ROOF_MAT) = 'K';
      else if (pChar->Roof_Material_Metal[0] == 'T')
         *(pOutbuf+OFF_ROOF_MAT) = 'L';
      else if (pChar->Roof_Material_Other[0] == 'T')
         *(pOutbuf+OFF_ROOF_MAT) = 'Z';
      else if (pChar->Roof_Material_Tile[0] == 'T')
         *(pOutbuf+OFF_ROOF_MAT) = 'I';
      else if (pChar->Roof_Material_Wood[0] == 'T')
         *(pOutbuf+OFF_ROOF_MAT) = 'V';
      if (strstr(pChar->Foundation_Type, "CONCRETE"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'C';
      else if (strstr(pChar->Foundation_Type, "CONC SLAB"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'E';
      else if (strstr(pChar->Foundation_Type, "PIERS"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'P';
      else if (strstr(pChar->Foundation_Type, "STEM WALL"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'A';
      else if (strstr(pChar->Foundation_Type, "PILINGS"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'B';
      else if (strstr(pChar->Foundation_Type, "CONCRETE BLOCK"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'D';
      else if (strstr(pChar->Foundation_Type, "FULL"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'F';
      else if (strstr(pChar->Foundation_Type, "BRICK"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'G';
      else if (strstr(pChar->Foundation_Type, "STONE"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'H';
      else if (strstr(pChar->Foundation_Type, "CONT. FOOTING"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'I';
      else if (strstr(pChar->Foundation_Type, "MUD SILL"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'L';
      else if (strstr(pChar->Foundation_Type, "MASONRY"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'M';
      else if (strstr(pChar->Foundation_Type, "METAL"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'N';
      else if (strstr(pChar->Foundation_Type, "REINFORCED CONCRETE"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'Q';
      else if (strstr(pChar->Foundation_Type, "RAISED"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'R';
      else if (strstr(pChar->Foundation_Type, "SLAB"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'S';
      else if (strstr(pChar->Foundation_Type, "CRAWL SPACE"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'W';
      else if (strstr(pChar->Foundation_Type, "WOOD"))
         *(pOutbuf+OFF_FNDN_TYPE) = 'X';
      */
  
      // Heating
      if (pChar->Stove_Heating[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'S';
      else if (pChar->Gas_Heating[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'N';
      else if (pChar->Electric_Heating[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'F';
      else if (pChar->Steam_Heating[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'H';
      else if (pChar->Hot_Water_Heating[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'E';
      else if (pChar->Heat_Pumpthere_Heating[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'G';
      else if (pChar->Space_Heater_Heating[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'J';
      else if (pChar->Central_Air[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'Z';
      else if (pChar->Other_Heating_Or_AC[0] == 'T')
         *(pOutbuf+OFF_HEAT) = 'X';

      // A/C
      if (pChar->Central_Air[0] == 'T')
         *(pOutbuf+OFF_AIR_COND) = 'C';
      else if (pChar->Other_Heating_Or_AC[0] == 'T')
         *(pOutbuf+OFF_AIR_COND) = 'X';

      // Year built
      iTmp = atoin(pChar->Constr_Year, 4);
      if (!lYrBlt && iTmp > 1700)
      {
         lYrBlt = iTmp;
         memcpy(pOutbuf+OFF_YR_BLT, pChar->Constr_Year, SIZ_YR_BLT);
         iTmp = atoin(pChar->Eff_Age, CSIZ_EFF_AGE);
         if (iTmp > 0)
         {
            sprintf(acTmp, "%d", lYrBlt+iTmp);
            memcpy(pOutbuf+OFF_YR_EFF, acTmp, SIZ_YR_EFF);
         }
      }

      // Others ...

      // Get next Char record
      pRec = fgets(acRec, 2048, fdChar);
      if (pRec)
      {
         formatIny(pChar->Apn, acTmp);
         iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      } else
         break;     
   }
   lCharMatch++;

   return 0;
}

/********************************** Iny_LoadUpdt ****************************
 *
 * This function will either merge new data into old record or create new
 * record for new parcel.
 *
 ****************************************************************************

int Iny_LoadUpdt(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     cFileCnt=1;
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];
   char     *pCnty = "TUO";

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lRollCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Use last out file for input if lien file missing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Input file missing: %s", acRawFile);
         return 3;
      }
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }
   
   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acCharFile);
      return 2;
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Read first rec
   iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   lRollCnt++;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }
      iSkip = 0;

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:

      strncpy(acTmp, (char *)&acRollRec[CREOFF_APN], CRESIZ_APN);
      iTmp = replChar(acTmp, ' ', '0', CRESIZ_APN);

      iTmp = memcmp(acBuf, acTmp, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Iny_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         if (fdChar)
            iRet = Iny_MergeChar(acBuf);

         //if (fdSale)
         //   iRet = Iny_UpdateSale(acBuf);

         // Read next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         lRollCnt++;
         bEof = (iRet==iRollLen ? false:true);
      } else
      {
         if (iTmp > 0)       // Roll not match, new roll record
         {
            if (bDebug)
               LogMsg0("New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);

            // Create new R01 record
            iRet = Iny_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
            iNewRec++;

            if (fdChar)
               iRet = Iny_MergeChar(acRec);

            //if (fdSale)
            //   iRet = Iny_UpdateSale(acRec);

            // Get last recording date
            iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iTmp >= lToday)
               LogMsg0("* WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acRec);
            else if (lLastRecDate < iTmp)
               lLastRecDate = iTmp;


            // Write to file
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            lCnt++;

            // Read next record
            iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

            if (iRet != iRollLen)
               bEof = true;    // Signal to stop
            else
            {
               lRollCnt++;
               goto NextRollRec;
            }
         } else
         {
            // Record may be retired
            if (bDebug)
               LogMsg0("Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
            iRetiredRec++;
            iSkip = 1;
         }
      }

      // Only output active parcel, not retired ones
      if (iSkip==0)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg0("* WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         // Scan record for bad characters

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      if (!(++lCnt % 1000))
      {
         printf("\r%u", lCnt);
         //LogMsg("At %d: %.10s", lCnt, acBuf);
      }

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // reset buffer
   memset(acBuf, ' ', MAX_RECSIZE);

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);

      // Create new R01 record
      iRet = Iny_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      if (fdChar)
         iRet = Iny_MergeChar(acBuf);

      if (fdSale)
         iRet = Iny_UpdateSale(acBuf);

      // Get last recording date
      iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (iTmp >= lToday)
         LogMsg0("* WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acRec);
      else if (lLastRecDate < iTmp)
         lLastRecDate = iTmp;

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

      if (iRet != iRollLen)
         bEof = true;    // Signal to stop
      else
         lRollCnt++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   // Copy file for assessor
   GetPrivateProfileString(myCounty.acCntyCode, "AsrFile", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
   {
      LogMsg("Copying file %s ==> %s", acOutFile, acTmp);
      iTmp = CopyFile(acOutFile, acTmp, false);
      if (!iTmp)
         LogMsg("***** Fail copying file %s ==> %s", acOutFile, acTmp);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u\n", iRetiredRec);
   LogMsg("Total sale skipped:         %u", lSaleSkip);
   LogMsg("Total Tax skipped:          %u\n", lTaxSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Total roll records input:   %u", lRollCnt);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Iny_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 ****************************************************************************/

int Iny_LoadRoll(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acCharFile);
      return -2;
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Iny_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdLienExt)
            iRet = PQ_MergeLien(acBuf, fdLienExt);

         if (fdChar)
            iRet = Iny_MergeChar(acBuf);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      } else
         LogMsg("--- Drop record %d: %.40s", lCnt, acRollRec);

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("        CHAR matched:       %u", lCharMatch);
   LogMsg("        CHAR skiped:        %u", lCharSkip);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Iny_LoadLien ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************

int Iny_LoadLien(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Sales file
   fdSale = NULL;

   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acCharFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      iRet = Iny_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);

      if (fdChar)
         iRet = Iny_MergeChar(acBuf);

      //if (fdSale)
      //   iRet = Iny_UpdateSale(acBuf);

      // Get last recording date
      iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
      if (iTmp >= lToday)
         LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
      else if (lLastRecDate < iTmp)
         lLastRecDate = iTmp;

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      // Read next roll record
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Iny_Load_LDR ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Iny_Load_LDR(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Sales file
   /*
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }
   */
   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acCharFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "001021004", 9))
      //   iRet=0;
#endif
      iRet = Iny_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdChar)
            iRet = Iny_MergeChar(acBuf);

         //if (fdSale)
         //   iRet = Iny_UpdateSale(acBuf);

         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            lRet = WRITE_ERR;
            break;
         }
      } else
         LogMsg("--- Drop record %d: %.40s", lLDRRecCount, acRollRec);

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   //if (fdSale)
   //   fclose(fdSale);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lLDRRecCount;
   return 0;
}

//int Iny_Load_LDR1(char *pCnty)
//{
//   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
//   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];
//
//   HANDLE   fhOut;
//
//   int      iRet, iOutRecs=0;
//   DWORD    nBytesWritten;
//   BOOL     bRet, bEof;
//   long     lRet=0, lCnt=0;
//
//   // Open Char file
//   LogMsg("Open Char file %s", acCharFile);
//   fdChar = fopen(acCharFile, "r");
//   if (fdChar == NULL)
//   {
//      LogMsg("***** Error opening char file: %s\n", acCharFile);
//      return -2;
//   }
//
//   // Open Roll file
//   LogMsg("Open Roll file %s", acRollFile);
//   fdRoll = fopen(acRollFile, "rb");
//   if (fdRoll == NULL)
//   {
//      LogMsg("***** Error opening roll file: %s\n", acRollFile);
//      return -2;
//   }
//
//   // Open lien extract file
//   sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
//   LogMsg("Open lien extract file %s", acLienExt);
//   fdLienExt = fopen(acLienExt, "r");
//   if (fdLienExt == NULL)
//   {
//      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
//      return -2;
//   }
//
//   // Open Output file
//   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
//   LogMsg("Open output file %s", acOutFile);
//   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
//          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
//
//   if (fhOut == INVALID_HANDLE_VALUE)
//   {
//      LogMsg("***** Error opening output file: %s\n", acOutFile);
//      return -4;
//   }
//
//   // Write first record
//   memset(acBuf, '9', iRecLen);
//   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//
//   // Get first RollRec
//   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
//   bEof = (iRet==iRollLen ? false:true);
//
//   // Init buffer
//   memset(acBuf, ' ', iRecLen);
//   iNoMatch=iBadCity=iBadSuffix=0;
//
//   // Merge loop
//   while (!bEof)
//   {
//      // Replace all TAB and NULL character in input record
//      replCharEx(acRollRec, 31, 32, iRollLen);
//
//      iRet = Iny_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
//      if (!iRet)
//      {
//         if (fdLienExt)
//            iRet = PQ_MergeLien(acBuf, fdLienExt, GRP_MB);
//
//         if (fdChar)
//            iRet = Iny_MergeChar(acBuf);
//
//         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
//         iOutRecs++;
//      } else
//         LogMsg("--- Drop record %d: %.40s", lCnt, acRollRec);
//
//      // Read next roll record
//      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
//      bEof = (iRet==iRollLen ? false:true);
//
//      if (!(++lCnt % 1000))
//         printf("\r%u", lCnt);
//
//      if (!bRet)
//      {
//         LogMsg("Error occurs: %d\n", GetLastError());
//         lRet = WRITE_ERR;
//         break;
//      }
//   }
//
//   // Close files
//   if (fdRoll)
//      fclose(fdRoll);
//   if (fdChar)
//      fclose(fdChar);
//   if (fdLienExt)
//      fclose(fdLienExt);
//   if (fhOut)
//      CloseHandle(fhOut);
//
//   LogMsg("Total records processed:  %u", lCnt);
//   LogMsg("         records output:  %u", iOutRecs);
//   LogMsg("           CHAR matched:  %u", lCharMatch);
//   LogMsg("            CHAR skiped:  %u", lCharSkip);
//   LogMsg("       bad-city records:  %u", iBadCity);
//   LogMsg("     bad-suffix records:  %u", iBadSuffix);
//
//   lRecCnt = iOutRecs;
//   printf("\nTotal output records: %u", iOutRecs);
//   return 0;
//}

/******************************* Iny_ExtrSale *******************************
 *
 * Extract sale data from Pamsale.txt and output to Iny_Sale.sls
 * - Only take doc with docnum & docdate
 *
 ****************************************************************************/

int Iny_ExtrSale(char *pInFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acSaleRec[1024], *pTmp;
   char     acOutFile[_MAX_PATH], acTmp[256], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice, lDate;
   double   dTmp;

   INY_SALE  *pRec;
   SCSAL_REC *pSale;

   LogMsg0("Extract sale data from county sale file %s", pInFile);

   // Open Sale file
   LogMsg("Open Sale file %s", pInFile);
   fdSale = fopen(pInFile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pInFile);
      return -2;
   }

   // Open Output file - Iny_Sale.dat
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");
   LogMsg("Open output file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating sale extract file: %s\n", acOutFile);
      return -2;
   }

   // Get first sale rec
   pTmp = fgets((char *)&acSaleRec[0], 1024, fdSale);
   pRec= (INY_SALE *)acSaleRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (pTmp)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
      //if (!memcmp(pRec->Apn, "001-011-10", 10))
      //   iRet = 0;
#endif

      lDate = atoin(pRec->DocDate, 8);
      Iny_FmtPamDoc(acDoc, pRec->DocNum, pRec->DocDate);
      if (acDoc[0] > ' ' && lDate > 19000101 && lDate < lToday)
      {
         // APN
         memcpy(acTmp, pRec->Apn, SSIZ_APN);
         acTmp[SSIZ_APN] = 0;
         remChar(acTmp, '-');                            // Remove hyphen
         memcpy(pSale->Apn, acTmp, strlen(acTmp));
         replChar(pSale->Apn, ' ', '0', CRESIZ_APN);   // Padded with 0

         // Doc info
         memcpy(pSale->DocDate, pRec->DocDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, strlen(acDoc));

         // Doc tax
         memcpy(acTmp, pRec->DocTax, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            memcpy(pSale->StampAmt, acTmp, strlen(acTmp));
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice > 0)
            {
               if (dTmp > 1999.0 && !((long)dTmp % 100) && (lPrice % 100))
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               else if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);

               if (dTmp > 9999.00)
               {
                  if (((long)dTmp % 100) == 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
                     memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
                  } else
                  {
                     if ((lPrice % 100) == 0)
                        memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
                     else
                        LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %10d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRec->DocTax);
                  }
               } else
                  memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
            }
         }

         // DocType
         memcpy(pSale->DocCode, pRec->DocType, SSIZ_DOCTYPE);
         iRet = atoin(pRec->DocType, SSIZ_DOCTYPE);
         if (iRet > 0)
         {
            if (iRet == 3 || iRet == 70)
               pSale->SaleCode[0] = 'P';
            else if (iRet == 1)
               pSale->SaleCode[0] = 'F';

            iRet = findDocType(pRec->DocType, (IDX_TBL5 *)&INY_DocCode[0]);
            if (iRet >= 0)
            {
               memcpy(pSale->DocType, INY_DocCode[iRet].pCode, INY_DocCode[iRet].iCodeLen);
               if (lPrice < 100)
                  pSale->NoneSale_Flg = INY_DocCode[iRet].flag;
            } else
               LogMsg("*** Unknown DocType: %.2s", pRec->DocType); 
         } 

         // Seller/buyer
         memcpy(pSale->Seller1, pRec->Seller, SALE_SIZ_SELLER);

         // Multisale
         if (pRec->MultiSale[0] == 'T')
            pSale->MultiSale_Flg = 'Y';

         // Save last recording date
         if (lLastRecDate < lDate)
            lLastRecDate = lDate;

         // Write output
         fputs(acBuf, fdCSale);
         lNewRec++;
      }

      // Read next roll record
      pTmp = fgets((char *)&acSaleRec[0], 1024, fdSale);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   // Sort on APN, DocDate, DocNum, SalePrice, MultiSaleFlg
   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A,440,1,C,D) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lNewRec);
   LogMsg("Last sale date from %s: %u", pInFile, lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/******************************** Iny_UpdSale1 ******************************
 *
 * Copy from Cres_UpdSale1() and correct sale price for INY. Only take sale1 with 
 * sale tax amt.
 * Return 0 if successful.
 *
 ****************************************************************************/

int Iny_UpdSale1(char *pOutfile)
{
   char     acBuf[512], acRollRec[1024];
   char     acOutfile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice;
   double   dTmp;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsgD("\nExtract sales from roll file %s to update %s", acRollFile, pOutfile);

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open roll sale file
   LogMsg("Open cum sale file file %s for update", pOutfile);
   fdCSale = fopen(pOutfile, "a+");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", pOutfile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      strncpy(acTmp, pRoll->APN, CRESIZ_APN);
      replChar(acTmp, ' ', '0', CRESIZ_APN);
      memcpy(pSale->Apn, acTmp, CRESIZ_APN);
      Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > '0' && acDoc[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         // Doc tax
         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         dTmp = atof(acTmp);
         if (dTmp > 1.0)
         {
            memcpy(pSale->StampAmt, acTmp, strlen(acTmp));
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice > 0)
            {
               if (dTmp > 1999.0 && !((long)dTmp % 100) && (lPrice % 100))
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               else if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else if (lPrice > 1000000)
               {
                  //pRoll->LglDesc2[0] = 0;
                  //if (pRoll->MH_Lic[0] >= 'A' || strstr(pRoll->LglDesc1, " MH"))
                  if (pRoll->MH_Lic[0] >= 'A')
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
                     dTmp = 0.1;
                  } else if (!(lPrice % 100))
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                  } else
                  {
                     LogMsg0("***1 Questionable sale price on APN=%.10s, SalePrice= %10d, StampAmt=%.*s, RecDate=%.8s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt, acDate);
                     memset(acTmp, ' ', SIZ_SALE1_AMT);
                     dTmp = 0.1;
                  }
               } else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);

               if (dTmp > 9999.00)
               {
                  if (((long)dTmp % 100) == 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
                     memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
                  } else
                     LogMsg0("***2 Questionable sale price on APN=%.10s, SalePrice= %s, StampAmt=%.*s, RecDate=%.8s", acBuf, acTmp, CRESIZ_STMP_AMT, pRoll->StampAmt, acDate);
               } else
                  memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
            }

            // Save last recording date
            iRet = atoin(acDate, 8);
            if (iRet < lToday)
            {
               if (lLastRecDate < iRet)
                  lLastRecDate = iRet;

               // Write output
               fputs(acBuf, fdCSale);
               lNewRec++;
            } else
               LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
         }
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acOutfile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,34,C,A,57,10,C,D,69,2,C,D) F(TXT) DUPO(1,34)");
   iRet = sortFile(pOutfile, acOutfile, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", pOutfile, acOutfile);
      iRet = -1;
   } else
   {
      // Rename files
      sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
      if (!_access(acTmp, 0))
         remove(acTmp);
      iRet = rename(pOutfile, acTmp);
      if (!iRet)
         iRet = rename(acOutfile, pOutfile);
      else
         LogMsg("***** Error renaming %s to %s", pOutfile, acTmp);
   }

   LogMsgD("\nTotal input records:        %u", lCnt);
   LogMsg("Total sale extracted:       %u", lNewRec);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);

   return iRet;
}

/****************************** Iny_FmtChar **********************************
 *
 *
 *****************************************************************************/

int Iny_FmtChar(char *pOutbuf, char *pInbuf)
{
   char     acTmp[_MAX_PATH], acTmp2[_MAX_PATH], acCode[16];
   int      iTmp, lTmp, lTmp2;


   STDCHAR  *pCharRec=(STDCHAR *)pOutbuf;
   INY_CHAR *pRawRec =(INY_CHAR *)pInbuf;
   
   // Init output buffer
   memset(pOutbuf, ' ', sizeof(STDCHAR));

   // APN
   memcpy(acTmp, pRawRec->Apn, CSIZ_APN);
   remChar(acTmp, '-', CSIZ_APN);
   iTmp = blankRem(acTmp, CSIZ_APN);
   strcat(acTmp, "0000");
   acTmp[iApnLen] = 0;
   memcpy(pCharRec->Apn, acTmp, iApnLen);

   iTmp = formatApn(acTmp, acTmp2, &myCounty);
   memcpy(pCharRec->Apn_D, acTmp2, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pCharRec->Apn, "002010037", 9) )
   //   iTmp = 0;
#endif

   // Bldg#
   lTmp = atoin(pRawRec->Bldg_Nbr, CSIZ_BLDG_NBR);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->BldgSeqNo, acTmp, iTmp);
   }

   // BldgClass
   if (pRawRec->Build_Class[0] > ' ')
      pCharRec->BldgClass = pRawRec->Build_Class[0];

   // BldgQual
   if (pRawRec->Bldg_Qual[0] > ' ')
   {
      memcpy(acTmp, pRawRec->Bldg_Qual, 3);
      acTmp[3] = 0;

      iTmp = Value2Code(acTmp, acCode, NULL);
      if (iTmp >= 0)
         pCharRec->BldgQual = acCode[0];
   }

   // Impr Condition
   if (pRawRec->Bldg_Cond_Class[0] > ' ')
   {
      switch (pRawRec->Bldg_Cond_Class[0])
      {
         case '1':
            acCode[0] = 'E';
            break;
         case '2':
            acCode[0] = 'G';
            break;
         case '3':
            acCode[0] = 'A';
            break;
         case '4':
            acCode[0] = 'F';
            break;
         case '5':
            acCode[0] = 'P';
            break;
         default:
            acCode[0] = pRawRec->Bldg_Cond_Class[0];
            break;
      }
     pCharRec->ImprCond[0] = acCode[0];
   }

   // Bldg Sqft
   lTmp = atoin(pRawRec->Usable_Bldg_Sqft, CSIZ_USABLE_BLDG_SQFT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->BldgSqft, acTmp, iTmp);
   }
        
   // BsmtSqft
   lTmp = atoin(pRawRec->Full_Basement_Sqft, CSIZ_FULL_BASEMENT_SQFT);
   lTmp += atoin(pRawRec->Partial_Basement_Sqft, CSIZ_PARTIAL_BASEMENT_SQFT);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->BsmtSqft, acTmp, iTmp);
   }

   // Garage
   int   iDetGar, iAttGar;

   iAttGar=iDetGar=0;
   lTmp = atoin(pRawRec->Detached_Garages_Sqft, CSIZ_DETACHED_GARAGES_SQFT);
   lTmp += atoin(pRawRec->Garage_Sqft, CSIZ_GARAGE_SQFT);

   // Garage Sqft
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->GarSqft, acTmp, iTmp);
   }

   // ParkType
   if (pRawRec->Detached_Garage[0] == 'T')
   {
      iDetGar = atoin(pRawRec->Detached_Garages_Nbr, CSIZ_DETACHED_GARAGES_NBR);
      pCharRec->ParkType[0] = 'L';
   }
   if (pRawRec->Dif_Detached_Garage[0] == 'T')
   {
      iDetGar += atoin(pRawRec->Cars_Detached_Garages, CSIZ_CARS_DETACHED_GARAGES);
      pCharRec->ParkType[0] = 'L';
   } 

   if (pRawRec->Car_Garage1[0] == 'T')
      iAttGar += 1;
   else if (pRawRec->Car_Garage2[0] == 'T')
      iAttGar += 2;
   else if (pRawRec->Car_Garage3[0] == 'T')
      iAttGar += 3;
   else if (pRawRec->Car_Garage4[0] == 'T')
      iAttGar += 4;
   if (iAttGar > 0)
      pCharRec->ParkType[0] = 'I';
   
   // ParkSpace
   lTmp = iAttGar + iDetGar;
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->ParkSpace, acTmp, iTmp);
   }

   // Fireplace
   lTmp = atoin(pRawRec->Fireplace_Nbr, CSIZ_FIREPLACE_NBR);
   lTmp += atoin(pRawRec->Fireplace2_Nbr, CSIZ_FIREPLACE_NBR);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Fireplace, acTmp, iTmp);
   }

   // Pool
   if (pRawRec->In_Ground_Pool[0] == 'T' && pRawRec->Spa[0] == 'T')
      pCharRec->Pool[0] = 'C';
   else if (pRawRec->In_Ground_Pool[0] == 'T')
      pCharRec->Pool[0] = 'B';
   else if (pRawRec->Spa[0] == 'T')
      pCharRec->Pool[0] = 'S';

   // Rooms
   lTmp = atoin(pRawRec->Rooms, CSIZ_ROOMS);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Rooms, acTmp, iTmp);
   }

   // Beds
   lTmp = atoin(pRawRec->Bedrooms, CSIZ_BEDROOMS);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Beds, acTmp, iTmp);
   }

   // Full Baths
   if (pRawRec->Bath1[0] == 'T')
   {
      lTmp = atoin(pRawRec->Nbr_Bathr1, CSIZ_NBR_BATHR1);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->FBaths, acTmp, iTmp);
      memcpy(pCharRec->Bath_4Q, acTmp, iTmp);
   }

   // Half bath
   if (pRawRec->Bath2[0] == 'T')
   {
      lTmp = atoin(pRawRec->Nbr_Bathr2, CSIZ_NBR_BATHR1);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->HBaths, acTmp, iTmp);
      memcpy(pCharRec->Bath_2Q, acTmp, iTmp);
   }

   // Quarter bath
   if (pRawRec->Bath3[0] == 'T')
   {
      lTmp = atoin(pRawRec->Nbr_Bathr3, CSIZ_NBR_BATHR1);
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Bath_1Q, acTmp, iTmp);
   }

   // Heating
   if (pRawRec->Stove_Heating[0] == 'T')
      pCharRec->Heating[0] = 'S';
   else if (pRawRec->Gas_Heating[0] == 'T')
      pCharRec->Heating[0] = 'N';
   else if (pRawRec->Electric_Heating[0] == 'T')
      pCharRec->Heating[0] = 'F';
   else if (pRawRec->Steam_Heating[0] == 'T')
      pCharRec->Heating[0] = 'H';
   else if (pRawRec->Hot_Water_Heating[0] == 'T')
      pCharRec->Heating[0] = 'E';
   else if (pRawRec->Heat_Pumpthere_Heating[0] == 'T')
      pCharRec->Heating[0] = 'G';
   else if (pRawRec->Space_Heater_Heating[0] == 'T')
      pCharRec->Heating[0] = 'J';
   else if (pRawRec->Central_Air[0] == 'T')
      pCharRec->Heating[0] = 'Z';
   else if (pRawRec->Other_Heating_Or_AC[0] == 'T')
      pCharRec->Heating[0] = 'X';

   // A/C
   if (pRawRec->Central_Air[0] == 'T')
      pCharRec->Cooling[0] = 'C';
   else if (pRawRec->Other_Heating_Or_AC[0] == 'T')
      pCharRec->Cooling[0] = 'X';

   // Year built
   lTmp = atoin(pRawRec->Constr_Year, 4);
   if (lTmp > 1700)
   {
      memcpy(pCharRec->YrBlt, pRawRec->Constr_Year, SIZ_YR_BLT);
      lTmp2 = atoin(pRawRec->Eff_Age, CSIZ_EFF_AGE);
      if (lTmp2 > 0)
      {
         sprintf(acTmp, "%d", lTmp+lTmp2);
         memcpy(pCharRec->YrEff, acTmp, SIZ_YR_EFF);
      }
   }

   // Deck
   //lTmp = atoin(pRawRec->Deck_Sqft, CSIZ_DECK_SQFT);
   //if (lTmp > 0)
   //{
   //   iTmp = sprintf(acTmp, "%d", lTmp);
   //   memcpy(pCharRec->DeckSqft, acTmp, iTmp);
   //}
        

   // Porch        

   // Guest house

   // Other Impr1 & Impr2

   // Roof Type

   // Roof Material

   // Elevator
   lTmp = atoin(pRawRec->No_Elevator, CSIZ_NO_ELEVATOR);
   if (lTmp > 0)
      pCharRec->HasElevator = 'Y';

   // Units
   lTmp = atoin(pRawRec->Units, CSIZ_UNITS);
   if (lTmp > 0)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->Units, acTmp, iTmp);
   }


/*

   // Zoning
   if (pRawRec->Zoning[0] > ' ')
      memcpy(pCharRec->Zoning, pRawRec->Zoning, CSIZ_ZONE);

   // UseCode
   if (pRawRec->UseCode[0] > ' ')
      memcpy(pCharRec->LandUse, pRawRec->UseCode, CSIZ_USE_CODE);

  // Acreage
   lTmp = atoin(pRawRec->LotSqft, CSIZ_LOT_SQFT);
   if (lTmp > 100)
   {
      iTmp = sprintf(acTmp, "%d", lTmp);
      memcpy(pCharRec->LotSqft, acTmp, iTmp);

      lTmp2 = ((double)lTmp*ACRES_FACTOR)/SQFT_PER_ACRE;
      iTmp = sprintf(acTmp, "%d", lTmp2);
      memcpy(pCharRec->LotAcre, acTmp, iTmp);
   }

*/
   pCharRec->CRLF[0] = '\n';
   pCharRec->CRLF[1] = '\0';

   return 0;
}

/***************************** Iny_ConvertChar *******************************
 *
 * Convert Building.txt to STDCHAR format.
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Iny_ConvStdChar(char *pInfile, char *pOutfile)
{
   char  *pTmp, acCharRec[1024], acInRec[MAX_RECSIZE], acTmpFile[_MAX_PATH];
   long  lCnt=0, lChars=0, iRet;
   FILE  *fdIn;

   LogMsg0("Extracting Chars from %s", pInfile);

   // Check current sale file
   if (_access(pInfile, 0))
   {
      LogMsg("***** Missing input file %s", pInfile);
      return -1;
   }

   // Open char file
   LogMsg("Open char file to extract CHARS %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", pInfile);
      return -2;
   }

   // Create output file
   sprintf(acTmpFile, "%s\\%s\\%s_Char.Dat", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode); 
   LogMsg("Create output chars file %s", acTmpFile);
   fdChar = fopen(acTmpFile, "w");
   if (fdChar == NULL)
   {
      LogMsg("***** Error creating char file: %s\n", acTmpFile);
      return -3;
   }

   // Merge loop
   while (!feof(fdIn))
   {
      // Get next record
      pTmp = fgets(acInRec, MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      iRet = Iny_FmtChar(acCharRec, acInRec);

      if (!iRet)
      {
         fputs(acCharRec, fdChar);
         lChars++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

   }

   // Close files
   if (fdChar)
      fclose(fdChar);
   if (fdIn)
      fclose(fdIn);

   LogMsg("Total input records processed: %u", lCnt);
   LogMsg("Total char records output:     %u", lChars);

   // Sort output
   sprintf(acInRec, "S(1,%d,C,A) ", iApnLen);
   iRet = sortFile(acTmpFile, pOutfile, acInRec);

   return iRet;
}

/***************************** Iny_ConvStdChar *******************************
 *
 * Convert CharacteristicsData.csv to STD_CHAR format
 *
 *****************************************************************************/

int Iny_ConvStdChar()
{
   char     acInbuf[1024], acOutbuf[2048], *pRec;
   char     acTmpFile[256], acTmp[256];
   double   dTmp;
   int      iRet, iTmp, iBeds, iFBath, iHBath, iBath3Q, iBath4Q, iCnt, lSqft;

   STDCHAR  *pStdChar = (STDCHAR *)acOutbuf;
   FILE     *fdChar, *fdOut;

   LogMsg0("Loading characteristic data");

   LogMsg("Open char file %s\n", acCharFile);
   if (!(fdChar = fopen(acCharFile, "r")))
   {
      LogMsg("***** Error opening file %s.  Please verify file existence", acCharFile);
      return -1;
   }

   sprintf(acTmpFile, "%s\\%s\\%s_char.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      fclose(fdChar);
      LogMsg("***** Error creating output file %s", acTmpFile);
      return -2;
   }

   // Skip header
   pRec = fgets(acInbuf, 1024, fdChar);

   iCnt = 0;
   while (!feof(fdChar))
   {
      pRec = fgets(acInbuf, 1024, fdChar);
      if (!pRec) break;

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "009619939", 9))
      //   iTokens = 0;
#endif

      iTokens = ParseString(pRec, cDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens < C_FLDS)
      {
         if (iTokens > 1)
            LogMsg("*** Bad CHAR record (%d): %s [%d]", iCnt, pRec, iTokens);
         continue;
      } else if (iTokens > C_FLDS)
         LogMsg("*** Please check for possible corruption record: %s", pRec);

      memset(acOutbuf, ' ', sizeof(STDCHAR));

      // Format APN
      iTmp = Iny_FormatApn(apTokens[S_PIN], acTmp);
      if (iTmp == iApnLen)
         memcpy(pStdChar->Apn, acTmp, iApnLen);
      else
      {
         if (bDebug)
            LogMsg("*** Bad APN in CHAR record (%d): %s", iCnt, apTokens[S_PIN]);
         continue;
      }

      iRet = formatApn(pStdChar->Apn, acTmp, &myCounty);
      memcpy(pStdChar->Apn_D, acTmp, iRet);

      // Yrblt
      //lTmp = atol(apTokens[C_EFFYRBLT]);
      //if (lTmp > 1700)
      //   memcpy(pStdChar->YrBlt, apTokens[C_EFFYRBLT], SIZ_YR_BLT);
      //lTmp = atol(apTokens[C_EFFYRBLT]);
      //if (lTmp > 1900)
      //   memcpy(pStdChar->YrEff, apTokens[C_EFFYRBLT], SIZ_YR_BLT);

      // Stories
      iTmp = atol(apTokens[C_STORIES]);
      if (iTmp > 0 && iTmp < 100)
      {
         if (bDebug && iTmp > 9)
            LogMsg("*** Please verify number of stories for %s (%d)", apTokens[C_PIN], iTmp);

         iRet = sprintf(acTmp, "%d.0", iTmp);
         memcpy(pStdChar->Stories, acTmp, iRet);
      }

      // Garage Sqft
      // Property may have more than one garage, but we only have space for one
      long lGarSqft = atol(apTokens[C_GARAGEAREA]) + atol(apTokens[C_GARAGE2AREA]);
      long lCarpSqft= atol(apTokens[C_CARPORTSIZE]) + atol(apTokens[C_CARPORT2SIZE]);

      // Garage type
      if (*apTokens[C_GARAGETYPE] == 'A' || *apTokens[C_GARAGE2TYPE] == 'A')
         pStdChar->ParkType[0] = 'I';              // Attached garage
      else if (*apTokens[C_GARAGETYPE] == 'D')
         pStdChar->ParkType[0] = 'L';              // Detached garage
      else if (lCarpSqft > 100)
         pStdChar->ParkType[0] = 'C';              // Carport

      if (lGarSqft > 100 || lCarpSqft > 100)
      {
         if (lGarSqft >= 100 && lCarpSqft > 100)
         {
            lGarSqft += lCarpSqft;
            pStdChar->ParkType[0] = '2';           // Garage/Carport
         }
         iTmp = sprintf(acTmp, "%d", lGarSqft);
         memcpy(pStdChar->GarSqft, acTmp, iTmp);
      }

      // Central Heating-Cooling
      if (*apTokens[C_HASCENTRALHEATING] == 'Y')
         pStdChar->Heating[0] = 'Z';
      if (*apTokens[C_HASCENTRALCOOLING] == 'Y')
         pStdChar->Cooling[0] = 'C';

      // Beds
      iBeds = atol(apTokens[C_BEDROOMCOUNT]);
      if (iBeds > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBeds);
         memcpy(pStdChar->Beds, acTmp, iTmp);
      }

#ifdef _DEBUG
      //if (!memcmp(pStdChar->Apn, "009600002", 9) )
      //   lTmp = 0;
#endif

      // Bath
      iBath4Q = atol(apTokens[C_BATHSFULL]);
      iHBath  = atol(apTokens[C_BATHSHALF]);
      iBath3Q = atol(apTokens[C_BATHS3_4]);
      iFBath = iBath3Q+iBath4Q;
      if (iBath3Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath3Q);
         memcpy(pStdChar->Bath_3QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_3Q, acTmp, iTmp);
      }

      if (iBath4Q > 0)
      {
         iTmp = sprintf(acTmp, "%d", iBath4Q);
         memcpy(pStdChar->Bath_4QX, acTmp, iTmp);
         if (iTmp < 3)
            memcpy(pStdChar->Bath_4Q, acTmp, iTmp);
      }

      if (iFBath > 0 && iFBath < 100)
      {
         iTmp = sprintf(acTmp, "%d", iFBath);
         memcpy(pStdChar->FBaths, acTmp, iTmp);
      }

      if (iHBath > 0)
      {
         iTmp = sprintf(acTmp, "%d", iHBath);
         memcpy(pStdChar->Bath_2QX, acTmp, iTmp);
         if (iTmp < 3)
         {
            memcpy(pStdChar->HBaths, acTmp, iTmp);
            memcpy(pStdChar->Bath_2Q, acTmp, iTmp);
         }
      }

      // Fireplace
      if (*apTokens[C_HASFIREPLACE] == 'Y')
         pStdChar->Fireplace[0] = 'Y';
      else if (*apTokens[C_HASFIREPLACE] == 'N')
         pStdChar->Fireplace[0] = 'N';

      // Roof
      if (*apTokens[C_ROOFTYPE] >= 'A')
      {
         _strupr(apTokens[C_ROOFTYPE]);
         pRec = findXlatCodeA(apTokens[C_ROOFTYPE], &asRoofType[0]);
         if (pRec)
            pStdChar->RoofMat[0] = *pRec;
      }

      // Pool/Spa
      if (*apTokens[C_HASPOOL] == 'Y')
         pStdChar->Pool[0] = 'P';       // Pool
      else if (*apTokens[C_HASPOOL] == 'N')
         pStdChar->Pool[0] = 'N';

      // BldgSqft
      lSqft = atol(apTokens[C_LIVINGAREA]);
      if (lSqft > 0)
      {
         iTmp = sprintf(acTmp, "%d", lSqft);
         memcpy(pStdChar->BldgSqft, acTmp, iTmp);
      }

      // BldgClass 
      //if (pRec = strchr(apTokens[C_CONSTRUCTIONTYPE], '('))
      //   pStdChar->BldgClass = *(pRec+1);

      // Quality
      if (!_memicmp(apTokens[C_QUALITYCODE], "Rank", 4))
         dTmp = atof(apTokens[C_QUALITYCODE]+5);
      else if (*apTokens[C_QUALITYCODE] > '0' && *apTokens[C_QUALITYCODE] < '9')
         dTmp = atof(apTokens[C_QUALITYCODE]);
      else
         dTmp = 0;
      if (dTmp > 0.1)
      {
         char acCode[16];

         sprintf(acTmp, "%.1f", dTmp);
         iTmp = Value2Code(acTmp, acCode, NULL);
         if (acCode[0] > ' ')
            pStdChar->BldgQual = acCode[0];
      }

      // QualityClass
      //if (pStdChar->BldgClass > ' ' && dTmp > 0.1)
      //{
      //   sprintf(acTmp, "%c%.1f%c", pStdChar->BldgClass, dTmp, *apTokens[C_SHAPECODE]);
      //   vmemcpy(pStdChar->QualityClass, acTmp, SIZ_CHAR_QCLS, strlen(acTmp));
      //}

      // Lot sqft - Lot Acres
      //dTmp = atof(apTokens[C_ACREAGE]);
      //if (dTmp > 0.001)
      //{
      //   if (*apTokens[C_LANDUNITTYPE] == 'A' || *apTokens[C_LANDUNITTYPE] == 'U')
      //   {
      //      iTmp = sprintf(acTmp, "%d", (long)(dTmp*1000));
      //      memcpy(pStdChar->LotAcre, acTmp, iTmp);
      //      lSqft = long(dTmp*SQFT_PER_ACRE);
      //   } else
      //   {
      //      iTmp = sprintf(acTmp, "%d", (long)(dTmp/SQFT_FACTOR_1000));
      //      memcpy(pStdChar->LotAcre, acTmp, iTmp);
      //      lSqft = (long)dTmp;
      //   }

      //   iTmp = sprintf(acTmp, "%d", lSqft);
      //   memcpy(pStdChar->LotSqft, acTmp, iTmp);
      //}

      pStdChar->CRLF[0] = '\n';
      pStdChar->CRLF[1] = '\0';
      fputs(acOutbuf, fdOut);

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }

   if (fdChar) fclose(fdChar);
   if (fdOut)  fclose(fdOut);
   LogMsg("Number of records processed: %d", iCnt);

   // Sort output on ASMT
   if (iCnt > 100)
   {
      // Rename old CHAR file if needed
      if (!_access(acCChrFile, 0))
      {
         strcpy(acInbuf, acCChrFile);
         replStr(acInbuf, ".dat", ".sav");
         if (!_access(acInbuf, 0))
         {
            LogMsg("Delete old %s", acInbuf);
            DeleteFile(acInbuf);
         }
         LogMsg("Rename %s to %s", acCChrFile, acInbuf);
         RenameToExt(acCChrFile, "sav");
      }

      LogMsgD("\nSorting char file %s --> %s", acTmpFile, acCChrFile);
      // Sort on APN, YrBlt to keep the latest record on top
      iRet = sortFile(acTmpFile, acCChrFile, "S(1,14,C,A,53,4,C,D) F(TXT) ");
   } else
   {
      printf("\n");
      iRet = 0;
   }

   return iRet;
}

/******************************* Iny_MergeLien *******************************
 *
 * For LDR "2020 Secured Roll.csv".  
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Iny_MergeLienCsv(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256], acTmp1[64], acApn[32];
   long     lTmp;
   int      iRet;

   // Replace null char with space
   iRet = replNull(pRollRec, ' ', 0);

   // Parse input rec
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < L_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[L_APN], iRet);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // Copy APN
   strcpy(acApn, apTokens[L_APN]);
   iRet = iTrim(acApn);
   memcpy(pOutbuf, acApn, iRet);

   // Format APN
   iRet = formatApn(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

   // Create MapLink and output new record
   iRet = formatMapLink(acApn, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

   // Create index map link
   if (getIndexPage(acTmp, acTmp1, &myCounty))
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

   // County code
   memcpy(pOutbuf+OFF_CO_NUM, "14INY", 5);

   // status
   *(pOutbuf+OFF_STATUS) = 'A';

   // TRA
   vmemcpy(pOutbuf+OFF_TRA, apTokens[L_TRA], SIZ_TRA);

   // Year assessed
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = dollar2Num(apTokens[L_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = dollar2Num(apTokens[L_IMPR]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = dollar2Num(apTokens[L_FIXTURES]);
   long lPers  = dollar2Num(apTokens[L_PPVAL]);
   long lGrowVal = dollar2Num(apTokens[L_LIVINGIMPR]);
   lTmp = lFixtr+lPers+lGrowVal;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         sprintf(acTmp, "%d         ", lFixtr);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = dollar2Num(apTokens[L_HOEXE]);
   long lTotalExe = dollar2Num(apTokens[L_TOTALEXE]);
   if (lTotalExe > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTotalExe);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }  

   if (lExe1 > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Legal
   if (*apTokens[L_PROPDESC] > ' ')
      updateLegal(pOutbuf, apTokens[L_PROPDESC]);

   // UseCode

   // Owner
   Iny_MergeOwner(pOutbuf, apTokens[L_OWNER], apTokens[L_OWNERX]);

   // Situs
   Iny_MergeSAdr(pOutbuf, apTokens[L_SADDR1], apTokens[L_SCITY], apTokens[L_SZIP]);

   // Mailing
   Iny_MergeMAdr(pOutbuf, apTokens[L_MADDR1], apTokens[L_MADDR2]);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "006380014000", 9))
   //   iTmp = 0;
#endif

   return 0;
}

/**************************** Iny_MergeRollChar ******************************
 *
 * Merge LotSize, BldgCls, UseCode
 *
 * Return 0 if successful, otherwise error
 *
 *****************************************************************************/

int Iny_MergeRollChar(char *pOutbuf)
{
   static   char  acRec[2048];
   static   int   iRead=0;
   int      iLoop;
   char     acTmp[64];

   REDIFILE  *pChar;

   // Get first Char rec for first call
   if (!iRead)
   {
      iRead = fread(acRec, 1, iRollLen, fdRoll);
   }

   pChar = (REDIFILE *)&acRec[0];

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00106311", 8))
   //   iTmp = 0;
#endif

   do
   {
      if (!iRead)
      {
         fclose(fdRoll);
         fdRoll = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      strncpy(acTmp, pChar->APN, CRESIZ_APN);
      replChar(acTmp, ' ', '0', CRESIZ_APN);
      iLoop = memcmp(pOutbuf, acTmp, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Roll rec  %.*s", iApnLen, pChar->APN);
         iRead = fread(acRec, 1, iRollLen, fdRoll);
         lRollSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Merge BldgCls
   *(pOutbuf+OFF_BLDG_CLASS) = pChar->BldgCls[0];

   // Merge UseCode
   memcpy(pOutbuf+OFF_USE_CO, pChar->UseCode, 3); 

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pChar->UseCode, 3, pOutbuf);

   // Merge LotSize
   long lLotSqft = atoin(pChar->LotSqft, CRESIZ_LOT_SQFT);
   double dTmp = atofn(pChar->Acres, CRESIZ_ACRES);
   if (dTmp > 0.0)
   {
      lLotSqft = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
   } else if (lLotSqft > 0)
      dTmp = (double)lLotSqft/SQFT_FACTOR_1000;

   if (lLotSqft > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lLotSqft);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Get next Char record
   iRead = fread(acRec, 1, iRollLen, fdRoll);
   lRollMatch++;

   return 0;
}

/******************************** Iny_Load_LDR ******************************
 *
 * Load LDR 2020
 *
 ****************************************************************************/

int Iny_Load_LDR_Csv(int iFirstRec /* 1=create header rec */)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[2048];
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH], acLienFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdLien;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt;

   GetIniString(myCounty.acCntyCode, "LienFile", "", acTmpFile, _MAX_PATH, acIniFile);

   // Sort roll file on ASMT
   sprintf(acLienFile, "%s\\%s\\%s_lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   strcpy(acRec, "S(#1,C,A) OMIT(#1,C,GT,\"1\")");
   iRet = sortFile(acTmpFile, acLienFile, acRec);  
   if (!iRet)
      return -1;

   // Open Lien file
   LogMsg("Open lien file %s", acLienFile);
   fdLien = fopen(acLienFile, "r");
   if (fdLien == NULL)
   {
      LogMsg("***** Error opening lien file: %s\n", acLienFile);
      return -1;
   }  

   // Open Char file
   LogMsg("Open Char file %s", acCharFile);
   fdChar = fopen(acCharFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening char file: %s\n", acCharFile);
      return -2;
   }

   // Open roll file
   GetIniString(myCounty.acCntyCode, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Output first header record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdLien);
   lCnt = 1;

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop 
   while (!feof(fdLien))
   {
      // Create new R01 record
      iRet = Iny_MergeLienCsv(acBuf, acRec);     // 2020
      if (!iRet)
      {
         // Merge LotSize, BldgCls, UseCode from REDIFILE
         if (fdRoll)
            iRet = Iny_MergeRollChar(acBuf);

         if (fdChar)
            iRet = Iny_MergeChar(acBuf);

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error writing to output file at record %d\n", lCnt);
            lRet = WRITE_ERR;
            break;
         }
      } 

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdLien);
      if (!pTmp)
         break;
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdLien)
      fclose(fdLien);
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut); 

   LogMsg(" Total input records:  %u", lCnt);
   LogMsg("      output records:  %u", lLDRRecCount);
   LogMsg("        Char matched:  %u", lCharMatch);
   LogMsg("         Char skiped:  %u", lCharSkip);
   LogMsg("        Roll matched:  %u", lRollMatch);
   LogMsg("         Roll skiped:  %u", lRollSkip);
   LogMsg("    bad-city records:  %u", iBadCity);
   LogMsg("  bad-suffix records:  %u\n", iBadSuffix);
   printf("\nTotal output records: %u\n", lLDRRecCount);

   lRecCnt = lLDRRecCount;
   return iRet;
}

/******************************* Iny_ExtrLienCsv *****************************
 *
 *****************************************************************************/

int Iny_ExtrLienRec(char *pOutbuf, char *pRollRec)
{
   int      iRet, lTmp, iTmp;
   char     acTmp[64], acApn[32];
   LIENEXTR *pLienExtr = (LIENEXTR *)pOutbuf;

   // Parse string ignoring quote
   iRet = ParseStringNQ(pRollRec, ',', MAX_FLD_TOKEN, apTokens);
   if (iRet < V_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s (%d)", apTokens[V_APN], iRet);
      return -1;
   }

   // Ignore voided transaction
   if (*apTokens[V_TRANTYPE] == 'V')
      return -2;

   // Clear output buffer
   memset(pOutbuf, ' ', sizeof(LIENEXTR));

   // Copy APN
   iRet = iApnLen-strlen(apTokens[V_APN]);
   iTmp = sprintf(acApn, "%.*s%s", iRet, "000", apTokens[V_APN]);
   vmemcpy(pLienExtr->acApn, acApn, iTmp);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001100061000", 9) )
   //   iTmp = 0;
#endif

   // Year assessed
   memcpy(pLienExtr->acYear, myCounty.acYearAssd, 4);

   // Land
   long lLand = atol(apTokens[V_LAND]);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLienExtr->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atol(apTokens[V_IMPROVEMENTS]);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLienExtr->acImpr, acTmp, SIZ_IMPR);
   }

   // Other value: Fixture, PersProp, PPMH
   long lFixtr = atol(apTokens[V_TRADEFIXTURES]);
   long lPers  = atol(apTokens[V_PERSONALPROPERTY]);
   long lGrow  = atol(apTokens[V_LIVINGIMPROVEMENTS]);
   lTmp = lFixtr+lPers+lGrow;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pLienExtr->acOther, acTmp, SIZ_OTHER);

      if (lFixtr > 0)
      {
         iTmp = sprintf(acTmp, "%d", lFixtr);
         memcpy(pLienExtr->acME_Val, acTmp, iTmp);
      }
      if (lPers > 0)
      {
         iTmp = sprintf(acTmp, "%d", lPers);
         memcpy(pLienExtr->acPP_Val, acTmp, iTmp);
      }
      if (lGrow > 0)
      {
         iTmp = sprintf(acTmp, "%d", lGrow);
         memcpy(pLienExtr->extra.MB.GrowImpr, acTmp, iTmp);
      }
   }

   // Gross total
   lTmp += (lLand+lImpr);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLienExtr->acGross, acTmp, SIZ_GROSS);
   }

   // Ratio
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
      memcpy(pLienExtr->acRatio, acTmp, SIZ_RATIO);
   }

   // Exemption
   long lExe1 = atol(apTokens[V_HOMEOWNEREXEMPTIONS]);
   long lExe2 = atol(apTokens[V_ALLOTHEREXEMPTIONS]);
   lTmp = lExe1+lExe2;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pLienExtr->acExAmt, acTmp, SIZ_EXE_TOTAL);

      if (lExe1 > 0)
         pLienExtr->acHO[0] = '1';
      else
         pLienExtr->acHO[0] = '2';
   }  

   pLienExtr->LF[0] = 10;
   pLienExtr->LF[1] = 0;

   return 0;
}

/******************************* Iny_ExtrLienCsv *****************************
 *
 *****************************************************************************/

int Iny_ExtrLienCsv(LPCSTR pCnty)
{
   char     *pTmp, acBuf[1024], acRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   int      iRet, iNewRec=0, lCnt=0;

   LogMsg0("Extract LDR roll for %s", pCnty);

   GetIniString(pCnty, "ValueFile", "", acBuf, _MAX_PATH, acIniFile);
   fdVal = fopen(acBuf, "r");
   if (fdVal == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -2;
   }

   // Create lien extract
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLienExt = fopen(acOutFile, "w");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -3;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdVal);
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdVal);

   // Merge loop
   while (pTmp && !feof(fdVal))
   {
      // Create new record
      iRet = Iny_ExtrLienRec(acBuf, acRec);
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLienExt);
         iNewRec++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next roll record
      pTmp = fgets(acRec, MAX_RECSIZE, fdVal);
      if (!isdigit(acRec[1]))
         break;      // EOF
   }

   // Close files
   if (fdVal)
      fclose(fdVal);
   if (fdLienExt)
      fclose(fdLienExt);

   LogMsg("Total output records:       %u", iNewRec);
   LogMsg("Total records processed:    %u", lCnt);
   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/******************************** Riv_ExtrSale ******************************
 *
 * Extract sale file SalesData.csv 08/19/2020
 *
 *****************************************************************************/

int Iny_ExtrSale()
{
   char      acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char      acTmp[256], acRec[2048], *pRec, *pTmp;
   long      lCnt=0, lPrice, iTmp;
   FILE      *fdOut;
   SCSAL_REC SaleRec;

   LogMsg0("Loading Sale file");

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return 2;
   }

   // Skip header
   pRec = fgets(acRec, 2048, fdSale);

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Loop through record set
   while (!feof(fdSale))
   {  
      pRec = fgets(acRec, 2048, fdSale);
      if (!pRec)
         break;

      // Parse input rec
      iTokens = ParseStringNQ(pRec, ',', MAX_FLD_TOKEN, apTokens);

      if (iTokens < S_FLDS)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[S_DOCNUM] == ' ' || !isdigit(*apTokens[S_DOCNUM]) || *apTokens[S_DOCDATE] == ' ')
         continue;

      // Reset output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));

      // APN
      iTmp = strlen(apTokens[S_PIN]);
      if (iTmp < iApnLen)
         sprintf(acTmp, "%.*s%s", iApnLen-iTmp, "000", apTokens[S_PIN]);
      else
         strcpy(acTmp, apTokens[S_PIN]);
      vmemcpy(SaleRec.Apn, acTmp, iApnLen);

      // Doc date
      pRec = dateConversion(apTokens[S_DOCDATE], acTmp, MM_DD_YYYY_1);
      if (pRec)
      {
         memcpy(SaleRec.DocDate, pRec, 8);
         iTmp = atol(acTmp);
         if (iTmp > lLastRecDate)
            lLastRecDate = iTmp;
      } else
      {
         LogMsg("*** Bad sale date: %s [%s]", apTokens[S_DOCDATE], apTokens[S_PIN]);
         continue;
      }

      // Docnum - 2012/3410
      if (*(apTokens[S_DOCNUM]+4) == '/')
         vmemcpy(SaleRec.DocNum, apTokens[S_DOCNUM], 9);
      else
      {
         LogMsg("*** Bad docnum: %s [%s]", apTokens[S_DOCNUM], apTokens[S_PIN]);
         continue;
      }

      // Sale price
      lPrice = atol(apTokens[S_SALEPRICE]);
      if (lPrice > 0)
      {
         sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
         SaleRec.DocType[0] = '1';
         SaleRec.NoneSale_Flg='N';
      } else if (!_memicmp(apTokens[S_DOCTYPE], "DEED", 4))
      {
         memcpy(SaleRec.DocType, "13", 2);
         SaleRec.NoneSale_Flg='Y';
      } else
      {
         memcpy(SaleRec.DocType, "74", 2);
         SaleRec.NoneSale_Flg='Y';
      }

      // Group sale?
      if ((pTmp = strchr(apTokens[S_DOCNUM], '-')) && isdigit(*(pTmp+1)))
         SaleRec.MultiSale_Flg = 'Y';

      // Seller

      // Buyer

      SaleRec.ARCode = 'A';
      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);
   
   LogMsg("Total processed records: %u", lCnt);
   LogMsg("    Last recording date: %u\n", lLastRecDate);

   // Update cumulative sale file
   if (lCnt > 0)
   {
      // Append and resort SLS file
      // Sort on APN asc, DocDate asc, DocNum asc
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A) F(TXT) DUPO(1,34) ");
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");     
      if (!_access(acCSalFile, 0))
         sprintf(acRec, "%s+%s", acTmpFile, acCSalFile);
      else
         strcpy(acRec, acTmpFile);
      lCnt = sortFile(acRec, acOutFile, acTmp);
      if (lCnt > 0)
      {
         // Rename old cum sale
         sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            iTmp = remove(acTmpFile);
         if (!_access(acCSalFile, 0))
            iTmp = rename(acCSalFile, acTmpFile);
         if (!iTmp)
            iTmp = rename(acOutFile, acCSalFile);
         else
            LogMsg("***** Error removing %s.  Please rerun with -Xs option", acCSalFile);
      }
   } else
      iTmp = -1;

   LogMsg("Total Sale records output: %d.\n", lCnt);
   return iTmp;
}

/******************************** Riv_MergeSAdr ******************************
 *
 * Merge Situs address from AssessmentRoll.csv
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

//int Riv_MergeSAdr(char *pOutbuf)
//{
//   static   char acRec[512], *pRec=NULL;
//   char     acTmp[256], acCode[32], acAddr1[256];
//   long     lTmp;
//   int      iTmp;
//
//   // Remove old situs
//   removeSitus(pOutbuf);
//
//   // Merge data
//   acAddr1[0] = 0;
//   lTmp = atol(apTokens[R_SITUSSTREETNUMBER]);
//   if (lTmp > 0)
//   {
//      memcpy(pOutbuf+OFF_S_HSENO, apTokens[R_SITUSSTREETNUMBER], strlen(apTokens[R_SITUSSTREETNUMBER]));
//      iTmp = sprintf(acTmp, "%d ", lTmp);
//      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
//      strcpy(acAddr1, acTmp);
//
//      if (*apTokens[R_SITUSSTREETNUMBERSUFFIX] > ' ')
//         vmemcpy(pOutbuf+OFF_S_STR_SUB, apTokens[R_SITUSSTREETNUMBERSUFFIX], SIZ_S_STR_SUB);
//
//      if (*apTokens[R_SITUSSTREETPREDIRECTIONAL] > ' ')
//      {
//         strcat(acAddr1, apTokens[R_SITUSSTREETPREDIRECTIONAL]);
//         strcat(acAddr1, " ");
//         memcpy(pOutbuf+OFF_S_DIR, apTokens[R_SITUSSTREETPREDIRECTIONAL], strlen(apTokens[R_SITUSSTREETPREDIRECTIONAL]));
//      }
//   }
//
//   strcpy(acTmp, _strupr(apTokens[R_SITUSSTREETNAME]));
//   strcat(acAddr1, acTmp);
//   vmemcpy(pOutbuf+OFF_S_STREET, acTmp, SIZ_S_STREET);
//
//   if (*apTokens[R_SITUSSTREETTYPE] > ' ')
//   {
//      strcat(acAddr1, " ");
//      strcat(acAddr1, apTokens[R_SITUSSTREETTYPE]);
//
//      iTmp = GetSfxCodeX(apTokens[R_SITUSSTREETTYPE], acTmp);
//      if (iTmp > 0)
//         Sfx2Code(acTmp, acCode);
//      else
//      {
//         LogMsg0("*** Invalid suffix: %s", apTokens[R_SITUSSTREETTYPE]);
//         iBadSuffix++;
//         memset(acCode, ' ', SIZ_S_SUFF);
//      }
//      memcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
//   } 
//
//#ifdef _DEBUG
//   //if (!memcmp(pOutbuf, "616141018", 9))
//   //   acTmp[0] = 0;
//#endif
//
//   if (isalnum(*apTokens[R_SITUSUNITNUMBER]) || *apTokens[R_SITUSUNITNUMBER] == '#')
//   {
//      strcat(acAddr1, " ");
//      if (isdigit(*apTokens[R_SITUSUNITNUMBER]))
//         strcat(acAddr1, "#");
//      else if ((*apTokens[R_SITUSUNITNUMBER]) != '#')
//         strcat(acAddr1, "STE ");
//      strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]);
//      memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER], strlen(apTokens[R_SITUSUNITNUMBER]));
//   } else if (*(pOutbuf+OFF_M_UNITNO) > ' ' &&
//      !memcmp(pOutbuf+OFF_S_STRNUM, pOutbuf+OFF_M_STRNUM, SIZ_S_STRNUM) &&
//      !memcmp(pOutbuf+OFF_S_STREET, pOutbuf+OFF_M_STREET, SIZ_S_STREET) )
//   {
//      memcpy(pOutbuf+OFF_S_UNITNO, pOutbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
//      sprintf(acTmp, " %.*s", SIZ_M_UNITNO, pOutbuf+OFF_M_UNITNO);
//      strcat(acAddr1, acTmp);
//   } else if (*apTokens[R_SITUSUNITNUMBER] > ' ')
//   {
//      if (isalnum(*(1+apTokens[R_SITUSUNITNUMBER])))
//      {
//         strcat(acAddr1, " #");
//         strcat(acAddr1, apTokens[R_SITUSUNITNUMBER]+1);
//         memcpy(pOutbuf+OFF_S_UNITNO, apTokens[R_SITUSUNITNUMBER]+1, strlen(apTokens[R_SITUSUNITNUMBER])-1);
//      } else
//         LogMsg("*** Invalid situs Unit#: %s [%s]", apTokens[R_SITUSUNITNUMBER], apTokens[R_APN]);
//   }
//
//   iTmp = blankRem(acAddr1);
//   memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);
//   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
//
//   // Situs city
//   if (*apTokens[R_SITUSCITYNAME] > ' ')
//   {
//      char sCity[32];
//
//      iTmp = City2CodeEx(_strupr(apTokens[R_SITUSCITYNAME]), acCode, sCity, apTokens[R_APN]);
//      if (iTmp > 0)
//      {
//         vmemcpy(pOutbuf+OFF_S_CITY, acCode, SIZ_S_CITY);
//      } else
//         strcpy(sCity, apTokens[R_SITUSCITYNAME]);
//
//      if (*apTokens[R_SITUSZIPCODE] == '9')
//         vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[R_SITUSZIPCODE], SIZ_S_ZIP);
//
//      iTmp = sprintf(acTmp, "%s, CA ", sCity, apTokens[R_SITUSZIPCODE]);
//      memcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, iTmp);
//   }
//
//   return 0;
//}

/****************************** Iny_MergeRollCsv *****************************
 *
 * Return 0 if successful, < 0 if error
 *        1 retired record, not use
 *
 *****************************************************************************/

int Iny_MergeRollCsv(char *pOutbuf, char *pRollRec, char *pApn, int iFlag)
{
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   double   dTmp;
   int      iRet=0, iTmp;

   // Parse roll record
   iRet = ParseStringNQ(pRollRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iRet < R_FLDS)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[R_PIN]);
      return -1;
   }
#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "164760072", 9) )
   //   iTmp = 0;
#endif

   // Ignore non-taxable parcels - Keep non-taxable parcel during update
   //if (*apTokens[R_ISTAXABLE] == 'N')
   //   return 1;

   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Start copying data
      memcpy(pOutbuf, pApn, iApnLen);
      *(pOutbuf+OFF_STATUS) = 'A';

      // Format APN
      iRet = formatApn(pApn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(pApn, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      // County code
      memcpy(pOutbuf+OFF_CO_NUM, "14INYA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoi(apTokens[R_LAND]);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoi(apTokens[R_IMPR]);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other values
      long lFixt  = atoi(apTokens[R_FIXTURES]);
      long lPP    = atoi(apTokens[R_PPVALUE]);
      long lGrow  = atoi(apTokens[R_LIVINGIMPR]);
      lTmp = lFixt+lPP+lGrow;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixt > 0)
         {
            sprintf(acTmp, "%d         ", lFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP > 0)
         {
            sprintf(acTmp, "%d         ", lPP);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lGrow > 0)
         {
            sprintf(acTmp, "%d         ", lGrow);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }

      // Gross total
      lTmp += (lLand+lImpr);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Bad data HOX=WelfareCollegeExemption
      lTmp =  atoi(apTokens[R_HOX]);
      if (lTmp != 7000)
      {
         lTmp =  atoi(apTokens[R_VET_EXE]);
         lTmp += atoi(apTokens[R_DVET_EXE]);
         lTmp += atoi(apTokens[R_CHURCH_EXE]);
         lTmp += atoi(apTokens[R_REL_EXE]);
         lTmp += atoi(apTokens[R_CEMETERY_EXE]);
         lTmp += atoi(apTokens[R_PUBSCHOOL_EXE]);
         lTmp += atoi(apTokens[R_PUBLIBRAY_EXE]);
         lTmp += atoi(apTokens[R_PUBMUSEUM_EXE]);
         lTmp += atoi(apTokens[R_WELCOLL_EXE]);
         lTmp += atoi(apTokens[R_WELHOSP_EXE]);
         lTmp += atoi(apTokens[R_WELPRISCH_EXE]);
         lTmp += atoi(apTokens[R_WELCHARREL_EXE]);
         lTmp += atoi(apTokens[R_OTHER_EXE]);
         *(pOutbuf+OFF_HO_FL) = '2';      // 'Y'
      } else
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'

      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   }

   // TRA
   lTmp = atol(apTokens[R_TRA]);
   sprintf(acTmp, "%.6d", lTmp);
   memcpy(pOutbuf+OFF_TRA, acTmp, 6);

   // Legal
   iTmp = updateLegal(pOutbuf, apTokens[R_LEGAL]);
   if (iTmp > iMaxLegal)
      iMaxLegal = iTmp;

   // UseCode - being updated at LDR

   // Acres
   dTmp = atof(apTokens[R_ACREAGE]);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Format Acres
      lTmp = (long)(dTmp * ACRES_FACTOR);
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, lTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   }

   // Owner
   Iny_MergeOwner(pOutbuf, apTokens[R_OWNER], "");

#ifdef _DEBUG
   //if (!memcmp(apTokens[R_APN], "004161088000", 9) )
   //   iTmp = 0;
#endif
   // Situs
   Iny_MergeSAdr(pOutbuf, apTokens[R_S_STREETNUM], apTokens[R_S_STREETSUB], apTokens[R_S_STREETNAME], apTokens[R_S_CITY]);

   // Mailing
   Iny_MergeMAdr(pOutbuf, apTokens[R_M_ADDRESS], apTokens[R_M_CITY], apTokens[R_M_STATE], apTokens[R_M_ZIPCODE]);

   return 0;
}

/****************************** Iny_Load_RollCsv ******************************
 *
 * Load updated roll file AssessmentRollData.csv (08/20/2020)
 *
 ******************************************************************************/

int Iny_Load_RollCsv(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acLastApn[16], acApn[16];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acSrtFile[_MAX_PATH], *pTmp;
   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0, lCnt=0;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet, bEof;
   HANDLE   fhIn, fhOut;

   LogMsg0("Loading roll update");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Rename files for processing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
         return -1;
      }
   }

   // Sort input, omit unsecured
   sprintf(acSrtFile, "%s\\INY\\Iny_Roll.srt", acTmpPath);
   sprintf(acRec, "S(#1,N,A) OMIT(#2,C,GE,\"U\",OR,#1,N,LT,\"99999\") DEL(%d) F(TXT)", cDelim);
   iTmp = sortFile(acRollFile, acSrtFile, acRec);

   // Open roll file
   LogMsg("Open Roll file %s", acSrtFile);
   fdRoll = fopen(acSrtFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acSrtFile);
      return 2;
   }

   LogMsg("Open Char file %s", acCChrFile);
   fdChar = fopen(acCChrFile, "r");
   if (fdChar == NULL)
   {
      LogMsg("***** Error opening Char file: %s\n", acCChrFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Get 1st rec
   pTmp = fgets((char *)&acRollRec[0], MAX_RECSIZE, fdRoll);    
   bEof = (pTmp ? false:true);

   // Init variables
   iNoMatch=iBadCity=iBadSuffix=0;
   strcpy(acLastApn, "000000000");

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      // Ignore record start with 99999999
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      // EOF ?
      if (!nBytesRead)
         break;

NextRollRec:
#ifdef _DEBUG
      //if (!memcmp(acRollRec, "164760072", 9) )
      //   iTmp = 0;
#endif

      iTmp = Iny_FormatApn(acRollRec, acApn, true);
      if (iTmp != iApnLen)
      {
         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
         {
            bEof = true;
            break;
         } else
            goto NextRollRec;
      }
      iTmp = memcmp(acBuf, acApn, iApnLen);
      if (!iTmp)
      {
         // Create R01 record from roll data
         iRet = Iny_MergeRollCsv(acBuf, acRollRec, acApn, UPDATE_R01);
         if (!iRet)
         {
            // Merge Char
            //if (fdChar)
            //   iTmp = Iny_MergeStdChar(acBuf);

            iRollUpd++;
         }

         // Read next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

         if (!pTmp)
            bEof = true;         // Signal to stop
      } else if (iTmp > 0)       // Roll not match, new roll record?
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, acRollRec, lCnt);

         // Create new R01 record
         iRet = Iny_MergeRollCsv(acRec, acRollRec, acApn, CREATE_R01);
         if (!iRet)
         {
            // Merge Char
            //if (fdChar)
            //   iTmp = Riv_MergeStdChar(acRec);

            if (memcmp(acLastApn, acRec, 14))
            {
               memcpy(acLastApn, acRec, 14);
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               iNewRec++;
            } else
            {
               LogMsg("---> Duplicate APN: %.14s", acLastApn);
            }
         }
         lCnt++;

         // Get next roll record
         pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);
         if (!pTmp)
            bEof = true;    // Signal to stop
         else
            goto NextRollRec;
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("Retired record? : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[1], lCnt);
         iRetiredRec++;

         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
      if (!iRet)
      {
         if (memcmp(acLastApn, acBuf, 14))
         {
            memcpy(acLastApn, acBuf, 14);
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            if (!bRet)
            {
               LogMsg("Error occurs: %d\n", GetLastError());
               break;
            }
         }
      }
   }

   // Do the rest of the file
   while (!bEof && isdigit(acRollRec[0]))
   {
      // Create new R01 record
      iRet = Iny_MergeRollCsv(acRec, acRollRec, acApn, CREATE_R01);

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d) ", iApnLen, acRollRec, lCnt);

         // Merge Char
         //if (fdChar)
         //   iRet = Riv_MergeStdChar(acRec);

         if (memcmp(acLastApn, acRec, 14))
         {
            memcpy(acLastApn, acRec, 14);
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            iNewRec++;
         } else
         {
            LogMsg("---> Duplicate APN: %.14s", acLastApn);
         }
      }
      lCnt++;

      // Get next roll record
      pTmp = fgets(acRollRec, MAX_RECSIZE, fdRoll);

      if (!pTmp)
         bEof = true;    // Signal to stop
      else
      {
         iTmp = Iny_FormatApn(acRollRec, acApn);
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdChar)
      fclose(fdChar);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   LogMsg("Number of Char matched:     %u", lCharMatch);
   LogMsg("Number of Char skiped:      %u\n", lCharSkip);

   printf("\nTotal output records: %u", lCnt);

   lRecCnt = lCnt;
   return 0;
}

/*********************************** loadIny ********************************
 *
 * Input files:
 *    redifile          899-bytes roll file
 *
 * Problems:
 *
 * Processing:
 *
 * Commands:
 *    -U    Monthly update
 *    -L    Lien update
 *
 ****************************************************************************/

int loadIny(int iLoadFlag, int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;
   iRollLen = guessRecLen(acRollFile, iRollLen);

   // Load tax
   if (iLoadTax)                                   // -T
   {
      // Load tax agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      // Load Tax_Base 
      iRet = Cres_Load_Cortac(bTaxImport, false);
      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Cres_Load_TaxDelq(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode);

         // Load Detail & Agency
         if (!iRet)
            iRet = Cres_Load_GFGIS(bTaxImport, false);

         //if (!iRet)
         //   iRet = Cres_Load_TaxOwner(bTaxImport);

         //iRet = doTaxPrep(myCounty.acCntyCode, TAX_OWNER);
      }

      if (iRet > 0)
         iRet = 0;
      if (!iLoadFlag)
         return iRet;
   }

   // Use SaleTmpl for cum sale file
   strcpy(acESalTmpl, acSaleTmpl);

   /*
   strcpy(acRollFile, "G:\\CO_DATA\\Iny\\2014\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SIny_CD\\raw\\Iny_Sale.2014");
   iRet = Iny_UpdSale1(acTmpFile);
   strcpy(acRollFile, "G:\\CO_DATA\\Iny\\redifile");
   */

   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      // Extract sales from Pamsale.txt 07/17/2008 spn
      //iRet = Iny_ExtrSale(acSaleFile, NULL, false);
      //if (!iRet)
      //{
      //   // Extract roll sale and update Iny_Sale.rol
      //   if (iRet = Iny_UpdSale1(acRollSale))
      //      return iRet;

      //   // Merge pamsales with roll sales --> Iny_Sale.sls
      //   iRet = MergeSaleFiles(acCSalFile, acRollSale, NULL, true);

      //   if (!iRet)
      //      iLoadFlag |= MERG_CSAL;
      //}

      // 2020
      iRet = Iny_ExtrSale();
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Extract NDC recorder sale 
   if (iLoadFlag & EXTR_NRSAL)                     // -Xn
   {
      iRet = GetIniString(myCounty.acCntyCode, "NdcSale", "", acSaleFile, _MAX_PATH, acIniFile);
      if (!_access(acSaleFile, 0))
      {
         iRet = NR_CreateSCSale(myCounty.acCntyCode, acSaleFile, iApnLen);
         if (iRet)
            return iRet;
      } else
      {
         LogMsg("*** Sale file not available.  Please verify: %s", acSaleFile);
         return -1;
      }
   }

   if (iLoadFlag & EXTR_ATTR)                      // -Xa
   {
      // Extract to STDCHAR format
      //GetIniString("Data", "CChrFile", "", acTmpFile, _MAX_PATH, acIniFile);
      //sprintf(acOutFile, acTmpFile, myCounty.acCntyCode, myCounty.acCntyCode);
      //iRet = Iny_ConvStdChar(acCharFile, acOutFile);
      iRet = Iny_ConvStdChar();
      if (iRet > 0)
         iRet = 0;
   }

   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Iny_ExtrLienCsv(myCounty.acCntyCode);
      //iRet = Cres_ExtrLien(myCounty.acCntyCode);

   if (iLoadFlag & LOAD_LIEN)                      // -L 
   {
      LogMsg0("Loading LDR file");
      //iRet = Iny_Load_LDR(myCounty.acCntyCode);
      // 2020
      iRet = Iny_Load_LDR_Csv(iSkip);
   } else if (iLoadFlag & LOAD_UPDT)               // -U
   {
      LogMsg0("Loading roll update file");
      //iRet = Iny_LoadRoll(myCounty.acCntyCode, iSkip);
      iRet = Iny_Load_RollCsv(iSkip);
   }

   // Implement this when using Pamsales.txt.  Ignore it otherwise
   if (!iRet && (iLoadFlag & MERG_CSAL) )          // -Ms
   {
      // Apply Iny_Sale.sls to R01 file
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE);
   }

   // Apply NDC sale file to R01
   if (!iRet && (iLoadFlag & UPDT_XSAL))           // -Mn
   {
      // Apply Col_Ash.sls to R01 file
      GetIniString("Data", "ASH_File", acESalTmpl, acESalTmpl, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
      iRet = ApplyCumSaleNR(iSkip, acCSalFile, SALE_USE_SCUPDXFR);
      if (!iRet)
         iLoadFlag |= LOAD_UPDT;
   }

   return iRet;
}
