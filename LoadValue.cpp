/******************************************************************************
 *
 * This module is created to support loading data into Pcl_Values table. Since
 * this task is run once a year, the import into SQL task will be done manually.
 *
 * 05/26/2015 14.16.0   Clean up this file and make it avail for all.
 * 04/11/2017 16.14.5   Modify createValueOutrec() to output NULL when ValueSetType="0"
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "LoadValue.h"

extern   COUNTY_INFO myCounty;

/******************************************************************************
 *
 * Return 0 if success.  Otherwise error;
 *
 ******************************************************************************/

int createValueOutrec(VALUE_REC *pValue, LPSTR pSql)
{
   char     acTmp[1024];

   sprintf(acTmp, "%.3s|%.20s|%.1s|%.4s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.10s|%.2s|%.10s|%.50s|%.10s|%.2s|%.2s\n",
      myCounty.acCntyCode,
      pValue->Apn,
      pValue->ValueSetType[0] > '0' ? pValue->ValueSetType : "",
      myCounty.acYearAssd,
      pValue->Land,
      pValue->Impr,
      pValue->Other,
      pValue->Exe,
      pValue->TotalVal,
      pValue->Fixtr,
      pValue->PersProp,
      pValue->PP_MH,
      pValue->Businv,
      pValue->GrowImpr,
      pValue->TreeVines,
      pValue->Mineral,
      pValue->Timber,
      pValue->ClcaLand,
      pValue->ClcaImpr,
      pValue->HomeSite,
      pValue->Oth_Impr,
      pValue->Fixtr_RP,
      pValue->HH_PP,
      pValue->Reason, pValue->BaseYearVal, pValue->Misc, pValue->NetVal, pValue->Fixtr_Type, pValue->Exe_Type);

   remChar(acTmp, 32);
   strcpy(pSql, acTmp);

   return 0;
}

LPSTR createValueHeader(LPSTR sHdr)
{
   sprintf(sHdr, "pv_co3|pv_apn|pv_vst_type|pv_year|pv_land|pv_impr|pv_other|pv_exe|pv_total|pv_fixtr|pv_persprop|pv_pp_mh|"
      "pv_businv|pv_growimpr|pv_treevines|pv_mineral|pv_timber|pv_clcaland|pv_clcaimpr|pv_homesite|pv_oth_impr|pv_fixtr_rp|"
      "pv_hh_pp|pv_reason|pv_byv|pv_misc|pv_net|pv_fixtrtype|pv_exetype\n");
   return sHdr;
}

/******************************************************************************
 *
 * Return: Number of output records
 *
 ******************************************************************************/

int createValueImport(LPSTR pInfile, LPSTR pOutfile, bool bInclHdr)
{
   char     *pTmp, acRec[1024], acTmp[1024];
   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   VALUE_REC *pVal;

   LogMsg("Extract Values for import using %s", pInfile);

   // Open Value file
   LogMsg("Open value file %s", pInfile);
   fdIn = fopen(pInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening value file: %s\n", pInfile);
      return -1;
   }

   LogMsg("Open output file %s", pOutfile);
   if (!(fdOut = fopen(pOutfile, "w")))
   {
      LogMsg("***** Error creating output file %s", pOutfile);
      return -2;
   }

   // Write header
   if (bInclHdr)
      fputs(createValueHeader(acTmp), fdOut);

   pVal = (VALUE_REC *)&acRec[0];

   // Merge loop
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], 1024, fdIn);
      if (!pTmp)
         break;

      acTmp[0] = 0;
      iRet = createValueOutrec(pVal, acTmp);

      fputs(acTmp, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   LogMsg("Total Value records for import: %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return lCnt;
}
