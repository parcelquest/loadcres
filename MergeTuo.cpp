/*****************************************************************************
 *
 * Notes: 
 *    - Sale data also contains characteristic.  However, we currently not
 *      use it.  Function Tuo_MergeChar() is written but not being used.  This
 *      needs a revisit later.
 *    - salesdata is updated annually and about 3 years behind. 
 *    - Before loading LDR, copy GrGr_Exp.dat from production RAW folder to current
 *      working folder, also copy TUO_Sale.sls.
 *    - TUO is asking to keep all sales even if they are on the same date
 *
 * Revision:
 * 02/09/2006 1.0.0     First version by Tung Bui
 * 12/19/2006 1.8.5.1   Fix Tuo_LoadUpdt() and Tuo_LoadLien() to skip blank records
 *                      at the beginning of redifile.
 * 04/26/2007 1.8.6     Redo sale update procedure.  Using SALESDATA file and GRGR
 *                      as main source of sale info.  Use rec info from redifile
 *                      only when sale data is not available or with the same date.
 * 05/24/2007 1.8.8.1   Reformat Doc#, removing two digit year for docs from 2002
 *                      and later.  Docs in 2001 and before are kept the same.
 * 06/05/2007 1.8.9     Add Tuo_FmtDocLink() to format DocLink for DocNum.
 * 07/22/2007 1.8.12    Adding option -Xs to create Tuo_Sale.dat using SALE_REC1 record
 *                      Load lien is now automatically resorted cum sale and applied
 *                      to R01 file.  These options ca be used to load lien:
 *                      -L -Xs -G [-Xc (only use this if Sale_Exp.sls is not avail.)]
 * 08/07/2007 1.8.13    Adding code to check for new GrGr input.  They can be in
 *                      zip file structure or regular multi HFW files.  There are now
 *                      two separate functions Tuo_LoadGrGr() and Tuo_LoadGrGrNoZip()
 *                      to process GrGr.
 * 08/15/2007 1.8.15    Fix Owner name problem as in "SONORA/FIVE ASSOCIATES"
 * 09/14/2007 1.8.18    Copy grgr zip file to storage after processed.
 * 12/16/2007 1.9.0     Use Cres_UpdCumSale() to update cum sale and ApplyCumSale()
 *                      to merge it into R01 file. Merge GrGr will be done later.
 * 12/18/2007 1.9.1     GrGr extract is now merged separately in the last step.  It is
 *                      no longer appended to cum sale file.
 * 01/23/2008 1.10.2    Adding code to support standard use code
 * 02/25/2008 1.10.3.1  Adding code to create update file
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 06/24/2008 1.12.1.1  Remove unused code
 * 06/25/2008 8.1.0     Sense the GrGr input file for delimited type then use appropriate
 *                      function to parse it.
 * 12/15/2008 8.5.1     Ignore sale data from redifile.  Rename CreateTuoSale() to Tuo_ExtrSale()
 *                      and update history sale records with new data from SALESDATA file.
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 *                      Process county files for assessor specific products.
 * 06/17/2009 8.6.2     Add LoadTuoG() to process GrGr data for CD-Assr.
 * 06/25/2009 9.1.0     Add Cres_ExtrLien() and modify Tuo_CreateR01() to support
 *                      new value fields.
 * 08/04/2009 9.1.3     Populate other values.
 * 10/14/2009 9.1.5     Set lAssrRecCnt = 999999999 in Tuo_ProcAssr()to bypass Products 
 *                      table update of Record Count.
 * 10/19/2009 9.1.6     Modify Tuo_ProcAssr() to update Products table in place rather than in main().
 * 12/07/2009 9.1.8     Copy TUOG.S01 to TUOG.R01 if there is no new GrGr data.
 * 06/21/2010 10.0.0    Add option to merge cumulative sale file.
 * 06/28/2011 11.0.0    Modify Tuo_ExtrSale() to create output in SCSAL_REC format.
 *                      Call Cres_UpdSale1() to extract Sale1 from redifile and update cum sale.
 * 06/30/2011 11.0.1    Save original StrNum in S_HSENO.
 * 10/10/2011 11.2.5    Clear situs before updating to avoid left over in Tuo_MergeAdr().
 * 10/26/2011 11.2.8.1  Change sort output command on Tuo_ExtrSale().
 * 05/16/2012 11.5.1    Fix sum sale file by setting MultiSale_Flg='Y' where NumOfPrclXfer='M'.
 *                      Use Cres_UpdCumSale() instead of Cres_UpdSale1() to pull all transactions
 *                      from redifile to cum sale file. Add Cres_UpdXfer() to update transfer with
 *                      SALE1 regardless of sale price. Clear all sales in R01 before update.
 * 05/17/2012 11.5.2    Add -T option to load Tax file.
 * 07/10/2012 12.0.2    Add Tuo_MergeZoning() to add Zoning data from GIS file R01.
 * 01/09/2013 12.5.1    Add Tuo_MakeDocLink() to create PQ DocLink.
 * 05/07/2013 12.6.3    Change prototype of Tuo_MakeDocLink() not to use pDocPath.  Local function
 *                      should use acDocPath global variable.
 * 06/30/2013 13.0.1    Add zoning to LDR roll in Tuo_LoadLien().
 * 09/20/2013 13.2.7    Fix MergeOwner() to keep names as is if they are a corp name.
 *                      Update Vesting if found.
 * 09/27/2013 13.3.0    Modify Tuo_MergeOwner() and Tuo_CleanName() to update Vesting.
 *                      Use removeNames() to clear owner names
 * 09/30/2013 13.3.2    Updating AC, Heating, FP, and ParkType in 'N' case.
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 11/07/2013 13.4.2    Get delimiter from INI for zoning file. Modify Tuo_MergeZoning()
 * 11/12/2013 13.4.2.5  Fix Tuo_MergeZoning() to keep primary zone only if it's too long.
 * 11/14/2013 13.4.3    Fix bug in Tuo_MergeAdr()
 * 02/03/2014 13.5.1    Use Cres_UpdSale1() to create Tuo_Sale.rol with added SaleCode.
 *                      And use MergeSaleFiles() to update Tuo_Sales.sls. Some dup records
 *                      will be removed.
 * 06/09/2014 13.6.2    Add TUO_DocTitle[] for GRGR translation. Modify Tuo_ExtrSale() to validate
 *                      RecDate, use DocTax to calculate sale price (use conf sale price if needed).
 *                      Add Tuo_FixDocTax() to remove repeated sale price and questionable DocNum.
 *                      Change sale update logic to create new sale history when -Xs is used.  
 *                      Accummulate new sales from redifile in Tuo_Sale.rol and update this file
 *                      daily since SALESDATA is not up to date.
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 03/13/2015 14.6.3    Modify Tuo_MakeDocLink() to format DOCLINK starting with year.
 *                      Replace Tuo_FmtDocLinks() with updateDocLinks().
 * 05/26/2015 14.7.0    Add Tuo_LoadValue() to load value notice file from Tax Collector.
 * 06/04/2015 14.7.1    Fix bug in Tuo_MakeDocLink() that causes memory overflow.
 * 06/09/2015 14.7.2    Modify Tuo_LoadValue() to create extra record for BYV with VST=2
 *                      and Reason=''; and sort output file.
 * 06/15/2015 14.7.3    Modify Tuo_LoadValue() to remove LICNO, FIXTR_TYPE, and EXE_TYPE.
 * 07/02/2015 15.0.0    Replace NetTaxVal with NetVal to mirror change in VALUE_REC.
 * 08/11/2015 15.1.1    Modify Tuo_LoadValue() to create extra Prop8 record if BYV > Total Value.
 * 08/25/2015 15.1.2    Fix bug Tuo_LoadValue() to add extra Prop8 record.
 * 12/07/2015 15.3.0    Add Load_Owner() to LoadTuo().
 * 02/12/2016 15.5.1    Modify Tuo_MergeOwner() to fix SWAP_NAME problem and remove Name2
 *                      if it's the same as Name1.
 * 02/19/2016 15.7.1    Clean up Tuo_MergeOwner().
 * 02/26/2016 15.7.2    Fix Name2 bug in Tuo_MergeOwner()
 * 07/13/2016 16.1.0    Include SqlExt.h and move all tables to .H file
 * 08/18/2016 16.2.0    Add option to load Tax Agency from external text file.
 * 02/02/2017 16.8.2    Add check for empty sale file in Tuo_ExtrSale()
 *
 *****************************************************************************/

#include "stdafx.h"

#define  _DOC_LINK

#include "CountyInfo.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doSort.h"
#include "doOwner.h"
#include "doGrGr.h"
#include "doZip.h"
#include "formatApn.h"
#include "SaleRec.h"
#include "MergeTuo.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "CSpreadSheet.h"
#include "LoadValue.h"
#include "SendMail.h"
#include "SqlExt.h"
#include "Tax.h"

hlAdo    hTuoDb;
FILE     *fdZoning;
long     lZoningMatch, lZoningSkip;


/******************************* Tuo_LoadValue ******************************
 *
 * Load value file and create value import file
 * Return #records extracted.
 *
 ****************************************************************************/

int Tuo_LoadValue(char *pInfile, char *pSheet, char *pExt, char *pOutfile)
{
   char     acTmp[256], sOutbuf[1024], acOutfile[_MAX_PATH];
   int      iRet, iRowCnt, lCnt, iRows, iCols, iSkipRows, iRecCnt;
   long     lLand, lImpr, lPP, lFixtr, lExe, lTmp, lTotal;
   FILE     *fdOut;

   CString      strTmp, strApn;
   CStringArray asRows;
   VALUE_REC   *pVal = (VALUE_REC *)&sOutbuf[0];

   LogMsgD("\nProcess value file %s on sheet %s", pInfile, pSheet);

   // Open input file - no backup
   CSpreadSheet sprSht(pInfile, pSheet, false, pExt);

   iRows = sprSht.GetTotalRows();
   iCols = sprSht.GetTotalColumns();

   if (iCols < TUO_VALUE_COLS)
   {
      LogMsg("WARNING: Number of columns is smaller than expected: %d", iCols);
      return 0;
   }

   // Open output file
   sprintf(acOutfile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "vmp");
   if (!(fdOut = fopen(acOutfile, "w")))
   {
      LogMsg("ERROR - Unable to create output file: %s", acOutfile);
      return -1;
   }

   // Initialize pointers
   lCnt = 0;
   iSkipRows = iRecCnt = 0;
   sprSht.GetFieldNames(asRows);

   // Skip first header row
   for (iRowCnt = 2; iRowCnt <= iRows; iRowCnt++)
   {
      sprSht.ReadRow(asRows, iRowCnt);  
      if (asRows.GetSize() < iCols)
      {
         iSkipRows++;
         LogMsg("Skip row# %d (wrong number of columns)", iRowCnt);
         continue;
      } else
      {
         lLand=lImpr=lPP=lFixtr = 0;
         memset(&sOutbuf, ' ', sizeof(VALUE_REC));
         strApn = asRows.GetAt(TUO_VALUE_APN);  
         if (!strApn.IsEmpty())
         {
            strcpy(acTmp, strApn);
		      iRet = remChar(acTmp, '-');
            memcpy(pVal->Apn, acTmp, iRet);
         } else
         {
            iSkipRows++;
            LogMsg("Skip row# %d (no APN)", iRowCnt);
            continue;
         }
#ifdef _DEBUG
         //if (!memcmp(pVal->Apn, "0380212300", 10))
         //   iRet = 0;
#endif

         // Tax year
         strTmp = asRows.GetAt(TUO_VALUE_YEAR);  
         memcpy(pVal->TaxYear, strTmp, strTmp.GetLength());
         pVal->ValueSetType[0] = '1';
         pVal->Reason[0] = '1';

         // License No
         //strTmp = asRows.GetAt(TUO_VALUE_LICNO);  
         //if (strTmp > "0")
         //   memcpy(pVal->Misc, strTmp, strTmp.GetLength());

         // Land
         strTmp = asRows.GetAt(TUO_VALUE_LAND);  
         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
         if (iRet > 0)
         {
            lLand = atol(acTmp);
            iRet = sprintf(acTmp, "%d", lLand);
            memcpy(pVal->Land, acTmp, iRet);
         }

         // Impr
         strTmp = asRows.GetAt(TUO_VALUE_IMPR);  
         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
         if (iRet > 0)
         {
            lImpr = atol(acTmp);
            iRet = sprintf(acTmp, "%d", lImpr);
            memcpy(pVal->Impr, acTmp, iRet);
         }

         // Personal Prop
         strTmp = asRows.GetAt(TUO_VALUE_PPVAL);  
         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
         if (iRet > 0)
         {
            lPP = atol(acTmp);
            iRet = sprintf(acTmp, "%d", lPP);
            memcpy(pVal->PersProp, acTmp, iRet);
         }

         // Fixture
         strTmp = asRows.GetAt(TUO_VALUE_FIXTVAL);  
         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
         if (iRet > 0)
         {
            lFixtr = atol(acTmp);
            iRet = sprintf(acTmp, "%d", lFixtr);
            memcpy(pVal->Fixtr, acTmp, iRet);
            //strTmp = asRows.GetAt(TUO_VALUE_FIXTTYPE);  
            //strTmp.TrimLeft();
            //if (strTmp.GetLength() > 0)
            //   pVal->Fixtr_Type[0] = strTmp.GetAt(0);
         }

         // Exemption
         strTmp = asRows.GetAt(TUO_VALUE_EXEAMT);  
         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
         if (iRet > 0)
         {
            lExe = atol(acTmp);
            iRet = sprintf(acTmp, "%d", lExe);
            memcpy(pVal->Exe, acTmp, iRet);
            //strTmp = asRows.GetAt(TUO_VALUE_EXETYPE);  
            //strTmp.TrimLeft();
            //if (strTmp.GetLength() > 0)
            //   pVal->Exe_Type[0] = strTmp.GetAt(0);
         }

         // Others
         lTmp = lFixtr + lPP;
         if (lTmp > 0)
         {
            iRet = sprintf(acTmp, "%d", lTmp);
            memcpy(pVal->Other, acTmp, iRet);
         }

         // Gross
         lTotal = lTmp+lLand+lImpr;
         if (lTotal > 0)
         {
            iRet = sprintf(acTmp, "%d", lTotal);
            memcpy(pVal->TotalVal, acTmp, iRet);
         }

         // Net
         strTmp = asRows.GetAt(TUO_VALUE_NET);  
         iRet = dollar2Num(strTmp.GetBuffer(0), acTmp);
         if (iRet > 0)
         {
            lTmp = atol(acTmp);
            iRet = sprintf(acTmp, "%d", lTmp);
            memcpy(pVal->NetVal, acTmp, iRet);
         }

         pVal->CRLF[0] = '\n';
         pVal->CRLF[1] = 0;
         iRet = fputs((char *)&sOutbuf, fdOut);
         iRecCnt++;

         // Base Year value - create new record
         strTmp = asRows.GetAt(TUO_VALUE_BYVVAL);  
         if (strTmp.Left(15) == "BASE YEAR VALUE")
         {
            long lBYV=0;
            CString sDollar = strTmp.Mid(16);
            iRet = dollar2Num(sDollar.GetBuffer(0), acTmp);
            if (iRet > 0)
            {
               lBYV = atol(acTmp);
               // Create Prop8 record
               if (lBYV > lTotal)
               {
                  pVal->Reason[0] = ' ';
                  pVal->ValueSetType[0] = '8';
                  memset(pVal->Exe, ' ', sizeof(pVal->Exe));
                  iRet = fputs((char *)&sOutbuf, fdOut);
                  iRecCnt++;
               }
            }

            if (lBYV > 0)
            {
               memset(&pVal->ValueSetType, ' ', sizeof(VALUE_REC) - PV_SIZ_APN);
               lTmp = atol(acTmp);
               iRet = sprintf(acTmp, "%d", lTmp);
               memcpy(pVal->TotalVal, acTmp, iRet);
               pVal->ValueSetType[0] = '2';
               pVal->CRLF[0] = '\n';
               pVal->CRLF[1] = 0;
               iRet = fputs((char *)&sOutbuf, fdOut);
               iRecCnt++;
            }
         } else
         {
            iRet = fputs((char *)&sOutbuf, fdOut);
            iRecCnt++;
         }
      }

      lCnt++;
      if (!(lCnt % 1000))
         printf("\r%d", lCnt);
   }

   fclose(fdOut);

   // Sort output file
   if (iRecCnt > 0)
   {
      // Sort on APN and ValueSetType
      iRet = sortFile(acOutfile, pOutfile, "S(1,12,C,A, 21,1,C,A) DUPO");
      if (iRet > 0)
      {
         LogMsg("Total record output: %d", iRet);
         // Remove temp file
         if (iRet == iRecCnt)
         {
            LogMsg("Remove temp file: %s", acOutfile);
            DeleteFile(acOutfile);
         }
      } else
         iRet = -3;
   }

   LogMsgD("\nTotal processed records  : %u", lCnt);
   LogMsgD("        skipped records  : %u", iSkipRows);
   LogMsgD("         output records  : %u\n", iRecCnt);

   return lCnt;
}

/******************************* Tuo_MatchRoll ******************************
 *
 * Match GrGr file against R01 and populate APNMatch and Owner match field.
 *
 ****************************************************************************/

int Tuo_MatchRoll(LPCSTR pGrGrFile)
{
   char     acBuf[2048], acRoll[2048], acTmpFile[_MAX_PATH], acTmp[256];
   char     *pTmp;
   FILE     *fdIn, *fdOut;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];
   int      iRet, iTmp, iNoApn, iApnMatch, iApnUnmatch, iOwnerMatch;
   HANDLE   hRoll;
   DWORD    nBytesRead;
   bool     bEof;

   // Open input file
   if (!(fdIn = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error opening %s", pGrGrFile);
      return -1;
   }

   // Create output temp file
   strcpy(acTmpFile, pGrGrFile);
   pTmp = strrchr(acTmpFile, '.');
   strcpy(pTmp, ".tmp");
   if (!(fdOut = fopen(acTmpFile, "w")))
   {
      LogMsg("***** Error creating %s", acTmpFile);
      fclose(fdIn);
      return -2;
   }

   // Open roll file
   sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (_access(acTmp, 0))
   {
      sprintf(acTmp, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acTmp, 0))
      {
         LogMsg("***** Missing input file %s.  Please check!", acTmp);
         fclose(fdIn);
         fclose(fdOut);
         return -3;
      }
   }

   LogMsg("Open input file %s", acTmp);
   hRoll = CreateFile(acTmp, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (hRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening %s in Mon_MatchRoll().", acTmp);
      fclose(fdIn);
      fclose(fdOut);
      return -3;
   }

   // Skip first record
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
   iRet = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);

   // Initialize counters
   iNoApn=iApnMatch=iApnUnmatch=iOwnerMatch = 0;
   bEof = false;

   // Loop through input file
   while (!bEof)
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
      {
         iNoApn++;
         fputs(acBuf, fdOut);    // No APN, output anyway for data entry
         continue;
      }
#ifdef _DEBUG
      //if (!memcmp(pGrGr->APN, "0104015000000", 13))
      //   iRet = 0;
#endif
      // Match APN
      do
      {
         // Compare
         iRet = memcmp(pGrGr->APN, acRoll, iApnLen);
         if (!iRet)
         {
            pGrGr->APN_Matched = 'Y';
            iApnMatch++;

            // Match owner - match last name or the first 10 bytes of grantors
            int   iIdx, iLen;
            char  acOwner[64];

            memcpy(acOwner, (char *)&acRoll[OFF_NAME1], SIZ_NAME1);
            acOwner[SIZ_NAME1] = 0;
            if (pTmp = strchr(acOwner, ' '))
            {
               iLen = pTmp - (char *)&acOwner[0];
               if (iLen < 3) iLen = 10;
            }

            for (iIdx = 0; iIdx < atoin(pGrGr->NameCnt, SIZ_GR_NAMECNT); iIdx++)
            {
               // Compare grantors only
               if (!memcmp((char *)&pGrGr->Grantors[iIdx].Name[0], acOwner, iLen))
               {
                  pGrGr->Owner_Matched = 'Y';
                  iOwnerMatch++;
                  break;
               }
            }

            // Populate with roll data - to be done when needed

            break;
         } else
         { 
            if (iRet == 1)
            {
               // Read R01 record
               iTmp = ReadFile(hRoll, acRoll, iRecLen, &nBytesRead, NULL);
               // Check for EOF
               if (!iTmp)
               {
                  LogMsg("***** Error reading roll file (%f)", GetLastError());
                  bEof = true;
                  break;
               }

               // EOF ?
               if (!nBytesRead)
               {
                  bEof = true;
                  break;
               }
            }
         }
      } while (iRet > 0);
      
      // Output record
      fputs(acBuf, fdOut);
   }

   // Check for leftover
   while (!feof(fdIn))
   {
      // Read input record
      if (!(pTmp = fgets(acBuf, 2048, fdIn)))
         break;

      // If no APN, skip
      if (pGrGr->APN[0] == ' ')
         iNoApn++;

      pGrGr->APN_Matched = 'N';
      pGrGr->Owner_Matched = 'N';

      // Output record
      fputs(acBuf, fdOut);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (hRoll)
      CloseHandle(hRoll);

   // If everything OK, rename output file to original file
   if (remove(pGrGrFile))
      LogMsg("***** Error removing temp file: %s (%d)", pGrGrFile, errno);
   else if (rename(acTmpFile, pGrGrFile))
      LogMsg("***** Error renaming temp file: %s --> %s (%d)", acTmpFile, pGrGrFile, errno);

   LogMsg("                     APN matched: %d", iApnMatch);
   LogMsg("                   Owner matched: %d", iOwnerMatch);
   LogMsg("                         No APN : %d", iNoApn);

   return 0;
}

/**************************** Tuo_ExtrSaleMatched ***************************
 *
 * Extract data to GrGr_Exp.dat or GrGr_Exp.sls.  This output is to be merged
 * into R01 file.  Extract only records with ApnMatch=Y. with known DocType, 
 * and has sale price >= 5000.
 *
 * Input: pMode can be "w" or "a+" (overwrite or append)
 * Output: GrGr_Exp.dat or GrGr_Exp.sls in SALE_REC1 format
 *
 * Return 0 if successful, otherwise error
 *
 ****************************************************************************/

int Tuo_ExtrSaleMatched(char *pGrGrFile, char *pExSaleFile, char *pMode, bool bOwner=false)
{
   char     *pTmp, acBuf[2048];
   long     lCnt=0, lPrice, iTmp;

   CString   sTmp, sApn, sType;
   FILE     *fdGrGr;
   SALE_REC1 SaleRec;
   GRGR_DEF *pGrGr = (GRGR_DEF *)&acBuf[0];

   LogMsg("Extract matched grgr from %s to %s", pGrGrFile, pExSaleFile);

   // Open output file
   if (!(fdSale = fopen(pExSaleFile, pMode)))
   {
      LogMsg("***** Error creating %s file", pExSaleFile);
      return -1;
   }

   // Open input file
   if (!(fdGrGr = fopen(pGrGrFile, "r")))
   {
      LogMsg("***** Error creating %s file", pGrGrFile);
      return -2;
   }

   // Loop through
   while (!feof(fdGrGr))
   {
      // Get input rec
      pTmp = fgets(acBuf, 2048, fdGrGr);
      if (!pTmp)
         break;         // EOF

      // Drop all record without APN matched
      if (pGrGr->APN_Matched != 'Y')
         continue;

      // Drop records without sale price
      lPrice = atoin(pGrGr->SalePrice, SIZ_GR_SALE);
      if (lPrice < 5000)
         continue;

      memset((void *)&SaleRec, 32, sizeof(SALE_REC1));

      // DocType
      iTmp = atoin(pGrGr->DocTitle, 2);
      if (iTmp == 21)
         memcpy(SaleRec.acDocType, "13  ", 4);     // Deed
      else if (iTmp == 91)
         memcpy(SaleRec.acDocType, "27  ", 4);     // Trustee's deed
      else if (iTmp == 14)
         memcpy(SaleRec.acDocType, "32  ", 4);     // Bill of Sale
      else if (iTmp == 86)
         memcpy(SaleRec.acDocType, "67  ", 4);     // Tax Deed
      else if (iTmp == 97)
         memcpy(SaleRec.acDocType, "8   ", 4);     // Agreement of Sale
      else if (iTmp == 22)
         memcpy(SaleRec.acDocType, "40  ", 4);     // Deed Easement
      else if (iTmp == 23)
         memcpy(SaleRec.acDocType, "13  ", 4);     // Deed In Lieu - Short sale
      else
      {
         LogMsg("** Unknown doc type %d for parcel %.10s - ignore**", iTmp, pGrGr->APN);
         continue;
      }

      memcpy(SaleRec.acApn, pGrGr->APN, SALE_SIZ_APN);
      memcpy(SaleRec.acDocDate, pGrGr->DocDate, SALE_SIZ_DOCDATE);
      memcpy(SaleRec.acDocNum, pGrGr->DocNum, SALE_SIZ_DOCNUM);
      memcpy(SaleRec.acStampAmt, pGrGr->Tax, SIZ_GR_TAX);
      memcpy(SaleRec.acSalePrice, pGrGr->SalePrice, SIZ_GR_SALE);
      
      // Get grantors grantees
      memcpy(SaleRec.acSeller, pGrGr->Grantors[0].Name, SALE_SIZ_SELLER);

      // Copy Owner name
      if (bOwner)
      {
         memcpy(SaleRec.acName1, pGrGr->Grantees[0].Name, SALE_SIZ_NAME);
         memcpy(SaleRec.acName2, pGrGr->Grantees[1].Name, SALE_SIZ_NAME);
      }

      SaleRec.CRLF[0] = '\n';
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec, fdSale);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdGrGr);
   fclose(fdSale);

   LogMsg("Total number of output matched records: %ld", lCnt);
   return 0;
}

/****************************** Tuo_LoadGrGrCsv *****************************
 *
 * Remove two digit year from Doc# - spn 05/23/2007
 *
 *
 ****************************************************************************/

int Tuo_LoadGrGrCsv(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn;
   char     acBuf[2048], acTmp[256], *pRec;
   int      iRet, iCnt=0, iRecCnt=0, iRecSkip=0;
   GRGR_DEF myGrGrRec;
   int		ReadGrGr=0;
   long		lTax, lPrice;
   bool     bTabIncl = false;

   LogMsg("Processing GrGr file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   // Skip header
   pRec = fgets(acBuf, 2048, fdIn);

   // Check for type of input file
   if (pRec = isTabIncluded(acBuf))
      bTabIncl = true;

   // Processing loop
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 2048, fdIn);
      if (!pRec)
         break;

      if (bTabIncl)
         iRet = ParseStringIQ(acBuf, 9, TUO_GR_COMMENTS+1, apTokens);
      else
         iRet = ParseStringNQ(acBuf, ',', TUO_GR_COMMENTS+1, apTokens);

      if (iRet < TUO_GR_COMMENTS)
      {
         iRecSkip++;
         continue;
      }
      memset((void *)&myGrGrRec, ' ', sizeof(GRGR_DEF));

      strcpy(acTmp, apTokens[TUO_GR_APN]);
      strcat(acTmp, "0000000000");
      acTmp[iApnLen] = 0;
      memcpy(myGrGrRec.APN, acTmp, iApnLen);
      
#ifdef _DEBUG
      //if (!memcmp(acTmp, "08529001", 8))
      //   iRet = 0;
#endif
      if (memcmp(acTmp, "0000000000", iApnLen))
      {
         memcpy(myGrGrRec.SourceTable, apTokens[TUO_GR_PRDOC], strlen(apTokens[TUO_GR_PRDOC]));

         memcpy(myGrGrRec.Grantors[0].Name, apTokens[TUO_GR_GRANTOR], strlen(apTokens[TUO_GR_GRANTOR]));
         //memcpy(myGrGrRec.Grantors[0].NameType, "O", 1);
         memcpy(myGrGrRec.Grantees[0].Name, apTokens[TUO_GR_GRANTEE], strlen(apTokens[TUO_GR_GRANTEE]));
         //memcpy(myGrGrRec.Grantees[0].NameType, "O", 1);
         myGrGrRec.NameCnt[0] = '1';

         memcpy(myGrGrRec.DocTitle, apTokens[TUO_GR_DOCTYPE], strlen(apTokens[TUO_GR_DOCTYPE]));

         if (strlen(apTokens[TUO_GR_DOCDATE]) > 8)
         {
            strcpy(acTmp, apTokens[TUO_GR_DOCDATE]);
            remChar(acTmp, '-');		
            memcpy(myGrGrRec.DocDate, acTmp, 8);
         } 

         // skip 4 digit year in Doc#
         if (strlen(apTokens[TUO_GR_DOCNUM]) > 0)
         {
            strcpy(acTmp, apTokens[TUO_GR_DOCNUM]);
            iRet = atol(&acTmp[4]);
            sprintf(acTmp, "%.6d", iRet);
            memcpy(myGrGrRec.DocNum, acTmp, 6);
         }

         // AW_DTT -> Tax
         double dTax;
         strcpy(acTmp, apTokens[TUO_GR_DOCTAX]);
         dTax = atof(acTmp);
         lTax = (long)(dTax*100.0);
         lPrice = (long)(dTax*SALE_FACTOR);
         if (lPrice > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GR_TAX, lTax);
            memcpy(myGrGrRec.Tax, acTmp, SIZ_GR_TAX);

            sprintf(acTmp, "%*d", SIZ_GR_SALE, lPrice);
            memcpy(myGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);
         }

         myGrGrRec.CRLF[0] = '\n';
         myGrGrRec.CRLF[1] = '\0';
         iRet = fputs((char *)&myGrGrRec, fdOut);
         iRecCnt++;
      } else
         iRecSkip++;

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }


   if (fdIn) fclose(fdIn);

   LogMsgD("\nNumber of GrGr record in:    %d", iCnt);
   LogMsg("Number of GrGr record out:   %d", iRecCnt);
   LogMsg("Number of GrGr record Skip:  %d\n", iRecSkip);

   printf("Number of GrGr record in:    %d\n", iCnt);
   printf("Number of GrGr record out:   %d\n", iRecCnt);
   printf("Number of GrGr record Skip:  %d\n\n", iRecSkip);

   return iRecCnt;
}

/********************************* Tuo_LoadGrGr *****************************
 *
 * After processed, the zip file is copied to storage folder if defined and then 
 * moved to backup folder.
 *
 * 1. Process GRGR file
 * 2. Match against roll file, set APN_Match & Owner_Match flags
 * 3. Append to cummulative file Tuo_GrGr.sls
 * 4. Resort and extract to GrGr_Exp.dat for import. The output is used to merge
 *    into R01 via MergeGrGrFile().
 *
 * If successful, return 0.  Otherwise error.
 *
 * Output GrGr_Exp.dat and TUO_GrGr.sls
 *
 ****************************************************************************/

int Tuo_LoadGrGr(LPCSTR pCnty) 
{
   char     *pTmp;
   char     acTmp[256];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH], acGrGrCopy[_MAX_PATH];
   char     acCsvFile[64], acGrGrFile[_MAX_PATH], acZipFile[_MAX_PATH], acUnzipFolder[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle;

   // GrGr file we want to process in zip file
   GetIniString(pCnty, "GrGrCsv", "TCROR.HFW", acCsvFile, _MAX_PATH, acIniFile);

   // Get backup folder
   GetIniString("System", "GrGrBak", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acGrGrBak, acTmp, pCnty, acToday);

   // Get copy to folder
   GetIniString(pCnty, "GrGrCopy", "", acGrGrCopy, _MAX_PATH, acIniFile);

   // Form GrGr input path
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   if (strstr(acGrGrIn, "$date$"))
      replStr(acGrGrIn, "$date$", acToday);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;

      // Create backup folder if not exist
      if (_access(acGrGrBak, 0))
         _mkdir(acGrGrBak);
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return 0;
   }

   // Create Output file - Tuo_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   // Initialize Zip server
   doZipInit();

   // Set unzip folder
   GetIniString(myCounty.acCntyCode, "UnzipFolder", "", acUnzipFolder, _MAX_PATH, acIniFile);
   if (!acUnzipFolder[0])
      sprintf(acUnzipFolder, "%s\\%s", acTmpPath, pCnty);
   setUnzipToFolder(acUnzipFolder);

   // Set to replace file if exist
   setReplaceIfExist(true);

   iCnt = lCnt = 0;
   while (!iRet)
   {
      //sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);
      sprintf(acZipFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Unzip input file
      LogMsg("Unzip input file %s", acZipFile);
      iRet = startUnzip(acZipFile, acCsvFile);
      if (!iRet)
      {
         // Rename the unzipped file - getting file date from zip file name
         strcpy(acTmp, sFileInfo.name);
         if (pTmp = strrchr(acTmp, '.'))
            *pTmp = 0;
         sprintf(acGrGrFile, "%s\\%.5s%s.HFW", acUnzipFolder, acCsvFile, acTmp);
         sprintf(acTmp, "%s\\%s", acUnzipFolder, acCsvFile);

         // Update GrGr file date
         iRet = getFileDate(acTmp);
         if (iRet > lLastGrGrDate)
            lLastGrGrDate = iRet;

         if (!_access(acGrGrFile, 0))
            remove(acGrGrFile);
         rename(acTmp, acGrGrFile);

         // Load GrGr file
         iRet = Tuo_LoadGrGrCsv(acGrGrFile, fdOut);
         if (iRet < 0)
            LogMsg("*** Skip %s", acGrGrFile);
         else
            lCnt += iRet;

         // Copy file to storage
         if (acGrGrCopy[0] > 'A')
         {
            BOOL bTmp;
            sprintf(acTmp, "%s\\%s", acGrGrCopy, sFileInfo.name);
            bTmp = CopyFile(acZipFile, acTmp, true);
         }

         // Move input file to backup folder
         sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
         rename(acZipFile, acTmp);

         iCnt++;
      } else
         LogMsg("*** Skip %s", acZipFile);

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   // Shut down Zip server
   doZipShutdown();

   LogMsg("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc, DocNum asc, Data source
      sprintf(acTmp,"S(17,13,C,A,37,8,C,A,1,10,C,A,1109,2,C,A) F(TXT) DUPO(B%d, 1,44) ", sizeof(GRGR_DEF)+64);
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");

      // Sort Tuo_GrGr.dat to Tuo_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Match with roll file
      iRet = Tuo_MatchRoll(acGrGrOut);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         char  acSlsFile[_MAX_PATH];

         // Append Tuo_GrGr.srt to Tuo_GrGr.sls for backup
         sprintf(acSlsFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
         iRet = appendTxtFile(acGrGrOut, acSlsFile);

         // Resort SLS file
         lCnt = sortFile(acSlsFile, acGrGrOut, acTmp);
         if (lCnt > 0)
         {
            remove(acSlsFile);
            rename(acGrGrOut, acSlsFile);
         }

         // Extract Tuo_GrGr.sls to GrGr_Exp.dat 
         sprintf(acTmp, acEGrGrTmpl, myCounty.acCntyCode, "dat");
         iRet = Tuo_ExtrSaleMatched(acSlsFile, acTmp, "w", false);    
      } else
         iRet = 1;
   } else
      iRet = 1;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

int Tuo_LoadGrGrNoZip(LPCSTR pCnty) 
{
   char     *pTmp;
   char     acTmp[256];
   char     acGrGrIn[_MAX_PATH], acGrGrOut[_MAX_PATH], acGrGrBak[_MAX_PATH];
   char     acCsvFile[_MAX_PATH], acToday[16];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle;

   // Get raw file name
   GetIniString("System", "GrGrBak", "", acTmp, _MAX_PATH, acIniFile);
   iRet = sprintf(acToday, "%d", lToday);
   if (iRet > 1)
   {
      sprintf(acGrGrBak, acTmp, pCnty, acToday);
   } else
   {
      strcpy(acGrGrBak, acGrGrIn);
      pTmp = strrchr(acGrGrBak, '\\');
      if (pTmp)
         *++pTmp = 0;
      sprintf(acTmp, "GrGr_%s", acToday);
      strcat(acGrGrBak, acTmp);
   }

   // Form GrGr input path
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);
   if (strstr(acGrGrIn, "$date$"))
      replStr(acGrGrIn, "$date$", acToday);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;

      // Create backup folder if not exist
      if (_access(acGrGrBak, 0))
         _mkdir(acGrGrBak);
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return 0;
   }

   // Create Output file - Tuo_GrGr.dat
   sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Dat");
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "w");
   if (fdOut == NULL)
   {
      printf("Error creating GrGr output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Parse input file
      iRet = Tuo_LoadGrGrCsv(acCsvFile, fdOut);
      if (iRet < 0)
         LogMsg("*** Skip %s", acCsvFile);
      else
         lCnt += iRet;

      // Move input file to backup folder
      sprintf(acTmp, "%s\\%s", acGrGrBak, sFileInfo.name);
      rename(acCsvFile, acTmp);

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc, DocNum asc, Data source
      sprintf(acTmp,"S(17,13,C,A,37,8,C,A,1,10,C,A,1109,2,C,A) F(TXT) DUPO(B%d, 1,44) ", sizeof(GRGR_DEF)+64);
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");

      // Sort Tuo_GrGr.dat to Tuo_GrGr.srt
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);

      // Match with roll file
      iRet = Tuo_MatchRoll(acGrGrOut);

      // Update cumulative sale file
      if (lCnt > 0)
      {
         // Append Tuo_GrGr.srt to Tuo_GrGr.sls for backup
         sprintf(acTmp, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
         iRet = appendTxtFile(acGrGrOut, acTmp);

          // Extract Tuo_GrGr.srt to Sale_Exp.dat and append to Tuo_sale_cum.dat
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, "DAT");
         iRet = Tuo_ExtrSaleMatched(acGrGrOut, acTmp, "w");    
      } else
         iRet = 1;
   } else
      iRet = 1;

   LogMsg("Total output records after dedup: %u", lCnt);
   printf("\nTotal output records after dedup: %u\n", lCnt);

   return iRet;
}

/********************************* Tuog_LoadCsv *****************************
 *
 *
 *
 ****************************************************************************/

int Tuog_LoadCsv(char *pInfile, FILE *fdOut)
{
   FILE     *fdIn;
   char     acBuf[2048], acTmp[256], *pRec;
   int      iRet, iCnt=0, iRecCnt=0, iRecSkip=0;
   TUOG_DEF myGrGrRec;
   bool     bTabIncl = false;

   LogMsg("Processing GrGr file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   // Skip header
   pRec = fgets(acBuf, 2048, fdIn);

   // Check for type of input file
   if (pRec = isTabIncluded(acBuf))
      bTabIncl = true;

   // Processing loop
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 2048, fdIn);
      if (!pRec)
         break;

      if (bTabIncl)
         iRet = ParseStringIQ(acBuf, 9, TUO_GR_COMMENTS+1, apTokens);
      else
         iRet = ParseStringNQ(acBuf, ',', TUO_GR_COMMENTS+1, apTokens);

      if (iRet < TUO_GR_COMMENTS)
      {
         iRecSkip++;
         continue;
      }

      strcpy(acTmp, apTokens[TUO_GR_APN]);
      //strcat(acTmp, "0000000000");
      //acTmp[iApnLen] = 0;
      
#ifdef _DEBUG
      //if (!memcmp(acTmp, "08529001", 8))
      //   iRet = 0;
#endif
      if (memcmp(acTmp, "0000000000", iApnLen))
      {
         memset((void *)&myGrGrRec, ' ', sizeof(TUOG_DEF));
         memcpy(myGrGrRec.APN, acTmp, strlen(acTmp));
         memcpy(myGrGrRec.PrStat, apTokens[TUO_GR_PRSTAT], strlen(apTokens[TUO_GR_PRSTAT]));
         memcpy(myGrGrRec.PrDoc,  apTokens[TUO_GR_PRDOC],  strlen(apTokens[TUO_GR_PRDOC]));
         memcpy(myGrGrRec.PrServ, apTokens[TUO_GR_PRSERV], strlen(apTokens[TUO_GR_PRSERV]));
         memcpy(myGrGrRec.PrType, apTokens[TUO_GR_PRTYPE], strlen(apTokens[TUO_GR_PRTYPE]));
         memcpy(myGrGrRec.PrQueue,apTokens[TUO_GR_PRQUEUE],strlen(apTokens[TUO_GR_PRQUEUE]));

         memcpy(myGrGrRec.DocNum, apTokens[TUO_GR_DOCNUM], strlen(apTokens[TUO_GR_DOCNUM]));
         memcpy(myGrGrRec.DocType,apTokens[TUO_GR_DOCTYPE],strlen(apTokens[TUO_GR_DOCTYPE]));
         if (strlen(apTokens[TUO_GR_DOCDATE]) > 8)
         {
            strcpy(acTmp, apTokens[TUO_GR_DOCDATE]);
            remChar(acTmp, '-');		
            memcpy(myGrGrRec.DocDate, acTmp, 8);
         } 

         memcpy(myGrGrRec.Grantors, apTokens[TUO_GR_GRANTOR], strlen(apTokens[TUO_GR_GRANTOR]));
         memcpy(myGrGrRec.Grantees, apTokens[TUO_GR_GRANTEE], strlen(apTokens[TUO_GR_GRANTEE]));

         memcpy(myGrGrRec.Book, apTokens[TUO_GR_BOOK], strlen(apTokens[TUO_GR_BOOK]));
         memcpy(myGrGrRec.Page, apTokens[TUO_GR_PAGE], strlen(apTokens[TUO_GR_PAGE]));

         myGrGrRec.CRLF[0] = '\n';
         myGrGrRec.CRLF[1] = '\0';
         iRet = fputs((char *)&myGrGrRec, fdOut);
         iRecCnt++;
      } else
         iRecSkip++;

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }


   if (fdIn) fclose(fdIn);

   LogMsgD("\nNumber of GrGr record in:    %d", iCnt);
   LogMsgD("Number of GrGr record out:   %d", iRecCnt);
   LogMsgD("Number of GrGr record Skip:  %d\n", iRecSkip);

   return iRecCnt;
}

/****************************************************************************
 *
 * Return 0 if data is available and processing OK
 *       -1 if error occurs
 *        1 if data not avail.
 *
 ****************************************************************************/

int LoadTuoG(LPCSTR pCnty) 
{
   char     *pTmp;
   char     acTmp[256];
   char     acGrGrIn[_MAX_PATH], acGrGrOutTmpl[_MAX_PATH], acGrGrOut[_MAX_PATH];
   char     acCsvFile[_MAX_PATH], acUnzipFolder[_MAX_PATH];
   struct   _finddata_t  sFileInfo;

   FILE     *fdOut;

   int      iCnt, iRet;
   long     lCnt, lHandle;

   // Form output file name
   GetIniString(myCounty.acCntyCode, "AsrGFile", "", acGrGrOutTmpl, _MAX_PATH, acIniFile);
   sprintf(acGrGrOut, acGrGrOutTmpl, "S01");

   // Form GrGr input path
   GetIniString(myCounty.acCntyCode, "UnzipFolder", "", acUnzipFolder, _MAX_PATH, acIniFile);
   if (!acUnzipFolder[0])
      sprintf(acUnzipFolder, "%s\\%s", acTmpPath, pCnty);
   sprintf(acGrGrIn, "%s\\*.HFW", acUnzipFolder);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);

      // If R01 file does not exist, copy S01 file to R01 file
      sprintf(acTmp, acGrGrOutTmpl, "R01");
      if (_access(acTmp, 0) && !_access(acGrGrOut, 0))
         CopyFile(acGrGrOut, acTmp, false);
      return -1;
   }

   // Create Output file 
   LogMsg("Open output file %s", acGrGrOut);
   fdOut = fopen(acGrGrOut, "a+");
   if (fdOut == NULL)
   {
      LogMsgD("Error oprning output file: %s\n", acGrGrOut);
      _findclose(lHandle);
      return -2;
   }

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Parse input file
      iRet = Tuog_LoadCsv(acCsvFile, fdOut);
      if (iRet < 0)
         LogMsg("*** Skip %s", acCsvFile);
      else
         lCnt += iRet;

      // Delete input file
      DeleteFile(acCsvFile);

      iCnt++;
      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);
   iRet = 0;

   // Close files
   if (fdOut)
      fclose(fdOut);

   LogMsg("Total processed records  : %u", lCnt);

   // Sort output
   if (lCnt > 0)
   {
      // Sort output file and dedup if same docdate and docnum
      // Sort on APN asc, RecDate asc, DocNum asc
      sprintf(acTmp,"S(%d,%d,C,A) F(TXT) DUPO(%d,%d,%d,8,%d,%d) ", OFF_TUOG_DOCNUM, SIZ_GR_DOCNUM,         
         OFF_TUOG_APN, SIZ_GR_APN, OFF_TUOG_DOCDATE, OFF_TUOG_DOCNUM, SIZ_GR_DOCNUM);
      strcpy(acGrGrIn, acGrGrOut);
      sprintf(acGrGrOut, acGrGrOutTmpl, "R01");

      // Sort 
      lCnt = sortFile(acGrGrIn, acGrGrOut, acTmp);
      if (!lCnt)
         iRet = 1;
   } else
      iRet = 1;

   LogMsgD("\nTotal output records after dedup: %u", lCnt);

   return iRet;
}

/******************************** Tuo_Fmt1RecDoc *****************************
 *
 *  1) 0477003219761118
 *  2) 1641007219991206
 *  3) 0501021920050519
 *
 *****************************************************************************/

void Tuo_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   char  acDate[16], acDoc[32], *pTmp, *pDate;
   bool  bMisAligned = false;


   acDate[8] = 0;
   memset(acDoc, ' ', SIZ_SALE1_DOC);

   pTmp = pRecDoc;
   pDate = pTmp+8;

   DoValidDate(pDate,8);
   if (!memcmp(pDate, "00000000", 8))
      memset(pDate,' ',8);

   memcpy(acDoc, pTmp, 8);
   if (*pRecDoc == ' ' || !memcmp(pRecDoc, "00000000", 8))
      memset(pOutDoc,' ',SIZ_SALE1_DOC);
   else
   {
      int iTmp, iYear;

      iTmp = atoin(pRecDoc, 2);
      iYear = atoin(pDate+2, 2);
      if (iTmp == iYear)
      {
         // Strip off two digit year
         iTmp = atoin(pRecDoc+2, 6);
         sprintf(acDoc, "%.06d            ", iTmp);
      } else
         memcpy(acDoc, pRecDoc, 8);
      memcpy(pOutDoc, acDoc, SIZ_SALE1_DOC);
   }

   // This check will take out lots of date
   // Just use as is for now - SPN
   //if (isValidYMD(pDate))
      memcpy(pOutDate, pDate, SIZ_SALE1_DT);
   //else
   //   memset(pOutDate, ' ', SIZ_SALE1_DT);
}

/********************************* Tuo_FmtRecDoc *****************************
 *
 *
 *****************************************************************************/

void Tuo_FmtRecDoc(char *pOutbuf, char *pRollRec)
{
   long     lPrice, lDate;
   char     acTmp[32], acDate[16], acDoc[64], *pTmp;
   bool     bMisAligned = false;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   // Clear old data
   memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
   memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
   memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
   memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
   memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
   memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
   memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
   memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);

   pTmp = pRec->RecBook1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00101138", 8))
   //   iTmp = 0;
#endif

   memcpy(acTmp, pTmp, 16);
   acTmp[16] = 0;

   Tuo_Fmt1Doc(acDoc, acDate, acTmp);

   if (acDate[0] > ' ' || acDoc[0] >  ' ')
   {
      memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);
      *(pOutbuf+OFF_AR_CODE1) = 'A';               // Assessor-Recorder code

      // Update last recording date
      lDate = atol(acDate);
      if (lDate > lLastRecDate && lDate < lToday)
         lLastRecDate = lDate;

      // Sale price is stored in StampAmt location
      lPrice = atoin(pRec->StampAmt, CRESIZ_STMP_AMT);
      if (lPrice > 0)
      {
         sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }

      // Advance to Rec #2
      pTmp += 16;
      memcpy(acTmp, pTmp, 16);
      acTmp[16] = 0;
      Tuo_Fmt1Doc(acDoc, acDate, acTmp);

      if (acDate[0] > ' ' || acDoc[0] >  ' ')
      {
         memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
         memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);
         *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code

         // Advance to Rec #3
         pTmp += 16;
         memcpy(acTmp, pTmp, 16);
         acTmp[16] = 0;
         Tuo_Fmt1Doc(acDoc, acDate, acTmp);

         if (acDate[0] > ' ' || acDoc[0] >  ' ')
         {
            memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
            memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
            *(pOutbuf+OFF_AR_CODE3) = 'A';        // Assessor-Recorder code
         }
      }
   }
}

/******************************** Tuo_UpdRecInfo *****************************
 *
 * 12/13/2007 - TUO is asking to keep all sales even if they are on the same date
 *
 *****************************************************************************/

void Tuo_UpdRecInfo(char *pOutbuf, char *pRollRec)
{
   int      iUpdFlg=0;
   double   dTmp;
   char     acTmp[32], acDate[16], acDoc[64], *pTmp;
   long		lPrice, lCurSaleDt, lLstSaleDt;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   pTmp = pRec->RecBook1;

   Tuo_Fmt1Doc(acDoc, acDate, pTmp);

   lCurSaleDt = atoin(acDate, SIZ_SALE1_DT);
   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // Skip wrong date
   if (lCurSaleDt >= lToday)
      return;

   if (lCurSaleDt > lLstSaleDt)
   {
      if (acDoc[0] > ' ')
      {
         memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE1_DT);
         memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE1_DOC);
         memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE1_AMT);

         memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);
         memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC);
         memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
               
         *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
         memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
         memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
         memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
         memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

         memcpy(acTmp, pRec->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
         } else
            memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);

         iUpdFlg = 1;
      }
   } else if (lCurSaleDt == lLstSaleDt && acDoc[0] > ' ')
   {  // This is considered verified sale
      memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);
      memcpy(acTmp, pRec->StampAmt, CRESIZ_STMP_AMT);
      acTmp[CRESIZ_STMP_AMT] = 0;
      dTmp = atof(acTmp);
      if (dTmp > 0.0)
      {
         lPrice = (long)(dTmp * SALE_FACTOR);
         if (lPrice < 100)
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         else
            sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }
   }

   if (iUpdFlg && *(pOutbuf+OFF_SALE2_DT) == ' ')
   {
      // Advance to Rec #2
      pTmp += 16;
      Tuo_Fmt1Doc(acDoc, acDate, pTmp);
      if (acDoc[0] > ' ' && acDate[0] > '0')
      {
         *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
         memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);
         memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);

         if (*(pOutbuf+OFF_SALE3_DT) == ' ')
         {
            // Advance to Rec #3
            pTmp += 16;
            Tuo_Fmt1Doc(acDoc, acDate, pTmp);
            memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
            if (acDoc[0] > ' ')
            {
               *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
               memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
            }
         }
      }
   }
}

/******************************** Cres_UpdXfer *******************************
 *
 * Use DOC1 to update transfer.
 *
 *****************************************************************************/

void Cres_UpdXfer(char *pOutbuf, char *pRollRec)
{
   char     acDate[16], acDoc[64];
   long		lCurSaleDt;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   Tuo_Fmt1Doc(acDoc, acDate, pRec->RecBook1);

   lCurSaleDt = atoin(acDate, SIZ_TRANSFER_DT);

   // Skip questionable date
   if (lCurSaleDt >= lToday || lCurSaleDt < 19000101)
      return;

   if (acDoc[0] > ' ')
   {
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);
   }
}

/******************************** Tuo_CleanName ******************************
 *
 * Remove all garbage not belong to owner name
 * Return 0 if nothing removed, update vesting if found
 *
 *****************************************************************************/

int Tuo_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;
   int   iCnt;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);
   blankRem((char *)&acTmp[0]);

   // Remove 1/2 or 25% 
   if ((pTmp=strchr(acTmp, '/')) || (pTmp=strchr(acTmp, '%')))
   {
      pTmp--;
      while ((pTmp > acTmp) && (isdigit(*pTmp) || *pTmp == '.'))
         *pTmp-- = 0;
   }
   
   // PALMA, SALVATORE T 27.5
   if (pTmp=strchr(acTmp, '.'))
   {
      if (isdigit(*(pTmp+1)))
      {
         while (pTmp > acTmp && *pTmp > ' ') pTmp--;

         if (pTmp > acTmp)
            *pTmp = 0;
      }
   }

   iCnt = 0;
   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Tuo_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Tuo_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iMrgFlg;
   char  acTmp[64], acName1[64], acName2[64], acVesting1[8], acVesting2[8], acOrgName[64];
   char  *pTmp, *pName1;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "03933009", 8))
   //   iTmp = 0;
#endif
   // Initialize
   iMrgFlg = 0;
   acVesting1[0] = 0;
   acVesting2[0] = 0;
   acName2[0] = 0;

   memcpy(acOrgName, pNames, CRESIZ_NAME_1);
   myTrim(acOrgName, CRESIZ_NAME_1);

   if (*(pNames+CRESIZ_NAME_1) == '&')
   {
      iMrgFlg  = Tuo_CleanName(pNames, acName1, acVesting1, CRESIZ_NAME_1);
      iMrgFlg |= Tuo_CleanName(pNames+CRESIZ_NAME_1+2, acName2, acVesting2, CRESIZ_NAME_2-2);

      if (!iMrgFlg)
         iMrgFlg = MergeName2(acName1, acName2, acTmp, ',');
      if (iMrgFlg == 1)
      {
         // Name merged OK - drop name2
         strcpy(acName1, acTmp);
         acName2[0] = 0;
      } else if (iMrgFlg == 2)
      {
         // Same name - drop name2
         acName2[0] = 0;
      } else if (iMrgFlg == 99 || !iMrgFlg)
      {
         if (!strcmp(acName1, acName2))
            acName2[0] = 0;
      } else
      {
         // Keep them as is
         memcpy(acName1, pNames, CRESIZ_NAME_1);
         memcpy(acName2+CRESIZ_NAME_1+2, pNames, CRESIZ_NAME_2-2);
         acName1[CRESIZ_NAME_1] = 0;
         acName2[CRESIZ_NAME_2-2] = 0;
      }
   } else
   {
      iMrgFlg  = Tuo_CleanName(pNames, acName1, acVesting1, CRESIZ_NAME_1);
      if (iMrgFlg == 99)
      {
         // If Name1 has only one word, keep original
         if (!strchr(acName1, ' '))
         {
            memcpy(acName1, acOrgName, CRESIZ_NAME_1);
            acName1[CRESIZ_NAME_1] = 0;
         }
      }

      if (*(pNames+CRESIZ_NAME_1) > ' ')
         iMrgFlg |= Tuo_CleanName(pNames+CRESIZ_NAME_1, acName2, acVesting2, CRESIZ_NAME_2);
   }

   myTrim(acName2);
   pName1 = myTrim(acName1);

   // Special case
   if (*pName1 == '(')
      pName1++;

   // Remove Name2 if it's the same as Name1
   if (!strcmp(pName1, acName2))
      acName2[0] = 0;

   // Check owner2 for # - drop them
   if (acName2[0] == '%')
   {
      updateCareOf(pOutbuf, acName2, strlen(acName2));
      acName2[0] = 0;
   } else if (!memcmp(acName2, "C/O", 3))
   {
      updateCareOf(pOutbuf, acName2, strlen(acName2));
      acName2[0] = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, acName2, strlen(acName2));
      acName2[0] = 0;
   }  
      
   pTmp = pName1;

   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == ',' || *pTmp == '`' || *pTmp == '(' || *pTmp == ')')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 3);
   if (acVesting1[0] > ' ' && isVestChk(acVesting1))
   {
      vmemcpy(myOwner.acSwapName, acOrgName, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);

   if (iMrgFlg == 1)
      vmemcpy(pOutbuf+OFF_NAME1, myOwner.acName1, SIZ_NAME1);
   else
   {
      if (acOrgName[0] == '(')
      {
         remChar(acOrgName, ')');
         strcpy(acTmp, &acOrgName[1]);
      } else
         strcpy(acTmp, acOrgName);
      replChar(acTmp, ',', ' ');

      // Remove 1/2 or 25% or 30.25
      if ((pTmp=strchr(acTmp, '/')) || (pTmp=strchr(acTmp, '%')) || (pTmp=strchr(acTmp, '.')))
      {
         pTmp--;
         while ((pTmp > acTmp) && (isdigit(*pTmp) || *pTmp == '.'))
            *pTmp-- = 0;
      }

      blankRem(acTmp);
      vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1);
   }
   if (acName2[0] > ' ')
   {
      replChar(acName2, ',', ' ');
      iTmp = blankRem(acName2, SIZ_NAME2);
      vmemcpy(pOutbuf+OFF_NAME2, acName2, SIZ_NAME2);
      if (bDebug && iTmp < 5)
         LogMsg("***** Bad Name2 at %.10s: %s", pOutbuf, acName2);
   }

   // Check vesting
   if (acVesting1[0] > '0')
   {
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));
      // Check EtAl
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }
}

/********************************* Tuo_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Tuo_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[64], acAddr2[64], acChar;
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pRec->M_Addr1, "     ", 5))
      return;

   memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
   blankRem(acAddr1, CRESIZ_ADDR_1);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ') 
            pAddr1++;
         /*
         iTmp = strlen(pAddr1);
         if (iTmp > SIZ_CARE_OF)
            iTmp = SIZ_CARE_OF;
         memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
         */
         updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00120107", 8))
   //   iTmp = 0;
#endif

   // Start processing
   memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
   iTmp = atoin(pRec->M_Zip, 5);
   if (iTmp > 400)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   } else
   {
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
   }

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1(&sMailAdr, pAddr1);

   memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
   acAddr2[CRESIZ_ADDR_2] = 0;
   parseAdr2(&sMailAdr, myTrim(acAddr2));

   if (sMailAdr.lStrNum > 0)
   {
      iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      } else
      {
         acChar = isChar(sMailAdr.strNum, SIZ_M_STRNUM);
         if (acChar > ' ')
         {
            if (pTmp = strchr(sMailAdr.strNum, acChar))
               memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, strlen(pTmp));
         }
      }
   }
   blankPad(sMailAdr.strDir, SIZ_M_DIR);
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   blankPad(sMailAdr.strName, SIZ_M_STREET);
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
   blankPad(sMailAdr.City, SIZ_M_CITY);
   memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   blankPad(sMailAdr.State, SIZ_M_ST);
   memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

   // Situs
   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      ADR_REC sSitusAdr;
      int   iIdx=0;

      // Clear old Situs
      removeSitus(pOutbuf);

      // Remove leading space
      iTmp = 0;
      while (pRec->S_StrNo[iTmp] == ' ')
         iTmp++;
      while (iTmp < CRESIZ_SITUS_STREETNO)
         acTmp[iIdx++] = pRec->S_StrNo[iTmp++];
      acTmp[iIdx] = 0;

      // Save original StrNum that may includes hyphen
      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, iIdx);

      // Prepare display field
      iTmp = sprintf(acAddr1, "%s %s", myTrim(acTmp), myTrim(pRec->S_StrName, CRESIZ_SITUS_STREETNAME));
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // Prevent situation likes 123 1/2 NORTH STREET.  We have to break
      // into strnum=123, strsub=1/2
      pTmp = strchr(acTmp, ' ');
      if (pTmp)
      {
         *pTmp++ = 0;
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, strlen(pTmp));
      }

      // Remove '-' in strNum
      if (pTmp = strchr(acTmp, '-'))
      {
         *pTmp = 0;
         sprintf(acAddr2, "%s%s    ", acTmp, pTmp+1);
         acAddr2[SIZ_S_STRNUM] = 0;
         *pTmp = '-';               // Put '-' back
         memcpy(pOutbuf+OFF_S_STRNUM, acAddr2, SIZ_S_STRNUM);
      } else
      {
         sprintf(acTmp, "%d       ", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      }

      // Copy street name
      //memcpy(acTmp, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
      //acTmp[CRESIZ_SITUS_STREETNAME] = 0;
      //parseAdr1S(&sSitusAdr, myTrim(acTmp));
      strcpy(acTmp, pRec->S_StrName);
      parseAdr1S(&sSitusAdr, acTmp);

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

      // Assuming that no situs city and we have to use mail city
      acAddr2[0] = 0;
      if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         City2Code(sMailAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            memcpy(pOutbuf+OFF_S_ST, "CA", 2);
            strcpy(acAddr2, sMailAdr.City);
         }
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      } else
      {
         // Condition: Valid city name, Ex_Val is 7000,
         // Use_Code is N?,B1,A1,A2,C1,T1,R1,S1
         long lVal;
         lVal = atoin(pRec->ExVal, CRESIZ_EX_VAL);

         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] == '9') && (sMailAdr.lStrNum > 0) &&
             (iStrNo == sMailAdr.lStrNum) && (*(pOutbuf+OFF_HO_FL) == '1') )
         {
            // Get city code
            City2Code(sMailAdr.City, acTmp);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ST, "CA", 2);
               memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
               strcpy(acAddr2, sMailAdr.City);
            }
         } 
      }

      if (acAddr2[0])
      {
         strcat(myTrim(acAddr2), " CA");
         iTmp = strlen(acAddr2);
         if (iTmp > SIZ_S_CTY_ST_D)
            iTmp = SIZ_S_CTY_ST_D;
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************** Tuo_MergeChar *****************************
 *
 * Merge char data from sale record into roll file
 *
 ******************************************************************************/

int Tuo_MergeChar(char *pOutbuf, char *pSalesRec)
{
   int      iTmp;
   char     acTmp[32];
   int      iFp;

   TUO_SALE *pSales = (TUO_SALE *)pSalesRec;

#ifdef _DEBUG
//   if (!memcmp(pOutbuf, "0250600600", 10))
//      iTmp = 0;
#endif
   // Rooms
   iTmp = atoin(pSales->TotalRooms, SSIZ_TOTALROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pSales->BedRooms[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths
   if (pSales->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pSales->Baths[0];
   }
   if (pSales->Baths[2] == '7' )
   {
      iTmp = atoin(pOutbuf+OFF_BATH_F, 2);
      iTmp++;
      sprintf(acTmp, "%*u", SIZ_BATH_F, iTmp);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, SIZ_BATH_F);
   } else if (pSales->Baths[2] <= '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Garage
   if (pSales->Garage > '0' && pSales->Garage <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pSales->Garage;

   acTmp[0] = 0;
   iTmp = 0;
   if (pSales->BuildingClass[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pSales->BuildingClass[0];
      if (isdigit(pSales->BuildingClass[1]))
      {
         acTmp[iTmp++] = pSales->BuildingClass[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pSales->BuildingClass[2]))
            acTmp[iTmp++] = pSales->BuildingClass[2];
         else if (isdigit(pSales->BuildingClass[3]))
            acTmp[iTmp++] = pSales->BuildingClass[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pSales->BuildingClass[0]))
   {
      acTmp[iTmp++] = pSales->BuildingClass[0];
      acTmp[iTmp++] = '.';
      if (isdigit(pSales->BuildingClass[1]))
         acTmp[iTmp++] = pSales->BuildingClass[1];
      else if (isdigit(pSales->BuildingClass[2]))
         acTmp[iTmp++] = pSales->BuildingClass[2];
      else
         acTmp[iTmp++] = '0';
   }


   iFp = pSales->FirePlace & 0x0F;
   if (iFp > 0)
   {
      sprintf(acTmp, "%*d", SSIZ_FIREPLACE, iFp);
      memcpy(pOutbuf+OFF_FIRE_PL, acTmp, SSIZ_FIREPLACE);
   } else
      memcpy(pOutbuf+OFF_FIRE_PL, BLANK32, SSIZ_FIREPLACE);

   // AC - 
   if (pSales->Cooling > ' ')
      LogMsg("SAir: %c", pSales->Cooling);

   // Heating - 
   if (pSales->Heating > ' ')
      LogMsg("SHeat: %c", pSales->Heating);

   // Pool 
   if (pSales->Pool > ' ')
      *(pOutbuf+OFF_POOL) = pSales->Pool;
   return 0;
}

/********************************** Tuo_MergeSale *****************************
 *
 * Return value > 0 if sale rec is older than current
 *
 ******************************************************************************

int Tuo_MergeSale(char *pOutbuf, char *pSaleRec, int iCnt)
{
   TUO_SALE *pRec;
   long     lDttAmt, lRecDate, lTmp;
   int      iTmp;
   char     *pDate, *pAmt, *pDoc, *pSaleDate, acDocNum[32], acAmt[32];
   char     *pNxtDate, *pNxtAmt, *pNxtDoc;
   double   dTax;
   pRec = (TUO_SALE *)pSaleRec;

   
#ifdef _DEBUG
   if (!memcmp(pOutbuf, "0325203500", 10))
      iTmp = 0;
#endif
   
//   pSaleDate = pRec->Rec_Date;
   pSaleDate = pRec->ProcessDate;

   // Check for date
   lRecDate = atoin(pSaleDate, SSIZ_REC_DATE);
   if (!lRecDate)
   {
      pSaleDate = pRec->Rec_Date;
      if (!lRecDate)
         return 1;
   }

   switch (iCnt)
   {
      case 1:
         pAmt    = pOutbuf+OFF_SALE1_AMT;
         pDate   = pOutbuf+OFF_SALE1_DT;
         pDoc    = pOutbuf+OFF_SALE1_DOC;
         pNxtAmt = pOutbuf+OFF_SALE2_AMT;
         pNxtDate= pOutbuf+OFF_SALE2_DT;
         pNxtDoc = pOutbuf+OFF_SALE2_DOC;
         break;
      case 2:
         pAmt    = pOutbuf+OFF_SALE2_AMT;
         pDate   = pOutbuf+OFF_SALE2_DT;
         pDoc    = pOutbuf+OFF_SALE2_DOC;
         pNxtAmt = pOutbuf+OFF_SALE3_AMT;
         pNxtDate= pOutbuf+OFF_SALE3_DT;
         pNxtDoc = pOutbuf+OFF_SALE3_DOC;
         break;
      case 3:
         pAmt    = pOutbuf+OFF_SALE3_AMT;
         pDate   = pOutbuf+OFF_SALE3_DT;
         pDoc    = pOutbuf+OFF_SALE3_DOC;
         pNxtAmt = NULL;
         pNxtDate= NULL;
         pNxtDoc = NULL;
         break;
      default:
         return 1;
   }

   dTax = atofn(pRec->DocTax, SSIZ_STAMPAMT);
   if (dTax > 1.0)
   {
      lDttAmt = (long)(dTax*SALE_FACTOR);
   } else
   {
      lDttAmt = atoin(pRec->SalePrice, SSIZ_SALEPRICE);
   }
   if (lDttAmt > 0)
      sprintf(acAmt, "%*u", SIZ_SALE1_AMT, lDttAmt);
   else
      memset(acAmt, ' ', SIZ_SALE1_AMT);
   sprintf(acDocNum, "%.*s      ", SSIZ_BOOKPAGE, pRec->Rec_Doc);

   // If sale date is older than current sale
   if ((iTmp = memcmp(pDate, pSaleDate, 8)) > 0)
   {
      if (iCnt == 2 && lDttAmt > 0)
      {
         // Check next sale record
         pAmt  = pOutbuf+OFF_SALE3_AMT;
         pDate = pOutbuf+OFF_SALE3_DT;
         pDoc  = pOutbuf+OFF_SALE3_DOC;
         iTmp = memcmp(pDate, pSaleDate, 8);
      }

      // Ignore if it doesn't match sale3.
      // If it is the same date as Sale3, update sale price and docnum
      if (iTmp)
         return iTmp;   // sale rec is older than current
   }
   
   // If same date, update sale amt and Doc#
   if (!iTmp)
   {
      lTmp = atoin(pAmt, SIZ_SALE1_AMT);
      if (!lTmp && lDttAmt > 0)
      {
         memcpy(pAmt, acAmt, SIZ_SALE1_AMT);
         memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);
      } else if (*pDoc == ' ' && acDocNum[0] > ' ')
         memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);
   } else
   {
      // New sale - move current sale to next
      if (pNxtDate && *pDate > '0')
      {
         memcpy(pNxtAmt, pAmt, SIZ_SALE1_AMT);
         *(pNxtAmt+SIZ_SALE1_AMT) = *(pAmt+SIZ_SALE1_AMT);  // Sale code
         memcpy(pNxtDoc, pDoc, SIZ_SALE1_DOC);
         memcpy(pNxtDate, pDate, SIZ_SALE1_DT);
      }

      memcpy(pDate, pSaleDate, 8);
      memcpy(pAmt, acAmt, SIZ_SALE1_AMT);
      memcpy(pDoc, acDocNum, SIZ_SALE1_DOC);

      // Update transfer on first sale
      if (1 == iCnt)
      {
         memcpy(pOutbuf+OFF_TRANSFER_DT, pSaleDate, 8);
         if (acDocNum[0] > ' ')
            memcpy(pOutbuf+OFF_TRANSFER_DOC, acDocNum, SIZ_SALE1_DOC);
         else
            memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);

         // Update seller
         memcpy(pOutbuf+OFF_SELLER, pRec->Seller, SIZ_SELLER);
      }
   }
   return 0;
}
*/
/******************************** Tuo_UpdateSale ******************************
 *
 *
 ******************************************************************************

int Tuo_UpdateSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop, iSaleCnt=0;
   TUO_SALE *pSale;

   // Get first Char rec for first call
   if (!pRec)
   {
      iRet = fread(acRec, 1, iSaleLen, fdSale);
      if (iRet == iSaleLen)
      {
         pRec = acRec;

         // Replace all characters 31 or less with '-'
         replCharEx(acRec, 31, '-', iSaleLen);
      }
   } else
      iRet = iSaleLen;

   pSale = (TUO_SALE *)&acRec[0];
   do
   {
      if (iRet != iSaleLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, SSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);
         iRet = fread(acRec, 1, iSaleLen, fdSale);
         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   do
   {
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "004020015", 9))
      //   iRet = 0;
#endif

      // Replace all characters 31 or less with blank space
      replCharEx(acRec, 31, 32, iSaleLen);

      // Ignore record number starts with RLT
      if (iSaleCnt < 3)
      {
         iSaleCnt++;
         iRet = Tuo_MergeSale(pOutbuf, acRec, iSaleCnt);
      }

      // Get next sale record
      iRet = fread(acRec, 1, iSaleLen, fdSale);
      if (iRet != iSaleLen)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->Apn, SSIZ_APN));

   lSaleMatch++;
 
   return 0;
}
*/
/***************************** Tuo_MergeSaleRec ******************************
 *
 * Merge new sale data into current sale.  Move other sales accordingly.
 * Only update seller if that is the sale from assessor.
 * This is the modified version of MergeSale() which will not update sale
 * without SaleAmt.  Only transfer is updated.
 *
 * Remove two digit year from Doc# - spn 05/23/2007
 *
 *****************************************************************************

int Tuo_MergeSaleRec(TUO_SALE *pSaleRec, char *pOutbuf, bool bSaleFlag)
{
   long  lCurSaleDt, lLstSaleDt, lPrice, lLastAmt;
   char  acRecDate[16], acRecDoc[32];
   int iTmp, iYear;

   lCurSaleDt = atoin(pSaleRec->ProcessDate, SSIZ_PROCESSDATE);
   if (lCurSaleDt < 19500101 || lCurSaleDt > lToday)
   {
      sprintf(acRecDate, "%.4s%.4s", &pSaleRec->Rec_Date[4], pSaleRec->Rec_Date);
      lCurSaleDt = atol(acRecDate);
   } else
   {
      memcpy(acRecDate, pSaleRec->ProcessDate, SSIZ_PROCESSDATE);
      acRecDate[SSIZ_PROCESSDATE] = 0;
   }

   lLstSaleDt = atoin(pOutbuf+OFF_SALE1_DT, SIZ_SALE1_DT);

   // If older than last sale, ignore it
   if (lCurSaleDt < lLstSaleDt)
      return -1;

   // If no Doc#, ignore it
   if (pSaleRec->Rec_Doc[0] == ' ')
      return -1;

   lPrice = atoin(pSaleRec->SalePrice, SIZ_SALE1_AMT); 
   if (lCurSaleDt == lLstSaleDt)
   {
      lLastAmt = atoin(pOutbuf+OFF_SALE1_AMT, SIZ_SALE1_AMT);
      if (!lPrice && lLastAmt > 0)
         return 0;
   } else if (lPrice > 0)
   {
      // Move sale2 to sale3
      memcpy(pOutbuf+OFF_SALE3_DOC, pOutbuf+OFF_SALE2_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE3_DT, pOutbuf+OFF_SALE2_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOCTYPE, pOutbuf+OFF_SALE2_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE3_AMT, pOutbuf+OFF_SALE2_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE3_CODE, pOutbuf+OFF_SALE2_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE3) = *(pOutbuf+OFF_AR_CODE2);

      // Move sale1 to sale2
      memcpy(pOutbuf+OFF_SALE2_DOC, pOutbuf+OFF_SALE1_DOC, SIZ_SALE3_DOC);
      memcpy(pOutbuf+OFF_SALE2_DT, pOutbuf+OFF_SALE1_DT, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE2_DOCTYPE, pOutbuf+OFF_SALE1_DOCTYPE, SIZ_SALE3_DOCTYPE);
      memcpy(pOutbuf+OFF_SALE2_AMT, pOutbuf+OFF_SALE1_AMT, SIZ_SALE3_AMT);
      memcpy(pOutbuf+OFF_SALE2_CODE, pOutbuf+OFF_SALE1_CODE, SIZ_SALE3_CODE);
      *(pOutbuf+OFF_AR_CODE2) = *(pOutbuf+OFF_AR_CODE1);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "03830021", 8))
   //   iTmp = 0;
#endif

   // Format Doc#
   iTmp = atoin(pSaleRec->Rec_Doc, 2);
   iYear = atoin((char *)&acRecDate[2], 2);
   if (iTmp == iYear)
   {
      // Strip off two digit year
      iTmp = atoin(pSaleRec->Rec_Doc+2, 6);
      sprintf(acRecDoc, "%.06d            ", iTmp);
   } else
      sprintf(acRecDoc, "%.*s      ", SSIZ_BOOKPAGE, pSaleRec->Rec_Doc);

   // Update current sale
   if (lPrice > 0)
   {
      memcpy(pOutbuf+OFF_SALE1_DOC, acRecDoc, SSIZ_BOOKPAGE);
      memcpy(pOutbuf+OFF_SALE1_DT, acRecDate, SIZ_SALE3_DT);

      // Check for questionable sale amt
      if (lPrice > 1000)
      {
         memcpy(pOutbuf+OFF_SALE1_AMT, pSaleRec->SalePrice, SIZ_SALE1_AMT);

         // Seller
         if (bSaleFlag && pSaleRec->Seller[0] > ' ')
            memcpy(pOutbuf+OFF_SELLER, pSaleRec->Seller, SIZ_SELLER);
         else
            memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);

         if (pSaleRec->SaleType == 'M')
            *(pOutbuf+OFF_MULTI_APN) = '1';
         else
            *(pOutbuf+OFF_MULTI_APN) = ' ';
      } else
      {
         memset(pOutbuf+OFF_SALE1_AMT, ' ', SIZ_SALE1_AMT);
         memset(pOutbuf+OFF_SELLER, ' ', SIZ_SELLER);
      }
   }

   // Update transfers
   lLstSaleDt = atoin(pOutbuf+OFF_TRANSFER_DT, SIZ_TRANSFER_DT);
   if (lCurSaleDt >= lLstSaleDt)
   {
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acRecDoc, SSIZ_BOOKPAGE);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acRecDate, SIZ_TRANSFER_DT);
   }

   *(pOutbuf+OFF_AR_CODE1) = 'A';

   return 1;
}
*/

/********************************* Tuo_MergeSale ******************************
 *
 * Input: Sale file is output 
 *
 ******************************************************************************

int Tuo_MergeSale(char *pOutbuf)
{
   static   char  acRec[1024], *pRec=NULL;
   int      iRet, iLoop;
   SALE_REC *pSale;

   // Get first Char rec for first call
   if (!pRec)
   {
      iRet = fread(acRec, 1, iSaleLen, fdSale);
      // Replace all characters 31 or less with '-'
      replCharEx(acRec, 31, '-', iSaleLen);

      pRec = &acRec[0];
   }

   pSale = (TUO_SALE *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pSale->Apn, SSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Sale rec  %.*s", SSIZ_APN, pSale->Apn);
         
         iRet = fread(acRec, 1, iSaleLen, fdSale);
         replCharEx(acRec, 31, '-', iSaleLen);

         if (iRet != iSaleLen)
            pRec = NULL;

         lSaleSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 0;

   do
   {
      iRet = Tuo_MergeSaleRec((TUO_SALE *)&acRec[0], pOutbuf, true);

      // Get next sale record
      iRet = fread(acRec, 1, iSaleLen, fdSale);
      replCharEx(acRec, 31, '-', iSaleLen);
      if (iRet != iSaleLen)
      {
         pRec = NULL;
         fclose(fdSale);
         fdSale = NULL;
         break;
      }

   } while (!memcmp(pOutbuf, pSale->Apn, SSIZ_APN));

   lSaleMatch++;
  
   return 0;
}
*/

int Tuo_MergeSale(char *pOutbuf)
{
   static    char acRec[512], *pRec=NULL;
   int       iTmp;
   SALE_REC *pSale;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdSale);
   }

   pSale = (SALE_REC *)&acRec[0];
   do 
   {
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, myCounty.iApnLen);
      if (iTmp > 0)
         pRec = fgets(acRec, 512, fdSale);
   } while (iTmp > 0);

   if (iTmp < 0)
      return 1;

   do
   {
      MergeSale1(pSale, pOutbuf, true);

      // Get next sale record
      pRec = fgets(acRec, 512, fdSale);
      if (!pRec)
      {
         fclose(fdSale);
         fdSale = NULL;
         break;
      } 
   } while (!memcmp(pOutbuf, pRec, myCounty.iApnLen));

   lSaleMatch++;

   return 0;
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Tuo_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acCode[32], acTmp1[32];
   long     lTmp;
   int      iTmp;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "55TUO", 5);

      // Create APN_D 
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTH_VALUE);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_OTH_VALUE);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_OTH_VALUE);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

         // Ratio
         if (lImpr > 0)
         {
            sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
            memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
         }
      }      

      // HO Exempt
      lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
      if (!memcmp(pRec->ExeCode1, "10", 2) && lTmp > 0)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exempt total
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }

      // Prop 8
      if (!memcmp(pRec->BaseCode, "67", 2))
         *(pOutbuf+OFF_PROP8_FLG) = 'Y';
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"TUO");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // Year Blt
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1700)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1700)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
   } else
   {
      lTmp = 0;
      strcpy(acTmp, BLANK32);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

   // Lot Sqft
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0961102700", 10))
   //   iTmp = 0;
#endif

   //if (fdSale)
   //  iRet = Tuo_MergeSale(pOutbuf);
  
   // Recording info - update transfer only
   if (pRec->RecBook1[3] > ' ')
      Cres_UpdXfer(pOutbuf, pRollRec);

   // Owners
   Tuo_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address
   Tuo_MergeAdr(pOutbuf, pRollRec);

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      memcpy(acTmp, pRec->LglDesc1, CRESIZ_LGL_DESC*2);
      acTmp[CRESIZ_LGL_DESC*2] = 0;
      updateLegal(pOutbuf, acTmp);
   }

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // FP - Y/N
   if (pRec->Fp[0] == 'Y')
      *(pOutbuf+OFF_FIRE_PL) = '1';
   else if (pRec->Fp[0] == 'N')
      *(pOutbuf+OFF_FIRE_PL) = '0';

   // AC - Y/N
   if (pRec->Ac[0] == 'Y')
      *(pOutbuf+OFF_AIR_COND) = 'X';
   else if (pRec->Ac[0] == 'N')
      *(pOutbuf+OFF_AIR_COND) = 'N';

   // Heating - Y/N
   if (pRec->Heating[0] == 'Y')
      *(pOutbuf+OFF_HEAT) = 'Y';
   else if (pRec->Heating[0] == 'N')
      *(pOutbuf+OFF_HEAT) = 'L';

   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage - Y/N
   if (pRec->Garage[0] == 'Y')
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';
   else if (pRec->Garage[0] == 'N')
      *(pOutbuf+OFF_PARK_TYPE) = 'H';

   // Pool/Spa - Y/N
   if (pRec->Pool[0] == 'Y' && pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'C';
   else if (pRec->Pool[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';
   else if (pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'S';

   // Zoning - random data not usable

   // Neighborhood

   // Type_Sqft - M, C
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
      acTmp[iTmp++] = pRec->BldgCls[0];
      acTmp[iTmp++] = '.';
      if (isdigit(pRec->BldgCls[1]))
         acTmp[iTmp++] = pRec->BldgCls[1];
      else if (isdigit(pRec->BldgCls[2]))
         acTmp[iTmp++] = pRec->BldgCls[2];
      else
         acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   return 0;
}

/********************************** Tuo_MergeTax ****************************
 *
 *
 ****************************************************************************/

int Tuo_MergeTax(char *pOutbuf)
{
   static   char  acRec[512], *pRec=NULL;
   int      iLoop;
   double   dTax;
   char     acTmp[64];
   TUO_TAX  *pTax;

   // Get first Char rec for first call
   if (!pRec && !lTaxMatch)
      pRec = fgets(acRec, 512, fdTax);

   pTax = (TUO_TAX *)&acRec[0];
   do
   {
      if (!pRec)
      {
         fclose(fdTax);
         fdTax = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pTax->Apn, TSIZ_APN);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Tax rec  %.*s", TSIZ_APN, pTax->Apn);
         pRec = fgets(acRec, 512, fdTax);
         lTaxSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "004020015", 9))
      //   iRet = 0;
#endif

   dTax = atofn(pTax->TotalTaxAmt, TSIZ_TOTALTAXAMT);
   if (dTax > 0.0)
   {
      sprintf(acTmp, "%*d", SIZ_TAX_AMT, (long)(dTax*100));
      memcpy(pOutbuf+OFF_TAX_AMT, acTmp, SIZ_TAX_AMT);
   } else
      memset(pOutbuf+OFF_TAX_AMT, ' ', SIZ_TAX_AMT);

   // Get next sale record
   pRec = fgets(acRec, 512, fdTax);

   lTaxMatch++;
 
   return 0;
}

/******************************* Tuo_MergeZoning ****************************
 *
 * Merge Zoning data into R01.
 *
 ****************************************************************************/

int Tuo_MergeZoning(char *pOutbuf)
{
   static   char  acRec[512], *pRec=NULL;
   int      iLoop, iTmp;
   char     *pTmp, *pTmp1;

   if (!pRec)
   {
      // Skip header record
      pRec = fgets(acRec, 512, fdZoning);

      // Get first rec
      pRec = fgets(acRec, 512, fdZoning);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0326501900", 10))
   //   iTmp = 0;
   //pTmp = pRec;         // for debuging
#endif

   do
   {
      if (!pRec)
      {
         fclose(fdZoning);
         fdZoning = NULL;
         return 1;      // EOF
      }

      // Compare Apn
      iLoop = memcmp(pOutbuf, pRec, iApnLen);
      if (iLoop > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zoning rec  %s", pRec);
         pRec = fgets(acRec, 512, fdZoning);
         lZoningSkip++;
      }
   } while (iLoop > 0);

   // If not match, return
   if (iLoop)
      return 1;

   // Copy Zoning to output
   pTmp = strchr(acRec, cDelim);
   iTmp = strlen(++pTmp)-1;
   if (iTmp > SIZ_ZONE)
   {
      if (strstr(pTmp, "NOT FOUND"))
         iTmp = 0;
      else if (pTmp1 = strchr(pTmp, '&'))
      {
         *pTmp1 = 0;
         iTmp = strlen(pTmp);
      }
      if (iTmp > SIZ_ZONE && (pTmp1 = strchr(pTmp, ':')))
      {
         *pTmp1 = 0;
         iTmp = strlen(pTmp);
      }
   }

   if (iTmp > SIZ_ZONE) iTmp = SIZ_ZONE;
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   memcpy(pOutbuf+OFF_ZONE, pTmp, iTmp);

   // Get next sale record
   pRec = fgets(acRec, 512, fdZoning);

   lZoningMatch++;
 
   return 0;
}

/********************************** Tuo_LoadUpdt ****************************
 *
 * This function will either merge new data into old record or create new
 * record for new parcel.
 *
 ****************************************************************************/

int Tuo_LoadUpdt(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lRollCnt=0;

   LogMsgD("\nLoad update roll");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Use last out file for input if lien file missing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Input file missing: %s", acRawFile);
         return -1;
      }
   }

   // Open Zoning file if exist
   GetIniString(myCounty.acCntyCode, "ZoningFile", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > 'A' && !_access(acTmp, 0))
   {
      LogMsg("Open Zoning file %s", acTmp);
      fdZoning = fopen(acTmp, "r");
      if (fdZoning == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmp);
         return -2;
      }
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Read first rec, skip blank rec as found
   do 
   {
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      lRollCnt++;
   } while (acRollRec[0] == ' ');

   bEof = (iRet==iRollLen ? false:true);

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }
      iSkip = 0;

      if (!nBytesRead)
         break;

      NextRollRec:
      // Replace all characters 31 or less with blank space
      replCharEx(acRollRec, 31, 32, iRollLen);

#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "01630004", 8))
      //   iSkip=0;
#endif

      iTmp = memcmp(acBuf, (char *)&acRollRec[CREOFF_APN], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Tuo_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);
         iRollUpd++;

         if (fdTax)
            iRet = Tuo_MergeTax(acBuf);

         if (fdZoning)
            iRet = Tuo_MergeZoning(acBuf);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

         // Read next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         lRollCnt++;
         bEof = (iRet==iRollLen ? false:true);
      } else if (iTmp > 0)       // Roll not match, new roll record
      {
         if (bDebug)
            LogMsg0("New roll record: %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);

         // Create new R01 record
         iRet = Tuo_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
         iNewRec++;

         if (fdTax)
            iRet = Tuo_MergeTax(acRec);

         if (fdZoning)
            iRet = Tuo_MergeZoning(acRec);

         // Write to file
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;

         // Read next record
         iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

         if (iRet != iRollLen)
            bEof = true;    // Signal to stop
         else
         {
            lRollCnt++;
            goto NextRollRec;
         }
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
         iRetiredRec++;
         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         bEof = true;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);

      // Replace all characters 31 or less with blank space
      replCharEx(acRollRec, 31, 32, iRollLen);

      // Create new R01 record
      iRet = Tuo_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
      iNewRec++;

      if (fdTax)
         iRet = Tuo_MergeTax(acRec);

      if (fdZoning)
         iRet = Tuo_MergeZoning(acRec);

      bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      // Get next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

      if (iRet != iRollLen)
         bEof = true;    // Signal to stop
      else
         lRollCnt++;
   }

   // Close files
   if (fdZoning)
      fclose(fdZoning);
   if (fdRoll)
      fclose(fdRoll);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   // Copy file for assessor
   GetIniString(myCounty.acCntyCode, "AsrFile", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] > ' ')
   {
      LogMsg("Copying file %s ==> %s", acOutFile, acTmp);
      iTmp = CopyFile(acOutFile, acTmp, false);
      if (!iTmp)
         LogMsg("***** Fail copying file %s ==> %s", acOutFile, acTmp);
   }

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total Tax skipped:          %u\n", lTaxSkip);
   if (fdZoning)
   {
      LogMsg("Total Zoning matched:       %u", lZoningMatch);
      LogMsg("Total Zoning skipped:       %u\n", lZoningSkip);
   }

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Total roll records input:   %u", lRollCnt);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return lRet;
}

/********************************** Tuo_LoadLien ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Tuo_LoadLien(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Tax file
   LogMsg("Open Tax file %s", acTaxFile);
   fdTax = fopen(acTaxFile, "r");
   if (fdTax == NULL)
   {
      LogMsg("***** Error opening Tax file: %s\n", acTaxFile);
      return -2;
   }

   // Open Zoning file if exist
   GetIniString(myCounty.acCntyCode, "ZoningFile", "", acBuf, _MAX_PATH, acIniFile);
   if (acBuf[0] > 'A' && !_access(acBuf, 0))
   {
      LogMsg("Open Zoning file %s", acBuf);
      fdZoning = fopen(acBuf, "r");
      if (fdZoning == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acBuf);
         return -2;
      }
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   do
   {
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
   } while (acRollRec[0] == ' ');

   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "00102007", 8))
      //   iRet = 0;
#endif
      // Replace all characters 31 or less with blank space
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Tuo_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);

      if (fdTax)
         iRet = Tuo_MergeTax(acBuf);

      if (fdZoning)
         iRet = Tuo_MergeZoning(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      // Read next roll record
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdTax)
      fclose(fdTax);
   if (fdZoning)
      fclose(fdZoning);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   if (fdZoning)
   {
      LogMsg("Total Zoning matched:       %u", lZoningMatch);
      LogMsg("Total Zoning skipped:       %u\n", lZoningSkip);
   }

   lRecCnt = lCnt;
   lLDRRecCount = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Tuo_FixDoc ******************************
 *
 * Reformat Doc#
 *
 ****************************************************************************/

int Tuo_FixDoc(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[64];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];
   char     *pCnty = "TUO";

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0;

   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "T01");

   LogMsg("Fix Docnum format ...");

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

#ifdef _DEBUG
//      if (!memcmp(acBuf, "08529006", 8))
//         bRet = true;
#endif

      // Reformat doc#
      char *pDate, *pDoc, *pTmp;
      int   iBk, iPg, iTmp;

      // Doc #1
      pDoc  = (char *)&acBuf[OFF_SALE1_DOC];
      pDate = (char *)&acBuf[OFF_SALE1_DT];
      memcpy(acTmp, pDoc, 8);
      acTmp[8] = 0;
      remChar(acTmp, ' ');
      iTmp = strlen(acTmp);
      if ((pTmp = strchr(acTmp, '/')) || (pTmp = strchr(acTmp, '-')))
      {
         iBk = atoi(acTmp);
         iPg = atoi(pTmp+1);
         sprintf(acTmp, "%.4d%.4d", iBk, iPg);
         memcpy(pDoc, acTmp, 8);
      } else if (iTmp == 7)
      {
         // If year 2001 and earlier
         if (memcmp(pDate, "2002", 4) < 0)
         {
            iBk = atoin(acTmp, 4);
            iPg = atoi(&acTmp[4]);
            sprintf(acTmp, "%.4d%.4d", iBk, iPg);
            memcpy(pDoc, acTmp, 8);
         } else if (!memcmp(pDate+2, acTmp, 2))
         {
            iBk = atoi(&acTmp[2]);
            sprintf(acTmp, "%.6d  ", iBk);
            memcpy(pDoc, acTmp, 8);
         }
      }

      if (*(pDoc+6) > ' ' && memcmp(pDate, "2001", 4) > 0 && !memcmp(pDoc, pDate+2, 2))
         memcpy((char *)&acBuf[OFF_SALE1_DOC], pDoc+2, 8);
      else if (!memcmp(pDate, "000000", 6))
         memset((char *)&acBuf[OFF_SALE1_DT], ' ', 8);
      else if (!memcmp(pDoc, "000000", 6))
         memset((char *)&acBuf[OFF_SALE1_DOC], ' ', 8);

      // Doc #2
      pDoc  = (char *)&acBuf[OFF_SALE2_DOC];
      pDate = (char *)&acBuf[OFF_SALE2_DT];
      memcpy(acTmp, pDoc, 8);
      acTmp[8] = 0;
      remChar(acTmp, ' ');
      iTmp = strlen(acTmp);
      if ((pTmp = strchr(acTmp, '/')) || (pTmp = strchr(acTmp, '-')))
      {
         iBk = atoi(acTmp);
         iPg = atoi(pTmp+1);
         sprintf(acTmp, "%.4d%.4d", iBk, iPg);
         memcpy(pDoc, acTmp, 8);
      } else if (iTmp == 7)
      {
         // If year 2001 and earlier
         if (memcmp(pDate, "2002", 4) < 0)
         {
            iBk = atoin(acTmp, 4);
            iPg = atoi(&acTmp[4]);
            sprintf(acTmp, "%.4d%.4d", iBk, iPg);
            memcpy(pDoc, acTmp, 8);
         } else if (!memcmp(pDate+2, acTmp, 2))
         {
            iBk = atoi(&acTmp[2]);
            sprintf(acTmp, "%.6d  ", iBk);
            memcpy(pDoc, acTmp, 8);
         }
      }

      if (*(pDoc+6) > ' ' && memcmp(pDate, "2001", 4) > 0 && !memcmp(pDoc, pDate+2, 2))
         memcpy((char *)&acBuf[OFF_SALE2_DOC], pDoc+2, 8);
      else if (!memcmp(pDate, "000000", 6))
         memset((char *)&acBuf[OFF_SALE2_DT], ' ', 8);
      else if (!memcmp(pDoc, "000000", 6))
         memset((char *)&acBuf[OFF_SALE2_DOC], ' ', 8);
      
      // Doc #3
      pDoc  = (char *)&acBuf[OFF_SALE3_DOC];
      pDate = (char *)&acBuf[OFF_SALE3_DT];
      memcpy(acTmp, pDoc, 8);
      acTmp[8] = 0;
      remChar(acTmp, ' ');
      iTmp = strlen(acTmp);
      if ((pTmp = strchr(acTmp, '/')) || (pTmp = strchr(acTmp, '-')))
      {
         iBk = atoi(acTmp);
         iPg = atoi(pTmp+1);
         sprintf(acTmp, "%.4d%.4d", iBk, iPg);
         memcpy(pDoc, acTmp, 8);
      } else if (iTmp == 7)
      {
         // If year 2001 and earlier
         if (memcmp(pDate, "2002", 4) < 0)
         {
            iBk = atoin(acTmp, 4);
            iPg = atoi(&acTmp[4]);
            sprintf(acTmp, "%.4d%.4d", iBk, iPg);
            memcpy(pDoc, acTmp, 8);
         } else if (!memcmp(pDate+2, acTmp, 2))
         {
            iBk = atoi(&acTmp[2]);
            sprintf(acTmp, "%.6d  ", iBk);
            memcpy(pDoc, acTmp, 8);
         }
      }

      if (*(pDoc+6) > ' ' && memcmp(pDate, "2001", 4) > 0 && !memcmp(pDoc, pDate+2, 2))
         memcpy((char *)&acBuf[OFF_SALE3_DOC], pDoc+2, 8);
      else if (!memcmp(pDate, "000000", 6))
         memset((char *)&acBuf[OFF_SALE3_DT], ' ', 8);
      else if (!memcmp(pDoc, "000000", 6))
         memset((char *)&acBuf[OFF_SALE3_DOC], ' ', 8);

      // Transfer
      pDoc  = (char *)&acBuf[OFF_TRANSFER_DOC];
      pDate = (char *)&acBuf[OFF_TRANSFER_DT];
      memcpy(acTmp, pDoc, 8);
      acTmp[8] = 0;
      remChar(acTmp, ' ');
      iTmp = strlen(acTmp);
      if ((pTmp = strchr(acTmp, '/')) || (pTmp = strchr(acTmp, '-')))
      {
         iBk = atoi(acTmp);
         iPg = atoi(pTmp+1);
         sprintf(acTmp, "%.4d%.4d", iBk, iPg);
         memcpy(pDoc, acTmp, 8);
      } else if (iTmp == 7)
      {
         // If year 2001 and earlier
         if (memcmp(pDate, "2002", 4) < 0)
         {
            iBk = atoin(acTmp, 4);
            iPg = atoi(&acTmp[4]);
            sprintf(acTmp, "%.4d%.4d", iBk, iPg);
            memcpy(pDoc, acTmp, 8);
         } else if (!memcmp(pDate+2, acTmp, 2))
         {
            iBk = atoi(&acTmp[2]);
            sprintf(acTmp, "%.6d  ", iBk);
            memcpy(pDoc, acTmp, 8);
         }
      }

      if (*(pDoc+6) > ' ' && memcmp(pDate, "2001", 4) > 0 && !memcmp(pDoc, pDate+2, 2))
         memcpy((char *)&acBuf[OFF_TRANSFER_DOC], pDoc+2, 8);
      else if (!memcmp(pDate, "000000", 6))
         memset((char *)&acBuf[OFF_TRANSFER_DT], ' ', 8);
      else if (!memcmp(pDoc, "000000", 6))
         memset((char *)&acBuf[OFF_TRANSFER_DOC], ' ', 8);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename out file
   if (lCnt > 10000)
   {
      if (remove(acRawFile))
         LogMsg("***** Error removing file: %s (%d)", acRawFile, errno);
      else if (rename(acOutFile, acRawFile))
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acOutFile, acRawFile, errno);
   }

   LogMsg("Fix Docs completed\n");
   return 0;
}

/******************************* Tuo_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Tuo_MakeDocLink(char *pDocLink, char *pDoc, char *pDate)
{
   int   iTmp, iDocNum;
   char  acTmp[256], acDocName[256];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      iTmp = atoin((char *)pDate, 4);
      iDocNum = atoin((char *)pDoc, 8);

      if (iTmp >= 2002 && iDocNum > 0)
         sprintf(acDocName, "%.4s\\%.3s\\%.4s%06d", pDate, pDoc, pDate, iDocNum);
      else
         sprintf(acDocName, "%.4s\\%.4s\\%.4s%.8s", pDate, pDoc, pDate, pDoc);

      sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);

      try
      {
         if (!_access(acTmp, 0))
            strcpy(pDocLink, &acDocName[0]);
         else
            *pDocLink = 0;
      } catch (...)
      {
         LogMsg("*** Bad file name: %s", acTmp);
      }
   }
}

/******************************* Tuo_FmtDocLinks ****************************
 *
 * Format DocLinks as bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp
 * which are Doclink1, Doclink2, Doclink3, DocXferlink
 *
 ****************************************************************************/

int Tuo_FmtDocLinks(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH],
            acDocLink[256], acDocLinks[256];

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lLinkCnt=0;
   int      iTmp, iRet = 0;

   LogMsg("Format Doclinks ...");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

#ifdef _DEBUG
//      if (!memcmp(acBuf, "08529006", 8))
//         bRet = true;
#endif

      // Reformat doc#

      // Reset old links
      memset((char *)&acBuf[OFF_DOCLINKS], ' ', SIZ_DOCLINKS);
      acDocLinks[0] = 0;

      // Doc #1
      Tuo_MakeDocLink(acDocLink, (char *)&acBuf[OFF_SALE1_DOC], (char *)&acBuf[OFF_SALE1_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Doc #2
      Tuo_MakeDocLink(acDocLink, (char *)&acBuf[OFF_SALE2_DOC], (char *)&acBuf[OFF_SALE2_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Doc #3
      Tuo_MakeDocLink(acDocLink, (char *)&acBuf[OFF_SALE3_DOC], (char *)&acBuf[OFF_SALE3_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Transfer Doc
      Tuo_MakeDocLink(acDocLink, (char *)&acBuf[OFF_TRANSFER_DOC], (char *)&acBuf[OFF_TRANSFER_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }

      // Update DocLinks
      iTmp = strlen(acDocLinks);
      if (iTmp > 10)
         memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, iTmp);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename out file
   if (lCnt > 10000)
   {
      if (remove(acRawFile))
      {
         LogMsg("***** Error removing file: %s (%d)", acRawFile, errno);
         iRet = errno;
      } else if (rename(acOutFile, acRawFile))
      {
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acOutFile, acRawFile, errno);
         iRet = errno;
      }
   }

   LogMsg("FmtDocLinks completed.  %d doclinks assigned.\n", lLinkCnt);
   return iRet;
}

/********************************* Tuo_ExtrSale ******************************
 *
 * Extract Sale data from SALESDATA file and output to Tuo_Sale.dat
 * and append to Tuo_Sale.sls
 *
 * Return: 0 if successful, <0 if error.
 *
 *****************************************************************************/

int Tuo_ExtrSale(bool bAppend)
{
   char     acOutFile[_MAX_PATH], acTmpFile[_MAX_PATH];
   char     acTmp[256], acRec[1024];

   int      iTmp, iRet, iSaleHdrLen;
   long     lCnt=0, lTmp, lCurSaleDt, lAdjPrice, lPrice;
   double   dTmp;

   FILE     *fdOut;
   TUO_SALE *pSale;
   SCSAL_REC SaleRec;

   LogMsg("Creating Sale export file for %s", myCounty.acCntyCode);

   // Open Sales file
   LogMsg("Open Sales file %s", acSaleFile);
   fdSale = fopen(acSaleFile, "rb");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acSaleFile);
      return -1;
   }

   // Open Output file
   sprintf(acTmpFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "TMP");
   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   // Skip header
   iSaleHdrLen = GetPrivateProfileInt(myCounty.acCntyCode, "SaleHdrSize", 0, acIniFile);
   if (iSaleHdrLen > 0)
      iRet = fread(acRec, 1, iSaleHdrLen, fdSale);

   pSale = (TUO_SALE *)&acRec[0];

   // Loop through record set
   while (!feof(fdSale))
   {  
      iRet = fread(acRec, 1, iSaleLen, fdSale);
      if (iRet != iSaleLen)
         break;

      // If no APN, ignore it
      if (pSale->Apn[0] == ' ')
         continue;

      // Replace unwanted char with '-'
      replCharEx(acRec, 31, '-', iSaleLen);

      // Clear output record
      memset((void *)&SaleRec, ' ', sizeof(SCSAL_REC));
#ifdef _DEBUG
      //if (!memcmp(pSale->Apn, "065050090", 9))
      //   iTmp = 0;
#endif
      // APN
      memcpy(SaleRec.Apn, pSale->Apn, iApnLen);

      // DocDate
      char acDate[32], *pTmp;
      memcpy(acDate, pSale->ProcessDate, SSIZ_PROCESSDATE);
      acDate[SSIZ_PROCESSDATE] = 0;
      if (!isValidYMD(acDate))
         pTmp = dateConversion(pSale->Rec_Date, acDate, MMDDYYYY);

      lCurSaleDt = atol(acDate);
      if (lCurSaleDt > 19000101 && lCurSaleDt <= lToday)
      {
         memcpy(SaleRec.DocDate, acDate, SSIZ_PROCESSDATE);
         if (lCurSaleDt > lLastRecDate)
            lLastRecDate = lCurSaleDt;
      }

      // Format Doc#
      if (!memcmp(pSale->Rec_Doc, (char *)&SaleRec.DocDate[2], 2))
      {
         // Strip off two digit year
         iTmp = atoin(pSale->Rec_Doc+2, 6);
         sprintf(acTmp, "%.06d", iTmp);
         memcpy(SaleRec.DocNum, acTmp, 6);
      } else
         memcpy(SaleRec.DocNum, pSale->Rec_Doc, SSIZ_BOOKPAGE);

      // Adj sale price
      lAdjPrice = atoin(pSale->AdjPrice, SSIZ_SALEPRICE);
      if (lAdjPrice > 1000)
      {
         sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lAdjPrice);
         memcpy(SaleRec.AdjSalePrice, acTmp, SIZ_SALE1_AMT);
      }

      // Sale price
      lPrice = atoin(pSale->SalePrice, SSIZ_SALEPRICE);
      if (lPrice > 1000)
      {
         sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.ConfirmedSalePrice, acTmp, SIZ_SALE1_AMT);
         if (lPrice != lAdjPrice)
            LogMsg("??? Sale price is questionable. Check APN: %.10s", pSale->Apn);
      }

      // Sale price - Use stamp amt to calculate.  If not avail, use sale price
      memcpy(acTmp, pSale->DocTax, SSIZ_STAMPAMT);
      acTmp[SSIZ_STAMPAMT] = 0;
      try {
         dTmp = atof(acTmp);
      } catch (...)
      {
         LogMsg("*** Invalid DocTax for APN: %.10s, DocTax: %s", pSale->Apn, acTmp);
         dTmp = 0;
      }
      if (dTmp > 0.0)
      {
         iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
         memcpy(SaleRec.StampAmt, acTmp, iTmp);
         lTmp = (long)(dTmp*SALE_FACTOR);
      } else
      {
         lTmp = lPrice;
      }

      if (lTmp > 1000)
      {
         if (lPrice > 0 && lPrice != lTmp)
         {
            LogMsg("??? DocTax is questionable. Check APN: %.10s DocTax: %.2f Conf Price: %d", pSale->Apn, dTmp, lPrice);

            if ((lTmp % 100) > 0)
               lTmp = lPrice;
         }
         
         //if (lTmp > lPrice)
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lTmp);
         //else
         //   sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(SaleRec.SalePrice, acTmp, SIZ_SALE1_AMT);
         SaleRec.Spc_Flg = SALE_FLG_CONFIRM;
      }

      // Seller
      if (pSale->Seller[0] > ' ')
         memcpy(SaleRec.Seller1, pSale->Seller, SSIZ_SELLER);

      if (pSale->SaleType == 'M')
         SaleRec.MultiSale_Flg = 'Y';
      
      SaleRec.ARCode = 'A';

      // New owner/Buyer
      memcpy(SaleRec.Name1, pSale->Buyer, SSIZ_BUYER);

      SaleRec.CRLF[0] = 10;
      SaleRec.CRLF[1] = 0;
      fputs((char *)&SaleRec,fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdSale);
   fclose(fdOut);  
   LogMsg("Total processed records: %u\n", lCnt);

   // Sort output file and dedup on APN asc, DocDate asc, DocNum asc
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(B2000,1,34)");

   if (bAppend)
   {
      // Update cumulative sale file
      if (!_access(acCSalFile, 0))
      {
         char acSrtFile[_MAX_PATH];

         LogMsg("Append %s to %s.", acCSalFile, acTmpFile);
         sprintf(acSrtFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
         sprintf(acOutFile, "%s+%s", acTmpFile, acCSalFile);
         lCnt = sortFile(acOutFile, acSrtFile, acTmp);
         if (lCnt > 1)
         {
            sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
            if (!_access(acTmp, 0))
               DeleteFile(acTmp);

            // Rename SLS to SAV file
            iTmp = rename(acCSalFile, acTmp);

            // Rename srt to SLS file
            iTmp = rename(acSrtFile, acCSalFile);
         } else
            iTmp = -1;
      } else
      {
         lCnt = sortFile(acTmpFile, acCSalFile, acTmp);
         iTmp = 0;
      }
   } else if (lCnt > 100)
   {
      lCnt = sortFile(acTmpFile, acCSalFile, acTmp);
      iTmp = 0;
   } else
   {
      LogMsg("***** Input file looks empty ...  Please check!");
      iTmp = -1;
   }

   LogMsg("Create Sale export completed: %d.", lCnt);
   return iTmp;
}

/******************************** Tuo_ProcAssr ******************************
 *
 * Cleanup and copy county files to assessor product folders
 * Return -1 if failed.  Number of records copied otherwise
 *
 ****************************************************************************/

int localCopyFile(LPCSTR pFrom, LPCSTR pTo, int iHdrSize, int iRecSize)
{
   int   iRet, iCnt;
   char  acBuf[4096];
   FILE  *fdIn, *fdOut;

   LogMsgD("Copying %s to %s", pFrom, pTo);
   if (_access(pFrom, 0))
   {
      LogMsg("***** Missing input file: %s", pFrom);
      return -1;
   }

   if (!(fdIn = fopen(pFrom, "rb")))
   {
      LogMsg("***** Error opening %s [%d]", pFrom, _errno);
      return -1;
   }

   if (!(fdOut = fopen(pTo, "wb")))
   {
      LogMsg("***** Error creating %s [%d]", pTo, _errno);
      fclose(fdIn);
      return -2;
   }

   // Skip header
   if (iHdrSize > 0)
      iRet = fread(acBuf, 1, iHdrSize, fdIn);

   // Start copying
   iCnt = 0;
   while (true)
   {
      // Read/Write in binary mode
      iRet = fread(acBuf, 1, iRecSize, fdIn);
      if (iRet == iRecSize)
      {
         iRet = fwrite(acBuf, 1, iRecSize, fdOut);
         if (iRet != iRecSize)
         {
            LogMsg("***** Error write to output file at rec# %d", iCnt);
            break;
         }
      } else
         break;      // EOF

      printf("\r%d", ++iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   return iCnt;
}

static int fixEtalFile(LPCSTR pFrom, LPCSTR pTo)
{
   int   iRet, iCnt, iRecSize;
   char  acRec[512], *pTmp;
   FILE  *fdIn, *fdOut;
   
   TUO_ETAL OutRec;
   
   iRecSize = sizeof(TUO_ETAL);

   LogMsgD("Copying %s to %s", pFrom, pTo);
   if (_access(pFrom, 0))
   {
      LogMsg("***** Missing input file: %s [0x%x]", pFrom, _errno);
      return -1;
   }

   if (!(fdIn = fopen(pFrom, "r")))
   {
      LogMsg("***** Error opening %s [%d]", pFrom, _errno);
      return -1;
   }

   if (!(fdOut = fopen(pTo, "wb")))
   {
      LogMsg("***** Error creating %s [%d]", pTo, _errno);
      fclose(fdIn);
      return -2;
   }

   // Start copying
   iCnt = 0;
   while (true)
   {
      pTmp = fgets(acRec, 512, fdIn);
      if (!pTmp)
         break;

      // Parse input record
      iRet = ParseString(acRec, '~', 4, apTokens);
      if (iRet < 4)
      {
         LogMsg("*** Invalid input record %s.  Skip ...", acRec);
         continue;
      }

      memset(&OutRec, ' ', iRecSize);
      memcpy(OutRec.Owner, apTokens[0], strlen(apTokens[0]));
      memcpy(OutRec.PctOwn, apTokens[1], strlen(apTokens[1]));
      memcpy(OutRec.DocNum, apTokens[2], strlen(apTokens[2]));
      memcpy(OutRec.Apn, apTokens[3], strlen(apTokens[3]));

      iRet = fwrite(&OutRec, 1, iRecSize, fdOut);
      if (iRet != iRecSize)
      {
         LogMsg("***** Error write to output file at rec# %d", iCnt);
         break;
      }

      printf("\r%d", ++iCnt);
   }

   fclose(fdIn);
   fclose(fdOut);

   return iCnt;
}

/****************************** Tuo_ImportGrGrCsv *****************************
 *
 * Remove two digit year from Doc# - spn 05/23/2007
 *
 *
 ****************************************************************************/

int Tuo_ImportGrGrCsv(char *pInfile)
{
   FILE     *fdIn;
   char     acBuf[2048], acTmp[256], acFlds[256], acValues[256], *pRec;
   int      iRet, iCnt=0, iRecCnt=0, iRecSkip=0;
   GRGR_DEF myGrGrRec;
   int		ReadGrGr=0;
   long		lTax, lPrice;
   bool     bTabIncl = false;

   LogMsg("Processing GrGr file %s", pInfile);
   if (!(fdIn = fopen(pInfile, "r")))
   {
      LogMsg("***** Error opening GrGr file %s", pInfile);
      return -1;
   }

   // Skip header
   pRec = fgets(acBuf, 2048, fdIn);

   // Check for type of input file
   if (pRec = isTabIncluded(acBuf))
      bTabIncl = true;

   // Processing loop
   while (!feof(fdIn))
   {
      pRec = fgets(acBuf, 2048, fdIn);
      if (!pRec)
         break;

      if (bTabIncl)
         iRet = ParseStringIQ(acBuf, 9, TUO_GR_COMMENTS+1, apTokens);
      else
         iRet = ParseStringNQ(acBuf, ',', TUO_GR_COMMENTS+1, apTokens);

      if (iRet < TUO_GR_COMMENTS)
      {
         iRecSkip++;
         continue;
      }

      strcpy(acTmp, apTokens[TUO_GR_APN]);
      strcat(acTmp, "0000000000");
      acTmp[iApnLen] = 0;
      
#ifdef _DEBUG
      //if (!memcmp(acTmp, "08529001", 8))
      //   iRet = 0;
#endif
      if (memcmp(acTmp, "0000000000", iApnLen))
      {
         memcpy(myGrGrRec.SourceTable, apTokens[TUO_GR_PRDOC], strlen(apTokens[TUO_GR_PRDOC]));

         memcpy(myGrGrRec.Grantors[0].Name, apTokens[TUO_GR_GRANTOR], strlen(apTokens[TUO_GR_GRANTOR]));
         //memcpy(myGrGrRec.Grantors[0].NameType, "O", 1);
         memcpy(myGrGrRec.Grantees[0].Name, apTokens[TUO_GR_GRANTEE], strlen(apTokens[TUO_GR_GRANTEE]));
         //memcpy(myGrGrRec.Grantees[0].NameType, "O", 1);
         myGrGrRec.NameCnt[0] = '1';

         memcpy(myGrGrRec.DocTitle, apTokens[TUO_GR_DOCTYPE], strlen(apTokens[TUO_GR_DOCTYPE]));

         if (strlen(apTokens[TUO_GR_DOCDATE]) > 8)
         {
            strcpy(acTmp, apTokens[TUO_GR_DOCDATE]);
            remChar(acTmp, '-');		
            memcpy(myGrGrRec.DocDate, acTmp, 8);
         } 

         // skip 4 digit year in Doc#
         if (strlen(apTokens[TUO_GR_DOCNUM]) > 0)
         {
            strcpy(acTmp, apTokens[TUO_GR_DOCNUM]);
            iRet = atol(&acTmp[4]);
            sprintf(acTmp, "%.6d", iRet);
            memcpy(myGrGrRec.DocNum, acTmp, 6);
         }

         // AW_DTT -> Tax
         double dTax;
         strcpy(acTmp, apTokens[TUO_GR_DOCTAX]);
         dTax = atof(acTmp);
         lTax = (long)(dTax*100.0);
         lPrice = (long)(dTax*SALE_FACTOR);
         if (lPrice > 0)
         {
            sprintf(acTmp, "%*d", SIZ_GR_TAX, lTax);
            memcpy(myGrGrRec.Tax, acTmp, SIZ_GR_TAX);

            sprintf(acTmp, "%*d", SIZ_GR_SALE, lPrice);
            memcpy(myGrGrRec.SalePrice, acTmp, SIZ_GR_SALE);
         }

         sprintf(acTmp, "INSERT INTO Tuo_GrGr (%s) VALUES(%s)", acFlds, acValues);
         iRet = execSqlCmd(acTmp, &hTuoDb);
         if (iRet)
            LogMsg("***** Error import GrGr into Tuo_GrGr: %s", acTmp);
         iRecCnt++;
      } else
         iRecSkip++;

      if (!(++iCnt % 1000))
         printf("\r%u", iCnt);
   }


   if (fdIn) fclose(fdIn);

   LogMsg("Number of GrGr record in:    %d", iCnt);
   LogMsg("Number of GrGr record out:   %d", iRecCnt);
   LogMsg("Number of GrGr record Skip:  %d\n", iRecSkip);

   printf("Number of GrGr record in:    %d\n", iCnt);
   printf("Number of GrGr record out:   %d\n", iRecCnt);
   printf("Number of GrGr record Skip:  %d\n\n", iRecSkip);

   return iRecCnt;
}

/********************************* Tuo_ImportGrGr ***************************
 *
 * Import GrGr data into SQL server.  For testing only
 *
 * If successful, return 0.  Otherwise error.
 *
 ****************************************************************************/

int Tuo_ImportGrGr(LPCSTR pCnty) 
{
   char     *pTmp;
   char     acGrGrIn[_MAX_PATH];
   char     acCsvFile[64];
   struct   _finddata_t  sFileInfo;

   int      iCnt, iRet;
   long     lCnt, lHandle;

   // Form GrGr input path
   GetIniString(pCnty, "GrGrIn", "", acGrGrIn, _MAX_PATH, acIniFile);

   // Open Input file
   lHandle = _findfirst(acGrGrIn, &sFileInfo);
   if (lHandle > 0)
   {
      pTmp = strrchr(acGrGrIn, '\\');
      if (pTmp)
         *pTmp = 0;
      iRet = 0;
   } else
   {
      LogMsg("*** No new file avail for processing: %s", acGrGrIn);
      return 0;
   }

   if (!sqlConnect("CountyDb", "TUO", &hTuoDb))
      return -1;

   iCnt = lCnt = 0;
   while (!iRet)
   {
      sprintf(acCsvFile, "%s\\%s", acGrGrIn, sFileInfo.name);

      // Load GrGr file
      iRet = Tuo_ImportGrGrCsv(acCsvFile);
      if (iRet < 0)
         LogMsg("*** Skip %s", acCsvFile);
      else
         lCnt += iRet;

         iCnt++;

      // Find next file
      iRet = _findnext(lHandle, &sFileInfo);
   }


   // Close handle
   _findclose(lHandle);
   iRet = 0;

   LogMsg("Total processed records  : %u", lCnt);

   return iRet;
}

/*****************************************************************************/

extern bool updateTable(LPCTSTR strCmd);
void Tuo_ProcAssr()
{
   int   iRet, iHdrSiz, iRecSiz;
   char  acSrcFile[_MAX_PATH], acDstFile[_MAX_PATH], acTmp[_MAX_PATH];


   // Copy Redifile
   GetIniString(myCounty.acCntyCode, "AsrRFile", "", acDstFile, _MAX_PATH, acIniFile);
   LogMsgD("Copying %s to %s", acRollFile, acDstFile);
   if (!CopyFile(acRollFile, acDstFile, false))
      LogMsgD("***** Error copying %s to %s", acRollFile, acDstFile);
   else
   {
      sprintf(acTmp, "UPDATE Products SET State='P' WHERE (Prefix='%sG')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Copy cortacfile
   GetIniString(myCounty.acCntyCode, "TaxFile", "", acSrcFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "AsrTFile", "", acDstFile, _MAX_PATH, acIniFile);
   LogMsgD("Copying %s to %s", acSrcFile, acDstFile);
   if (!CopyFile(acSrcFile, acDstFile, false))
      LogMsgD("***** Error copying %s to %s", acSrcFile, acDstFile);
   else
   {
      sprintf(acTmp, "UPDATE Products SET State='P' WHERE (Prefix='%sT')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Process GrGr
   iRet = LoadTuoG(myCounty.acCntyCode);
   if (!iRet)
   {
      sprintf(acTmp, "UPDATE Products SET State='P' WHERE (Prefix='%sG')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Copy salesdata - remove header and first record
   GetIniString(myCounty.acCntyCode, "SaleFile", "", acSrcFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "AsrSFile", "", acDstFile, _MAX_PATH, acIniFile);
   iRecSiz = GetPrivateProfileInt(myCounty.acCntyCode, "SaleRecSize", 0, acIniFile);
   iHdrSiz = GetPrivateProfileInt(myCounty.acCntyCode, "SaleHdrSize", 0, acIniFile);
   iRet = localCopyFile(acSrcFile, acDstFile, iHdrSiz+iRecSiz, iRecSiz);
   if (iRet > 0)
   {
      sprintf(acTmp, "UPDATE Products SET State='P' WHERE (Prefix='%sS')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Copy suppfile - skip header & first record
   GetIniString(myCounty.acCntyCode, "SuppFile", "", acSrcFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "AsrXFile", "", acDstFile, _MAX_PATH, acIniFile);
   LogMsgD("Copying %s to %s", acSrcFile, acDstFile);
   iRecSiz = GetPrivateProfileInt(myCounty.acCntyCode, "SuppRecSize", 0, acIniFile);
   iHdrSiz = GetPrivateProfileInt(myCounty.acCntyCode, "SuppHdrSize", 0, acIniFile);
   sprintf(acTmp, "S(1,%d,C,A) F(FIX,%d) B(%d,D)", iApnLen, iRecSiz, iHdrSiz+iRecSiz);
   iRet = sortFile(acSrcFile, acDstFile, acTmp);
   if (iRet <= 0)
      LogMsgD("***** Error copying %s to %s", acSrcFile, acDstFile);
   else
   {
      sprintf(acTmp, "UPDATE Products SET State='P' WHERE (Prefix='%sX')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Copy umastertape - remove header
   GetIniString(myCounty.acCntyCode, "UnsFile", "", acSrcFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "AsrUFile", "", acDstFile, _MAX_PATH, acIniFile);
   iRecSiz = GetPrivateProfileInt(myCounty.acCntyCode, "UnsRecSize", 0, acIniFile);
   iHdrSiz = GetPrivateProfileInt(myCounty.acCntyCode, "UnsHdrSize", 0, acIniFile);
   iRet = localCopyFile(acSrcFile, acDstFile, iHdrSiz, iRecSiz);
   if (iRet > 0)
   {
      sprintf(acTmp, "UPDATE Products SET State='P' WHERE (Prefix='%sU')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Copy cddataetal - convert delimited to fixed length
   GetIniString(myCounty.acCntyCode, "EtalFile", "", acSrcFile, _MAX_PATH, acIniFile);
   GetIniString(myCounty.acCntyCode, "AsrEFile", "", acDstFile, _MAX_PATH, acIniFile);
   iRet = fixEtalFile(acSrcFile, acDstFile);
   if (iRet > 0)
   {
      sprintf(acTmp, "UPDATE Products SET State='P' WHERE (Prefix='%sE')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Work around to avoid update to lAssrRecCnt
   lAssrRecCnt = 0;
}

/******************************** FixDocTax **********************************
 *
 * Remove DocTax & SalePrice if it is carried over from prior sale.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Tuo_FixDocTax(char *pInfile)
{
   char     acInbuf[1024], acLastRec[1024], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0, lClean=0, iTmp, lPrice, lLastPrice=0;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];
   SCSAL_REC *pLastRec= (SCSAL_REC *)&acLastRec[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumSale(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Fix DocTax and Sale Price for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      // Remove bad seller name
      if (pInRec->Seller2[0] > ' ' && pInRec->Seller2[0] <= 'A')
         memset(pInRec->Seller2, ' ', SALE_SIZ_SELLER);

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0801413100", 9) )
      //   iTmp = 0;
#endif
      lPrice = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
      if (lPrice <= 1000)
      {
         lPrice = 0;
         memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
      }

      if (!memcmp(pInRec->Apn, pLastRec->Apn, iApnLen))
      {
         if (pInRec->SalePrice[SALE_SIZ_SALEPRICE-2] > ' ' && 
             //pInRec->Spc_Flg != SALE_FLG_CONFIRM &&
             lPrice == lLastPrice &&
             memcmp(pInRec->DocDate, pLastRec->DocDate, 8) >= 0)
         {
            if (bDebug)
               LogMsg("P&T %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);
            memset(pInRec->SalePrice,' ', SALE_SIZ_SALEPRICE);
            memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
            memset(pInRec->Seller1,  ' ', SALE_SIZ_SELLER);
            pInRec->SaleCode[0] = ' ';
            lClean++;
         } else if (!memcmp(pInRec->DocNum, pLastRec->DocNum, SALE_SIZ_DOCNUM) && pInRec->DocNum[0] > ' ' &&
                    memcmp(pInRec->SalePrice, pLastRec->SalePrice, SALE_SIZ_SALEPRICE) && lPrice > 1000 &&
                    //memcmp(pInRec->DocDate, pLastRec->DocDate, 8) &&
                    pLastRec->SalePrice[SALE_SIZ_SALEPRICE-1] == '0'
                   )
         {

            if (!memcmp(&pInRec->SalePrice[SALE_SIZ_SALEPRICE-2], "00", 2) )
            {
               // DocNum could be bad
               LogMsg("*** Remove DocNum for APN: %.10s, DocNum: %.12s", pInRec->Apn, pInRec->DocNum);
               memset(pInRec->DocNum,' ', SALE_SIZ_DOCNUM);
            } else
            {
               // Sale price might be typo
               memset(pInRec->SalePrice,' ', SALE_SIZ_SALEPRICE);
               memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
               memset(pInRec->Seller1,  ' ', SALE_SIZ_SELLER);
               pInRec->SaleCode[0] = ' ';
            }
         } else
         {
            strcpy(acLastRec, acInbuf);
            if (lPrice > 100)
               lLastPrice = lPrice;
         }
      } else
      {
         strcpy(acLastRec, acInbuf);
         lLastPrice = lPrice;
      }

      if (lPrice == 0 && pInRec->Seller1[0] > ' ')
      {
         memset(pInRec->Seller1,  ' ', SALE_SIZ_SELLER);
         pInRec->SaleCode[0] = ' ';
      }

      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Dedup output file
      strcpy(acLastRec, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(B2000,1,34)");
      iTmp = sortFile(acOutFile, pInfile, acLastRec);

      // Rename output file
      //iTmp = rename(acOutFile, pInfile);
   } else
   {
      iTmp = lCnt;
      LogMsg("Good sale file, no record drop!");
   }

   LogMsg("Total input records:     %u", lCnt);
   LogMsg("    records output :     %u", iTmp);
   LogMsg("    records cleaned:     %u", lClean);

   return 0;
}

/*********************************** loadTuo ********************************
 *
 * Input files:
 *    redifile          899-bytes roll file
 *    cortacfile        68-Bytes tax file with CRLF
 *    suppfile          72-Bytes with 512 bytes header, skip 1st rec (not use)
 *    umastertape       668-Bytes with 512 bytes header (not use)
 *    salesdata         300-Bytes with 512 bytes header, skip 1st rec (not use)
 *    cddataetal        delimited file (not use)
 *    redifile_nl 2013  900-bytes LDR roll with LF.  Starting 2013 LDR
 *    
 * Problems:
 *    1) SALESDATA is incomplete.  Should extract sale1 from redifile?
 *    2) 
 *
 * Processing:
 *    1) Extract sale for cumulative file (to be merge when needed)
 *    2) Daily option: -CTUO -U -O -G -Xs -La
 *    3) LDR options:  -CTUO -L -Xl -Ms -Mg 
 *    
 * Commands:
 *    -Xc   Extract cum sale from O01 if needed
 *    -Xs   Convert SALESDATA to Tuo_Sale.dat
 *    -U    Monthly update
 *    -L    Lien update
 *    -La   Load assessor file
 *    -G    Process GrGr files
 *    -U -G -O -Xsi for normal monthly update
 *    -L -O for lien processing (-CTUO -O -L)
 *
 ****************************************************************************/

int loadTuo(int iLoadFlag, int iSkip)
{
   int   iRet=0;
   char  acTmpFile[_MAX_PATH];

   iApnLen  = myCounty.iApnLen;
   iRollLen = guessRecLen(acRollFile, iRollLen);

   char  acSlsFile[_MAX_PATH];
   sprintf(acSlsFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
   sprintf(acTmpFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Cnv");
   //iRet = GrGr_To_Sale(acSlsFile, acTmpFile, (IDX_TBL5 *)&TUO_DocTitle[0]);

   if (bLoadTax)
   {
      int iLRecLen = GetPrivateProfileInt(myCounty.acCntyCode, "LienRecSize", iRollLen, acIniFile);
      // Load tax agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(acTmpFile);

      iRet = Cres_Load_TaxBase(bTaxImport);
      if (!iRet && lLastTaxFileDate > 0)
         iRet = Cres_Load_TaxDelq(bTaxImport);
      //if (!iRet)
      //   iRet = Cres_Load_TaxOwner(bTaxImport, iLRecLen);
      //if (iRet < 0)
      //{
      //   char sMailTo[64], sSubj[80], sBody[256];

      //   iRet = GetPrivateProfileString("Mail", "MailDeveloper", "", sMailTo, 256, acIniFile);
      //   if (iRet > 10)
      //   {
      //      sprintf(sSubj, "*** %s - Error loading tax file (%d)", myCounty.acCntyCode, iRet);
      //      sprintf(sBody, "Please review log file \"%s\" for more info", acLogFile);
      //      mySendMail(acIniFile, sSubj, sBody, sMailTo);
      //   }
      //}

      if (iRet > 0)
         iRet = 0;
   }

   if (!iLoadFlag)
      return iRet;

   // Set county specific delim
   GetPrivateProfileString(myCounty.acCntyCode, "Delim", "", acTmpFile, _MAX_PATH, acIniFile);
   if (acTmpFile[0] > 0)
      cDelim = acTmpFile[0];

   // Load Value file
   if (iLoadFlag & EXTR_VALUE)
   {
      char sDbName[64], sCVFile[_MAX_PATH], sSheet[_MAX_PATH];

      // Load value file
      GetPrivateProfileString(myCounty.acCntyCode, "ValueSheet", "", sSheet, _MAX_PATH, acIniFile);
      sprintf(sCVFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Value");
      iRet = Tuo_LoadValue(acValueFile, sSheet, "xlsx", sCVFile);
      if (iRet > 0)
      {
         sprintf(sDbName, "LDR%d", lLienYear);
         GetIniString("Data", "SqlValueFile", "", sSheet, _MAX_PATH, acIniFile);
         sprintf(acValueFile, sSheet, sDbName, myCounty.acCntyCode);
         lRecCnt = createValueImport(sCVFile, acValueFile, false);
         if (lRecCnt > 0)
            iRet = 0;
         else
            iLoadFlag ^= EXTR_IVAL;      // Turn off import
      }
   }

   // Extract LDR values
   if (iLoadFlag & EXTR_LIEN)          // -Xl
      iRet = Cres_ExtrLien(myCounty.acCntyCode);

   iRet = 0;
   if (iLoadFlag & (EXTR_SALE|UPDT_SALE))
   {
      if (iLoadFlag & EXTR_SALE)          // -Xs
         iRet = Tuo_ExtrSale(false);
      else
         iRet = Tuo_ExtrSale(true);       // Append to history sale

      if (!iRet)
      {
         // Extract roll sale and update Tuo_Sale.rol
         if (iRet = Cres_UpdSale1(acRollSale))
            return iRet;

         // Merge cum sales with roll sales --> Tuo_Sale.sls
         iRet = MergeSaleFiles(acCSalFile, acRollSale, NULL, false);

         if (!iRet)
            iLoadFlag |= MERG_CSAL;

         // Remove bad sale price and dup DocNum
         iRet = Tuo_FixDocTax(acCSalFile);
      }
   }

   // Load GrGr data
   if (iLoadFlag & LOAD_GRGR)          // -G
   {
      // Create Tuo_GrGr.dat, Tuo_GrGr.sls, Sale_Exp.dat and append to Tuo_sale_cum.dat
      LogMsg("Load %s GrGr file", myCounty.acCntyCode);

      GetIniString(myCounty.acCntyCode, "GrGrIn", "", acTmpFile, _MAX_PATH, acIniFile);
      if (strstr(acTmpFile, ".zip") )
         iRet = Tuo_LoadGrGr(myCounty.acCntyCode);
      else
         // Use following function to process GrGr not in the zip file
         iRet = Tuo_LoadGrGrNoZip(myCounty.acCntyCode);

      if (!iRet && !(iLoadFlag & LOAD_LIEN))
         iLoadFlag |= MERG_GRGR;       // Trigger merge grgr
      iRet = 0;
   }

   // Merge roll file
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN)) )
   {
      if (iLoadFlag & LOAD_LIEN)       // -L
         iRet = Tuo_LoadLien(myCounty.acCntyCode);
      else if (iLoadFlag & LOAD_UPDT)  // -U
      {
         iRet = Tuo_LoadUpdt(iSkip);
      }
   }
   
   // Apply cum sale to R01 file
   if (!iRet && (iLoadFlag & MERG_CSAL))
      iRet = ApplyCumSaleWP(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_UPD_SALE);
      //iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCSALREC, CLEAR_UPD_SALE);

   // Merge sale & GrGr data
   if (!iRet && (iLoadFlag & MERG_GRGR) )
   {
      // Merge sale data (GrGr_Exp.dat).  
      iRet = MergeGrGrFile(myCounty.acCntyCode);
      if (iRet > 0)
         iRet= 0;
   }

   // Fix old Doc# - One time
   //iRet = Tuo_FixDoc(iSkip);

   // Format DocLinks - Doclinks are concatenate fields separated by comma 
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN|MERG_GRGR)) )
      iRet = updateDocLinks(Tuo_MakeDocLink, myCounty.acCntyCode, iSkip, 1);
      //iRet = Tuo_FmtDocLinks(iSkip);

   // Process county files
   if (iLoadFlag & LOAD_ASSR)
      Tuo_ProcAssr();

   return iRet;
}
