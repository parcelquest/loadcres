
#if !defined(AFX_LOADCRES_H__B09D3D8D_F429_45B6_BACD_9BC0B309D0CF__INCLUDED_)
#define AFX_LOADCRES_H__B09D3D8D_F429_45B6_BACD_9BC0B309D0CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "hlAdo.h"

#define  ROLL_SIZE      899
#define  MAX_SUFFIX     256
#define  MAX_RECSIZE    4096
#define  MAX_FLD_TOKEN  256

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004
#define  NOTFOUND_ERR   0xF0000008
#define  BADRECSIZE_ERR 0xF0000010

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4
#define  UPDATE_SALES   8

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100
#define  LOAD_UNSC      0x00000010

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_VALUE     0x00000200
#define  EXTR_PRP8      0x00000020
#define  EXTR_CONF      0x00000002

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_GISA      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400
#define  MERG_PUBL      0x00000040
//#define  MERG_XFER      0x00000004
#define  MERG_ZONE      0x00000004

#define  EXTR_IVAL      0x80000000
#define  EXTR_ISAL      0x08000000
#define  EXTR_IGRGR     0x00800000

#define  UPDT_XSAL      0x00080000     // Merge sale from NDC Recorder data
#define  EXTR_NRSAL     0x00008000     // Extract sale from NDC Recorder data
#define  UPDT_ASSR      0x00000800

#define  CREOFF_PARCTYPE            1-1
#define  CREOFF_TRA                 5-1
#define  CREOFF_APN                 11-1
#define  CREOFF_5YR_PLAN            21-1
#define  CREOFF_PP_STMT             22-1
#define  CREOFF_PP_PENL             23-1
#define  CREOFF_INFL_FACTOR         24-1     // CPI Code
#define  CREOFF_BUS_TYPE            27-1
#define  CREOFF_NOTIFY              30-1
#define  CREOFF_EXE_CLAIM           31-1
#define  CREOFF_BASE_CODE           32-1
#define  CREOFF_USE_CODE            34-1
#define  CREOFF_MH_LIC              40-1
#define  CREOFF_NAME_1              47-1
#define  CREOFF_NAME_2              76-1
#define  CREOFF_ADDR_1              105-1
#define  CREOFF_ADDR_2              134-1
#define  CREOFF_ROLL_YEAR           163-1
#define  CREOFF_LAND_BASE_YEAR      167-1    // LAS � An �X� in the fourth digit means a partial interest transfer.
#define  CREOFF_PARTIAL_TRANSFER    170-1
#define  CREOFF_IMPROV_BASE_YEAR    171-1
#define  CREOFF_PENL_AMT            175-1
#define  CREOFF_ZIP                 182-1
#define  CREOFF_FILLER1             187-1
#define  CREOFF_ZIP_4               188-1
#define  CREOFF_DEFAULT_NO          192-1
#define  CREOFF_DEFAULT_DATE        197-1    // YYMMDD
#define  CREOFF_BLDG_CLS            203-1
#define  CREOFF_BEDROOMS            208-1
#define  CREOFF_BATHS               209-1    // 9V99
#define  CREOFF_STRUCT_SQFT         212-1
#define  CREOFF_LOT_SQFT            220-1
#define  CREOFF_APPRAISER_ID        229-1
#define  CREOFF_LABEL               232-1
#define  CREOFF_REAPP_CODE          233-1
#define  CREOFF_ASSIGN_DATE         235-1    // YYMMDD
#define  CREOFF_NOTICE_DATE         241-1    // YYMMDD
#define  CREOFF_REAPP_PCT           247-1
#define  CREOFF_RECORDING_DOC1      251-1
#define  CREOFF_RECORDING_DATE1     259-1
#define  CREOFF_RECORDING_DOC2      267-1
#define  CREOFF_RECORDING_DATE2     275-1
#define  CREOFF_RECORDING_DOC3      283-1
#define  CREOFF_RECORDING_DATE3     291-1
#define  CREOFF_DBA                 299-1
#define  CREOFF_LGL_DESC1           328-1
#define  CREOFF_LGL_DESC2           359-1
#define  CREOFF_BRD_ORDER_NO        390-1
#define  CREOFF_UNITS               396-1
#define  CREOFF_TOT_EDU             399-1
#define  CREOFF_SF_REC              407-1
#define  CREOFF_PROBLEM_NO          412-1
#define  CREOFF_MH_DECAL            416-1
#define  CREOFF_MH_SERIAL           424-1
#define  CREOFF_REAPP_DATE          439-1
#define  CREOFF_STMP_AMT            447-1
#define  CREOFF_FILLER7             458-1
#define  CREOFF_HLTV                464-1
#define  CREOFF_DIRECT_BILL         472-1
#define  CREOFF_EFF_YR              473-1
#define  CREOFF_YR_BUILT            477-1
#define  CREOFF_FLAG                481-1
#define  CREOFF_REAPP_REASON        482-1
#define  CREOFF_ADDR_CHG_DATE       486-1
#define  CREOFF_NAME_CHG_DATE       494-1
#define  CREOFF_VAL_CHG_DATE        502-1
#define  CREOFF_SITUS_STREETNO      510-1
#define  CREOFF_SITUS_STREETNAME    518-1
#define  CREOFF_NON_RES_NC          540-1
#define  CREOFF_NRNC_RBP            550-1
#define  CREOFF_SPECIAL_FEE1        560-1
#define  CREOFF_SPECIAL_FEE2        568-1
#define  CREOFF_BUS_ACCT            576-1
#define  CREOFF_BRD_ACTION          586-1
#define  CREOFF_SW_CHG              595-1
#define  CREOFF_TYPESQFT            605-1
#define  CREOFF_SPECIAL_FEE3        606-1
#define  CREOFF_SPECIAL_FEE4        614-1
#define  CREOFF_FIREPLACE           622-1
#define  CREOFF_AIR_COND            623-1
#define  CREOFF_FLOORS              624-1
#define  CREOFF_MISC_IMPROV         625-1
#define  CREOFF_GARAGE              626-1
#define  CREOFF_POOL                627-1
#define  CREOFF_SPA                 628-1
#define  CREOFF_HEATING             629-1
#define  CREOFF_SITUS_CITY          630-1
#define  CREOFF_COMMERCIAL_SQFT     660-1
#define  CREOFF_NEIGHBOR_CODE       666-1
#define  CREOFF_BOND_NO             669-1
#define  CREOFF_POSTPONE_FLG        675-1
#define  CREOFF_WORK_ORDER_NO       676-1
#define  CREOFF_WORK_ORDER_DATE     684-1
#define  CREOFF_PERNIT_NO           692-1
#define  CREOFF_APPR_REC_NO         703-1
#define  CREOFF_BASE_IMP            709-1
#define  CREOFF_BASE_LAND           719-1
#define  CREOFF_ZONE                729-1
#define  CREOFF_ROOMS               739-1
#define  CREOFF_ACRES               742-1
#define  CREOFF_NET_VAL             752-1
#define  CREOFF_LAND_VAL            762-1
#define  CREOFF_IMP_VAL             772-1
#define  CREOFF_PP_VAL              782-1
#define  CREOFF_EX_VAL              792-1
#define  CREOFF_FIXT_VAL            802-1
#define  CREOFF_EXEMP_CODE1         812-1
#define  CREOFF_EXEMP_CODE2         814-1
#define  CREOFF_EXEMP_CODE3         816-1
#define  CREOFF_EXEMP_CODE4         818-1
#define  CREOFF_EXEMP_VAL1          820-1
#define  CREOFF_EXEMP_VAL2          830-1
#define  CREOFF_EXEMP_VAL3          840-1
#define  CREOFF_EXEMP_VAL4          850-1
#define  CREOFF_MOBILE_VAL          860-1
#define  CREOFF_LEASE_VAL           870-1
#define  CREOFF_PERS_FIXT_VAL       880-1
#define  CREOFF_PERS_PEN_VAL        890-1

#define  CRESIZ_PARCTYPE            2
#define  CRESIZ_TRA                 6
#define  CRESIZ_APN                 10
#define  CRESIZ_BASE_CODE           2
#define  CRESIZ_USE_CODE            6
#define  CRESIZ_NAME_1              29
#define  CRESIZ_NAME_2              29
#define  CRESIZ_ADDR_1              29
#define  CRESIZ_ADDR_2              29
#define  CRESIZ_ROLL_YEAR           4
#define  CRESIZ_LAND_BASE_YEAR      4
#define  CRESIZ_IMPROV_BASE_YEAR    4
#define  CRESIZ_ZIP                 5
#define  CRESIZ_ZIP_4               4
#define  CRESIZ_DEFAULT_NO          5
#define  CRESIZ_DEFAULT_DATE        6
#define  CRESIZ_BLDG_CLS            5
#define  CRESIZ_BEDROOMS            1
#define  CRESIZ_BATHS               3
#define  CRESIZ_STRUCT_SQFT         8
#define  CRESIZ_LOT_SQFT            9
#define  CRESIZ_RECORDING_DATE      8
#define  CRESIZ_RECORDING_DOC       8
#define  CRESIZ_DBA                 29
#define  CRESIZ_LGL_DESC            31
#define  CRESIZ_LGL_DESC1           31
#define  CRESIZ_LGL_DESC2           31
#define  CRESIZ_BRD_ORDERNO         6
#define  CRESIZ_UNITS               3
#define  CRESIZ_TOT_EDU             8
#define  CRESIZ_SF_RECNO            5
#define  CRESIZ_PROBLEMNO           4
#define  CRESIZ_MH_DECAL            8
#define  CRESIZ_MH_SERNO            15
#define  CRESIZ_STMP_AMT            11
#define  CRESIZ_FILLER7             6
#define  CRESIZ_HLTV                8
#define  CRESIZ_EFF_YR              4
#define  CRESIZ_YR_BUILT            4
#define  CRESIZ_FLAG                1
#define  CRESIZ_SITUS_STREETNO      8
#define  CRESIZ_SITUS_STREETNAME    22
#define  CRESIZ_NON_RES_NC          10
#define  CRESIZ_TYPESQFT            1
#define  CRESIZ_SPECIAL_FEE3        8
#define  CRESIZ_SPECIAL_FEE4        8
#define  CRESIZ_FIREPLACE           1
#define  CRESIZ_AIR_COND            1
#define  CRESIZ_FLOORS              1
#define  CRESIZ_MISC_IMPROV         1
#define  CRESIZ_GARAGE              1
#define  CRESIZ_POOL                1
#define  CRESIZ_SPA                 1
#define  CRESIZ_HEATING             1
#define  CRESIZ_SITUS_CITY          30
#define  CRESIZ_S_CITY              22
#define  CRESIZ_S_STATE             2
#define  CRESIZ_S_ZIP               5
#define  CRESIZ_COMMERCIAL_SQFT     6
#define  CRESIZ_BASE_IMP            10
#define  CRESIZ_BASE_LAND           10
#define  CRESIZ_ZONE                10
#define  CRESIZ_ROOMS               3
#define  CRESIZ_ACRES               10
#define  CRESIZ_NET_VAL             10
#define  CRESIZ_LAND_VAL            10
#define  CRESIZ_IMP_VAL             10
#define  CRESIZ_PP_VAL              10
#define  CRESIZ_EX_VAL              10
#define  CRESIZ_FIXT_VAL            10
#define  CRESIZ_EXEMP_CODE          2
#define  CRESIZ_EXEMP_CODE1         2
#define  CRESIZ_EXEMP_CODE2         2
#define  CRESIZ_EXEMP_CODE3         2
#define  CRESIZ_EXEMP_CODE4         2
#define  CRESIZ_MOBILE_VAL          10
#define  CRESIZ_LEASE_VAL           10
#define  CRESIZ_PERS_FIXT_VAL       10
#define  CRESIZ_PERS_PEN_VAL        10
#define  CRESIZ_APPRAISER_ID        3
#define  CRESIZ_LABEL               1
#define  CRESIZ_REAPP_CODE          2
#define  CRESIZ_ASSIGN_DATE         6      // YYMMDD
#define  CRESIZ_NOTICE_DATE         6      // YYMMDD
#define  CRESIZ_REAPP_PCT           4
#define  CRESIZ_FLAG                1
#define  CRESIZ_REAPP_REASON        4
#define  CRESIZ_ADDR_CHG_DATE       8
#define  CRESIZ_NAME_CHG_DATE       8
#define  CRESIZ_VAL_CHG_DATE        8
#define  CRESIZ_DATE                8

/***********************************************************************************
 *
 * Redifile record layout
 *
 ***********************************************************************************/

typedef struct _tRedifile
{
   char  ParcType[CRESIZ_PARCTYPE];
   char  filler1[2];
   char  TRA[CRESIZ_TRA];
   char  APN[CRESIZ_APN];
   char  FiveYearPlan;
   char  PropStatement;
   char  PropPenalty;
   char  InflationFactorCode[3];
   char  BusTypeCode[3];
   char  NotifyFlag;
   char  ExemptFormType;
   char  BaseCode[CRESIZ_BASE_CODE];
   char  UseCode[CRESIZ_USE_CODE];
   char  MH_Lic[7];                                            // 40
   char  Name1[CRESIZ_NAME_1];                                 // 47
   char  Name2[CRESIZ_NAME_2];                                 // 75
   char  M_Addr1[CRESIZ_ADDR_1];                               // 105
   char  M_Addr2[CRESIZ_ADDR_2];                               // 134
   char  RollYr[CRESIZ_ROLL_YEAR];                             // 163
   char  LandBaseYr[CRESIZ_ROLL_YEAR];                         // 167
   char  ImprBaseYr[CRESIZ_ROLL_YEAR];                         // 171
   char  PenAmt[7];                                            // 175
   char  M_Zip[CRESIZ_ZIP];                                    // 182
   char  filler2;                                              // 187 -
   char  M_Zip4[CRESIZ_ZIP_4];                                 // 188
   char  DefaultNo[CRESIZ_DEFAULT_NO];                         // 192
   char  DefaultDate[CRESIZ_DEFAULT_DATE];                     // 197 MMDDYY
   char  BldgCls[CRESIZ_BLDG_CLS];                             // 203
   char  Beds[1];                                              // 208
   char  Baths[CRESIZ_BATHS];                                  // 209
   char  StruSqft[CRESIZ_STRUCT_SQFT];                         // 212
   char  LotSqft[CRESIZ_LOT_SQFT];                             // 220
   char  Appr_ID[CRESIZ_APPRAISER_ID];                         // 229
   char  Label[CRESIZ_LABEL];                                  // 232
   char  ReappCode[CRESIZ_REAPP_CODE];                         // 233
   char  AssignDate[CRESIZ_ASSIGN_DATE];                       // 235
   char  Noticedate[CRESIZ_NOTICE_DATE];                       // 241
   char  ReappPct[CRESIZ_REAPP_PCT];                           // 247
   char  RecBook1[CRESIZ_RECORDING_DATE+CRESIZ_RECORDING_DOC]; // 251
   char  RecBook2[CRESIZ_RECORDING_DATE+CRESIZ_RECORDING_DOC];
   char  RecBook3[CRESIZ_RECORDING_DATE+CRESIZ_RECORDING_DOC];
   char  Dba[CRESIZ_DBA];                                      // 299
   char  LglDesc1[CRESIZ_LGL_DESC];                            // 328
   char  LglDesc2[CRESIZ_LGL_DESC];
   char  BrdOrderNo[6];                                        // 390
   char  NumOfUnits[CRESIZ_UNITS];                             // 396
   char  Tot_Edu[CRESIZ_TOT_EDU];                              // 399
   char  SF_RecNo[CRESIZ_SF_RECNO];                            // 407
   char  ProblemNo[CRESIZ_PROBLEMNO];                          // 412
   char  MH_DecalNo[CRESIZ_MH_DECAL];                          // 416 
   char  MH_SerialNo[CRESIZ_MH_SERNO];                         // 424 
   char  Reappr_Date[CRESIZ_DATE];                             // 439
   char  StampAmt[CRESIZ_STMP_AMT];                            // 447
   char  filler7[CRESIZ_FILLER7];
   char  HLTV[CRESIZ_HLTV];                                    // 464
   char  DirectBill;                                           // 472
   char  YrEff[CRESIZ_EFF_YR];                                 // 473
   char  YrBlt[CRESIZ_EFF_YR];                                 // 477
   char  RecFlag[CRESIZ_FLAG];                                 // 481
   char  ReappReason[CRESIZ_REAPP_REASON];                     // 482
   char  AddrChg_Date[CRESIZ_ADDR_CHG_DATE];                   // 486
   char  NameChg_Date[CRESIZ_NAME_CHG_DATE];                   // 494
   char  ValChg_Date[CRESIZ_VAL_CHG_DATE];                     // 502
   char  S_StrNo[CRESIZ_SITUS_STREETNO];                       // 510
   char  S_StrName[CRESIZ_SITUS_STREETNAME];                   // 518
   char  filler9[82];                                          // 540
   char  Fp[1];                                                // 622
   char  Ac[1];                                                // 623
   char  Fl[1];                                                // 624
   char  MiscImpr[1];                                          // 625
   char  Garage[1];                                            // 626
   char  Pool[1];                                              // 627
   char  Spa[1];                                               // 628
   char  Heating[1];                                           // 629
   char  S_City[CRESIZ_SITUS_CITY];                            // 630
   //char  S_State[2];                                         // 652
   //char  S_Zip[5];                                           // 655
   char  CommSqft[CRESIZ_COMMERCIAL_SQFT];                     // 660
   char  NeighHood[3];                                         // 666
   char  BondNo[6];                                            // 669
   char  Postponement;
   char  WorkOrderNo[8];
   char  WorkOrderDate[8];
   char  Permit[11];
   char  Appr_RecNo[6];
   char  BaseImpr[CRESIZ_BASE_IMP];                            // 709
   char  BaseLand[CRESIZ_BASE_LAND];                           // 719
   char  Zone[CRESIZ_ZONE];                                    // 729
   char  Rooms[CRESIZ_ROOMS];                                  // 739
   char  Acres[CRESIZ_ACRES];                                  // 742
   char  NetVal[CRESIZ_NET_VAL];                               // 752
   char  LandVal[CRESIZ_LAND_VAL];                             // 762
   char  ImprVal[CRESIZ_IMP_VAL];                              // 772
   char  PPVal[CRESIZ_PP_VAL];                                 // 782
   char  ExVal[CRESIZ_EX_VAL];                                 // 792
   char  FixtVal[CRESIZ_FIXT_VAL];                             // 802
   char  ExeCode1[CRESIZ_EXEMP_CODE];                          // 812
   char  ExeCode2[CRESIZ_EXEMP_CODE];
   char  ExeCode3[CRESIZ_EXEMP_CODE];
   char  ExeCode4[CRESIZ_EXEMP_CODE];
   char  ExeVal1[CRESIZ_EX_VAL];                               // 820
   char  ExeVal2[CRESIZ_EX_VAL];                               // 830
   char  ExeVal3[CRESIZ_EX_VAL];                               // 840
   char  ExeVal4[CRESIZ_EX_VAL];                               // 850
   char  MobileVal[CRESIZ_MOBILE_VAL];                         // 860
   char  LeaseVal[CRESIZ_LEASE_VAL];                           // 870
   char  PersFixtVal[CRESIZ_PERS_FIXT_VAL];                    // 880
   char  PersPenVal[CRESIZ_PERS_PEN_VAL];                      // 890
} REDIFILE;

int   CreateR01Rec(char *pCnty, char *pOutbuf, char *pRollRec, int iLen);
int   Cres_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend=false);
int   Cres_ExtrSale1(char *pRollFile, int iRollLen);
int   Cres_ExtrLien(char *pCnty);
int   Cres_ExtrLienLF(char *pCnty);
void  Cres_CreateLienRec(char *pOutbuf, char *pRollRec);
int   Cres_UpdCumSale(void);
int   Cres_UpdSale1(char *pOutFile=NULL);
//bool  sqlConnect(LPCSTR strProvider, LPCSTR strDb, hlAdo *phDb=NULL);
//int   execSqlCmd(LPCTSTR strCmd, hlAdo *phDb=NULL);
int   Cres_Load_TaxBase(bool bImport);
int   Cres_Load_TaxDelq(bool bImport);
int   Cres_Load_TaxOwner(bool bImport, int iLRecLen=0);
int   Cres_Load_Cortac(bool bImport, bool bCreateDelq=false);
int   Cres_Load_GFGIS(bool bImport, bool bCreateBase=false);

//int   doTaxImport(char *pCnty, int iType);
int   doSaleImport(char *pCnty, int iType);

#endif // !defined(AFX_LOADCRES_H__B09D3D8D_F429_45B6_BACD_9BC0B309D0CF__INCLUDED_)

