#ifndef _MERGETRI_H
#define _MERGETRI_H

IDX_TBL4 Tri_Vesting[] =
{
   " ETUX",       "EU", 5, 2,
   " ET UX",      "EU", 6, 2,
   " ETAL",       "EA", 5, 2,
   " ET AL",      "EA", 6, 2,
   " TSTE",       "TR", 5, 2,
   " SUCC TR",    "SU", 8, 2,    // Successor Trust
   " SUC TRS",    "SU", 8, 2,
   " TRUSTE",     "TR", 7, 2,
   " TR ",        "TR", 4, 2,
   " TRS",        "TR", 4, 2,
   " TSTES",      "TR", 6, 2,
   " CO-TR",      "TR", 6, 2,
   " CO TRS",     "TR", 7, 2,
   " COTRST",     "TR", 7, 2,
   " ESTATE OF",  "LE", 7, 2,
   " LIFE EST",   "LE", 9, 2,
   " L/E",        "LE", 4, 2,
   " FAMILY TR",  "FM", 10,2,
   " FAM TR",     "FM", 8, 2,
   " FMLY TR",    "FM", 8, 2,
   " DVA",        "DV", 4, 2,
   "", "", 0, 0
};

// If not match Vesting table, check last word using this table
IDX_TBL4 Tri_VestingLW[] =
{
   " L EST",      "LE", 6, 2,
   " ESTATE",     "LE", 7, 2,
   " HWCP",       "CP", 5, 2,
   " HWJT",       "JT", 5, 2,
   " TR",         "TR", 3, 2,
   " CP",         "CP", 3, 2,
   " JT",         "JT", 3, 2,
   " TC",         "TC", 3, 2,
   "", "", 0, 0
};

#endif