/**************************************************************************
 *
 *  Usage: LoadCres -C<County code> [-A] [-D] [-G] [-M] [-N] [-U] [-L] [-X?] [-S<n>]
 *          -A : Merge attribute file
 *          -D : Duplicate sale records from old file
 *          -Dn: Do not create update file (for debug only)\n");
 *          -G : Load GrGr file 
 *          -Gi: Create GrGr sql import file
 *          -Is: Import cumumative sale to SQL (???_Sale.sls)
 *          -Ig: Import cumumative GrGr to SQL (???_Grgr.sls)
 *          -Mg: Merge GrGr file
 *          -Ms: Merge cum sale file
 *          -L : Load Lien date roll (create lien file and load roll).
 *          -La: Create cd-assessor file
 *          -Lc: Load characteristic file
 *          -Lg: Load GrGr file
 *          -Lr: Load LDR roll file
 *          -Ls: Load sale file
 *          -N : Do not encode suffix and city
 *          -U : Update roll file using old S01 file
 *          -X8: Extract Prop8 parcels
 *          -Xa: Extract attribute from attr file.
 *          -Xc: Extract cumulative sale from mainframe sale file.
 *          -Xd: Extract DTW file for Dave (1900+899).
 *          -Xg: Extract GrGr data.
 *          -Xl: Extract lien value from lien file.
 *          -Xs: Extract sale data for MergeAdr from sale file.
 *          -Xx: Exception load of confidential roll.
 *          -Sn: Number of records skip (default 1)
 *
 * Notes:
 *   - So far only TUO has GRGR data.
 *   - DNX has confidential data (CHAR) but not allow to distribute yet. 10/17/2014
 *   - Only ALP, INY, LAS, SIE, & TRI are on CRES system.  DNX, MOD, TUO have moved to MB.
 *
 * History:
 * 08/12/2005 1.3.0    Standardize load options with LoadOne - spn
 * 08/17/2005 1.3.1    Get LoadLien work for MOD
 * 05/16/2006 1.7.0    Reset automation flag State='W' before processing and set
 *                     it to 'P' if successful, 'F' if fail
 * 05/02/2007 1.8.7    Move ALP processing into its own module MergeAlp.cpp
 * 09/14/2007 1.8.18   Standardize sale and lien extract.
 * 11/07/2007 1.8.22   Fix bug in GLE by sort input file before processing.
 * 12/16/2007 1.9.0    Use new logic to update sale data.  This applies to all 
 *                     CRES counties.  We now should take all sale records from
 *                     REDIFILE even if they occur on the same date.
 *                     Adding function Cres_UpdCumSale().
 * 12/18/2007 1.9.1    Modify MergeGrGrFile() to call MergeGrGrExp() instead of
 *                     MergeSale().
 * 01/23/2008 1.10.2   Adding code to support standard use code
 * 02/27/2008 1.10.5.3 Move DNX and TRI processing to loadDnx() and loadTri().
 *                     This makes it easier to customize their requirements.
 * 05/01/2008 1.10.6   Modify Cres_ExtrSale1() to support ALP new DocNum format.
 * 05/30/2008 1.11.0   Update county table LastRecDate, LastFileDate (roll), and LastGrGrDate.
 * 06/06/2008 1.11.2   Fix SalePrice in Cres_ExtrSale1() for TRI.
 * 06/10/2008 1.12.0   Remove DNX and TEH from Cres_ExtrSale1() and add email option.
 * 06/25/2008 8.0      Change versioning system.  Major version if the last two digit
 *                     of roll year.  So 2008 version starts with 8.0.
 * 10/13/2008 8.4.0    Verify record count.
 * 12/10/2008 8.5.0    Set County.Status='W' when process a county.  This prevents 
 *                     ChkCnty program from running it in parallel.  
 * 01/03/2009 8.5.2    Do not set file date.  This will be done in ChkCnty.
 * 03/25/2009 8.6      Add -La option
 * 10/14/2009 9.1.5    If loading a county with more than one assessor products,
 *                     set lAssrRecCnt=999999999 so status flag will be set properly for BuildCda.
 * 02/24/2010 9.3.0    Add default sendmail flag.  This can be overide by command line option.
 * 06/21/2010 10.0.0   Add -Mg and -Ms option to prepare for LDR 2010.
 * 07/02/2010 10.1.1   Drop processing MOD from main().  Calling loadMod() instead.
 *                     Change my mind, keep process MOD in main() until loadMod() is tested
 *                     with normal update.
 * 07/07/2010 10.2.0   Update LastRecsChg to County table
 * 10/10/2010 10.6.0   Fix number of output records in LoadUpdtRoll().
 * 11/08/2010 10.7.0   Add processing date to log file.
 * 06/28/2011 11.0.0   Add Cres_UpdSale1() to extract sale1 from redifile and append it
 *                     to cum sale file ???_Sales.sls.
 * 07/14/2011 11.0.1   Add -Ynnnn option for loading specific LDR year. Change log file
 *                     name to include LDR year.
 * 07/22/2011 11.1.2   Update TRANSFER via MergeCumSale1().
 * 10/10/2011 11.2.5   Remove -Xd obsolete option to extract data for DATAWARE product.
 *                     Add -Xsi option to export cum sale for SQL import.
 *                     Add Cres_ExtrSale() to extract all sales from roll file to SCSAL_REC.
 * 10/21/2011 11.2.8   Modify Cres_UpdSale1() to remove counties that has their own
 *                     version to easily customize.  Add -Xi option.
 * 11/10/2011 11.2.10  Add doSaleImport() & doTaxImport() to do bulk import into SQL.
 *                     -Xsi option now can import directly into SQL if setting AutoImport=Y.
 *                     Add -T option to load tax files. Add Cres_Load_TaxBase() to create
 *                     data for TaxBase & TaxDetail tables and Cres_Load_TaxDelq() for TaxDelq table.
 * 12/29/2011 11.4.0   Change tax processing functions due to TAXBASE & TAXDETAIL fields change.
 * 03/12/2012 11.5.0   Copy doSaleImport() from LoadOne.cpp over to import GrGr data.
 *                     Replace bAutoImport with bSaleImport, bGrgrImport, bTaxImport, bMixSale.
 * 05/16/2012 11.5.1   Populate stamp amount and change sort params in Cres_UpdSale1() and Cres_ExtrSale().
 * 05/17/2012 11.5.2   Add fake tax code for Agency table. When data available, 
 *                     modify Cres_ParseTaxAgency() to populate correct data.
 * 07/17/2012 12.1.3   Fix Cres_CreateLienRec() where wrong value was put in acPP_Val.
 * 08/31/2012 12.2.0   Add Cres_ExtrProp8() and option -X8 to extract pro8 parcels (TUO).
 * 11/16/2012 12.3.0   Add functions to load unsecured files.
 * 01/02/2013 12.4.0   Update DocLink to sale history table.
 * 01/09/2013 12.5.0   Add option to create DocLink for sale history import file.
 * 02/14/2013 12.6.0   Add DocLink to GrGr history table.
 * 06/03/2013 12.6.4   Add -Mp option to include unassessed parcels (MOD)
 * 07/24/2013 13.1.2   Add option to output unknown Usecode to specific file
 * 07/29/2013 13.2.3   Create flg file on successful.
 * 09/27/2013 13.3.0   Modify initCounty() to load Vesting tables
 * 10/02/2013 13.3.3   Replace loadVestingTbls() with loadVesting() since tables have been combines.
 * 01/27/2014 13.5.0   Remove -Lu option and related code dealing with unsecured data.
 * 02/03/2014 13.5.1   Adding SaleCode to Cres_UpdSale1() and allow caller to specify
 *                     output file. Default output is still cum sale file.
 * 09/12/2014 14.3.0   If load failed, ignore all import tasks.
 * 10/15/2014 14.4.0   Add -Ig option to allow import GRGR data.  Tested with TUO.
 * 10/29/2014 14.5.0   Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 02/04/2015 14.6.0   Replace GetIniString() with GetIniString() which will translate [Machine]
 *                     to local machine name and [CO_DATA] with CO_DATA token defined in INI file.
 * 05/26/2015 14.7.0   Add -Xv option to load value file
 * 10/12/2015 15.2.0   Add fdGrGr and make it global.
 * 12/03/2015 15.2.1   Add Cres_Load_TaxOwner(), modify Cres_ParseTaxBase() to add owner info.
 * 12/07/2015 15.3.0   Modify Cres_Load_TaxOwner() to take record length as input since
 *                     lien file may have different length.  Also allow county to have
 *                     different delimiter for tax file in Cres_Load_TaxBase().
 * 02/13/2016 15.6.0   Replace -Nu with -Dn: don't create chk file.
 * 03/07/2016 15.8.0   Modify Cres_Load_TaxBase() to output Items.csv instead of Detail.csv
 * 03/09/2016 15.8.0   Automatically update last roll file date unless specify SetFileDate=N under county section.
 *                     Rename initCounty() to MergeInit() to make it compatible with other load program.
 *                     Add lLastTaxFileDate and update to County table.
 * 04/14/2016 15.9.1   Add Cres_Load_Cortac() and Cres_Load_GFGIS().  Modify Cres_ParseDelq() to support
 *                     both SABSDATA (INY) & SABSDATA.WWW (LAS).
 * 04/16/2016 15.9.1.1 Modify Cres_ParseCortac() and Cres_Load_Cortac() to add option to import DELQ also.
 * 07/13/2016 16.1.0   Remove sqlConnect() and use new generic  version from SqlExt.xpp,  Set SQL timeout
 *                     value to SqlExt:m_iTimeOut in MergeInit().  Add doValueImport() to import value to SQL.
 * 07/20/2016 16.0.1   Add option to load ???City.N2CX
 * 08/16/2016 16.2.0   Remove GLE and TEH from LoadCres.  Modify Cres_ParseTaxDetail() & Cres_ParseTaxAgency
 *                     to update TaxCode from external table.  Modify Cres_Load_TaxBase() to add DelqYear to
 *                     Tax_Base table.  Add function Cres_UpdateDelqYear().
 * 09/14/2016 16.3.0   Add lTaxYear which can be defined in INI file for global use.  Default to lLienYear.
 * 10/01/2016 16.3.2   Fix bug in MergeInit() that set wrong LienYear for INY
 * 10/13/2016 16.4.0   Modify Cres_ParseTaxAgency() to use standard function Tax_CreateAgencyCsv()
 * 11/07/2016 16.6.0   Modify Cres_Load_GFGIS() to create TAXBASE if requested.
 * 01/18/2017 16.8.1   Fix bug that add 10 to BillNum in Cres_ParseGFGIS() - TRI.
 *                     Skip items that have no tax amt.
 * 02/07/2017 16.9.0   Modify Cres_ParseCortac(), Cres_ParseGFGIS() to set tax amt and tax rate correctly with negative value.
 *                     Add exception in Cres_ParseGFGIS() set tac code "999" for "PENALTY ASMT" in "INY"
 * 03/04/2017 16.10.0  Modify Cres_ParseTaxDetail() & Cres_ParseTaxAgency() & Cres_ParseGFGIS() to add TC_Flag to Items 
 *                     and Agency output and use findExactTaxDist() instead of findTaxDist().
 * 03/26/2017 16.12.0  Update tax import date as well as last tax file date.  Always check for new file
 *                     before processing.
 * 04/02/2017 16.12.2  Add global iToken.
 * 05/22/2017 16.13.2  Fix BillNum in Cres_CreateBaseWithGFGIS()
 * 06/16/2017 16.14.0  Modify Cres_ParseGFGIS() to set tax rate to 0 when not available. (ALP,INY,TRI)
 * 06/30/2017 17.0.0   Do not set county status when loading tax alone to avoid messing up production automation.
 * 07/12/2017 17.0.0.1 Set Status=R before exit except when loading tax.
 * 11/17/2017 17.1.0   Modify Cres_ParseDelq() to update DelqStatus.  Remove Delq update from Cres_Load_TaxBase().
 * 12/29/2017 17.3.0   Modify Cres_Load_Cortac() & Cres_ParseCortac() to populate TRA.
 *                     Modify Cres_CreateBaseWithGFGIS() to populate TotalDue.
 * 05/04/2018 17.6.0   Modify Cres_Load_Cortac() to update delq info in TaxBase if bCreateDelq is true.
 *                     Modify _tmain() not to set automation flag to "W" when loading tax update.
 *                     Add more Delq Status to Cres_Load_Cortac().
 * 05/08/2018 17.7.0   Fix redemption amt in Cres_ParseDelq().  Set isSecd to TaxBase in Cres_ParseCortac().
 *                     Modify Cres_ParseDelq() to set isDelq=1 on Installment delinquent (ParcelType=81)
 * 05/10/2018 17.8.0   Modify Cres_Load_GFGIS() to check input file date only when bCreateBase is true.
 *                     Set BillType=BILLTYPE_SECURED in ParseTaxBase().
 * 06/25/2018 17.9.0   Add addTaxAgency().  Modify Cres_ParseGFGIS() to update asTaxDist[] list on the fly.
 * 09/29/2018 18.1.0   Add -Mr option to merge lot area
 * 10/17/2018 18.2.0   Modify Cres_ParseTaxAgency() to format phone number
 * 11/05/2018 18.3.0   Modify Cres_ParseGFGIS() to search for agency using tax code first,
 *                     then agency.  Same agency (name cutoff) may have different tax code and 
 *                     same tax code may have different agency name (spelling).
 * 12/11/2018 18.4.0   Modify Cres_ParseTaxBase() & Cres_ParseCortac() to initialize Inst?Status.
 * 01/08/2019 18.5.0   Remove all code related to DNX, MOD, & TUO since they are not in LoadMB.
 * 06/28/2019 19.0.0   Remove SIE (move to MB). 
 * 07/16/2019 19.1.0   Add Cres_ExtrLienLF() to support REDIFILE_NL with LF.
 * 07/24/2019          Modify Cres_ExtrLien() to increase buffer size.
 * 11/04/2019 19.3.0   Modify Cres_Load_GFGIS() to update tb_TotalRate by calling doUpdateTotalRate()
 * 01/14/2020 19.5.0   Modify ParseCmd() to add -Ut option.
 * 04/27/2020 19.6.0   Remove -Ut and use -T to load both full & partial tax file.
 * 06/14/2020 19.6.1   Add -Xn & -Mn options
 * 06/24/2020 20.0.0   Add -Mz and modify Cres_CreateLienRec() to add EXE_CODE even without EXE_AMT
 *
 * If SalePrice and StampAmt are no longer mixed up, remove special handling
 *
 **************************************************************************/

#include "stdafx.h"
#include "hlAdo.h"
#include "Prodlib.h"
#include "Logs.h"
#include "getopt.h"
#include "CountyInfo.h"
#include "Tables.h"
#include "R01.h"
#include "RecDef.h"
#include "doSort.h"
#include "doOwner.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "FormatApn.h"
#include "SaleRec.h"
#include "LoadCres.h"

#include "UseCode.h"
#include "Update.h"
#include "PQ.h"
#include "Cres_TaxInfo.h"
#include "Tax.h"
#include "Sendmail.h"
#include "LoadValue.h"
#include "SqlExt.h"

CWinApp theApp;
using namespace std;

char  acRawTmpl[_MAX_PATH], acFlgTmpl[_MAX_PATH], acGrGrTmpl[_MAX_PATH], acESalTmpl[_MAX_PATH];
char  acRollFile[_MAX_PATH], acUnsRollFile[_MAX_PATH], acLogFile[_MAX_PATH], acIniFile[_MAX_PATH], acSaleTmpl[_MAX_PATH];
char  acCntyTbl[_MAX_PATH], acTaxFile[_MAX_PATH], acSaleFile[_MAX_PATH], acCharFile[_MAX_PATH], acValueFile[_MAX_PATH];
char  acTmpPath[_MAX_PATH], acDocPath[_MAX_PATH], acLienTmpl[_MAX_PATH], acEGrGrTmpl[_MAX_PATH];
char	acCSalFile[_MAX_PATH], acRollSale[_MAX_PATH], acToday[16], acPubParcelFile[_MAX_PATH], acCChrFile[_MAX_PATH];
char  sTCTmpl[_MAX_PATH], sTCMTmpl[_MAX_PATH], sTaxTmpl[_MAX_PATH], sRedTmpl[_MAX_PATH], 
      sBaseTmpl[_MAX_PATH], sTFTmpl[_MAX_PATH], sTaxOutTmpl[_MAX_PATH], sSuplTmpl[_MAX_PATH],
      sImportLogTmpl[_MAX_PATH], acProp8Tmpl[_MAX_PATH];

char  *apTokens[MAX_FLD_TOKEN], cDelim;

int   iRecLen, iAsrRecLen, iRollLen, iApnLen, iSaleLen, iTaxLen, iCharLen, iTokens, iLoadTax,
      iSkip, iNoMatch, iLoadFlag, iGrGrApnLen, iMaxLegal, iMaxChgAllowed;
bool  bEnCode, bUseSfxXlat, bCopySales, bOverwriteLogfile, bDebug, bDontUpd, bSendMail, bFixTRA, bFixLienExt,
      bSaleImport, bGrgrImport, bTaxImport, bMixSale, bSetLastFileDate;
long  lRecCnt, lLastRecCnt, lAssrRecCnt, lLastRecDate, lLienDate, lToday, lToyear, lLienYear, lTaxYear;
long  lCharSkip, lSaleSkip, lTaxSkip, lLastFileDate, lLastGrGrDate, lLastTaxFileDate;
long  lSaleMatch, lTaxMatch, lCharMatch, lLDRRecCount;

hlAdo    hl;
hlAdoRs  m_AdoRs;

bool     m_bConnected;
FILE     *fdRoll, *fdSale, *fdTax, *fdChar, *fdCSale, *fdLienExt, *fdGrGr, *fdDelq, *fdSitus;
XREFTBL  asDeed[MAX_DEED_ENTRIES];
int      iNumDeeds;
XREFTBL  asInst[MAX_INST_ENTRIES];
int      iNumInst;
XREFTBL  asTRA[MAX_TRA_ENTRIES];
int      iNumTRA;

COUNTY_INFO myCounty;

IDX_TBL2 asDocType[] =
{
   0,  "   ",
   1,  "   ",
   2,  "   ",
   3,  "   ",
   4,  "   ",
   5,  "   ",
   6,  "   ",
   7,  "   ",
   8,  "   ",
   9,  "   ",
   10, "   ",
   11, "   ",
   12, "   ",
   13, "   ",
   14, "32 ",
   15, "   ",
   16, "   ",
   17, "   ",
   18, "   ",
   19, "   ",
   20, "   ",
   21, "1  ",
   22, "   ",
   23, "   ",
   24, "   ",
   25, "   ",
   26, "   ",
   27, "   ",
   28, "   ",
   29, "   ",
   30, "   ",
   31, "   ",
   32, "   ",
   33, "   ",
   34, "   ",
   35, "   ",
   36, "   ",
   37, "   ",
   38, "   ",
   39, "   ",
   40, "   ",
   41, "   ",
   42, "   ",
   43, "   ",
   44, "   ",
   45, "   ",
   46, "   ",
   47, "   ",
   48, "   ",
   49, "   ",
   50, "   ",
   51, "   ",
   52, "   ",
   53, "   ",
   54, "   ",
   55, "   ",
   56, "   ",
   57, "   ",
   58, "   ",
   59, "   ",
   60, "   ",
   61, "   ",
   62, "   ",
   63, "   ",
   64, "   ",
   65, "   ",
   66, "   ",
   67, "   ",
   68, "   ",
   69, "   ",
   70, "   ",
   71, "   ",
   72, "   ",
   73, "   ",
   74, "   ",
   75, "   ",
   76, "   ",
   77, "   ",
   78, "   ",
   79, "   ",
   80, "   ",
   81, "   ",
   82, "   ",
   83, "   ",
   84, "   ",
   85, "   ",
   86, "67 ",
   87, "   ",
   88, "   ",
   89, "   ",
   90, "   ",
   91, "27 ",
   92, "   ",
   93, "   ",
   94, "   ",
   95, "   ",
   96, "   ",
   97, "8  ",
   98, "   ",
   99, "   "
};

int   Las_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iInitFlg);
int   Dnx_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iInitFlg);
int   Mod_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iInitFlg);
//int   Gle_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iInitFlg);
//int   Teh_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iInitFlg);
int   Tri_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iInitFlg);

int   loadAlp(int, int);
//int   loadDnx(int, int);
//int   loadGle(int, int);
int   loadIny(int, int);
int   loadLas(int, int);
//int   loadMod(int, int);
int   loadSie(int, int);
//int   loadTeh(int, int);
int   loadTri(int, int);
//int   loadTuo(int, int);

void Alp_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
void Alp_Fmt2Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
void Iny_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
void Sie_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
//void Dnx_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
//void Gle_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
//void Mod_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
//void Teh_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
void Tri_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
void Las_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);
//void Tuo_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc);

//void Tuo_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate);

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/********************************* sqlConnect *******************************
 *
 *
 ****************************************************************************/

//bool sqlConnect(LPCSTR strProvider, LPCSTR strDb, hlAdo *phDb)
//{
//   bool bRet = true;
//   char acServer[256], acTmp[256];
//
//   try
//   {
//      // open the database connection
//      GetIniString("Database", strProvider, "", acServer, 256, acIniFile);
//      if (acServer[0] == '\0')
//         return false;
//
//      if (strDb && *strDb > ' ')
//         sprintf(acTmp, acServer, strDb);
//      else
//         sprintf(acTmp, acServer, "Production");
//
//      LogMsg("Connecting to %s", acTmp);
//      if (phDb)
//         bRet = phDb->Connect(acTmp);
//      else
//      {
//         bRet = hl.Connect(acTmp);
//         m_bConnected = bRet;
//      }
//   }
//
//   // catch any ADO errors
//   AdoCatch(e)
//   {
//      LogMsg("***** SQL connect error: %s", ComError(e));
//      bRet = false;
//   }
//
//   return bRet;
//}

/******************************** getCountyInfo *****************************
 *
 * Retrieve info from SQL server
 *
 ****************************************************************************/

int getCountyInfo()
{
   char  acTmp[256], acServer[256];
   int   iRet;

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return -1;

   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);

      sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s'", myCounty.acCntyCode);
      m_AdoRs.Open(hl, acTmp);

      if (m_AdoRs.next())
      {
         CString	sTmp = m_AdoRs.GetItem("LastRecCount");
         myCounty.iLastRecCnt = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Delay");
         myCounty.iGrGrDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("GrGr_Freq");
         myCounty.GrGrFreq = sTmp.GetAt(0);
         sTmp = m_AdoRs.GetItem("Roll_Delay");
         myCounty.iRollDelay = atoi(sTmp);
         sTmp = m_AdoRs.GetItem("Roll_Freq");
         myCounty.RollFreq = sTmp.GetAt(0);
         m_AdoRs.Close();
      }
      iRet = 0;
   } AdoCatch(e)
   {
      LogMsg("***** Error exec cmd: %s", acTmp);
      LogMsgD("%s\n", ComError(e));
      iRet = -1;
   }

   return iRet;
}

/***************************** updateProcessFlags ***************************
 *
 * Update LastBldDate, LastRecDate, RecCount, State
 *
 ****************************************************************************/

bool updateTable(LPCTSTR strCmd)
{
   bool bRet = false;
   char acServer[256];

   GetIniString("Database", "Provider", "", acServer, 128, acIniFile);
   if (acServer[0] == '\0')
      return bRet;

   LogMsg((LPSTR)strCmd);
   try
   {
      // open the database connection
      if (!m_bConnected)
         m_bConnected = hl.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return bRet;
   }

   // Update county profile table
   int iRet;
   iRet = execSqlCmd(strCmd);
   if (!iRet)
      bRet = true;
   return bRet;
}

/********************************* getDate() ********************************
 *
 * Return current date + number of extended day.
 *
 ****************************************************************************/

void getDate(char *pDate, long lExtend)
{
   CString strTmp;

   CTime today = CTime::GetCurrentTime();
   CTimeSpan tsExtend(lExtend);

   today += tsExtend;
   strcpy(pDate, today.Format("%Y%m%d"));
}

/******************************** LoadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadCountyInfo(char *pCntyCode, char *pCntyTbl)
{
   char     acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS], acProvider[255];
   int      iRet=0, iTmp;
   FILE     *fd;
   hlAdoRs  myRs;

   GetIniString("Database", "Provider", "", acProvider, 255, acIniFile);      // Get provider template
   if (!sqlConnect(acProvider, "Production"))
      return -1;

   sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s' ", pCntyCode);
   try
   {
      myRs.Open(hl, acTmp);
   } AdoCatch(e)
   {
      LogMsg("***** Error executing command [%s] : %s", acTmp, ComError(e));
      return -1;
   }

   if (myRs.next())
   {
      CString  sTmp;

      //sTmp = myRs.GetItem("LastRecDate");
      //lLastRecDate = atol(sTmp);
      sTmp = myRs.GetItem("LastRecCount");
      lLastRecCnt = atol(sTmp);
   }
   myRs.Close();

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            if (!memcmp(acTmp, pCntyCode, 3))
            {
               iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
               if (iTmp > 0)
               {
                  strcpy(myCounty.acCntyCode, pCntyCode);
                  strcpy(myCounty.acStdApnFmt, pFlds[FLD_APN_FMT]);
                  strcpy(myCounty.acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
                  strcpy(myCounty.acCase[0], pFlds[FLD_SPC_BOOK]);
                  strcpy(myCounty.acCntyID, pFlds[FLD_CNTY_ID]);
                  strcpy(myCounty.acCntyName, pFlds[FLD_CNTY_NAME]);
                  strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD]);

                  iRet = atoi(myCounty.acCntyID);
                  myCounty.iCntyID = iRet;
                  sprintf(myCounty.acFipsCode, "06%.3d", iRet*2-1);
                  myCounty.iApnLen = atoi(pFlds[FLD_APN_LEN]);
                  myCounty.iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
                  myCounty.iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
                  myCounty.iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
                  iRet = 1;
               } else
               {
                  LogMsg("***** Bad county table file: %s", pCntyTbl);
               }

               break;
            }
         } else
            break;
      }

      fclose(fd);

      if (1 != iRet)
         LogMsg("***** County not found.  Please verify %s", pCntyTbl);
   } else
      LogMsg("***** Error opening county table %s", pCntyTbl);

   return iRet;
}

/********************************** MergeInit ********************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int MergeInit(char *pCnty)
{
   char  acTmp[_MAX_PATH];
   char  acCityFile[_MAX_PATH], acUseTbl[_MAX_PATH];

   int   iRet;

   // Get raw file name
   GetIniString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   iRecLen = GetPrivateProfileInt("Data", "RecSize", 1900, acIniFile);

   // Flag template if flag file needed
   GetIniString("Data", "FlgFile", "", acFlgTmpl, _MAX_PATH, acIniFile);

   // Get roll file name
   bSetLastFileDate = true;
   if (iLoadFlag & LOAD_LIEN)
   {
      GetIniString(pCnty, "LienFile", "", acRollFile, _MAX_PATH, acIniFile);
      iRollLen = GetPrivateProfileInt(pCnty, "LienRecSize", 0, acIniFile);
      if (!iRollLen)
         iRollLen = GetPrivateProfileInt(pCnty, "RollRecSize", 0, acIniFile);
   } else
   {
      GetIniString(pCnty, "RollFile", "", acRollFile, _MAX_PATH, acIniFile);
      iRollLen = GetPrivateProfileInt(pCnty, "RollRecSize", 0, acIniFile);
   }

   if (_access(acRollFile, 0))
   {
      LogMsg("***** ERROR: missing input file %s\n", acRollFile);
      return -99;
   }

   if (iLoadFlag & LOAD_UPDT)
   {
      GetPrivateProfileString(pCnty, "SetFileDate", "Y", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'N')
         bSetLastFileDate = false;
   }
   lLastFileDate = getFileDate(acRollFile);

   // Unsecured Roll File
   if (iLoadFlag & LOAD_UNSC)
   {
      GetIniString(pCnty, "UnsRollFile", "", acUnsRollFile, _MAX_PATH, acIniFile);
   }

   GetIniString(pCnty, "SaleFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acSaleFile, acTmp, pCnty, pCnty);
   iSaleLen = GetPrivateProfileInt(pCnty, "SaleRecSize", 0, acIniFile);

   GetIniString(pCnty, "TaxFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acTaxFile, acTmp, pCnty, pCnty);
   iTaxLen = GetPrivateProfileInt(pCnty, "TaxRecSize", 0, acIniFile);

   GetIniString(pCnty, "CharFile", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acCharFile, acTmp, pCnty, pCnty);
   iCharLen = GetPrivateProfileInt(pCnty, "CharRecSize", 0, acIniFile);

   // Cumulative Char file
   GetIniString(pCnty, "CChrFile", "", acCChrFile, _MAX_PATH, acIniFile);
   if (!acCChrFile[0])
   {
      GetIniString("Data", "CChrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCChrFile, acTmp, pCnty, pCnty);
   }

   fdRoll = (FILE *)NULL;
   fdSale= (FILE *)NULL;
   fdTax  = (FILE *)NULL;
   fdChar  = (FILE *)NULL;

   // Get GrGr output template
   GetIniString(pCnty, "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);
   if (!acGrGrTmpl[0])
      GetIniString("Data", "GrGrOut", "", acGrGrTmpl, _MAX_PATH, acIniFile);

   // Get Sale output template
   GetIniString(pCnty, "SaleOut", "", acSaleTmpl, _MAX_PATH, acIniFile);
   if (!acSaleTmpl[0])
      GetIniString("Data", "SaleOut", "", acSaleTmpl, _MAX_PATH, acIniFile);

   // Sale Rec export template
   GetIniString(pCnty, "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);
   if (!acESalTmpl[0])
      GetIniString("Data", "SaleExp", "", acESalTmpl, _MAX_PATH, acIniFile);

   // GrGr Rec export template
   GetIniString(pCnty, "GrGrExp", "", acEGrGrTmpl, _MAX_PATH, acIniFile);
   if (!acEGrGrTmpl[0])
      GetIniString("Data", "GrGrExp", "", acEGrGrTmpl, _MAX_PATH, acIniFile);

   // Lien template name
   GetIniString(pCnty, "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);
   if (acLienTmpl[0] < 'A')
      GetIniString("Data", "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);

   // Prop8 template
   GetIniString("Data", "Prop8Exp", "", acProp8Tmpl, _MAX_PATH, acIniFile);

   // Cumulative Sale file
   GetIniString(pCnty, "CSalFile", "", acCSalFile, _MAX_PATH, acIniFile);
   if (!acCSalFile[0])
   {
      GetIniString("Data", "CSalFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCSalFile, acTmp, pCnty, pCnty);
   }

   // Set debug flag
   GetPrivateProfileString(pCnty, "Debug", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bDebug = true;
   else
      bDebug = false;

   // Get TmpPath
   GetIniString("System", "TmpPath", "", acTmpPath, _MAX_PATH, acIniFile);
   sprintf(acTmp, "%s\\%s", acTmpPath, myCounty.acCntyCode);
   if (_access(acTmp, 0))
      _mkdir(acTmp);

   // Get DocPath
   iRet = GetIniString(pCnty, "DocPath", "", acDocPath, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      iRet = GetIniString("System", "DocPath", "", acTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         sprintf(acDocPath, acTmp, pCnty);
      else
         acDocPath[0] = 0;
   }

   // Load suffix table
   GetPrivateProfileString(pCnty, "UseSfxDev", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
   {
      bUseSfxXlat = true;
      GetIniString("System", "SfxDevTbl", "", acTmp, _MAX_PATH, acIniFile);
   } else
   {
      bUseSfxXlat = false;
      GetIniString("System", "SuffixTbl", ".\\Suffix.txt", acTmp, _MAX_PATH, acIniFile);
   }
   iRet = LoadSuffixTbl(acTmp, bUseSfxXlat);

   // Get Lookup file name
   GetIniString("System", "LookUpTbl", "", acTmp, _MAX_PATH, acIniFile);
   if (_access(acTmp, 0))
   {
      LogMsg("***** Lookup file [%s] is missing.", acTmp);
      return -1;
   } else
   {
      // Load tables
      iRet = LoadLUTable((char *)&acTmp[0], "[Quality]", NULL, MAX_ATTR_ENTRIES);
      if (!iRet)
      {
         LogMsg("*** Error Looking for table [Quality] in %s", acTmp);
         return -1;
      }
   }

   // Load Usecode table
   GetIniString("System", "UseTbl", "", acTmp, _MAX_PATH, acIniFile);
   iNumUseCodes = 0;
   if (acTmp[0])
   {
      sprintf(acUseTbl, acTmp, pCnty);
      if (!_access(acUseTbl, 0))
         iRet = LoadUseTbl(acUseTbl);
   }

   // Get county info
   GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = LoadCountyInfo(pCnty, acTmp);

   if (iRet == 1)
   {
      // Load city table
      GetIniString(pCnty, "CityFile", "", acCityFile, _MAX_PATH, acIniFile);
      if (acCityFile[0] < 'A')
      {
         GetIniString("Data", "CityFile", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acCityFile, acTmp, pCnty);
      }
      printf("Loading city table: %s\n", acCityFile);
      if (strstr(acCityFile, "_N2CX"))
         iRet = Load_N2CX(acCityFile);
      else
         iRet = LoadCities(acCityFile);

      if (!lLienYear)
         lLienYear = atol(myCounty.acYearAssd);
      else
         sprintf(myCounty.acYearAssd, "%d", lLienYear);
   } else
      return iRet;

   sprintf(acTmp, "%d0101", lLienYear);
   lLienDate = atol(acTmp);
   lTaxYear = GetPrivateProfileInt(pCnty, "TaxYear", lLienYear, acIniFile);

   // Check default send mail flag
   if (!bSendMail)
   {
      GetPrivateProfileString(pCnty, "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (!acTmp[0])
         GetPrivateProfileString("System", "SendMail", "", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         bSendMail = true;
   }

   // Get max changed records allowed
   iMaxChgAllowed = GetPrivateProfileInt(pCnty, "MaxChgAllowed", 0, acIniFile);
   if (!iMaxChgAllowed)
      iMaxChgAllowed = GetPrivateProfileInt("Data", "MaxChgAllowed", 50000, acIniFile);

   // Initialize direction table
   InitDirs();
   lRecCnt = 0;
   lLastRecDate = 0;
   lLastGrGrDate = 0;

   // Tax file template
   GetIniString(pCnty, "SuplInfo", "", sSuplTmpl, _MAX_PATH, acIniFile);
   if (sSuplTmpl[0] < ' ')
      GetIniString("Data", "SuplInfo", "", sSuplTmpl, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "Redemption", "", sRedTmpl, _MAX_PATH, acIniFile);
   if (sRedTmpl[0] < ' ')
      GetIniString("Data", "Redemption", "", sRedTmpl, _MAX_PATH, acIniFile);
   GetIniString(pCnty, "TaxRollInfo", "", sBaseTmpl, _MAX_PATH, acIniFile);
   if (sBaseTmpl[0] < ' ')
      GetIniString("Data", "TaxRollInfo", "", sBaseTmpl, _MAX_PATH, acIniFile);

   GetIniString("Data", "TaxOut", "", sTaxOutTmpl, _MAX_PATH, acIniFile);
   GetIniString("System", "ImportLog", "", sImportLogTmpl, _MAX_PATH, acIniFile);

   // Check AutoImport option
   GetPrivateProfileString(pCnty, "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportSale", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bSaleImport = true;
   else
      bSaleImport = false;

   GetPrivateProfileString(pCnty, "ImportGrGr", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportGrGr", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bGrgrImport = true;
   else
      bGrgrImport = false;

   GetPrivateProfileString(pCnty, "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "ImportTax", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bTaxImport = true;
   else
      bTaxImport = false;

   GetPrivateProfileString(pCnty, "MixSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] < ' ')
      GetPrivateProfileString("System", "MixSale", "", acTmp, _MAX_PATH, acIniFile);
   if (toupper(acTmp[0]) == 'Y')
      bMixSale = true;
   else
      bMixSale = false;

   GetPrivateProfileString(pCnty, "Delimiter", "~", acTmp, _MAX_PATH, acIniFile);   
   cDelim = acTmp[0];

   GetIniString(pCnty, "RollSale", "", acRollSale, _MAX_PATH, acIniFile);

   char acVestTbl[_MAX_PATH];
   GetIniString(pCnty, "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);
   if (acVestTbl[0] < ' ')
      GetIniString("System", "VestingTbl", "", acVestTbl, _MAX_PATH, acIniFile);

   if (acValueFile[0] < ' ')
   {
      GetIniString(pCnty, "ValueFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acValueFile, acTmp, pCnty, pCnty);
   }

   // Get SQL timeout setting
   m_iTimeOut = GetPrivateProfileInt("Database", "TimeOut", 0, acIniFile);

   iRet = loadVesting(pCnty, acVestTbl);

   return iRet;
}

/***************************** Cres_CreateLienRec ***************************
 *
 * 
 *
 ****************************************************************************/

void Cres_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   LIENEXTR *pLien = (LIENEXTR *)pOutbuf;
   REDIFILE *pRoll = (REDIFILE *)pRollRec;

   // Clear output buffer
   memset((void *)pLien, ' ', sizeof(LIENEXTR));

   // Start copying data
   if (!memcmp(myCounty.acCntyCode, "INY", 3))
   {
      strncpy(acTmp, pRoll->APN, CRESIZ_APN);
      replChar(acTmp, ' ', '0', CRESIZ_APN);
      memcpy(pLien->acApn, acTmp, CRESIZ_APN);
   } else
   {
      memcpy(pLien->acApn, pRoll->APN, CRESIZ_APN);

      // Prop 8
      if (!memcmp(myCounty.acCntyCode, "TUO", 3) && !memcmp(pRoll->BaseCode, "67", 2))
         pLien->SpclFlag = LX_PROP8_FLG;
      if (!memcmp(myCounty.acCntyCode, "LAS", 3) && !memcmp(pRoll->ReappReason, "42", 2))
         pLien->SpclFlag = LX_PROP8_FLG;
   }

   // Assessment year
   memcpy(pLien->acYear, myCounty.acYearAssd, SIZ_LIEN_YEAR);

   // Land
   long lLand = atoin(pRoll->LandVal, CRESIZ_LAND_VAL);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLien->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRoll->ImprVal, CRESIZ_IMP_VAL);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLien->acImpr, acTmp, SIZ_IMPR);
   }

   // Fixture, Personal Properties
   long lLeaseVal = atoin(pRoll->LeaseVal, CRESIZ_LEASE_VAL);
   long lPFixt = atoin(pRoll->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
   long lPP_MH = atoin(pRoll->MobileVal, CRESIZ_MOBILE_VAL);
   // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
   long lFixtVal = atoin(pRoll->FixtVal, CRESIZ_FIXT_VAL);
   long lPers  = atoin(pRoll->PPVal, CRESIZ_PP_VAL);

   // Other value
   lTmp = lFixtVal+lPers;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLien->acOther, acTmp, SIZ_LIEN_OTHERS);

      if (lFixtVal > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtVal);
         memcpy(pLien->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLien->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPFixt);
         memcpy(pLien->extra.Cres.PersFixtr, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPP_MH > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPP_MH);
         memcpy(pLien->extra.Cres.PP_MobileHome, acTmp, SIZ_LIEN_FIXT);
      }
      if (lLeaseVal > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lLeaseVal);
         memcpy(pLien->extra.Cres.LeaseVal, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLien->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
         memcpy(pLien->acRatio, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   lTmp = atoin(pRoll->ExVal, CRESIZ_EX_VAL);
   if (!memcmp(pRoll->ExeCode1, "10", 2) && lTmp > 0)
      pLien->acHO[0] = '1';      // 'Y'
   else
      pLien->acHO[0] = '2';      // 'N'

   // Exemption codes
   memcpy(pLien->extra.Cres.Exe_Code1, pRoll->ExeCode1, CRESIZ_EXEMP_CODE);
   memcpy(pLien->extra.Cres.Exe_Code2, pRoll->ExeCode2, CRESIZ_EXEMP_CODE);
   memcpy(pLien->extra.Cres.Exe_Code3, pRoll->ExeCode3, CRESIZ_EXEMP_CODE);
   memcpy(pLien->extra.Cres.Exe_Code4, pRoll->ExeCode4, CRESIZ_EXEMP_CODE);

   // Exempt total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pLien->acExAmt, acTmp, SIZ_EXE_TOTAL);

      lTmp = atoin(pRoll->ExeVal1, CRESIZ_EX_VAL);
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEVAL, lTmp);
      memcpy(pLien->extra.Cres.Exe_Amt1, acTmp, SIZ_LIEN_EXEVAL);
      lTmp = atoin(pRoll->ExeVal2, CRESIZ_EX_VAL);
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEVAL, lTmp);
      memcpy(pLien->extra.Cres.Exe_Amt2, acTmp, SIZ_LIEN_EXEVAL);
      lTmp = atoin(pRoll->ExeVal3, CRESIZ_EX_VAL);
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEVAL, lTmp);
      memcpy(pLien->extra.Cres.Exe_Amt3, acTmp, SIZ_LIEN_EXEVAL);
      lTmp = atoin(pRoll->ExeVal4, CRESIZ_EX_VAL);
      sprintf(acTmp, "%*d", SIZ_LIEN_EXEVAL, lTmp);
      memcpy(pLien->extra.Cres.Exe_Amt4, acTmp, SIZ_LIEN_EXEVAL);
   }

   // TRA
   memcpy(pLien->acTRA, pRoll->TRA, CRESIZ_TRA);

   pLien->LF[0] = 10;
   pLien->LF[1] = 0;
}

/******************************* Cres_ExtrLien ******************************
 *
 * Create lien extract file.
 *
 ****************************************************************************/

int Cres_ExtrLien(char *pCnty)
{
   char     acBuf[1024], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   int      iRet;
   BOOL     bEof;
   long     lCnt=0;
   FILE     *fdLien;

   // Open roll file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsgD("\nExtract lien data from Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Merge loop
   while (!bEof)
   {
      // Create new record
      Cres_CreateLienRec(acBuf, acRollRec);
    
      // Write to output
      fputs(acBuf, fdLien);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next RollRec
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsg("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/******************************* Cres_ExtrLien ******************************
 *
 * Create lien extract file.  Use this function for record with LF.
 *
 ****************************************************************************/

int Cres_ExtrLienLF(char *pCnty)
{
   char     acBuf[1024], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH], *pRec;
   int      lCnt=0;
   FILE     *fdLien;

   // Open roll file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsg0("\nExtract lien data from Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   pRec = fgets(acRollRec, 1024, fdRoll);

   // Merge loop
   while (pRec)
   {
      // Create new record
      Cres_CreateLienRec(acBuf, acRollRec);
    
      // Write to output
      fputs(acBuf, fdLien);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next RollRec
      pRec = fgets(acRollRec, 1024, fdRoll);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/************************* Cres_Load_TaxOwner() ******************************
 *
 * Since tax file doesn't include owner info, we get it from redifile
 *
 *****************************************************************************/

int Cres_ParseTaxOwner(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], acName1[64], acName2[64], *pTmp;
   int      iTmp;
   REDIFILE *pRoll = (REDIFILE *)pInbuf;
   TAXOWNER *pOwner = (TAXOWNER *)pOutbuf;

   // Clear output buffer
   memset((void *)pOwner, 0, sizeof(TAXOWNER));

   // Start copying data
   if (!memcmp(myCounty.acCntyCode, "INY", 3))
   {
      strncpy(acTmp, pRoll->APN, CRESIZ_APN);
      replChar(acTmp, ' ', '0', CRESIZ_APN);
      strncpy(pOwner->Apn, acTmp, CRESIZ_APN);
   } else
   {
      memcpy(pOwner->Apn, pRoll->APN, CRESIZ_APN);
   }

   // Year
   strcpy(pOwner->TaxYear, myCounty.acYearAssd);

   // Owner names
   memcpy(acName1, pRoll->Name1, CRESIZ_NAME_1);
   acName1[CRESIZ_NAME_1] = 0;
   memcpy(acName2, pRoll->Name2, CRESIZ_NAME_2);
   acName2[CRESIZ_NAME_2] = 0;

   memcpy(pOwner->Dba, pRoll->Dba, CRESIZ_DBA);
   blankRem(pOwner->Dba, CRESIZ_DBA);

   // Remove dot and single quote
   remCharEx(acName1, ".'");
   blankRem(acName1);
   remCharEx(acName2, ".'");
   blankRem(acName2);

   // Check owner2 for # - drop them
   if (acName2[0] == '%')
   {
      strcpy(pOwner->CareOf, acName2);
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      strcpy(pOwner->CareOf, acName2);
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      strcpy(pOwner->CareOf, acName2);
   } else if (acName2[0] == '&' || acName2[0] == '#')
   {  // Keep them as two separate names
      strcpy(pOwner->Name2, &acName2[2]);
   } else
      strcpy(pOwner->Name2, acName2);

   iTmp = strlen(acName1);
   if (acName1[iTmp - 1] == '&')
      acName1[iTmp - 1] = 0;
   strcpy(pOwner->Name1, acName1);

   // Mail addr
   memcpy(pOwner->MailAdr[0], pRoll->M_Addr1, CRESIZ_ADDR_1);
   blankRem(pOwner->MailAdr[0], CRESIZ_ADDR_1);
   memcpy(pOwner->MailAdr[1], pRoll->M_Addr2, CRESIZ_ADDR_2);
   blankRem(pOwner->MailAdr[1], CRESIZ_ADDR_2);
   sprintf(acTmp, " %.5s", pRoll->M_Zip);
   strcat(pOwner->MailAdr[1], acTmp);
   
   return 0;
}

int Cres_Load_TaxOwner(bool bImport, int iLRecLen)
{
   char     acBuf[1024], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   int      iRet, iRecLen;
   BOOL     bEof;
   long     lCnt=0;
   FILE     *fdOwner;

   // Open roll file
   GetIniString(myCounty.acCntyCode, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsgD("\nExtract Owner data from Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Owner");
   LogMsg("Open Owner extract file %s", acOutFile);
   fdOwner = fopen(acOutFile, "w");
   if (fdOwner == NULL)
   {
      LogMsg("***** Error creating owner extract file: %s\n", acOutFile);
      return -2;
   }

   // Confirm roll length
   if (!iLRecLen)
      iRecLen = iRollLen;
   else
      iRecLen = iLRecLen;

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRecLen, fdRoll);
   bEof = (iRet==iRecLen ? false:true);

   // Merge loop
   while (!bEof)
   {
      // Create new record
      Cres_ParseTaxOwner(acBuf, acRollRec);
      Tax_CreateTaxOwnerCsv(acRollRec, (TAXOWNER *)&acBuf[0]);
    
      // Write to output
      fputs(acRollRec, fdOwner);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next RollRec
      iRet = fread((char *)&acRollRec[0], 1, iRecLen, fdRoll);
      bEof = (iRet==iRecLen ? false:true);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdOwner)
      fclose(fdOwner);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_OWNER);
   } else
      iRet = 0;


   LogMsgD("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/******************************* Cres_ExtrSale1 *****************************
 *
 * Extract sale1 from redifile and output to Sale_Exp.dat & Sale_Exp.sls
 *
 ****************************************************************************/

int Cres_ExtrSale1(char *pRollFile, int iRollLen)
{
   char     acBuf[512], acRollRec[1024];
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice, lTmp;
   double   dTmp, dPrice, dTmp1;
   boolean  bEof;

   REDIFILE  *pRoll;
   SALE_REC1 *pSale;

   LogMsgD("\nExtract sale data from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -1;
   }

   // Open Output file - Sale_Exp.dat
   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, "dat");
   LogMsg("Open output file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error creating sale extract file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SALE_REC1 *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);

#ifdef _DEBUG
      //if (!memcmp(pRoll->APN, "0540100120", 10))
      //   iRet = 0;
#endif

      memcpy(pSale->acApn, pRoll->APN, CRESIZ_APN);
      if (!memcmp(myCounty.acCntyCode, "ALP", 3))
      {
         int iTmp = atoin(pRoll->RecBook1, 4);

         if (iTmp >= 2007 && iTmp < 2009)
            Alp_Fmt2Doc(acDoc, acDate, pRoll->RecBook1);
         else
            Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      } else if (!memcmp(myCounty.acCntyCode, "INY", 3))
      {
         Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
         strncpy(acTmp, pRoll->APN, CRESIZ_APN);
         replChar(acTmp, ' ', '0', CRESIZ_APN);
         memcpy(pSale->acApn, acTmp, CRESIZ_APN);
      } 
      else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
         Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
      //   Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "GLE", 3))
      //   Gle_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
      //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
      //   Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
         Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
         Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
      //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else
         break;   // Unknown county

      lTmp = atol(acDate);
      if (lTmp < lToday && acDoc[0] != ' ')
      {
         memcpy(pSale->acName1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->acName2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->acDocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->acDocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            // Keep this check here until counties correct their roll
            if (!memcmp(myCounty.acCntyCode, "SIE", 3) && (!strchr(acTmp, '.') && dTmp > 1900.00))
            {
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               //LogMsg0("??? Suspected stamp amount for APN: %.10s, Amt=%d", acBuf, (long)dTmp);
            } else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
            {  // TRI is always sale price
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
            } else if (!memcmp(myCounty.acCntyCode, "INY", 3))
            {  // INY contains mixed data
               if (strchr(acTmp, '.'))
               {  // DocTax ?
                  lPrice = (long)((dTmp * SALE_FACTOR)*100.0);
                  dPrice = lPrice/100.0;
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  dTmp1 = lPrice;
                  if (dTmp > 1000.00 && dPrice > dTmp1)
                  {
                     lPrice = (long)dTmp;
                     if (lPrice < 5000)
                        LogMsg0("*** 1. Questionable sale price on APN=%.10s, Amt=%.2f", acBuf, dTmp);
                  } else if (dTmp > 9999.0)
                  {
                     lPrice = (long)dTmp;
                     if (lPrice < 500)
                        LogMsg0("*** 2. Questionable sale price on APN=%.10s, Amt=%.2f", acBuf, dTmp);
                  } else if (dTmp > 5000.00)
                        LogMsg0("*** 3. Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.2f", acBuf, lPrice, dTmp);
               } else
               {  // Sale price
                  lPrice =  (long)dTmp;
                  if (lPrice < 1000)
                     LogMsg0("??? Suspected sale price for APN: %.10s, Amt=%d", acBuf, lPrice);
               }

               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            } else
            {
               lPrice = (long)(dTmp * SALE_FACTOR);
               if (lPrice > 0 && strchr(acTmp, '.'))
               {
                  if (lPrice < 100)
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                  else
                     sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
               } else
                  memset(acTmp, ' ', SIZ_SALE1_AMT);

               if (dTmp > 5000.00)
                  LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt);
            } 
            memcpy(pSale->acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if ((iRet > lLienDate && iRet < lToday) || 
            (!memcmp(myCounty.acCntyCode, "ALP", 3) && iRet >= 20070000))
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else if (iLoadFlag & EXTR_SALE)          // -Xs
         {
            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         }
         
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Append to cum sale
   if (iLoadFlag & EXTR_SALE) 
      iRet = CopyFile(acOutFile, acCSalFile, false);
   else
      iRet = appendTxtFile(acOutFile, acCSalFile);

   printf("\n");
   LogMsg("Total input records:        %u", lCnt);
   LogMsgD("Total output records:       %u", lNewRec);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);

   return lCnt;
}

/******************************* Cres_UpdCumSale ****************************
 *
 * Extract sales from redifile and update ???_sale_cum.dat.  Use this file
 * to update roll file.
 * Return 0 if successful.
 *
 ****************************************************************************/

int Cres_UpdCumSale()
{
   char     acBuf[512], acRollRec[1024];
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice;
   double   dTmp;
   boolean  bEof;

   REDIFILE  *pRoll;
   SALE_REC1 *pSale;

   printf("\nExtract sale data from roll file %s to update %s\n", acRollFile, acCSalFile);
   LogMsg("\nExtract sales from roll file %s to update %s", acRollFile, acCSalFile);

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open old cum sale file
   LogMsg("Open cum sale file file %s for update", acCSalFile);
   fdCSale = fopen(acCSalFile, "a+");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acCSalFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SALE_REC1 *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->acApn, pRoll->APN, CRESIZ_APN);
      if (!memcmp(myCounty.acCntyCode, "ALP", 3))
         Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "INY", 3))
      {
         Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
         strncpy(acTmp, pRoll->APN, CRESIZ_APN);
         replChar(acTmp, ' ', '0', CRESIZ_APN);
         memcpy(pSale->acApn, acTmp, CRESIZ_APN);
      } 
      else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
         Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
      //   Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "GLE", 3))
      //   Gle_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
      //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
      //   Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
         Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
         Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
      //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else
         break;   // Unknown county

      if (acDate[0] != ' ')
      {
         memcpy(pSale->acName1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->acName2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->acDocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->acDocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);

            memcpy(pSale->acSalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);

         // Sale 2
         memset(acBuf, ' ', sizeof(SALE_REC1)-2);
         memcpy(pSale->acApn, pRoll->APN, CRESIZ_APN);
         
         if (!memcmp(myCounty.acCntyCode, "ALP", 3))
            Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         else if (!memcmp(myCounty.acCntyCode, "INY", 3))
         {
            Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
            strncpy(acTmp, pRoll->APN, CRESIZ_APN);
            replChar(acTmp, ' ', '0', CRESIZ_APN);
            memcpy(pSale->acApn, acTmp, CRESIZ_APN);
         } 
         else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
            Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
         //   Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "GLE", 3))
         //   Gle_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
         //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
         //   Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
            Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
            Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
         //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         if (acDate[0] != ' ')
         {
            // Save last recording date
            iRet = atoin(acDate, 8);
            if (iRet < lToday)
            {
               memcpy(pSale->acDocDate, acDate, SALE_SIZ_DOCDATE);
               memcpy(pSale->acDocNum, acDoc, SALE_SIZ_DOCNUM);

               // Write output
               fputs(acBuf, fdCSale);
               lNewRec++;
            } else
               LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);

            // Sale 3
            memset(acBuf, ' ', sizeof(SALE_REC1)-2);
            memcpy(pSale->acApn, pRoll->APN, CRESIZ_APN);
         
            if (!memcmp(myCounty.acCntyCode, "ALP", 3))
               Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            else if (!memcmp(myCounty.acCntyCode, "INY", 3))
            {
               Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
               strncpy(acTmp, pRoll->APN, CRESIZ_APN);
               replChar(acTmp, ' ', '0', CRESIZ_APN);
               memcpy(pSale->acApn, acTmp, CRESIZ_APN);
            } 
            else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
               Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
            //   Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "GLE", 3))
            //   Gle_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
            //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
            //   Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
               Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
               Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
            //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            if (acDate[0] != ' ')
            {
               // Save last recording date
               iRet = atoin(acDate, 8);
               if (iRet < lToday)
               {
                  memcpy(pSale->acDocDate, acDate, SALE_SIZ_DOCDATE);
                  memcpy(pSale->acDocNum, acDoc, SALE_SIZ_DOCNUM);

                  // Write output
                  fputs(acBuf, fdCSale);
                  lNewRec++;
               } else
                  LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
            }
         }
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   char *pTmp;
   strcpy(acOutFile, acCSalFile);
   if (pTmp = strrchr(acOutFile, '.'))
      strcpy(pTmp, ".srt");
   else
      strcat(pTmp, ".srt");

   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D,69,2,C,D) F(TXT) DUPO(1,66)");
   iRet = sortFile(acCSalFile, acOutFile, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acCSalFile, acCSalFile);
      iRet = -1;
   } else
   {
      // Rename files
      strcpy(acTmp, acCSalFile);
      if (pTmp = strrchr(acTmp, '.'))
         strcpy(pTmp, ".tmp");
      else
         strcat(pTmp, ".tmp");
      if (!_access(acTmp, 0))
         remove(acTmp);
      iRet = rename(acCSalFile, acTmp);
      if (!iRet)
         iRet = rename(acOutFile, acCSalFile);
      else
         LogMsg("*** Unable to rename %s to %s", acCSalFile, acTmp);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lNewRec);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/******************************* Cres_ExtrSale ******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Cres_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024];
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice;
   double   dTmp;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      if (!memcmp(myCounty.acCntyCode, "ALP", 3))
         Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "INY", 3))
      {
         Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
         strncpy(acTmp, pRoll->APN, CRESIZ_APN);
         replChar(acTmp, ' ', '0', CRESIZ_APN);
         memcpy(pSale->Apn, acTmp, CRESIZ_APN);
      } 
      else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
         Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
      //   Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
      //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
      //   Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
         Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
         Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
      //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else
         break;   // Unknown county

      if (acDate[0] != ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);

            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
            iRet = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(pSale->StampAmt, acTmp, iRet);
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);

         // Sale 2
         memset(acBuf, ' ', sizeof(SALE_REC1)-2);
         memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
         
         if (!memcmp(myCounty.acCntyCode, "ALP", 3))
            Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         else if (!memcmp(myCounty.acCntyCode, "INY", 3))
         {
            Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
            strncpy(acTmp, pRoll->APN, CRESIZ_APN);
            replChar(acTmp, ' ', '0', CRESIZ_APN);
            memcpy(pSale->Apn, acTmp, CRESIZ_APN);
         } 
         else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
            Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
         //   Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
         //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
         //   Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
            Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
            Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
         //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
         if (acDate[0] != ' ')
         {
            // Save last recording date
            iRet = atoin(acDate, 8);
            if (iRet < lToday)
            {
               memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
               memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

               // Write output
               fputs(acBuf, fdCSale);
               lNewRec++;
            } else
               LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);

            // Sale 3
            memset(acBuf, ' ', sizeof(SALE_REC1)-2);
            memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
         
            if (!memcmp(myCounty.acCntyCode, "ALP", 3))
               Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            else if (!memcmp(myCounty.acCntyCode, "INY", 3))
            {
               Iny_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
               strncpy(acTmp, pRoll->APN, CRESIZ_APN);
               replChar(acTmp, ' ', '0', CRESIZ_APN);
               memcpy(pSale->Apn, acTmp, CRESIZ_APN);
            } 
            else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
               Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
            //   Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
            //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
            //   Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
               Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
               Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
            //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
            if (acDate[0] != ' ')
            {
               // Save last recording date
               iRet = atoin(acDate, 8);
               if (iRet < lToday)
               {
                  memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
                  memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

                  // Write output
                  fputs(acBuf, fdCSale);
                  lNewRec++;
               } else
                  LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
            }
         }
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   if (pOutFile)
      strcpy(acCSalFile, pOutFile);
   else
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");

   if (bAppend)
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,117,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acCSalFile, acCSalFile);
      iRet = -1;
   } else
   {
      // Rename files
      if (!_access(acCSalFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acCSalFile, acTmp);
      }
      iRet = rename(acBuf, acCSalFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acCSalFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", iRet);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);


   return iRet;
}

/******************************* Cres_UpdSale1 ******************************
 *
 * Extract sale1 from redifile and update ???_sale.sls. Only take sale1 with 
 * sale tax amt. Use this file to update roll file.
 * Return 0 if successful.
 *
 ****************************************************************************/

int Cres_UpdSale1(char *pOutFile)
{
   char     acBuf[1024], acRollRec[1024];
   char     acOutFile[_MAX_PATH], acSortFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet, iCnt;
   long     lCnt=0, lNewRec=0, lPrice;
   double   dTmp;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsgD("\nExtract sales from roll file %s to update %s", acRollFile, acCSalFile);

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");

   // Open old cum sale file
   LogMsg("Open cum sale file file %s for update", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC));

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      if (!memcmp(myCounty.acCntyCode, "ALP", 3))
         Alp_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
         Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
      //   Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
         Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
      //   Tuo_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);
      else
         break;   // Unknown county

      if (acDate[0] > '0')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            if (!memcmp(myCounty.acCntyCode, "TRI", 3))
            {  // TRI is always sale price
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
            } else
            {
               lPrice = (long)(dTmp * SALE_FACTOR);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            }

            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
            iRet = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(pSale->StampAmt, acTmp, iRet);

            iRet = atoin(pRoll->ReappPct, 3);
            if (iRet == 100)
               pSale->SaleCode[0] = 'F';
            else if (iRet > 0)
               pSale->SaleCode[0] = 'P';

            // Save last recording date
            iRet = atoin(acDate, 8);
            if (iRet < lToday)
            {
               if (lLastRecDate < iRet)
                  lLastRecDate = iRet;

               // Write output
               pSale->CRLF[0] = 10;
               pSale->CRLF[1] = 0;
               fputs(acBuf, fdCSale);
               lNewRec++;
            } else
               LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
         }
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   if (pOutFile)
   {
      if (!_access(pOutFile, 0))
         sprintf(acBuf, "%s+%s", acOutFile, pOutFile);
      else
         strcpy(acBuf, acOutFile);
      strcpy(acOutFile, pOutFile);
   } else
   {
      sprintf(acBuf, "%s+%s", acOutFile, acCSalFile);
      strcpy(acOutFile, acCSalFile);
   }

   // Sort cum sale and remove duplicate
   sprintf(acSortFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,117,10,C,D,15,12,C,A,69,2,C,D) F(TXT) DUPO(1,34)");
   iCnt = sortFile(acBuf, acSortFile, acTmp, &iRet);
   if (iCnt <= 0)
   {
      LogMsg("***** Error sorting %s to %s (%d)", acBuf, acSortFile, iRet);
      iRet = -1;
   } else
   {
      // Rename files
      sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "rav");
      if (!_access(acTmp, 0))
         remove(acTmp);
      if (!_access(acOutFile, 0))
         iRet = rename(acOutFile, acTmp);
      if (!iRet)
         iRet = rename(acSortFile, acOutFile);
      else
         LogMsg("***** Unable to rename %s to %s", acSortFile, acOutFile);
   }

   printf("\n");
   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total sale extracted:       %u", lNewRec);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);

   return iRet;
}

/********************************** LoadRedifile ****************************
 *
 * This function will either merge new data into old record or create new
 * record for new parcel.
 *
 ****************************************************************************/

int LoadUpdtRoll(char *pCnty, int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[1024];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], acTmp[256], acTmp1[32];

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof, bSkip;
   long     lRet=0, lCnt=0, lRollCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Use last out file for input if lien file missing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
   }

   if (_access(acRawFile, 0))
   {
      LogMsg("***** Input file missing: %s", acRawFile);
      return 3;
   }

   // Sort input file on APN
   sprintf(acTmp1, "%s\\%s\\redifile.srt", acTmpPath, myCounty.acCntyCode);
   sprintf(acTmp, "S(11,10,C,A) F(FIX,%d) ", iRollLen);
   iRet = sortFile(acRollFile, acTmp1, acTmp);
   if (iRet > 0)
      strcpy(acRollFile, acTmp1);
   else
   {
      LogMsg("***** Error sorting %s to %s", acRollFile, acTmp1);
      return -1;
   }

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return 4;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

   lRollCnt++;
   bEof = (iRet==iRollLen ? false:true);

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

      NextRollRec:
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);
      bSkip = false;

      iTmp = memcmp(acBuf, (char *)&acRollRec[CREOFF_APN], iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         try
         {
            if (!memcmp(pCnty, "LAS", 3))
               iRet = Las_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);
            //else if (!memcmp(pCnty, "DNX", 3))
            //   iRet = Dnx_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);
            //else if (!memcmp(pCnty, "MOD", 3))
            //   iRet = Mod_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);
            //else if (!memcmp(pCnty, "GLE", 3))
            //   iRet = Gle_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);
            //else if (!memcmp(pCnty, "TEH", 3))
            //   iRet = Teh_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);
            //else if (!memcmp(pCnty, "TRI", 3))
            //   iRet = Tri_CreateR01(acBuf, acRollRec, iRollLen, UPDATE_R01);

            if (!iRet)
               iRollUpd++;
            else
            {
               if (bDebug)
                  LogMsg0("Retired record : %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
               bSkip = true;
            }

         } catch(...)
         {
            LogMsg("***** ERROR: Data corruption at APN: %*s *****", iApnLen, acBuf);
            nBytesRead = 0;
            break;
         }

         // Read next roll record
         iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
         lRollCnt++;
         bEof = (iRet==iRollLen ? false:true);
      } else
      {
         if (iTmp > 0)       // Roll not match, new roll record
         {
            // Create new R01 record
            try
            {
               if (!memcmp(pCnty, "LAS", 3))
                  iRet = Las_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
               //else if (!memcmp(pCnty, "DNX", 3))
               //   iRet = Dnx_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
               //else if (!memcmp(pCnty, "GLE", 3))
               //   iRet = Gle_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
               //else if (!memcmp(pCnty, "MOD", 3))
               //   iRet = Mod_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
               //else if (!memcmp(pCnty, "TEH", 3))
               //   iRet = Teh_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
               //else if (!memcmp(pCnty, "TRI", 3))
               //   iRet = Tri_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);

               if (bDebug)
               {
                  if (!iRet)
                     LogMsg0("New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
                  else
                     LogMsg0("** Skip roll record : %.*s (%d) **", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
               }
            } catch(...)
            {
               LogMsg("***** ERROR: Data corruption at roll APN: %*s *****", iApnLen, (char *)&acRollRec[CREOFF_APN]);
               nBytesRead = 0;
               break;
            }

            if (!iRet)
            {
               iNewRec++;
               // Create APN_D new record
               iTmp = formatApn(acRec, acTmp, &myCounty);
               memcpy((char *)&acRec[OFF_APN_D], acTmp, iTmp);

               // Create MapLink and output new record
               iTmp = formatMapLink(acRec, acTmp, &myCounty);
               memcpy((char *)&acRec[OFF_MAPLINK], acTmp, iTmp);

               // Create index map link
               getIndexPage(acTmp, acTmp1, &myCounty);
               memcpy((char *)&acRec[OFF_IMAPLINK], acTmp1, iTmp);

               // Assessment year
               memcpy((char *)&acRec[OFF_YR_ASSD], myCounty.acYearAssd, 4);

               // Get last recording date
               iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
               if (iTmp >= lToday)
                  LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acRec);
               else if (lLastRecDate < iTmp)
                  lLastRecDate = iTmp;

#ifdef _DEBUG
               iTmp = replChar(acRec, 0, ' ', iRecLen);
               if (iTmp)
                  LogMsg("*** WARNING: Null char found at position %d for record# %d -->%.14s", iTmp, lCnt, acRec);
#endif
               // Write to file
               bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
               lCnt++;
            } else
            {
               bSkip = true;
               iRetiredRec++;
            }

            // Get next roll record
            iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

            if (iRet != iRollLen)
               bEof = true;    // Signal to stop
            else
            {
               lRollCnt++;
               goto NextRollRec;
            }
         } else
         {
            // Record may be retired
            if (bDebug)
               LogMsg0("Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
            bSkip = true;
         }
      }

      // Only output active parcel, not retired ones
      if (!bSkip)
      {
         // Create APN_D new record
         iTmp = formatApn(acBuf, acTmp, &myCounty);
         memcpy((char *)&acBuf[OFF_APN_D], acTmp, iTmp);

         // Create MapLink and output new record
         iTmp = formatMapLink(acBuf, acTmp, &myCounty);
         memcpy((char *)&acBuf[OFF_MAPLINK], acTmp, iTmp);

         // Create index map link
         getIndexPage(acTmp, acTmp1, &myCounty);
         memcpy((char *)&acBuf[OFF_IMAPLINK], acTmp1, iTmp);

         // Assessment year
         memcpy((char *)&acBuf[OFF_YR_ASSD], myCounty.acYearAssd, 4);

         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         // Scan record for bad characters
#ifdef _DEBUG
         iTmp = replChar(acBuf, 0, ' ', iRecLen);
         if (iTmp)
            LogMsg("*** WARNING: Null char found at position %d for record# %d -->%.14s", iTmp, lCnt, acBuf);
#endif

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         iRetiredRec++;


      if (!(lCnt % 1000))
      {
         printf("\r%u", lCnt);
         //LogMsg("At %d: %.10s", lCnt, acBuf);
      }

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // reset buffer
   memset(acBuf, ' ', MAX_RECSIZE);

   // Do the rest of the file
   while (!bEof)
   {
      // Create new R01 record
      try
      {
         //if (!memcmp(pCnty, "DNX", 3))
         //   iRet = Dnx_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
         //else 
         //if (!memcmp(pCnty, "GLE", 3))
         //   iRet = Gle_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
         if (!memcmp(pCnty, "LAS", 3))
            iRet = Las_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
         //else if (!memcmp(pCnty, "MOD", 3))
         //   iRet = Mod_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
         //else if (!memcmp(pCnty, "TEH", 3))
         //   iRet = Teh_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
         //else if (!memcmp(pCnty, "TRI", 3))
         //   iRet = Tri_CreateR01(acRec, acRollRec, iRollLen, CREATE_R01);
      } catch(...)
      {
         LogMsg("***** ERROR: Data corruption at roll APN: %*s *****", iApnLen, (char *)&acRollRec[CREOFF_APN]);
         nBytesRead = 0;
         break;
      }

      if (!iRet)
      {
         if (bDebug)
            LogMsg0("New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
         iNewRec++;

         // Create APN_D new record
         iTmp = formatApn(acRec, acTmp, &myCounty);
         memcpy((char *)&acRec[OFF_APN_D], acTmp, iTmp);

         // Create MapLink and output new record
         iTmp = formatMapLink(acRec, acTmp, &myCounty);
         memcpy((char *)&acRec[OFF_MAPLINK], acTmp, iTmp);

         // Create index map link
         getIndexPage(acTmp, acTmp1, &myCounty);
         memcpy((char *)&acRec[OFF_IMAPLINK], acTmp1, iTmp);

         // Assessment year
         memcpy((char *)&acRec[OFF_YR_ASSD], myCounty.acYearAssd, 4);

         // Get last recording date
         iTmp = atoin((char *)&acRec[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acRec);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

#ifdef _DEBUG
         iTmp = replChar(acRec, 0, ' ', iRecLen);
         if (iTmp)
            LogMsg("*** WARNING: Null char found at position %d for record# %d -->%.14s", iTmp, lCnt, acRec);
#endif

         // Write to file
         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
      {
         iRetiredRec++;
         LogMsg0("** Skip roll record : %.*s (%d) **", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
      }

      // Get next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

      if (iRet != iRollLen)
         bEof = true;    // Signal to stop
      else
         lRollCnt++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdSale)
      fclose(fdSale);
   if (fdTax)
      fclose(fdTax);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total roll records input:   %u", lRollCnt);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total retired records:      %u\n", iRetiredRec);
   LogMsg("Total sale skipped:         %u", lSaleSkip);
   LogMsg("Total Tax skipped:          %u\n", lTaxSkip);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/********************************** LoadLienRoll ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int LoadLienRoll(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256], acTmp1[32];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      lLDRRecCount++;
#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "001021004", 9))
      //   iRet=0;
#endif
      //if (!memcmp(pCnty,"DNX",3))
      //   iRet = Dnx_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      //else 
      //if (!memcmp(pCnty,"GLE",3))
      //   iRet = Gle_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!memcmp(pCnty,"LAS", 3))
         iRet = Las_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      //else if (!memcmp(pCnty,"MOD",3))
      //   iRet = Mod_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      //else if (!memcmp(pCnty,"TEH",3))
      //   iRet = Teh_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      //else if (!memcmp(pCnty, "TRI", 3))
      //   iRet = Tri_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);

      if (!iRet)
      {
         // Scan record for bad characters
#ifdef _DEBUG
         iTmp = isPrintable(acBuf, iRecLen);
         if (iTmp != iRecLen)
            LogMsg("***** ERROR: Bad byte found at %d in record %ld (APN=%.10s)", iTmp, lCnt, acBuf);
#endif
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         // Create APN_D new record
         iTmp = formatApn(acBuf, acTmp, &myCounty);
         memcpy((char *)&acBuf[OFF_APN_D], acTmp, iTmp);

         // Create MapLink and output new record
         iTmp = formatMapLink(acBuf, acTmp, &myCounty);
         memcpy((char *)&acBuf[OFF_MAPLINK], acTmp, iTmp);

         // Create index map link
         getIndexPage(acTmp, acTmp1, &myCounty);
         memcpy((char *)&acBuf[OFF_IMAPLINK], acTmp1, iTmp);

         // Assessment year
         memcpy((char *)&acBuf[OFF_YR_ASSD], myCounty.acYearAssd, 4);

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      } else
         iRetiredRec++;

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      // Scan for bad character - LAS May 05
      while (iRet-- > 0)
         if (acRollRec[iRet] < ' ')
            acRollRec[iRet] = ' ';

      if (!(++lCnt % 1000))
      {
         printf("\r%u", lCnt);
         //LogMsg("At %d: %.10s", lCnt, acBuf);
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/************************************** Usage ********************************
 *
 * Options:
 *    -L :  Load lien date.  This option create R01 file as usual.  It also
 *          creates a Lien file which contains assessment year values.
 *    -U :  Update roll file with new sale info.  New records will be added.
 *    -N :  Suffix and City name won't be encoded.
 *    -Xx:  Load exception (DNX)
 *    -Xd:  Create DTW file for Dave (1900+899)
 *    -Sn:  Skip number of records.
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: LoadCres -C<County code> [-A] [-D] [-F] [-G] [-M] [-N] [-O] [-L] [-U] [-X] [-S<n>]\n");
   printf("\t-A : Merge attribute file\n");
   printf("\t-D : Duplicate sale records from old file\n");
   printf("\t-Dn : Do not create update file (for debug only)\n");
   printf("\t-Fe: Fix Lien extract\n");
   printf("\t-Ft: Fix TRA in R01 file\n");
   printf("\t-G : Create GrGr file for MergeAdr\n");
   printf("\t-L : Load Lien date roll (create lien file and load roll).\n");
   printf("\t-La: Create cd-assessor file\n");
   printf("\t-Lc: Load characteristic file\n");
   printf("\t-Lg: Load GrGr file\n");
   printf("\t-Lr: Load LDR roll file\n");
   printf("\t-Ls: Load sale file\n");
   printf("\t-Lu: Load unsecured roll file\n");
   printf("\t-Mg: Merge history GrGr file\n");
   printf("\t-Ms: Merge history sale file\n");
   printf("\t-N : Do not encode suffix and city\n");
   printf("\t-O : Overwrite logfile (default append)\n");
   printf("\t-U : Update roll file using old S01 file\n");
   printf("\t-X8 : Extract Prop8 APN to be imported to SQL.\n");
   printf("\t-Xa: Extract attribute from attr file.\n");
   printf("\t-Xc: Extract cumulative sale from mainframe sale file.\n");
   printf("\t-Xd: Extract DTW file for Dave (1900+899).\n");
   printf("\t-Xg: Extract GrGr data.\n");
   printf("\t-Xl: Extract lien value from lien file.\n");
   printf("\t-Xs: Extract sale data for MergeAdr from sale file.\n");
   printf("\t-Xi: Create sale import from cumsale file.\n");
   printf("\t-Xx: Exception load of confidential roll.\n");
   printf("\t-Sn: Number of records skip (default 1)\n");
   printf("\t-Ynnnn: Process year.  This overwrites default year in CountyInfo.csv\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;
   char *pszParam;

   bEnCode = true;
   bCopySales = false;
   bOverwriteLogfile = false;
   bDebug = true;
   bDontUpd = false;
   bSendMail = false;
   bFixTRA = false;
   bFixLienExt = false;

   iSkip = 1;
   iLoadFlag = 0;
   lLienYear = 0;
   iLoadTax = 0;

   if (argv[1][0] != '-')
   {
      strcpy(myCounty.acCntyCode, argv[1]);
      return;
   }

   while (1)
   {
      chOpt = GetOption(argc, argv, "C:AD:EF:GI:L:M:N:OS:T:U:X:Y:?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'A':   // Merge attribute data
               iLoadFlag |= MERG_ATTR;
               break;

            case 'C':   // county code
               if (pszParam != NULL)
                  strcpy(myCounty.acCntyCode, pszParam);
               else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;

            case 'D':   // Duplicate sale record
               if (pszParam != NULL)
               {
                  if (*pszParam == 'n')
                     bDontUpd = true;
               } else
                  bCopySales = true;
               break;

            case 'E':   // Email if error occurs
               bSendMail = true;
               break;

            case 'F':   // Fix data field
               if (pszParam != NULL)
               {
                  if (*pszParam == 't')
                     bFixTRA = true;
                  else if (*pszParam == 'e')
                     bFixLienExt = true;
               }
               break;

            case 'G':   // Load GrGr data
               iLoadFlag |= LOAD_GRGR;
               break;

            case 'I':                           // Generate import file
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')         // Import sale
                     iLoadFlag |= EXTR_ISAL;
                  else if (*pszParam == 'g')    // Import Grgr
                     iLoadFlag |= EXTR_IGRGR;
               }
               break;

            case 'L':   // Default load lien date roll
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iLoadFlag |= LOAD_SALE;
                  else if (*pszParam == 'a')
                     iLoadFlag |= LOAD_ASSR;
                  else if (*pszParam == 'r')
                     iLoadFlag |= LOAD_LIEN;
                  else if (*pszParam == 'g')
                     iLoadFlag |= LOAD_GRGR;
                  else if (*pszParam == 'c')
                     iLoadFlag |= LOAD_ATTR;    // Load or convert Characteristic/Attribute
                  else if (*pszParam == 'u')
                     iLoadFlag |= LOAD_UNSC;    // Load unsecured roll
                  else if (*pszParam == 't')    // Load tax file
                     iLoadTax = TAX_LOADING;
                  else 
                     printf("***** Unknown option -L%s (ignore)\n", pszParam);
               } else
                  iLoadFlag |= LOAD_LIEN;
               break;

            case 'X':   // Extract data
               if (pszParam != NULL)
               {
                  if (*pszParam == 'l')
                     iLoadFlag |= EXTR_LIEN;
                  else if (*pszParam == 'a')
                     iLoadFlag |= EXTR_ATTR;
                  else if (*pszParam == 'c')
                     iLoadFlag |= EXTR_CSAL;
                  else if (*pszParam == 'n')
                     iLoadFlag |= EXTR_NRSAL;
                  else if (*pszParam == 's')
                  {
                     iLoadFlag |= EXTR_SALE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_ISAL;
                  }
                  else if (*pszParam == 'x')
                     iLoadFlag |= EXTR_CONF;
                  else if (*pszParam == '8')
                     iLoadFlag |= EXTR_PRP8;    // Extract prop8 flag
                  else if (*pszParam == 'v')
                  {
                     iLoadFlag |= EXTR_VALUE;
                     if (*(pszParam+1) == 'i')
                        iLoadFlag |= EXTR_IVAL;
                  } else
                  {
                     printf("Invalid extract option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;

            case 'M':   // Merge GrGr data
               if (pszParam != NULL)
               {
                  if (*pszParam == 'g')
                     iLoadFlag |= MERG_GRGR;    // Merge GrGr
                  else if (*pszParam == 'n')
                     iLoadFlag |= UPDT_XSAL;    // Merge NDC sale UPDT_XSAL
                  else if (*pszParam == 's')
                     iLoadFlag |= MERG_CSAL;    // Merge cum sale
                  else if (*pszParam == 'p')
                     iLoadFlag |= MERG_PUBL;    // Merge public parcel
                  else if (*pszParam == 'r')
                     iLoadFlag |= MERG_GISA;    // Merge acreage from GIS basemap ORG
                  else if (*pszParam == 'z')
                     iLoadFlag |= MERG_ZONE;    // Merge zoning file
               }
               break;

            case 'N':   // Do not encode
               bEnCode = false;
               break;

            case 'O':   // Overwrite logfile
               bOverwriteLogfile = true;
               break;

            case 'T':   // Load tax files
               iLoadTax = TAX_LOADING;
               break;

            case 'U':   // Update roll file
               //if (pszParam != NULL)
               //{
               //   if (*pszParam == 't')  // Update tax table
               //      iLoadTax = TAX_UPDATING;
               //} else
                  iLoadFlag |= LOAD_UPDT;
               break;

            case 'S':   // Skip records
               if (pszParam != NULL)
                  iSkip = atoi(pszParam);
               else
                  Usage();
               break;

            case 'Y':
               if (*pszParam > '0')
               {
                  lLienYear = atol(pszParam);
                  if (lLienYear > lToyear || lLienYear < 2000)
                  {
                     printf("Invalid LDR year option %s\n", pszParam);
                     Usage();
                  }
               } else
                  Usage();
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         LogMsg("Argument [%s] not recognized\n", pszParam);
         break;
      }
   }
}

/******************************** updateTaxDB *******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * Return TRUE if success
 *
 ****************************************************************************

int updateTaxDB(char *strCmd)
{                 
   static bool  bConnected=false;
   static hlAdo hDB;

   char acServer[256];
   int  iRet;

   iRet = GetIniString("Database", "TaxProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing TaxProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
         bConnected = hDB.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}
*/
int updateSaleDB(char *strCmd)
{                 
   static bool  bConnected=false;
   static hlAdo hDB;

   char acServer[256];
   int  iRet;

   iRet = GetIniString("Database", "SaleProvider", "", acServer, 128, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing SaleProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Execute command on: %s", acServer);
   LogMsg(strCmd);
   try
   {
      // open the database connection
      if (!bConnected)
         bConnected = hDB.Connect(acServer);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("%s", ComError(e));
      return -2;
   }

   // Update county profile table
   iRet = execSqlCmd(strCmd, &hDB);
   return iRet;
}

/******************************** doTaxImport *******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * iType values: see tax.h
 *
 * Return 0 if success
 *
 ****************************************************************************

int doTaxImport(char *pCnty, int iType)
{
   char sInFile[128], sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   LogMsg("Import Tax data to: %s", pCnty);
   GetIniString("Data", "TaxImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);
   switch (iType)
   {
      case TAX_BASE:
         sprintf(sFmtFile, sImportTmpl, "TaxBase");
         sprintf(sInFile, sTaxOutTmpl, pCnty, pCnty, "Base");
         sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, "TaxBase");
         sprintf(sTblName, "%s_Tax_Base", pCnty);
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         if (_access(sInFile, 0))
         {
            LogMsg("***** Missing import file: %s", sInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spPrep_TaxBase] '%s'", pCnty);
         iRet = updateTaxDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
         iRet = updateTaxDB(acTmp);
         break;

      case TAX_DETAIL:
         sprintf(sFmtFile, sImportTmpl, "TaxDetail");
         sprintf(sInFile, sTaxOutTmpl, pCnty, pCnty, "Detail");
         sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, "TaxDetail");
         sprintf(sTblName, "%s_Tax_Detail", pCnty);
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         if (_access(sInFile, 0))
         {
            LogMsg("***** Missing import file: %s", sInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spPrep_TaxDetail] '%s'", pCnty);
         iRet = updateTaxDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
         iRet = updateTaxDB(acTmp);
         break;

      case TAX_AGENCY:
         sprintf(sFmtFile, sImportTmpl, "TaxAgency");
         sprintf(sInFile, sTaxOutTmpl, pCnty, pCnty, "Agency");
         sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, "TaxAgency");
         sprintf(sTblName, "%s_Tax_Agency", pCnty);
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         if (_access(sInFile, 0))
         {
            LogMsg("***** Missing import file: %s", sInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spPrep_TaxAgency] '%s'", pCnty);
         iRet = updateTaxDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
         iRet = updateTaxDB(acTmp);
         break;

      case TAX_DELINQUENT:
         sprintf(sFmtFile, sImportTmpl, "TaxDelq");
         sprintf(sInFile, sTaxOutTmpl, pCnty, pCnty, "Delq");
         sprintf(sErrFile, sImportLogTmpl, "Tax", pCnty, "TaxDelq");
         sprintf(sTblName, "%s_Tax_Delq", pCnty);
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         if (_access(sInFile, 0))
         {
            LogMsg("***** Missing import file: %s", sInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spPrep_TaxDelq] '%s'", pCnty);
         iRet = updateTaxDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, sInFile, sFmtFile, sErrFile);
         iRet = updateTaxDB(acTmp);
         break;

      default:
         LogMsg("*** Import file type [%d] has not been implemented.  Please check calling function", iType);
         break;
   }

   return iRet;
}

/******************************** doSaleImport ******************************
 *
 * This function will call store procedure to do bulk insert
 *
 * iType values:
 *    1: Sale data
 *    2: Grgr data
 *    3: 
 *
 * Return 0 if success
 *
 ****************************************************************************/

int doSaleImport(char *pCnty, char *pDbName, char *pInFile, int iType)
{
   char sErrFile[128], sTblName[32], sImportTmpl[_MAX_PATH];
   char sFmtFile[128], acTmp[1024];
   int  iRet=1;

   GetIniString("Data", "SaleImportTmpl", "", sImportTmpl, _MAX_PATH, acIniFile);

   switch (iType)
   {
      case TYPE_SCSAL_REC:
         LogMsg("Import Sale data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "Sales");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "Sale");
         sprintf(sTblName, "%s_Sales", pCnty);

         // Delete old log files
         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spSalePrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);    

         // Update DocLink
         GetIniString("Database", "HSDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      case TYPE_GRGR_DOC:
      case TYPE_GRGR_DEF:
         LogMsg("Import Grgr data to: %s using %s", pCnty, pDbName);
         sprintf(sFmtFile, sImportTmpl, "GrGr");
         sprintf(sErrFile, sImportLogTmpl, "Sale", pCnty, "GrGr");
         sprintf(sTblName, "%s_GrGr", pCnty);

         if (!_access(sErrFile, 0))
            DeleteFile(sErrFile);
         sprintf(acTmp, "%s.Error.txt", sErrFile);
         if (!_access(acTmp, 0))
            DeleteFile(acTmp);

         if (_access(pInFile, 0))
         {
            LogMsg("***** Missing import file: %s", pInFile);
            return -1;
         }
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing import format file: %s", sFmtFile);
            return -1;
         }

         // Prepare to import - Create/truncate table
         sprintf(acTmp, "EXEC [dbo].[spGrGrPrep] '%s'", pCnty);
         iRet = updateSaleDB(acTmp);

         // Calling store procedure
         sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
         iRet = updateSaleDB(acTmp);

         // Update DocLink
         GetIniString("Database", "GrDocLink", "", acTmp, 128, acIniFile);
         sprintf(sFmtFile, acTmp, pCnty);
         if (_access(sFmtFile, 0))
         {
            LogMsg("***** Missing sql script file: %s to update DocLink", sFmtFile);
         } else
         {
            sprintf(acTmp, "EXEC sp_executesqlfromfile '%s', NULL, 0", sFmtFile);
            iRet = updateSaleDB(acTmp);    
         }
         break;

      default:
         break;
   }

   return iRet;
}

/******************************** doValueImport *****************************
 *
 * This function will call store procedure to do bulk insert
 *
 *
 * Return 0 if success
 *
 ****************************************************************************/

int doValueImport(char *pCnty, char *pInFile)
{
   char sErrFile[128], sTblName[32], sDBName[32], sServer[32];
   char sFmtFile[128], sProviderTmpl[255], acTmp[1024];
   int  iRet=1;

   iRet = GetIniString("Database", "DBProvider", "", sProviderTmpl, 255, acIniFile);
   if (!iRet)
   {
      LogMsg("***** Missing DBProvider in INI file.  Please review %s", acIniFile);
      return -1;
   }

   LogMsg("Import Value data to Pcl_Values for %s", pCnty);
   sprintf(sErrFile, sImportLogTmpl, "Value", pCnty, "Value");
   strcpy(sTblName, "Pcl_Values");

   // Delete old log files
   if (!_access(sErrFile, 0))
      DeleteFile(sErrFile);
   sprintf(acTmp, "%s.Error.txt", sErrFile);
   if (!_access(acTmp, 0))
      DeleteFile(acTmp);

   if (_access(pInFile, 0))
   {
      LogMsg("***** Missing import file: %s", pInFile);
      return -1;
   }

   GetIniString("Data", "ValueImportTmpl", "", sFmtFile, _MAX_PATH, acIniFile);
   if (_access(sFmtFile, 0))
   {
      LogMsg("***** Missing import format file: %s", sFmtFile);
      return -1;
   }

   // Prepare to import - Create/truncate table
   GetIniString("Database", "ValueDB", "", sDBName, _MAX_PATH, acIniFile);
   iRet = GetIniString("Database", "DBSvr2", "", sServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);    
   }
   iRet = GetIniString("Database", "DBSvr1", "", sServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);    
   }
   iRet = GetIniString("Database", "DBSvr3", "", sServer, _MAX_PATH, acIniFile);
   if (iRet > 1 && sDBName[0] > ' ')
   {
      sprintf(acTmp, "EXEC [dbo].[spValuePrep] '%s', %d ", pCnty, lLienYear);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);

      // Calling store procedure
      sprintf(acTmp, "EXEC [dbo].[spBulkImport] '%s', '%s', '%s', '%s', 1", sTblName, pInFile, sFmtFile, sErrFile);
      iRet = runSP(acTmp, sDBName, sServer, sProviderTmpl);    
   }

   return iRet;
}

/**************************** Cres_ParseTaxBase ******************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Cres_ParseTaxBase(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256], acInbuf[MAX_RECSIZE], *pTmp;
   int      iRet=0, iTmp;
   long     lPaidDate1, lPaidDate2, lDueDate1, lDueDate2;
   double	dTmp, dTax1, dTax2, dPen1, dPen2, dTaxTotal;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Parse input tax data
   strcpy(acInbuf, pInbuf);
   iTokens = ParseStringIQ(acInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < SEC_AUGUSTREDEMPTIONAMOUNT)
   {
      LogMsg("*** WARNING: bad TaxBase record for: %.50s (#tokens=%d < %d)", pInbuf, iTokens, SEC_AUGUSTREDEMPTIONAMOUNT);
      if (iTokens > SEC_APN)
         LogMsg("APN = %s", apTokens[SEC_APN]);
      else if (iTokens > SEC_NAME1)
         LogMsg("Owner = %s", apTokens[SEC_NAME1]);

      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // Some counties don't provide TAX_YEAR field (i.e. HUM)

   // APN
   strcpy(pOutRec->Apn, apTokens[SEC_APN]);
   remChar(pOutRec->Apn, '-');

   // Bill Number
   iTmp = atol(apTokens[SEC_BILLNUMBER]);
   sprintf(pOutRec->BillNum, "%d", iTmp);

   // TRA
   strcpy(pOutRec->TRA, apTokens[SEC_TRA]); 
   // Tax Year
   sprintf(pOutRec->TaxYear, "%d", lTaxYear);

   // Initialize
   pOutRec->isSecd[0] = '1';
   pOutRec->BillType[0] = BILLTYPE_SECURED;
   pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Check for Tax amount
   dTax1 = atof(apTokens[SEC_DUEAMOUNT1]);
   dTax2 = atof(apTokens[SEC_DUEAMOUNT2]);

   lPaidDate1=lPaidDate2=lDueDate1=lDueDate2=0;
   if (dTax1 > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      if (dateConversion(apTokens[SEC_DUEDATE1], acTmp, MMDDYY1, lToyear+1)) 
      {
         strcpy(pOutRec->DueDate1, acTmp);
         lDueDate1 = atol(acTmp);
      }
   }
   if (dTax2 > 0.0)
   {
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      if (dateConversion(apTokens[SEC_DUEDATE2], acTmp, MMDDYY1, lToyear+1))
      {
         strcpy(pOutRec->DueDate2, acTmp);
         lDueDate2 = atol(acTmp);
      }
   }

   // Total due
   dTaxTotal = atof(apTokens[SEC_TOTALDUE]);
   if (dTaxTotal > 0.0)
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);

   // Total outstanding
   remChar(apTokens[SEC_TOTALCURRENTOUTSTANDING], ',');
   dTmp = atof(apTokens[SEC_TOTALCURRENTOUTSTANDING]);
   if (dTmp > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTmp);

   // Populate penalty if dTaxTotal and current outstanding are not the same
   if (dTmp > dTaxTotal)
   {
      // Penalty
      if (pTmp = strchr(apTokens[SEC_PENALTY1], '='))
      {
         dPen1 = atof(++pTmp);
         if (dPen1 > 0.0)
            sprintf(pOutRec->PenAmt1, "%.2f", dPen1);
      }
      if (pTmp = strchr(apTokens[SEC_PENALTY2], '='))
      {
         dPen2 = atof(++pTmp);
         if (dPen2 > 0.0)
            sprintf(pOutRec->PenAmt2, "%.2f", dPen2);
      }
   } else
   {
      dPen1 = 0;
      dPen2 = 0;
   }

   // Payment Date
   if (!memcmp(apTokens[SEC_PAIDDATE1], "PAID", 4))
   {
      if (dateConversion(apTokens[SEC_PAIDDATE1]+5, acTmp, MMDDYY1))
      {
         strcpy(pOutRec->PaidDate1, acTmp);
         lPaidDate1 = atol(acTmp);
         sprintf(pOutRec->PaidAmt1, "%.2f", dTax1);
         pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      }
   }
   if (!memcmp(apTokens[SEC_PAIDDATE2], "PAID", 4))
   {
      if (dateConversion(apTokens[SEC_PAIDDATE2]+5, acTmp, MMDDYY1))
      {
         strcpy(pOutRec->PaidDate2, acTmp);
         lPaidDate2 = atol(acTmp);
         sprintf(pOutRec->PaidAmt2, "%.2f", dTax2);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      }
   }

   //if (lDueDate2 > lToday)
   //{
   //   if (lPaidDate2 == 0 && dPen2 > 0.0)
   //      pOutRec->isDelq[0] = '1';
   //} else if (lDueDate1 > lToday)
   //{
   //   if (lPaidDate1 == 0 && dPen1 > 0.0)
   //      pOutRec->isDelq[0] = '1';
   //}

   // Owner info
   if (*apTokens[SEC_NAME1] > ' ')
   {
      vmemcpy(pOutRec->OwnerInfo.Name1, apTokens[SEC_NAME1], TAX_NAME, 0);
      vmemcpy(pOutRec->OwnerInfo.Name2, apTokens[SEC_NAME2], TAX_NAME, 0);
   }

   // Mail addr
   if (*apTokens[SEC_STREETADDRESS] > ' ')
   {
      strcpy(pOutRec->OwnerInfo.MailAdr[0], apTokens[SEC_STREETADDRESS]);
      strcpy(pOutRec->OwnerInfo.MailAdr[1], apTokens[SEC_CITYSTATE]);
      strcpy(pOutRec->OwnerInfo.MailAdr[2], apTokens[SEC_ZIP]);
   }

   return 0;
}

/**************************** Cres_ParseTaxDetail ****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Cres_ParseTaxDetail(FILE *fdOut, char *pInbuf)
{
   char     acTmp[512], acInbuf[MAX_RECSIZE], acOutbuf[512];
   int      iRet=0, iTmp, iIdx;
   double	dTmp;
   TAXDETAIL *pOutRec = (TAXDETAIL *)acOutbuf;
   TAXAGENCY *pResult;

   // Parse input tax data
   strcpy(acInbuf, pInbuf);
   iTmp = ParseStringIQ(acInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < SEC_AUGUSTREDEMPTIONAMOUNT)
   {
      LogMsg("***** Error: bad TaxDetail record for: %.50s (#tokens=%d < %d)", pInbuf, iTmp, SEC_AUGUSTREDEMPTIONAMOUNT);
      return -1;
   }

   // Clear output buffer
   memset(acOutbuf, 0, sizeof(TAXDETAIL));

   // Some counties don't provide TAX_YEAR field (i.e. HUM)

   // APN
   strcpy(pOutRec->Apn, apTokens[SEC_APN]);
   remChar(pOutRec->Apn, '-');

   // BillNumber
   iTmp = atol(apTokens[SEC_BILLNUMBER]);
   sprintf(pOutRec->BillNum, "%d", iTmp);

   // Tax Year
   sprintf(pOutRec->TaxYear, "%d", lTaxYear);

   // Base Tax Amount
   dTmp = atof(apTokens[SEC_COUNTYTAX]);
   if (dTmp > 0.0)
   {
      // Tax Amt
      sprintf(pOutRec->TaxAmt, "%.2f", dTmp);

      // Tax Rate
      dTmp = atof(apTokens[SEC_TAXRATE]);
      sprintf(pOutRec->TaxRate, "%.4f", dTmp);

      // Agency - will be replaced by AgencyID
      blankRem(apTokens[SEC_AGENCYDESCRIPTION1]);
      strcpy(pOutRec->TaxDesc, apTokens[SEC_AGENCYDESCRIPTION1]);

      // Find Tax Code
      if (iNumTaxDist > 1)
      {
         pResult = findExactTaxDist(pOutRec->TaxDesc);
         //pResult = findTaxDist(pOutRec->TaxDesc);
         if (pResult)
         {
            strcpy(pOutRec->TaxCode, pResult->Code);
            pOutRec->TC_Flag[0] = pResult->TC_Flag[0];
         } else
            LogMsg("*** Unknown Tax Agency1: %s [%s]", pOutRec->TaxDesc, pOutRec->Apn);
      } else
         LogMsg("+++ Agency: %s", pOutRec->TaxDesc);

      // Generate csv line and write to file
      Tax_CreateDetailCsv(acTmp, pOutRec);
      fputs(acTmp, fdOut);
   }

   iTmp = 0;
   iIdx = SEC_AGENCYPHONE2;
   while (*apTokens[iIdx+1] > ' ')
   {
      dTmp = atof(apTokens[iIdx+4]);
      if (dTmp > 0.0)
      {
         // Tax amt
         sprintf(pOutRec->TaxAmt, "%.2f", dTmp);

         // Tax Rate
         strcpy(pOutRec->TaxRate, apTokens[iIdx+3]);

         // Agency - will be replaced by AgencyID
         blankRem(apTokens[iIdx+1]);
         strcpy(pOutRec->TaxDesc, apTokens[iIdx+1]);
         strcpy(pOutRec->Phone, apTokens[iIdx]);

         // Find Tax Code
         if (iNumTaxDist > 1)
         {
            pResult = findExactTaxDist(apTokens[iIdx+1]);
            //pResult = findTaxDist(pOutRec->TaxDesc);
            if (pResult)
            {
               strcpy(pOutRec->TaxCode, pResult->Code);
               pOutRec->TC_Flag[0] = pResult->TC_Flag[0];
            } else
               LogMsg("*** Unknown Tax Agency2: %s [%s]", pOutRec->TaxDesc, pOutRec->Apn);
         } else
            LogMsg("+++ Agency: %s", pOutRec->TaxDesc);

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pOutRec);
         fputs(acTmp, fdOut);
      }
      iIdx += 5;
      iTmp++;
   }

   // Reset Tax Rate
   pOutRec->TaxRate[0] = 0;
   iIdx = SEC_AGENCYPHONE17;
   while (*apTokens[iIdx+1] > ' ')
   {
      dTmp = atof(apTokens[iIdx+2]);
      if (dTmp > 0.0)
      {
         sprintf(pOutRec->TaxAmt, "%.2f", dTmp);

         // Agency - will be replaced by AgencyID
         blankRem(apTokens[iIdx+1]);
         strcpy(pOutRec->TaxDesc, apTokens[iIdx+1]);
         strcpy(pOutRec->Phone, apTokens[iIdx]);

         // Find Tax Code
         if (iNumTaxDist > 1)
         {
            pResult = findExactTaxDist(apTokens[iIdx+1]);
            //pResult = findTaxDist(apTokens[iIdx+1]);
            if (pResult)
            {
               strcpy(pOutRec->TaxCode, pResult->Code);
               pOutRec->TC_Flag[0] = pResult->TC_Flag[0];
            } else
            LogMsg("*** Unknown Tax Agency3: %s [%s]", pOutRec->TaxDesc, pOutRec->Apn);
         } else
            LogMsg("+++ Agency: %s", pOutRec->TaxDesc);

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pOutRec);
         fputs(acTmp, fdOut);
      }
      iIdx += 3;
      iTmp++;
   }

   return 0;
}

/**************************** Cres_ParseTaxAgency ****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Cres_ParseTaxAgency(FILE *fdOut, char *pInbuf)
{
   char     acInbuf[MAX_RECSIZE], acOutbuf[512], acAgency[512], acTmp[256];
   int      iRet=0, iTmp, iIdx;
   double	dAmt, dRate;
   TAXAGENCY *pResult, *pAgency=(TAXAGENCY *)&acAgency[0];

   // Parse input tax data
   strcpy(acInbuf, pInbuf);
   iTmp = ParseStringIQ(acInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < SEC_AUGUSTREDEMPTIONAMOUNT)
   {
      LogMsg("***** Error: bad TaxAgency record for: %.50s (#tokens=%d < %d)", pInbuf, iTmp, SEC_AUGUSTREDEMPTIONAMOUNT);
      return -1;
   }

   // Tax Rate
   dRate = atof(apTokens[SEC_TAXRATE]);
   if (dRate > 0.0)
   {
      memset(acAgency, 0, sizeof(TAXAGENCY));
      // Agency 
      blankRem(apTokens[SEC_AGENCYDESCRIPTION1]);
      strcpy(pAgency->Agency, apTokens[SEC_AGENCYDESCRIPTION1]);
      strcpy(pAgency->TaxRate, apTokens[SEC_TAXRATE]);

      // Find Tax Code
      if (iNumTaxDist > 1)
      {
         pResult = findExactTaxDist(apTokens[SEC_AGENCYDESCRIPTION1]);
         if (pResult)
         {
            pAgency->TC_Flag[0] = pResult->TC_Flag[0];
            strcpy(pAgency->Code, pResult->Code);
            if (pResult->Phone[0] > ' ')
               strcpy(pAgency->Phone, pResult->Phone);
            Tax_CreateAgencyCsv(acOutbuf, pAgency);
            fputs(acOutbuf, fdOut);
         } else
            LogMsg("*** Unknown Tax Agency: %s", apTokens[SEC_AGENCYDESCRIPTION1]);
      } else
      {
         pAgency->TC_Flag[0] = '1';
         strcpy(pAgency->Code, "N/A");
         Tax_CreateAgencyCsv(acOutbuf, pAgency);
         fputs(acOutbuf, fdOut);
      }
   }

   iTmp = 0;
   iIdx = SEC_AGENCYPHONE2;
   while (*apTokens[iIdx+1] > ' ')
   {
      memset(acAgency, 0, sizeof(TAXAGENCY));

      // Tax Rate
      dRate = atof(apTokens[iIdx+3]);
      dAmt  = atof(apTokens[iIdx+4]);

      // Agency - will be replaced by AgencyID
      blankRem(apTokens[iIdx+1]);

      // Find Tax Code
      if (iNumTaxDist > 1)
      {
         //pResult = findTaxDist(apTokens[iIdx+1]);
         pResult = findExactTaxDist(apTokens[iIdx+1]);
         if (pResult)
         {
            strcpy(pAgency->Agency, pResult->Agency);
            strcpy(pAgency->Code, pResult->Code);
            if (pResult->Phone[0] > ' ')
               strcpy(pAgency->Phone, pResult->Phone);
            else if (*apTokens[iIdx] > '0')
            {
               strcpy(acTmp, myTrim(apTokens[iIdx]));
               remChar(acTmp, '-');
               sprintf(pAgency->Phone, "(%.3s)%.3s-%.4s", acTmp, &acTmp[3], &acTmp[6]);
            }
            pAgency->TC_Flag[0] = pResult->TC_Flag[0];

            // Agency charges fix amount
            if (pResult->Code[0] == 'F')
               strcpy(pAgency->TaxAmt, apTokens[iIdx+4]);
            else if (pResult->Code[0] == 'R')
               strcpy(pAgency->TaxRate, apTokens[iIdx+3]);
            
            Tax_CreateAgencyCsv(acOutbuf, pAgency);
            fputs(acOutbuf, fdOut);
         } else
            LogMsg("*** Unknown Tax Agency: %s", apTokens[iIdx+1]);
      } else
      {
         pAgency->TC_Flag[0] = '1';
         strcpy(pAgency->Agency, apTokens[iIdx+1]);
         strcpy(pAgency->Code, "N/A");
         strcpy(pAgency->Phone, myTrim(apTokens[iIdx]));
         Tax_CreateAgencyCsv(acOutbuf, pAgency);
         fputs(acOutbuf, fdOut);
      }

      iIdx += 5;
      iTmp++;
   }

   // Reset Tax Rate
   iIdx = SEC_AGENCYPHONE17;
   while (*apTokens[iIdx+1] > ' ')
   {
      memset(acAgency, 0, sizeof(TAXAGENCY));

      // Agency - will be replaced by AgencyID
      blankRem(apTokens[iIdx+1]);

      // Ignore correction value
      //if (memcmp(apTokens[iIdx+1], "CORRECTIONS", 8))
      //{
         dAmt  = atof(apTokens[iIdx+2]);

         // Find Tax Code
         if (iNumTaxDist > 1)
         {
            pResult = findExactTaxDist(apTokens[iIdx+1]);
            if (pResult)
            {
               strcpy(pAgency->Agency, pResult->Agency);
               strcpy(pAgency->Code, pResult->Code);
               if (pResult->Phone[0] > ' ')
                  strcpy(pAgency->Phone, pResult->Phone);
               else if (*apTokens[iIdx] > '0')
               {
                  strcpy(acTmp, myTrim(apTokens[iIdx]));
                  remChar(acTmp, '-');
                  sprintf(pAgency->Phone, "(%.3s)%.3s-%.4s", acTmp, &acTmp[3], &acTmp[6]);
               }
               pAgency->TC_Flag[0] = pResult->TC_Flag[0];

               // Agency charges fix amount
               if (pResult->Code[0] == 'F')
                  strcpy(pAgency->TaxAmt, apTokens[iIdx+2]);
               
               Tax_CreateAgencyCsv(acOutbuf, pAgency);
               fputs(acOutbuf, fdOut);
            } else
               LogMsg("*** Unknown Tax Agency: %s", apTokens[iIdx+1]);
         } else
         {
            pAgency->TC_Flag[0] = '1';
            strcpy(pAgency->Code, "N/A");
            strcpy(pAgency->Agency, apTokens[iIdx+1]);
            strcpy(pAgency->Phone, myTrim(apTokens[iIdx]));
            Tax_CreateAgencyCsv(acOutbuf, pAgency);
            fputs(acOutbuf, fdOut);
         }

      //}
      iIdx += 3;
      iTmp++;
   }

   return 0;
}

/*************************** Cres_UpdateDelqYear *****************************
 *
 * Update Delq year in Tax_Base
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Cres_UpdateDelqYear(char *pOutbuf)
{
   static   char acRec[512], *pRec=NULL;
   char     acTmp[256];
   long     lRedDate, lDefDate;
   int      iTmp;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 512, fdDelq);
   }

   do 
   {
      if (!pRec)
      {
         fclose(fdDelq);
         fdDelq = NULL;
         return 1;      // EOF 
      }

      // Add 1 to Situs rec to skip double quote
      iTmp = memcmp(pOutbuf, pRec+7, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Delq rec %.80s", pRec);
         pRec = fgets(acRec, 512, fdDelq);
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 0;

   iTmp = ParseStringIQ(acRec, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTmp < ABS_REDEMPTIONDATE)
   {
      LogMsg("***** Error: bad Tax record for: %.50s (#tokens=%d)", acRec, iTmp);
      return -1;
   }

   // Parcel Type
   int iPrclType = atoi(apTokens[ABS_PARCELTYPE]);

   // Default date
   lDefDate = 0;
   if (dateConversion(apTokens[ABS_DEFAULTDATE], acTmp, MMDDYY2))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      lDefDate = atol(acTmp);
      acTmp[4] = 0;
      strcpy(pOutRec->DelqYear, acTmp);

      if (iPrclType < 80)
         pOutRec->isDelq[0] = '1';
   }

   // Redemption date
   lRedDate = 0;
   if (dateConversion(apTokens[ABS_REDEMPTIONDATE], acTmp, MMDDYY2))
   {
      lRedDate = atol(acTmp);
      if (lRedDate > lDefDate)
         pOutRec->isDelq[0] = '0';
   }

   // Get next record
   pRec = fgets(acRec, 512, fdDelq);

   return 0;
}

/**************************** Cres_Load_TaxBase ******************************
 *
 * Load scolbill.www to create import file for Tax_Base, Tax_Items, and Tax_Agency
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Cres_Load_TaxBase(bool bImport)
{
   char     *pTmp, acBase[MAX_RECSIZE], acRec[MAX_RECSIZE], acTmp[256];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH], acOutbuf[MAX_RECSIZE];

   int      iRet, iDrop=0;
   long     lBase=0, lDetail=0, lCnt=0;
   FILE     *fdBase, *fdIn, *fdDetail, *fdAgency;

   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   pTmp = strrchr(acOutFile, '\\');
   *pTmp = 0;
   if (_access(acOutFile, 0))
      _mkdir(acOutFile);
   *pTmp = '\\';

   LogMsg0("Loading TaxBase");

   sprintf(acInFile, sBaseTmpl, myCounty.acCntyCode);
   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(acBase, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open Secured tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Create Base file %s", acOutFile);
   fdBase = fopen(acOutFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   /* We need to populate the agency table first then extract detail   */
   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   LogMsg("Create Detail file %s", acOutFile);
   fdDetail = fopen(acOutFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acOutFile);
      return -4;
   }
   
   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
   LogMsg("Create Agency file %s", acOutFile);
   fdAgency = fopen(acOutFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acOutFile);
      return -4;
   }

   // Set delim
   GetPrivateProfileString(myCounty.acCntyCode, "Delimiter", "~", acTmp, _MAX_PATH, acIniFile);   
   cDelim = acTmp[0];

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp <= ' ')
         break;

      // Create new R01 record
      iRet = Cres_ParseTaxBase(acBase, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);

         iRet = Cres_ParseTaxDetail(fdDetail, acRec);
         if (!iRet)
            lDetail++;

         iRet = Cres_ParseTaxAgency(fdAgency, acRec);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:     %u", lCnt);
   LogMsg("Total base records output:   %u", lBase);
   LogMsg("Total detail records output: %u", lDetail);
   if (iDrop > 5)
   {
      LogMsg("Total detail records dropped: %u", iDrop);
      return -5;
   }

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (!iRet)
      {
         // Sort tax agency file before import
         sprintf(acInFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "AList");
         strcpy(acTmp, "S(#1,C,A) F(TXT) DUPO(#1) DEL(124)");
         iRet = sortFile(acOutFile, acInFile, acTmp);
         if (iRet > 0)
         {
            DeleteFile(acOutFile);
            iRet = rename(acInFile, acOutFile);
            if (!iRet)
            {
               iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
               //LogMsg("*** Tax Agency file is available.  Please import as needed");
            } else
               LogMsg("***** Error renaming file %s to %s (%d)", acInFile, acOutFile, _errno);
         }

         iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      }
   } else
      iRet = 0;

   return iRet;
}

/******************************** Cres_ParseDelq *****************************
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Cres_ParseDelq(char *pOutbuf, char *pInbuf)
{
   char     acTmp[256];
   int      iTmp, iPrclType;
   long     lRedDate, lDefDate;
   double	dTmp, dTaxAmt;
   TAXDELQ  *pOutRec = (TAXDELQ *)pOutbuf;

   // Parse input tax data
   iTokens = ParseStringIQ(pInbuf, cDelim, MAX_FLD_TOKEN, apTokens);
   if (iTokens < ABS_REDEMPTIONDATE)
   {
      LogMsg("***** Error: bad Tax record for: %.50s (#tokens=%d)", pInbuf, iTokens);
      return -1;
   }

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXDELQ));
   lRedDate=lDefDate = 0;

   // APN
   sprintf(acTmp, "%s00", apTokens[ABS_APN]);
   acTmp[iApnLen] = 0;
   strcpy(pOutRec->Apn, acTmp);
   iTmp = atol(apTokens[ABS_ACCTNUMBER]);
   sprintf(pOutRec->BillNum, "%d", iTmp);

   // Parcel Type
   // 10 = plain parcel with no installment plan
   // 60 = bankruptcy filed
   // 65 =
   // 70 = Tax Collectors Power to Sell recorded eligible for sale
   // 75 =
   // 80 = Installment plan in good standing
   // 85 =
   // 81 = defaulted installment plan or partial payments applied
   // 89 = Paid in full with multiple payments or by installment plan
   // 99 = Paid in full with one payment
   strcpy(pOutRec->PrclType, apTokens[ABS_PARCELTYPE]);
   iPrclType = atoi(pOutRec->PrclType);
   switch (iPrclType)
   {
      case 10:
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
         break;
      case 60:
         pOutRec->DelqStatus[0] = TAX_STAT_BKRPCY;
         break;
      case 70:
      case 75:
         pOutRec->DelqStatus[0] = TAX_STAT_POWER2SELL;
         break;
      case 80:
         pOutRec->DelqStatus[0] = TAX_STAT_ONPAYMENT;
         break;
      case 81:
         pOutRec->DelqStatus[0] = TAX_STAT_DFLTINST;
         pOutRec->isDelq[0] = '1';
         break;
      case 89:
      case 99:
         pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
         break;
      default:
         pOutRec->DelqStatus[0] = TAX_STAT_UNPAID;
   }
   if (iPrclType < 80)
      pOutRec->isDelq[0] = '1';

   // Updated date
   strcpy(pOutRec->Upd_Date, acToday);

   // Tax Year
   iTmp = 2000 + atol(apTokens[ABS_ROLLYEAR]);
   sprintf(pOutRec->TaxYear, "%d", iTmp);

   // Default amount
   dTaxAmt = atof(apTokens[ABS_BASETAX]);
   sprintf(pOutRec->Def_Amt, "%.2f", dTaxAmt);

   // Default date
   if (dateConversion(apTokens[ABS_DEFAULTDATE], acTmp, MMDDYY2))
   {
      strcpy(pOutRec->Def_Date, acTmp);
      lDefDate = atol(acTmp);
   }

   // Supplemental delq
   if (*apTokens[ABS_SUPPLIMENTALFLAG] == 'Y')
      pOutRec->isSupp[0] = '1';

   // Redemption date
   if (dateConversion(apTokens[ABS_REDEMPTIONDATE], acTmp, MMDDYY2))
   {
      strcpy(pOutRec->Red_Date, acTmp);
      lRedDate = atol(acTmp);
      if (lRedDate > lDefDate)
      {
         pOutRec->isDelq[0] = '0';
         pOutRec->DelqStatus[0] = TAX_STAT_REDEEMED;
      }
   }

   // Installment delinquent
   strcpy(pOutRec->InstDel, apTokens[ABS_INSTALLMENTDEL]);

   // Current default value
   dTmp = 0;
   if (iTokens > ABS_REDEMPTIONAUGUST2)
   {
      iTmp = atoin(&acToday[4], 2);
      switch(iTmp)
      {
         case 1:
            dTmp = atof(apTokens[ABS_REDEMPTIONJANUARY]);
            break;
         case 2:
            dTmp = atof(apTokens[ABS_REDEMPTIONFEBRUARY]);
            break;
         case 3:
            dTmp = atof(apTokens[ABS_REDEMPTIONMARCH]);
            break;
         case 4:
            dTmp = atof(apTokens[ABS_REDEMPTIONAPRIL]);
            break;
         case 5:
            dTmp = atof(apTokens[ABS_REDEMPTIONMAY]);
            break;
         case 6:
            dTmp = atof(apTokens[ABS_REDEMPTIONJUNE]);
            break;
         case 7:
            if (lToyear <= lLienYear)
               dTmp = atof(apTokens[ABS_REDEMPTIONJULY1]);
            else
               dTmp = atof(apTokens[ABS_REDEMPTIONJULY2]);
            break;
         case 8:
            if (lToyear <= lLienYear)
               dTmp = atof(apTokens[ABS_REDEMPTIONAUGUST1]);
            else
               dTmp = atof(apTokens[ABS_REDEMPTIONAUGUST2]);
            break;
         case 9:
            dTmp = atof(apTokens[ABS_REDEMPTIONSEPTEMBER]);
            break;
         case 10:
            dTmp = atof(apTokens[ABS_REDEMPTIONOCTOBER]);
            break;
         case 11:
            dTmp = atof(apTokens[ABS_REDEMPTIONNOVEMBER]);
            break;
         case 12:
            dTmp = atof(apTokens[ABS_REDEMPTIONDECEMBER]);
            break;
      }

      // Redemption amt
      if (dTmp > 0.0)
      {
         sprintf(pOutRec->Red_Amt, "%.2f", dTmp);
      }
   }

   return 0;
}

/**************************** Cres_Load_Delq *********************************
 *
 * Create import file for SABSDATA & SABSDATA.WWW and import into SQL if specified
 * SABSDATA is the same as SABSDATA.WWW upto ABS_REDEMPTIONDATE.  After that, county specific.
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Cres_Load_TaxDelq(bool bImport)
{
   char     *pTmp, acBuf[MAX_RECSIZE], acRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH];

   int      iRet;
   long     lOut=0, lCnt=0;
   FILE     *fdOut, *fdIn;

   LogMsg0("Loading TaxDelq");

   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
   sprintf(acInFile, sRedTmpl, myCounty.acCntyCode);

   // Open input file
   LogMsg("Open abstract tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acInFile);
      return -2;
   }  

   lLastFileDate = getFileDate(acInFile);

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp < ' ')
         break;

      // Create new R01 record
      iRet = Cres_ParseDelq(acBuf, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acBuf);

         // Output record			
         lOut++;
         fputs(acRec, fdOut);
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", acRec); 
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   printf("\nTotal records processed:    %u\n", lCnt);
   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lOut);

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
   } else
      iRet = 0;

   return iRet;
}

/**************************** Cres_ParseCortac ******************************
 *
 * Prepare for INY
 *
 * Return 0 if success
 *
 ****************************************************************************/

int Cres_ParseCortac(FILE *fdR01, char *pOutbuf, char *pInbuf)
{
   int      iRet=0, iTmp;
   long     lPaidDate1, lPaidDate2, lDueDate1, lDueDate2;
   double	dTmp, dTax1, dTax2, dTaxTotal;
   TAXBASE  *pOutRec = (TAXBASE *)pOutbuf;
   CORTAC   *pInRec  = (CORTAC *)pInbuf;

   // Clear output buffer
   memset(pOutbuf, 0, sizeof(TAXBASE));

   // APN
   memcpy(pOutRec->Apn, pInRec->Apn, iApnLen);
   iTmp = iTrim(pOutRec->Apn);
   if (iApnLen > iTmp)
      strncpy(&pOutRec->Apn[iTmp], "0000", iApnLen-iTmp);

   // Bill Number
   iTmp = atoin(pInRec->BillNo, TSIZ_BILLNO);
   sprintf(pOutRec->BillNum, "%d", iTmp);

   // TRA
   if (fdR01)
      UpdateTRA(pOutbuf, fdR01);

   // Tax Year
   sprintf(pOutRec->TaxYear, "%d", lTaxYear);

   // Set secured type
   pOutRec->isSecd[0] = '1';
   pOutRec->BillType[0] = BILLTYPE_SECURED;
   pOutRec->Inst1Status[0] = TAX_BSTAT_UNPAID;
   pOutRec->Inst2Status[0] = TAX_BSTAT_UNPAID;

   // Check for Tax amount
   dTax1 = atofn(pInRec->FirstTaxAmt, TSIZ_TOTALTAXAMT);
   dTax2 = atofn(pInRec->SecTaxAmt, TSIZ_TOTALTAXAMT);
   lPaidDate1=lPaidDate2=lDueDate1=lDueDate2= 0;

   // Paid or Due date
   if (dTax1 > 0.0)
   {
      sprintf(pOutRec->TaxAmt1, "%.2f", dTax1);
      if (pInRec->FirstPaymentDate[0] > ' ') 
      {
         sprintf(pOutRec->PaidDate1, "20%.2s%.4s", &pInRec->FirstPaymentDate[4], pInRec->FirstPaymentDate);
         lPaidDate1 = atol(pOutRec->PaidDate1);
         strcpy(pOutRec->PaidAmt1, pOutRec->TaxAmt1);
         pOutRec->Inst1Status[0] = TAX_BSTAT_PAID;
      } else
      {
         sprintf(pOutRec->DueDate1, "%d1210", lTaxYear);
         lDueDate1 = atol(pOutRec->DueDate1);
      }
   }
   if (dTax2 > 0.0)
   {
      sprintf(pOutRec->TaxAmt2, "%.2f", dTax2);
      if (pInRec->SecPaymentDate[0] > ' ') 
      {
         sprintf(pOutRec->PaidDate2, "20%.2s%.4s", &pInRec->SecPaymentDate[4], pInRec->SecPaymentDate);
         lPaidDate2 = atol(pOutRec->PaidDate2);
         strcpy(pOutRec->PaidAmt2, pOutRec->TaxAmt2);
         pOutRec->Inst2Status[0] = TAX_BSTAT_PAID;
      } else
         sprintf(pOutRec->DueDate2, "%d0410", lTaxYear+1);
   }

   // Total due
   dTaxTotal = atofn(pInRec->TotalTaxAmt, TSIZ_TOTALTAXAMT, true);
   if (dTaxTotal)
      sprintf(pOutRec->TotalTaxAmt, "%.2f", dTaxTotal);

   // Total outstanding
   if (lPaidDate1 == 0)
      dTmp = dTax1 + dTax2;
   else if (lPaidDate2 == 0)
      dTmp = dTax2;
   else
      dTmp = 0;

   if (dTmp > 0.0)
      sprintf(pOutRec->TotalDue, "%.2f", dTmp);

#ifdef _DEBUG
   //if (!memcmp(pOutRec->Apn, "0010800180", 10))
   //   dTmp = 0;
#endif

   // Delq
   if (pInRec->SoldToStateDate[0] > ' ')
   {
      sprintf(pOutRec->Def_Date, "%.4s%.4s", &pInRec->SoldToStateDate[4], pInRec->SoldToStateDate);
      if (lPaidDate1 == 0)
         pOutRec->isDelq[0] = '1';
   }
   memcpy(pOutRec->DelqStatus, pInRec->DelqStatus, 2);
   myTrim(pOutRec->DelqStatus);

   return 0;
}

/**************************** Cres_Load_Cortac ******************************
 *
 * Create import file from CortacFile and import into SQL ???_Base table if specified
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Cres_Load_Cortac(bool bImport, bool bCreateDelq)
{
   char     *pTmp, acBase[MAX_RECSIZE], acDelq[256], acRec[MAX_RECSIZE], acTmp[256];
   char     acOutFile[_MAX_PATH], acInFile[_MAX_PATH], acR01File[_MAX_PATH], acOutbuf[MAX_RECSIZE];

   int      iRet, iDrop=0;
   long     lDelq=0, lBase=0, lDetail=0, lCnt=0;
   FILE     *fdDelq, *fdBase, *fdIn, *fdR01;
   TAXBASE  *pBase = (TAXBASE *)acBase;
   TAXDELQ  *pDelq = (TAXDELQ *)acDelq;

   sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
   pTmp = strrchr(acOutFile, '\\');
   *pTmp = 0;
   if (_access(acOutFile, 0))
      _mkdir(acOutFile);
   *pTmp = '\\';

   sprintf(acInFile, sBaseTmpl, myCounty.acCntyCode);
   lLastTaxFileDate = getFileDate(acInFile);

   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0)
   {
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open Secured tax file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Secured tax file: %s\n", acInFile);
      return -2;
   }  

   // Open Output file
   LogMsg("Create Base file %s", acOutFile);
   fdBase = fopen(acOutFile, "w");
   if (fdBase == NULL)
   {
      LogMsg("***** Error creating output file: %s\n", acOutFile);
      return -4;
   }

      // Open R01 file
   iRet = GetIniString("Data", "RawFile", "", acRec, 128, acIniFile);
   sprintf(acR01File, acRec, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open R01 file %s", acR01File);
   if (_access(acR01File, 0))
      sprintf(acR01File, acRec, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   try 
   {
      fdR01 = fopen(acR01File, "rb");
      if (!fdR01)
         LogMsg("*** Missing R01 file: %s", acR01File);
   } catch (...)
   {
      LogMsg("***** Error opening R01 file: %s", acR01File);
      fdR01 = NULL;
   }

   if (bCreateDelq)
   {
      sprintf(acOutFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Delq");
      LogMsg("Create Base file %s", acOutFile);
      fdDelq = fopen(acOutFile, "w");
      memset(acDelq, 0, sizeof(TAXDELQ));
      pDelq->CRLF[0] = '\r';
   } else
      fdDelq = NULL;

   // Set delim
   GetPrivateProfileString(myCounty.acCntyCode, "Delimiter", "~", acTmp, _MAX_PATH, acIniFile);   
   cDelim = acTmp[0];

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp || *pTmp <= ' ')
         break;

      // Create new R01 record
      iRet = Cres_ParseCortac(fdR01, acBase, acRec);
      if (!iRet)
      {
         // Create delimited record
         Tax_CreateTaxBaseCsv(acOutbuf, (TAXBASE *)&acBase);

         // Output record			
         lBase++;
         fputs(acOutbuf, fdBase);

         if (fdDelq && pBase->Def_Date[0] > ' ')
         {
            strcpy(pDelq->Apn, pBase->Apn);
            strcpy(pDelq->BillNum, pBase->BillNum);
            strcpy(pDelq->Def_Amt, pBase->TotalTaxAmt);
            strcpy(pDelq->Def_Date, pBase->Def_Date);
            iRet = atoin(pBase->Def_Date, 4);
            sprintf(pDelq->TaxYear, "%.4d", iRet -1);

            // ALP: J,D,S,Y,Blank
            // INY: E9,E4,E3,E2,J,S
            if (pBase->DelqStatus[0] > ' ')
            {
               if (pBase->DelqStatus[0] == 'Y')
                  pDelq->DelqStatus[0] = TAX_STAT_ONPAYMENT;
               else if (pBase->DelqStatus[0] == 'S')
                  pDelq->DelqStatus[0] = TAX_STAT_POWER2SELL;
               else if (pBase->DelqStatus[0] == 'E')
               {
                  if (pBase->DelqStatus[1] > '5')
                     pDelq->DelqStatus[0] = TAX_STAT_CORRECTION;
                  else
                     pDelq->DelqStatus[0] = TAX_STAT_UNPAID;
               } else if (pBase->isDelq[0] == '1')
                  pDelq->DelqStatus[0] = TAX_STAT_UNPAID;
            }
            pDelq->isDelq[0] = pBase->isDelq[0];

            Tax_CreateDelqCsv(acRec, (TAXDELQ *)&acDelq);
            fputs(acRec, fdDelq);
            lDelq++;
         }
      } else
      {
         LogMsg("---> Drop record %d [%.40s]", lCnt, acRec); 
         iDrop++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdBase)
      fclose(fdBase);
   if (fdDelq)
      fclose(fdDelq);
   if (fdR01)
      fclose(fdR01);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:     %u", lCnt);
   LogMsg("Total base records output:   %u", lBase);
   if (bCreateDelq)
      LogMsg("Total delq records output:   %u", lDelq);

   if (iDrop > 5)
   {
      LogMsg("Total CORTAC records dropped: %u", iDrop);
      return -5;
   }

   // Import into SQL
   if (bImport)
   {
      iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);
      if (bCreateDelq)
      {
         iRet = doTaxImport(myCounty.acCntyCode, TAX_DELINQUENT);
         iRet = updateDelqFlag(myCounty.acCntyCode);
      }
   } else
      iRet = 0;

   return iRet;
}

/******************************** Cres_ParseGFGIS ****************************
 *
 * Use GFGIS to generate TAXDETAIL & TAXAGENCY
 * Skip items that have no tax amt
 *
 * Return 0 if success
 *
 *****************************************************************************/

int Cres_ParseGFGIS(FILE *fdDetail, FILE *fdAgency, char *pInbuf)
{
   char     acTmp[512], acDetail[512], acAgency[512], acCode[8];
   int      iRet=0, iTmp, iIdx;
   double	dTmp;
   TAXDETAIL *pDetail = (TAXDETAIL *)acDetail;
   TAXAGENCY *pResult, *pAgency = (TAXAGENCY *)acAgency;
   GFGIS     *pGfgis  = (GFGIS *)pInbuf;

   // Clear output buffer
   memset(acDetail, 0, sizeof(TAXDETAIL));
   memset(acAgency, 0, sizeof(TAXAGENCY));

   // Some counties don't provide TAX_YEAR field (i.e. HUM)

   // APN
   memcpy(acTmp, pGfgis->Apn, iApnLen);
   iTmp = iTrim(acTmp, iApnLen);
   if (iTmp < iApnLen)
   {
      strcat(acTmp, "000");
      acTmp[iApnLen] = 0;
   }
   strcpy(pDetail->Apn, acTmp);

   // TRA

   // BillNumber
   iTmp = atoin(pGfgis->BillNum, TSIZ_BILLNO);
   sprintf(pDetail->BillNum, "%d", iTmp);

   // Tax Year
   sprintf(pDetail->TaxYear, "%d", lTaxYear);

   iIdx = 0;
   while (pGfgis->asAgency[iIdx].Agency[0] > ' ')
   {      
      dTmp = atofn(pGfgis->asAgency[iIdx].TaxAmt, TSIZ_TAXAMT, true);
      if (dTmp != 0.0)
      {
         sprintf(pDetail->TaxAmt, "%.2f", dTmp);

         // Tax Rate
         dTmp = atofn(pGfgis->asAgency[iIdx].TaxRate, TSIZ_TAXRATE, true);
         if (dTmp != 0.0)
            sprintf(pDetail->TaxRate, "%.6f", dTmp);
         else
            pDetail->TaxRate[0] = 0;

         // Agency
         myTrim(pGfgis->asAgency[iIdx].Agency, TSIZ_ANAME);
         if (iNumTaxDist > 0 && pGfgis->asAgency[iIdx].delim == '/' && isdigit(pGfgis->asAgency[iIdx].TaxCode[0]))
         {
            memcpy(acCode, pGfgis->asAgency[iIdx].TaxCode, TSIZ_ACODE);
            acCode[TSIZ_ACODE] = 0;
            pResult = findTaxAgency(acCode);
            if (!pResult)
               pResult = findExactTaxDist(pGfgis->asAgency[iIdx].Agency);
         } else if (pGfgis->asAgency[iIdx].Agency[0] > ' ')
         {
            pResult = findExactTaxDist(pGfgis->asAgency[iIdx].TaxCode);
         } else
            pResult = NULL;

         if (pResult)
         {
            strcpy(pAgency->Code,  pResult->Code);
            strcpy(pAgency->Agency,pResult->Agency);
            strcpy(pAgency->Phone, pResult->Phone);
            strcpy(pDetail->TaxDesc, pResult->Agency);
            strcpy(pDetail->TaxCode, pResult->Code);
            pDetail->TC_Flag[0] = pResult->TC_Flag[0];
            pAgency->TC_Flag[0] = pResult->TC_Flag[0];
         } else
         {
            strcpy(pDetail->TaxDesc, pGfgis->asAgency[iIdx].Agency);
            strcpy(pAgency->Agency, pDetail->TaxDesc);
            if (!memcmp(myCounty.acCntyCode, "INY", 3) && !memcmp(pAgency->Agency, "PENALTY", 6))
            {
               strcpy(pAgency->Code, "V01");
               strcpy(pDetail->TaxCode, pAgency->Code);
               pAgency->TC_Flag[0] = '1';
               pDetail->TC_Flag[0] = '1';
            } else
            {
               pAgency->Code[0] = 0;
               LogMsg("--> New Tax Agency: %s", pGfgis->asAgency[iIdx].TaxCode);
            }
         }

         // Generate csv line and write to file
         Tax_CreateDetailCsv(acTmp, pDetail);
         fputs(acTmp, fdDetail);

         // Generate Agency record
         Tax_CreateAgencyCsv(acTmp, pAgency);
         fputs(acTmp, fdAgency);
      }

      iIdx++;
   }

   return 0;
}

int Cres_CreateBaseWithGFGIS(FILE *fdBase, char *pInbuf)
{
   char     acTmp[512], acBase[1024];
   int      iRet=0, iTmp;
   double	dTotalTax;
   TAXBASE  *pBase = (TAXBASE *)acBase;
   GFGIS     *pGfgis  = (GFGIS *)pInbuf;

   // Clear output buffer
   memset(acBase, 0, sizeof(TAXBASE));

   // APN
   memcpy(pBase->Apn, pGfgis->Apn, iApnLen);

   // TRA
   iTmp = atoin(pGfgis->TRA, TSIZ_TRA);
   sprintf(pBase->TRA, "%.6d", iTmp);

   // BillNumber
   iTmp = atoin(pGfgis->BillNum, TSIZ_BILLNO);
   sprintf(pBase->BillNum, "%d", iTmp);

   // Tax Year
   sprintf(pBase->TaxYear, "%d", lTaxYear);

   // Tax
   dTotalTax = atofn(pGfgis->TotalTax, TSIZ_TAXAMT);

   if (dTotalTax > 0.0)
   {
      sprintf(pBase->TaxAmt1, "%.2f", dTotalTax/2.0);
      sprintf(pBase->TotalTaxAmt, "%.2f", dTotalTax);
      sprintf(pBase->TotalDue, "%.2f", dTotalTax);
      strcpy(pBase->TaxAmt2, pBase->TaxAmt1);
      InstDueDate(pBase->DueDate1, 1, lTaxYear);
      InstDueDate(pBase->DueDate2, 2, lTaxYear);
   }

   pBase->isSecd[0] = '1';
   pBase->BillType[0] = BILLTYPE_SECURED;

   // Generate Base record
   Tax_CreateTaxBaseCsv(acTmp, pBase);
   fputs(acTmp, fdBase);

   return 0;
}

/****************************** Cres_Load_GFGIS ******************************
 *
 * Create TAXDETAIL & TAXAGENCY file from GFGIS and import into SQL ???_Base table if specified
 * Create TAXBASE if bCreateBase is true
 *
 * Used by ALP, INI, TRI
 *
 * Return 0 if success.
 *
 *****************************************************************************/

int Cres_Load_GFGIS(bool bImport, bool bCreateBase)
{
   char  *pTmp, acRec[MAX_RECSIZE], acTmp[256],
         acTmpFile[_MAX_PATH], acAgencyFile[_MAX_PATH], acInFile[_MAX_PATH];

   long  iRet, lDetail=0, lCnt=0;
   FILE  *fdIn, *fdDetail, *fdAgency, *fdBase;

   LogMsg0("Loading GFGIS");
   GetIniString(myCounty.acCntyCode, "GFGIS", "", acInFile, _MAX_PATH, acIniFile);   
   if (_access(acInFile, 0))
   {
      LogMsg("***** Missing GFGIS input file: %s", acInFile);
      return -1;
   }

   lLastTaxFileDate = getFileDate(acInFile);
   // Only process if new tax file
   iRet = isNewTaxFile(NULL, myCounty.acCntyCode);
   if (iRet <= 0 && bCreateBase)
   {
      LogMsg("*** Detail data is up to date.  Skip loading");
      lLastTaxFileDate = 0;
      return iRet;
   }

   // Open input file
   LogMsg("Open GFGIS file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening GFGIS file: %s\n", acInFile);
      return -2;
   }  

   // Create Base file
   if (bCreateBase)
   {
      // Open tax base
      sprintf(acTmpFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Base");
      LogMsg("Create Base file %s", acTmpFile);
      fdBase = fopen(acTmpFile, "w");
      if (fdBase == NULL)
      {
         LogMsg("***** Error creating Base file: %s\n", acTmpFile);
         return -4;
      }
   }

   // Create Items record file
   sprintf(acTmpFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Items");
   LogMsg("Create Detail file %s", acTmpFile);
   fdDetail = fopen(acTmpFile, "w");
   if (fdDetail == NULL)
   {
      LogMsg("***** Error creating Detail file: %s\n", acTmpFile);
      return -4;
   }
   
   // Create Agency file
   sprintf(acTmpFile, "%s\\%s\\%s_Agency.tmp", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Create Agency file %s", acTmpFile);
   fdAgency = fopen(acTmpFile, "w");
   if (fdAgency == NULL)
   {
      LogMsg("***** Error creating Agency file: %s\n", acTmpFile);
      return -4;
   }

   // Merge loop 
   while (!feof(fdIn))
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
      if (!pTmp)
         break;

      if (bCreateBase)
         iRet = Cres_CreateBaseWithGFGIS(fdBase, acRec);

      iRet = Cres_ParseGFGIS(fdDetail, fdAgency, acRec);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdIn)
      fclose(fdIn);
   if (fdDetail)
      fclose(fdDetail);
   if (fdAgency)
      fclose(fdAgency);
   if (bCreateBase && fdBase)
      fclose(fdBase);

   printf("\nTotal records processed:     %u\n", lCnt);
   LogMsg("Total records processed:     %u", lCnt);

   // Import into SQL
   if (bImport)
   {
      if (bCreateBase)
         iRet = doTaxImport(myCounty.acCntyCode, TAX_BASE);

      iRet = doTaxImport(myCounty.acCntyCode, TAX_DETAIL);
      if (!iRet)
      {
         // Populate TotalRate - EXEC spUpdateTotalRate 'INY'
         iRet = doUpdateTotalRate(myCounty.acCntyCode);

         // Sort tax agency file before import
         sprintf(acAgencyFile, sTaxOutTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Agency");
         strcpy(acTmp, "S(#1,C,A) F(TXT) DUPO");
         iRet = sortFile(acTmpFile, acAgencyFile, acTmp);
         if (iRet > 0)
            iRet = doTaxImport(myCounty.acCntyCode, TAX_AGENCY);
      }
   } else
      iRet = 0;

   return iRet;
}

/********************************* Cres_ExtrProp8 ***************************
 *
 * Extract Prop8 parcels.
 *
 ****************************************************************************/

int Cres_ExtrProp8(char *pProp8File)
{
   char     acTmp[256], acProp8Rec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   FILE     *fdProp8, *fdExt;
   REDIFILE *pRec = (REDIFILE *)&acProp8Rec[0];

   int      iRet, iProp8Match=0;
   long     lCnt=0;
   bool     bEof;

   if (lLienYear > 1900)
      sprintf(acTmp, "%d", lLienYear);
   else
      strcpy(acTmp, "yyyy");
   sprintf(acOutFile, acProp8Tmpl, myCounty.acCntyCode, myCounty.acCntyCode, acTmp, "P8");

   // Open roll file
   LogMsg("\nExtract Prop8 from %s", pProp8File);
   fdProp8 = fopen(pProp8File, "rb");
   if (fdProp8 == NULL)
   {
      LogMsg("***** Error opening: %s\n", pProp8File);
      return -1;
   }

   // Open Output file
   LogMsg("Open Prop8 extract file %s", acOutFile);
   fdExt = fopen(acOutFile, "w");
   if (fdExt == NULL)
   {
      LogMsg("***** Error creating Prop8 extract file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acProp8Rec[0], 1, iRollLen, fdProp8);
   bEof = (iRet==iRollLen ? false:true);

   // Merge loop
   while (!bEof)
   {
      // Prop 8
      if (!memcmp(pRec->BaseCode, "67", 2))
      {
         sprintf(acTmp, "%.*s\n", CRESIZ_APN, pRec->APN);
         fputs(acTmp, fdExt);
         iProp8Match++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      // Get next RollRec
      iRet = fread((char *)&acProp8Rec[0], 1, iRollLen, fdProp8);
      bEof = (iRet==iRollLen ? false:true);
   }

   // Close files
   if (fdExt)
      fclose(fdExt);
   if (fdProp8)
      fclose(fdProp8);

   LogMsgD("Total records processed:      %u", lCnt);
   LogMsgD("Total records extracted:      %u", iProp8Match);

   return 0;
}

/************************************** main *********************************
 *
 *
 *****************************************************************************/

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
   int   iRet=1, iTmp;
   char  *pTmp, acTmp[256], acLogPath[256], acVersion[128], acSubj[256], acBody[512];
   char  acTmpFile[_MAX_PATH], acOutFile[_MAX_PATH];

   // initialize MFC and print and error on failure
   if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
   {
      cerr << _T("Fatal Error: MFC initialization failed") << endl;
      return 1;
   }

   iRet = LoadString(theApp.m_hInstance, APP_VERSION_INFO, acVersion, 64);
   printf("%s\n\n", acVersion);

   strcat(acVersion, " Options: ");
   for (iTmp = 1; iTmp < argc; iTmp++)
   {
      strcat(acVersion, argv[iTmp]);
      strcat(acVersion, " ");
   }

   if (argc < 2)
      Usage();

   // Get today date - yyyymmdd
   getDate(acToday, 0);
   lToday = atoi(acToday);
   lToyear= atoin(acToday, 4);

   // Parse command line
   ParseCmd(argc, argv);

   pTmp = _getcwd((char *)&acIniFile[0], _MAX_PATH);
   strcat(acIniFile, "\\LoadCres.ini");

   // If not found INI file in working folder, check default location
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\LoadCres.ini");

   GetIniString("System", "LogPath", "", acLogPath, _MAX_PATH, acIniFile);

   // open log file
   if (iLoadFlag & LOAD_LIEN)
   {
      if (lLienYear > 0)
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lLienYear);
      else
         sprintf(acLogFile, "%s\\Load_%s_%d.ldr", acLogPath, myCounty.acCntyCode, lToyear);
      lLDRRecCount = 0;
   } else if (iLoadTax && !iLoadFlag)
      sprintf(acLogFile, "%s\\LoadTax_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);
   else
      sprintf(acLogFile, "%s\\Load_%s_%s.log", acLogPath, myCounty.acCntyCode, acToday);

   if (bOverwriteLogfile)
      open_log(acLogFile, "w");
   else
      open_log(acLogFile, "a+");

   // Open Usecode log
   if (!openUsecodeLog(myCounty.acCntyCode, acIniFile))
      LogMsg("***** %s", getLastErrorLog());

   LogMsg("%s\n\n", acVersion);
   LogMsg("Loading %s", myCounty.acCntyCode);

   if (bFixLienExt)
   {
      // Lien template name
      GetIniString(myCounty.acCntyCode, "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);
      if (acLienTmpl[0] < 'A')
         GetIniString("Data", "LienOut", "", acLienTmpl, _MAX_PATH, acIniFile);

      iRet = PQ_ConvLienExt(myCounty.acCntyCode);
      if (!iRet)
         LogMsgD("\nConvert complete!");
      close_log();
      exit (0);
   }

   // Initialize globals
   iMaxLegal = 0;
   lLDRRecCount = 0;
   LogMsgD("Initializing %s\n", acIniFile);
   iRet = MergeInit(myCounty.acCntyCode);
   if (iRet < 1)
   {
      LogMsg("***** Error initializing %s in LoadCres\n", myCounty.acCntyCode);
      if (!iRet)
         LogMsg("*** Please check Vesting table. Number of entries: %d\n", iRet);

      goto LoadCres_Exit;
   }

   // -X8 Extract prop8 flag to text file
   if (iLoadFlag & EXTR_PRP8)          
   {
      iRet = Cres_ExtrProp8(acRollFile);
      goto LoadCres_Exit;
   }

   if (iRollLen != sizeof(REDIFILE))
      LogMsgD("*** WARNING: Roll length is not the same as REDIFILE struc. %d <> %d ***", iRollLen, sizeof(REDIFILE));

   if (iLoadFlag & LOAD_LIEN)
      LogMsg("LDR %s processing.", myCounty.acYearAssd);
   else if (iLoadFlag & LOAD_UPDT)
      LogMsg("Regular update using %s base year.", myCounty.acYearAssd);

   // Reset automation flag to 'W' for working
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|LOAD_GRGR|EXTR_SALE))
   {
      sprintf(acTmp, "UPDATE County SET Status='W' WHERE (CountyCode='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }
   if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
   {
      sprintf(acTmp, "UPDATE Products SET State='W' WHERE (Prefix='S%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }
   if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
   {
      sprintf(acTmp, "UPDATE Products SET State='W' WHERE (Prefix='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Retrieve last record count
   iTmp = getCountyInfo() ;
   if (iTmp < 0)
   {
      close_log();
      return 1;
   }

   // Load roll file
   if (!memcmp(myCounty.acCntyCode, "INY", 3))
      iRet = loadIny(iLoadFlag, 1);
   else if (!memcmp(myCounty.acCntyCode, "LAS", 3))
      iRet = loadLas(iLoadFlag, 1);
   else if (!memcmp(myCounty.acCntyCode, "TRI", 3))
      iRet = loadTri(iLoadFlag, 1);
   //if (!memcmp(myCounty.acCntyCode, "ALP", 3))
   //   iRet = loadAlp(iLoadFlag, 1);
   //else if (!memcmp(myCounty.acCntyCode, "DNX", 3))
   //   iRet = loadDnx(iLoadFlag, 1);
   //else if (!memcmp(myCounty.acCntyCode, "GLE", 3))
   //   iRet = loadGle(iLoadFlag, 1);
   //else if (!memcmp(myCounty.acCntyCode, "MOD", 3))
   //   iRet = loadMod(iLoadFlag, 1);
   //else if (!memcmp(myCounty.acCntyCode, "SIE", 3))
   //   iRet = loadSie(iLoadFlag, 1);
   //else if (!memcmp(myCounty.acCntyCode, "TEH", 3))
   //   iRet = loadTeh(iLoadFlag, 1);
   //else if (!memcmp(myCounty.acCntyCode, "TUO", 3))
   //   iRet = loadTuo(iLoadFlag, 1);
   else
   {
      LogMsgD("***** Do nothing *****\n");
      return 1;
      /*
      iApnLen = myCounty.iApnLen;
      iRollLen = guessRecLen(acRollFile, iRollLen);

      // Extract sale1 from redifile and append to cum sale file - 08/06/2007 spn
      iRet = Cres_ExtrSale1(acRollFile, iRollLen);

      if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))                // -L or -Xl
         iRet = Cres_ExtrLien(myCounty.acCntyCode);

      if (iLoadFlag & LOAD_LIEN)
      {
         LogMsg("Load Lien %s Roll file", myCounty.acCntyCode);
         iRet = LoadLienRoll(myCounty.acCntyCode);
      } else if (iLoadFlag & LOAD_UPDT)
      {
         LogMsg("Update County %s Roll file", myCounty.acCntyCode);
         iRet = LoadUpdtRoll(myCounty.acCntyCode, iSkip);
      }

      if (!iRet)
         iRet = MergeCumSale1(iSkip, NULL, true, true, true);
      */
   }

   // Set state='F' if fail, 'P' for data processed and ready for product build 
   if (iRet)
   {
      acTmp[0] = 0;
      if (iLoadFlag & (LOAD_ASSR|UPDT_ASSR))
         sprintf(acTmp, "UPDATE Products SET State='F' WHERE (Prefix='%s')", myCounty.acCntyCode);

      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT))
         sprintf(acTmp, "UPDATE Products SET State='F' WHERE (Prefix='S%s')", myCounty.acCntyCode);

      if (acTmp[0] == 'U')
         updateTable(acTmp);

      // Send mail
      if (bSendMail)
      {
         sprintf(acSubj, "***** LoadCres - Error loading %s", myCounty.acCntyCode);
         sprintf(acBody, "%s\nSee %s for more info", getLastErrorLog(), acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }

      // Suspense all import tasks
      iLoadFlag = 0;
   } else
   {
      // Update acreage using GIS data
      if (iLoadFlag & MERG_GISA)                      // -Mr
      {
         GetPrivateProfileString(myCounty.acCntyCode, "OverwriteSqft", "N", acTmp, 10, acIniFile);
         if (acTmp[0] == 'Y')
            iRet = PQ_MergeLotArea(myCounty.acCntyCode, true);
         else
            iRet = PQ_MergeLotArea(myCounty.acCntyCode, false);
      }

      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         if (CreateFlgFile(myCounty.acCntyCode, acIniFile))
            LogMsg("***** Error creating flag file *****");
      }

      // Copy file for assessor
      sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
      GetIniString(myCounty.acCntyCode, "AsrFile", "", acTmpFile, _MAX_PATH, acIniFile);
      if (acTmpFile[0] > ' ' && !_access(acOutFile, 0))
      {
         LogMsg("Copying file %s ==> %s", acOutFile, acTmpFile);
         iTmp = CopyFile(acOutFile, acTmpFile, false);
         if (!iTmp)
            LogMsg("***** Fail copying file %s ==> %s", acOutFile, acTmpFile);
         else
         {
            iLoadFlag |= UPDT_ASSR;
            lAssrRecCnt = lRecCnt;
         }
      }

      // Copy redifile for assessor
      GetIniString(myCounty.acCntyCode, "CopyRoll", "", acOutFile, _MAX_PATH, acIniFile);
      if (acOutFile[0] > ' ')
      {
         LogMsg("Copying file %s ==> %s", acRollFile, acOutFile);
         iTmp = CopyFile(acRollFile, acOutFile, false);
         if (!iTmp)
            LogMsg("***** Fail copying file %s ==> %s", acRollFile, acOutFile);
         else
         {
            iLoadFlag |= UPDT_ASSR;
            lAssrRecCnt = lRecCnt;
         }
      }

      if (iLoadFlag & (LOAD_LIEN|LOAD_UPDT|MERG_GRGR))
      {
         if (lRecCnt > (myCounty.iLastRecCnt-((myCounty.iLastRecCnt/100)*5)))
         {
            // Update Products table
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='S%s')", lRecCnt, myCounty.acCntyCode);
            updateTable(acTmp);

            // Update CtyProfiles
            sprintf(acTmp, "UPDATE ctyProfiles SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d,State='P' WHERE (CountyCode='%s')", acToday, lRecCnt, lLastRecDate, myCounty.acCntyCode);
            updateTable(acTmp);

            // Update county table
            char sGrGrDt[32], sFileDt[32], sTaxFileDt[32];

            sGrGrDt[0] = 0;
            sFileDt[0] = 0;
            sTaxFileDt[0] = 0;

            if (lLastFileDate > 0)
               sprintf(sFileDt, ",LastFileDate=%d", lLastFileDate);
            if (lLastGrGrDate > 0)
               sprintf(sGrGrDt, ",LastGrGrDate=%d", lLastGrGrDate);

            // LastFileDate is set by ChkCnty.  But we provide option to do it here for manual run
            if (iLoadFlag & LOAD_LIEN)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LDRRecCount=%d,LastRecDate=%d %s %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLDRRecCount, lLastRecDate, sGrGrDt, sFileDt, sTaxFileDt, myCounty.acCntyCode);
            else if (bSetLastFileDate)
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d %s %s %s WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLastRecDate, sGrGrDt, sFileDt, sTaxFileDt, myCounty.acCntyCode);
            else
               sprintf(acTmp, "UPDATE County SET LastBldDate=%s,LastRecCount=%d,LastRecDate=%d WHERE (CountyCode='%s')", 
                  acToday, lRecCnt, lLastRecDate, myCounty.acCntyCode);
            updateTable(acTmp);
         } else if (lRecCnt > 0)
         {
            char acSubj[256], acBody[512];

            sprintf(acSubj, "*** LoadCres - WARNING: Number of records is too small for %s (%d) ***", myCounty.acCntyCode, lRecCnt);
            LogMsg(acSubj);
            sprintf(acBody, "Input file may be corrupted.  Please check FTP folder on User6 to verify file size or call Sony 714-247-9732");

            // Send mail
            if (bSendMail)
               mySendMail(acIniFile, acSubj, acBody, NULL);

            bDontUpd = true;
         }    
         
         // Check for bad character
         iTmp = PQ_ChkBadR01(myCounty.acCntyCode, acRawTmpl, iRecLen, ' ');

         if (iMaxLegal > 0)
            LogMsg("Max legal length:           %u", iMaxLegal);

         /*
         // Generate update file
         if ((iLoadFlag & LOAD_UPDT) && !bDontUpd)
         {
            iRet = chkS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, iApnLen);
            if (iRet >= 0)
            {
               // Store number of recs changed to County table
               sprintf(acTmp, "UPDATE County SET LastRecsChg=%d WHERE (CountyCode='%s')", iRet, myCounty.acCntyCode);
               updateTable(acTmp);

               // If number of changed records greater than allowed, we have to import manually
               if (iRet > iMaxChgAllowed)
               {
                  // Email production
                  LogMsg("*** WARNING: Number of changed records is too big for %s (%d) ***", myCounty.acCntyCode, iRet);
                  LogMsg("***          Please run CDDEXTR -Q -TWEBIMPORT -C%s ***", myCounty.acCntyCode);

                  GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
                  LogMsg("Send email to %s", acTmp);

                  sprintf(acSubj, "%s - %d recs changed.  Use CddExtr to update SQL server.", myCounty.acCntyCode, iRet);
                  sprintf(acBody, "Msg from %s.\nSee log file Load_%s.log for more info.", acVer, myCounty.acCntyCode);
                  mySendMail(acIniFile, acSubj, acBody, acTmp);
               }
               iRet = 0;
            }
         }
         */
      } 

      if (iLoadTax && lLastTaxFileDate > 0)
      {
         if (bTaxImport)
            sprintf(acTmp, "UPDATE County SET LastTaxImpDate=%s,LastTaxFileDate=%d WHERE (CountyCode='%s')", acToday, lLastTaxFileDate, myCounty.acCntyCode);
         else
            sprintf(acTmp, "UPDATE County SET LastTaxFileDate=%d WHERE (CountyCode='%s')", lLastTaxFileDate, myCounty.acCntyCode);
         updateTable(acTmp);
      }

      if ((iLoadFlag & (LOAD_ASSR|UPDT_ASSR)) && lAssrRecCnt > 0)
      {
         if (lAssrRecCnt == 999999999)
            sprintf(acTmp, "UPDATE Products SET State='P' WHERE (CountyCode='%s')", myCounty.acCntyCode);
         else
            sprintf(acTmp, "UPDATE Products SET LastRecsCount=%d,State='P' WHERE (Prefix='%s')", lAssrRecCnt, myCounty.acCntyCode);
         updateTable(acTmp);
      }
   }

   // Generate update file
   if (!iRet && !bDontUpd && (iLoadFlag & LOAD_UPDT))
   {
      iRet = chkS01R01(myCounty.acCntyCode, acRawTmpl, iRecLen, iApnLen);
      if (iRet >= 0)
      {
         // Store number of recs changed to County table
         sprintf(acTmp, "UPDATE County SET LastRecsChg=%d WHERE (CountyCode='%s')", iRet, myCounty.acCntyCode);
         updateTable(acTmp);

         // If number of changed records greater than allowed, we have to import manually
         if (iRet > iMaxChgAllowed)
         {
            // Email production
            LogMsg("*** WARNING: Number of changed records is too big for %s (%d) ***", myCounty.acCntyCode, iRet);
            LogMsg("***          Please run CDDEXTR -Q -TWEBIMPORT -C%s ***", myCounty.acCntyCode);

            GetIniString("Mail", "MailChgTo", "", acTmp, 256, acIniFile);
            //LogMsg("Send email to %s", acTmp);

            sprintf(acSubj, "LoadCres [%s] - %d recs changed.  Use CddExtr to update SQL server.", myCounty.acCntyCode, iRet);
            sprintf(acBody, "Msg from %s.\nSee log file Load_%s.log for more info.", acVersion, myCounty.acCntyCode);
            mySendMail(acIniFile, acSubj, acBody, acTmp);
         }

         iRet = 0;
      }
   }

   // Reset county status
   if (!iLoadTax)
   {
      sprintf(acTmp, "UPDATE County SET Status='R' WHERE (CountyCode='%s')", myCounty.acCntyCode);
      updateTable(acTmp);
   }

   // Create flag file
   if (!iRet && CreateFlgFile(myCounty.acCntyCode, acIniFile))
      LogMsg("***** Error creating flag file *****");

   // Import sale file
   if (iLoadFlag & EXTR_ISAL)
   {
      char sDbName[64];

      if (iLoadFlag & LOAD_LIEN)
         sprintf(sDbName, "LDR%d", lLienYear);
      else
         sprintf(sDbName, "UPD%d", lLienYear);

      GetIniString("Data", "SqlSalesFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);
      //if (strstr(myCounty.acCntyCode, "TUO"))
      //   iTmp = createSaleImport(Tuo_MakeDocLink, myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_SCSAL_REC, false);
      //else
         iTmp = createSaleImport(myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_SCSAL_REC, false);

      if (iTmp > 0 && bSaleImport)
         iTmp = doSaleImport(myCounty.acCntyCode, sDbName, acOutFile, 1);

      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** LoadCres [%s] - Error importing sale file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
   }

   // Import GrGr file
   if (iLoadFlag & EXTR_IGRGR)
   {
      char sDbName[64];

      if (iLoadFlag & LOAD_LIEN)
         sprintf(sDbName, "LDR%d", lLienYear);
      else
         sprintf(sDbName, "UPD%d", lLienYear);

      sprintf(acCSalFile, acGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");
      GetIniString("Data", "SqlGrgrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);
      //if (strstr(myCounty.acCntyCode, "TUO"))
      //   iTmp = createSaleImport(Tuo_MakeDocLink, myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_DEF, false);
      //else
         iTmp = createSaleImport(myCounty.acCntyCode, acCSalFile, acOutFile, TYPE_GRGR_DOC, false);

      if (iTmp > 0 && bGrgrImport)
         iTmp = doSaleImport(myCounty.acCntyCode, sDbName, acOutFile, 2);

      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** LoadCres [%s] - Error importing Grgr file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
   }

   // Import Value file
   if (iLoadFlag & EXTR_IVAL)
   {
      iTmp = doValueImport(myCounty.acCntyCode, acValueFile);

      // send email
      if (iTmp && bSendMail)
      {
         sprintf(acSubj, "*** LoadCres [%s] - Error importing Value file.", myCounty.acCntyCode);
         sprintf(acBody, "Please review log file \"%s\" for more info", acLogFile);
         mySendMail(acIniFile, acSubj, acBody, NULL);
      }
   }

LoadCres_Exit:
   // Clean up memory allocation
   if (iNumUseCodes > 0 && pUseTable)
      delete pUseTable;

   // Close log
   closeUsecodeLog();
   close_log();

   return iRet;
}
