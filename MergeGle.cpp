/*****************************************************************************
 *
 * Revision:
 * 10/18/2005 1.3.6     First production version
 * 03/21/2007 1.8.5.2   Fix bug that copies strname over suffix
 * 05/03/2007 1.8.8     Modify Gle_MergeOwner() to keep Name1 and Name2 as it
 *                      comes from the county.  Clean up name for swap name only.
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 07/01/2008 8.1.1     Add function Gle_Load_LDR() to load LDR data only, no update.
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 06/30/2009 9.1.0     Update to support 2009 LDR with new LIENEXTR format.
 * 08/04/2009 9.1.3     Populate situs address only if street name is available.
 *                      Populate other values.
 * 07/14/2011 11.0.1    Add S_HSENO. Modify MergeAdr() to collect original strNum
 *                      that may include '-' for range house number.
 * 07/22/2011 11.1.2    Clear out old situs before applying new one.
 * 10/13/2011 11.2.6    Add Gle_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      Modify Gle_Fmt1Doc() to correct DocNum. Use ApplyCumSale()
 *                      to update R01 file. Modify Gle_CreateR01() not to populate
 *                      sale info.  Let ApplyCumSale() populates it from GLE_Sale.sls.
 * 10/26/2011 11.2.8.1  Modify Gle_ExtrSale() to extract all 3 sales from roll rec
 *                      even when sale1 is bad.
 * 07/09/2013 13.0.2    Apply cum sale with DONT_CHK_ANY option to add all transactions.
 *            13.0.2.1  Using all three transactions in Redifile to update R01. 
 *                      Remove ApplyCumSale() and revisit this later when GrGr is avail.
 * 07/31/2013 13.2.4    Use updateCareOf()
 * 09/29/2013 13.3.0    Modify Gle_MergeOwner() and add Gle_CleanName() to update Vesting.
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 08/07/2014 14.0.2    Fix Gle_MergeAdr() to correctly formatting situs of "CO RD"
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 12/07/2015 15.3.0    Add option to load tax file.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "doSort.h"
#include "Tables.h"
#include "Pq.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "SendMail.h"

/********************************* Gle_Fmt1Doc *****************************
 *
 *   GLE has 3 different formats:
 *   1) 0892553         = 0892553 & no date
 *   2) 20003009-0608   = 20003009 & 20000608 (yyyymmdd)
 *   3) 2004-0793-0210  = 20040793 & 20040210
 *   4) 19875272        = 19875272 & no date
 *   5) 020029155-0919  = 20029155 & 20020919
 *
 *****************************************************************************/

void Gle_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp;
   char  acDate[16], acDoc[64], *pTmp, bARCode;
   bool  bMisAligned = false;

   pTmp = pRecDoc;
   bARCode = 'A';

   // Init date
   memset(acDate, ' ', SIZ_SALE1_DT);
   memset(acDoc, ' ', SIZ_SALE1_DOC);
   acDate[SIZ_SALE1_DT] = 0;
   acDoc[SIZ_SALE1_DOC] = 0;

   // Rec #1
   if (*(pTmp+8) == '-')
   {
      // case #2
      memcpy(acDate, pTmp, 4);
      memcpy((char *)&acDate[4], pTmp+9, 4);
      memcpy(acDoc, pTmp, 8);
   } else if (*(pTmp+4) == '-')
   {
      // case #3
      memcpy(acDate, pTmp, 4);
      memcpy((char *)&acDate[4], pTmp+10, 4);
      memcpy(acDoc, pTmp, 4);
      memcpy((char *)&acDoc[4], pTmp+5, 4);
   } else if (*pTmp == '0' && *(pTmp+9) == '-')
   {
      // case #5
      memcpy(acDate, pTmp+1, 4);
      memcpy((char *)&acDate[4], pTmp+10, 4);
      memcpy(acDoc, pTmp+1, 8);
   } else if (*pTmp >= '0')
   {
      // case #1 & #4
      iTmp = atoin(pTmp, 7);
      if (iTmp > 0)
      {
         iTmp = 0;
         while (*pTmp > ' ')
            acDoc[iTmp++] = *pTmp++;
         while (iTmp < SIZ_SALE1_DOC)
            acDoc[iTmp++] = ' ';
         acDoc[iTmp] = 0;
      }
   }

   if (acDate[0] != ' ' && !isValidYMD(acDate))
      memset(acDate, ' ', SIZ_SALE1_DT);
   strcpy(pOutDate, acDate);
   strcpy(pOutDoc, acDoc);
}

/********************************* Gle_FmtRecDoc *****************************
 *
 *
 *****************************************************************************/

void Gle_FmtRecDoc(char *pOutbuf, char *pRecDoc1)
{
   char  acDate[16], acDoc[64], *pTmp, bARCode;

   pTmp = pRecDoc1;

   Gle_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE1) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

   // Update last recording date
   long lDate = atol(acDate);
   if (lDate > lLastRecDate && lDate < lToday)
      lLastRecDate = lDate;

   // Advance to Rec #2
   pTmp += 16;
   Gle_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      bARCode = 'A';           // Assessor-Recorder code
   else
      bARCode = ' ';           // Assessor-Recorder code

   if (bARCode == ' ' || !memcmp(acDoc, pOutbuf+OFF_SALE1_DOC, SIZ_SALE1_DOC))
   {
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      *(pOutbuf+OFF_AR_CODE2) = ' ';
      *(pOutbuf+OFF_AR_CODE3) = ' ';
      return;
   } else
   {
      *(pOutbuf+OFF_AR_CODE2) = bARCode;
      memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
      memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);
   }

   // Advance to Rec #3
   pTmp += 16;
   Gle_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      bARCode = 'A';           // Assessor-Recorder code
   else
      bARCode = ' ';           // Assessor-Recorder code

   if (bARCode == ' ' || !memcmp(acDoc, pOutbuf+OFF_SALE2_DOC, SIZ_SALE2_DOC))
   {
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      *(pOutbuf+OFF_AR_CODE3) = ' ';
   } else
   {
      *(pOutbuf+OFF_AR_CODE3) = bARCode;
      memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
      memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
   }
}

/******************************** Gle_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Gle_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Gle_MergeOwner *****************************
 *
 * Merge Name1 & Name2 only for swap name. Keep Name1 & Name2 separate as provided
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Gle_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp, iNameFlg=5;
   char  acTmp[64], acName1[64], acName2[64], acVesting1[8], acVesting2[8];
   char  *pTmp, *pName1, *pName2, acOwners[64];

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   myTrim((char *)&acName1[0], CRESIZ_NAME_1);
   replStr(acName1, "& &", "&");
   strcpy(acOwners, acName1);
   pName1 = acName1;
   acVesting1[0] = 0;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   myTrim((char *)&acName2[0], CRESIZ_NAME_2);
   pName2 = acName2;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0411000230", 10) || !memcmp(pOutbuf, "0470100269", 10) )
   //   iTmp = 0;
#endif

   // Check owner2 for # and %
   if (*pName2 == '#' )
   {
      // Business name - ignore
      *pName2 = 0;
   } else if (!memcmp(pName2, "C/O", 3))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      pName2 += 2;

      // Can be merge with Name1
      if (strstr(pName1, "COUNTY OF") || strstr(pName1, "CITY OF") )
      {
         strcat(pName1, pName2-1);
         *pName2 = 0;
      } else if (acName1[strlen(acName1) -1] == '&' && !strchr(pName2, '&'))
      {  // Continuation of name1.  However, in following cases:
         // LANDBERG BETTY & KAY &       % HILDEBRAND JAN LE
         // B OF A &                     % FEENEY DONNA L TRS
         // Name2 has to be parse separately
         if (acName1[strlen(acName1) -2] == ' ')
            acName1[strlen(acName1) -2] = 0;
         else
            acName1[strlen(acName1) -1] = 0;

         if (!strchr(pName1, '&') && memcmp(pName1, "B OF A", 6))
         {
            iTmp = MergeName1(pName1, pName2, acOwners);
            if (!iTmp)
               strcpy(acOwners, acName1);
         }
      }

   } else if (*pName2 > ' ')
   {
      // 14 is a magic number. It works with 09/27/2013 data and might not work with other weird names
      if (!strchr(pName1, '&') || strlen(pName2) < 14)
      {
         // Continuation of name1
         memcpy(acName1, pNames, CRESIZ_NAME_1+CRESIZ_NAME_2);
         blankRem(acName1, CRESIZ_NAME_1+CRESIZ_NAME_2);
         strcpy(acOwners, acName1);
         *pName2 = 0;
      }
   }

   // Check name2
   if (*pName2 > ' ')
   {
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      // Find vesting in Name2 
      pTmp = findVesting(pName2, acVesting2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));

         // Check EtAl
         if (!memcmp(acVesting2, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } 
   }

   // Update Name1
   iTmp = strlen(acName1);
   if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

   // Cleanup Name1 
   iTmp = Gle_CleanName(acOwners, acTmp, acVesting1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acOwners, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));

      // Check Etal
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   } 

   // Now parse owners
   splitOwner(acOwners, &myOwner, iNameFlg);
   if (acVesting1[0] > ' ' && isVestChk(acVesting1))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/******************************** Gle_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Gle_MergeOwner1(char *pOutbuf, char *pNames)
{
   int   iTmp, iNameFlg=5;
   char  acTmp1[64], acName1[64], acName2[64], acSave1[64], acSave2[64];
   char  *pTmp, *pTmp1, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   pName1 = myTrim(acName1, CRESIZ_NAME_1);
   pName2 = myTrim(acName2, CRESIZ_NAME_2);

   // Check owner2 for # and %
   if (*pName2 == '#' )
   {
      // Business name - ignore
      *pName2 = 0;
   } else if (!memcmp(pName2, "C/O", 3))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      // Can be merge with Name1
      if (strstr(pName1, "COUNTY OF") || strstr(pName1, "CITY OF") )
      {
         strcat(pName1, pName2+1);
         *pName2 = 0;
      } else if (acName1[strlen(acName1) -1] == '&' && !strchr(pName2, '&'))
      {  // Continuation of name1.  However, in following cases:
         // LANDBERG BETTY & KAY &       % HILDEBRAND JAN LE
         // B OF A &                     % FEENEY DONNA L TRS
         // Name2 has to be parse separately
         acName1[strlen(acName1) -1] = 0;
         if (strchr(pName1, '&') || !memcmp(pName1, "B OF A", 6))
            strcpy(acName2, pName2+2);
         else
         {
            strcat(acName1, "&");
            strcat(pName1, pName2+1);
            *pName2 = 0;
            iNameFlg = 4;
         }
      } else
      {  
         strcpy(acName2, pName2+2);
      }

   }

   if ((pTmp = strstr(pName1, "S/S")) || (pTmp = strstr(pName1, "C/B")))
      memcpy(pTmp, "          ", 3);
   if ((pTmp = strstr(pName2, "S/S")) || (pTmp = strstr(pName2, "C/B")))
      memcpy(pTmp, "          ", 3);

   // Check for vesting in Name1
   pTmp = strrchr(pName1, ' ');
   if (pTmp)
   {
      strcpy(acSave1, pTmp+1);
      pTmp1 = getVestingCode_GLE(acSave1, acTmp1);
      if (*pTmp1)
      {
         memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
         *pTmp = 0;
      }
   }

   // Check for vesting in Name2
   pTmp = strrchr(pName2, ' ');
   if (pTmp)
   {
      strcpy(acSave1, pTmp+1);
      pTmp1 = getVestingCode_GLE(acSave1, acTmp1);
      if (*pTmp1)
      {
         if (*(pOutbuf+OFF_VEST) == ' ')
            memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
         *pTmp = 0;
      }
   }

   remCharEx(pName1, ".\'");
   remCharEx(pName2, ".\'");
   strcpy(acSave1, pName1); 
   strcpy(acSave2, pName2); 

   // Blank out "SUC TRS" and "CO TRS", "DVA"
   if (pTmp = strstr(pName1, "SUC TRS"))
      memcpy(pTmp, "          ", 7);
   else if ((pTmp = strstr(pName1, " CO TRS")) || (pTmp = strstr(pName1, " CO-TRS")))
      memcpy(pTmp, "          ", 7);
   else if (pTmp = strstr(pName1, " EST OF"))
      memcpy(pTmp, "          ", 7);
   else if (pTmp = strstr(pName1, " ETAL"))
      *pTmp = 0;

   // Remove multiple spaces
   blankRem(pName1);

   // Now parse owners
   splitOwner(pName1, &myOwner, iNameFlg);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   if (myOwner.acVest[0] > ' ')
      memcpy(pOutbuf+OFF_VEST, myOwner.acVest, strlen(myOwner.acVest));

   iTmp = strlen(acSave1);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acSave1, iTmp);

   if (myOwner.acName2[0] && strcmp(myOwner.acName1, myOwner.acName2) && acSave2[0] > ' ')
      memcpy(pOutbuf+OFF_NAME2, acSave2, strlen(acSave2));

}

/********************************* Gle_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Gle_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     *pTmp, *pTmp1, *pAddr1, acTmp[256], acAddr1[128], acAddr2[128];
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pRec->M_Addr1, "     ", 5))
      return;

   // Check for bad data
   iTmp = isPrintable(pRec->M_Addr1, CRESIZ_ADDR_1);
   if (iTmp != CRESIZ_ADDR_1)
      return;

   memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
   blankRem(acAddr1, CRESIZ_ADDR_1);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ') 
            pAddr1++;
         //iTmp = strlen(pAddr1);
         //if (iTmp > SIZ_CARE_OF)
         //   iTmp = SIZ_CARE_OF;
         //memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
         updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
      }
   }

   if (pTmp = strstr(pAddr1, "STREET"))
      strcpy(pTmp, "ST");
   else
      myTrim(pAddr1);

   // Start processing
   memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
   iTmp = atoin(pRec->M_Zip, 5);
   if (iTmp > 10001)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   } else
   {
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
   }

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseAdr1(&sMailAdr, pAddr1);

   memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
   acAddr2[CRESIZ_ADDR_2] = 0;
   parseAdr2(&sMailAdr, myTrim(acAddr2));

   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%*d", SIZ_M_STRNUM, sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, strlen(acTmp));
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
   }
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, strlen(sMailAdr.strDir));
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, strlen(sMailAdr.strName));
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, strlen(sMailAdr.strSfx));
   memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, strlen(sMailAdr.City));
   memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, strlen(sMailAdr.State));

   // Situs - Situs addr may come in Legal1 or Legal2
   // 1) Drop record with '.'
   // 2) Drop record start with alpha

   
   ADR_REC sSitusAdr;

   memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
   memcpy(acAddr1, pRec->LglDesc1, CRESIZ_LGL_DESC1);
   acAddr1[CRESIZ_LGL_DESC1] = 0;
   myTrim(acAddr1);

   if (strstr(acAddr1, " AC-"))
      return;
   if (strstr(acAddr1, "AC "))
      return;
   if (strstr(acAddr1, "AC-"))
      return;
   if (strstr(acAddr1, " INT "))
      return;
   if (strstr(acAddr1, "%"))
      return;

   if (strstr(acAddr1, "PCT"))
      return;
   if (strstr(acAddr1, " LS-"))
      return;
   if (strchr(acAddr1, '.'))        // Avoid acreage data
      return;
   if (!strchr(acAddr1, ' '))       // Avoid single word address
      return;

   if (strstr(acAddr1, "BLK"))
      return;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0400400010", 10))
   //   iTmp = 0;
#endif
   //replStrAll(acAddr1, " CO ", " COUNTY ");
   
   if (pTmp=strchr(acAddr1, '('))
   {
      *pTmp = 0;
      if (*(--pTmp) == ' ') *pTmp = 0;
   }

   // Take first number only
   iStrNo = atoi(acAddr1);

   if (iStrNo > 0)
   {
      removeSitus(pOutbuf);

      strcpy(acTmp, acAddr1);
      if (pTmp = strchr(acTmp, ' '))
         *pTmp = 0;
#ifdef _DEBUG
      //if (pTmp = strchr(acTmp, '-'))
      //   pTmp++;
#endif
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));

      pTmp = acAddr1;
      while (pTmp = strchr(pTmp, ' '))
      {
         pTmp++;
         if (*pTmp >= 'A')
            break;
         if (!memcmp(pTmp+1, "TH", 2) || 
             !memcmp(pTmp+1, "ST", 2) || 
             !memcmp(pTmp+1, "ND", 2) || 
             !memcmp(pTmp+1, "RD", 2))
            break;
      }

      // Take out common words
      pTmp1 = strrchr(pTmp, ' ');
      if (pTmp1 && !memcmp(pTmp1, " AREA", 5))
         *pTmp1 = 0;
      pTmp1 = strrchr(pTmp, ' ');
      if (pTmp1 && !memcmp(pTmp1, " COMM", 5))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " GRN"))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " WIL "))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, " L/C"))
         *pTmp1 = 0;
      if (pTmp1 = strstr(pTmp, "STREET"))
         strcpy(pTmp1, "ST");

      // Keep first address only
      // 158 S ENRIGHT  1030 OAK ST
      // 929 W WILLOW-305 N CULVER
      // 205 N SHASTA/321-325 W WALNUT
      // 705 S TEHAMA & 108 ELM
      
      if ((pTmp1 = strchr(pTmp, '/')) && strlen(pTmp1) > 2)
         *pTmp1 = 0;
      
      if (!strcmp(pTmp, "UNIT"))
         *pTmp1 = 0;
      else if (pTmp1 = strchr(pTmp, '-'))
      {
         if (!strchr(pTmp, '#'))
         {
            //char *pTmp2 = pTmp1;
            *pTmp1 = 0;

            // If last token is a number, drop it
            if (pTmp1 = strrchr(pTmp, ' '))
               if (isdigit(*(pTmp1+1)))
                  *pTmp1 = 0;
         }
      }
      if (pTmp1 = strstr(pTmp, "  "))
         *pTmp1 = 0;
      if (pTmp1 = strchr(pTmp, '&'))
         *pTmp1 = 0;

      //parseAdr1N(&sSitusAdr, pTmp);
   
#ifdef _DEBUG
      //if (!memcmp(pOutbuf, "745140018", 9))
      //   iTmp = 0;
#endif
      parseAdr1C_1(&sSitusAdr, pTmp);
      if (sSitusAdr.strName[0] > ' ')
      {
         iTmp = sprintf(acTmp, "%*d", SIZ_S_STRNUM, iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
         sprintf(acTmp, "%d", iStrNo);

         if (sSitusAdr.strDir[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
            strcat(acTmp, " ");
            strcat(acTmp, sSitusAdr.strDir);
         }

         /*
         if (!memcmp(sSitusAdr.strName, "CO RD", 5))
         {
            iTmp = sprintf(acTmp, "COUNTY RD %s", &sSitusAdr.strName[6]);
            if (iTmp <= SIZ_S_STREET)
               strcpy(sSitusAdr.strName, acTmp);
         }
         */

         // Make sure there is space before and no space after #
         if (pTmp = strchr(sSitusAdr.strName, '#'))
         {
            if (*++pTmp == ' ')
               strcpy(pTmp, pTmp+1);
            if (*(pTmp-2) > ' ')
            {
               strcpy(acTmp, sSitusAdr.strName);
               replStr(acTmp, "#", " #");
               strncpy(sSitusAdr.strName, acTmp, SIZ_S_STREET);
            }
         }

         iTmp = strlen(sSitusAdr.strName);
         if (iTmp > SIZ_S_STREET)
            iTmp = SIZ_S_STREET;
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, iTmp);
         strcat(acTmp, " ");
         strcat(acTmp, sSitusAdr.strName);

         if (sSitusAdr.strSfx[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
            iTmp = atoi(sSitusAdr.strSfx);
            if (iTmp > 0)
            {
               strcat(acTmp, " ");
               if (pTmp = GetSfxStr(iTmp))
                  strcat(acTmp, pTmp);
            }
         }
         if (sSitusAdr.Unit[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
            strcat(acTmp, " ");
            strcat(acTmp, sSitusAdr.Unit);
         }

         // Display field
         iTmp = blankRem(acTmp, SIZ_S_ADDR_D);
         memcpy(pOutbuf+OFF_S_ADDR_D, acTmp, iTmp);
      }
      acAddr2[0] = 0;

      // Assuming that no situs city and we have to use mail city
      if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         City2Code(sMailAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            memcpy(pOutbuf+OFF_S_ST, "CA", 2);
            sprintf(acAddr2, "%s CA", sMailAdr.City);
         }
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      } else
      {
         // Condition: Valid city name, Ex_Val is 7000,
         // Use_Code is N?,B1,A1,A2,C1,T1,R1,S1
         long lVal;
         lVal = atoin(pRec->ExVal, CRESIZ_EX_VAL);

         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && (lVal == 7000) )
         {
            // Get city code
            City2Code(sMailAdr.City, acTmp);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ST, "CA", 2);
               memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
               sprintf(acAddr2, "%s CA", sMailAdr.City);
            }
         }
      }
      iTmp = strlen(acAddr2);
      if (iTmp > SIZ_S_CTY_ST_D)
         iTmp = SIZ_S_CTY_ST_D;
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   }
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Gle_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iInitFlg)
{
   REDIFILE *pRec;
   char     acTmp[256], acTmp1[256], acCode[32];
   long     lTmp;
   int      iTmp;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Init output buffer
   if (iInitFlg & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "11GLE", 5);

      // Create APN_D new record
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // Other value
      /*
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lHpp = atoin(pRec->PPVal, CRESIZ_PP_VAL);
      long lPers = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      lTmp = lPers + lHpp + lFixture;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      }
      */
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTH_VALUE);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_OTH_VALUE);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_OTH_VALUE);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Year built
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);

   // Effective Year
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // Parcel Status
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"GLE");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // Recording info
   if ((iInitFlg & UPDATE_SALES) && (pRec->RecBook1[0] > ' '))
   {
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      Gle_FmtRecDoc(pOutbuf, pRec->RecBook1);
   }
   
   // Owners - always update owner before mailing addr to update CareOf properly
   Gle_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address
   Gle_MergeAdr(pOutbuf, pRollRec);

   // Delinquent Year - can support till year 2049
   iTmp = atoin((char *)&pRec->DefaultDate[4], 2);
   if (iTmp > 50)
   {
      memcpy(pOutbuf+OFF_DEL_YR, "19", 2);
      memcpy(pOutbuf+OFF_DEL_YR+2, (char *)&pRec->DefaultDate[4], 2);
   } else if (iTmp > 0)
   {
      memcpy(pOutbuf+OFF_DEL_YR, "20", 2);
      memcpy(pOutbuf+OFF_DEL_YR+2, (char *)&pRec->DefaultDate[4], 2);
   }

   // HO Exempt
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (!memcmp(pRec->ExeCode1, "10", 2) && lTmp > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      memset(pOutbuf+OFF_EXE_TOTAL, ' ', SIZ_EXE_TOTAL);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "005060003", 9))
   //   lTmp = 0;
#endif

   // Lot Acres
   long  lLotSqft, lLotAcre;
   char  acSqft[16], acAcre[16];

   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
      lLotAcre = (long)(dTmp * SQFT_PER_ACRE);
   else
      lLotAcre = 0;

   // Lot Sqft
   lLotSqft = atoin(pRec->LotSqft, CRESIZ_LOT_SQFT);

   if (lLotAcre > 0)
   {
      sprintf(acAcre, "%*u", SIZ_LOT_ACRES, (long)(dTmp*1000));
      sprintf(acSqft, "%*u", SIZ_LOT_SQFT, lLotAcre);
   } else if (lLotSqft > 0)
   {
      sprintf(acSqft, "%*u", SIZ_LOT_SQFT, lLotSqft);
      lLotAcre = (lLotSqft*1000)/SQFT_PER_ACRE;
      sprintf(acAcre, "%*u", SIZ_LOT_ACRES, lLotAcre);
   } else
   {
      memset(acAcre, ' ', SIZ_LOT_ACRES);
      memset(acSqft, ' ', SIZ_LOT_SQFT);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acAcre, SIZ_LOT_ACRES);
   memcpy(pOutbuf+OFF_LOT_SQFT, acSqft, SIZ_LOT_SQFT);

   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   if (pRec->Beds[0] > '0' && pRec->Beds[0] <= '9')
   {
      iTmp = pRec->Beds[0] & 0x0F;
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   } else if (pRec->Beds[0] != '0' && pRec->Beds[0] != ' ')
   {
      iTmp = pRec->Beds[0];
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      memcpy(acTmp, pRec->LglDesc1, CRESIZ_LGL_DESC1);
      acTmp[CRESIZ_LGL_DESC1] = 0;
      if (pRec->LglDesc2[0] > ' ')
      {
         myTrim(acTmp);
         pRec->LglDesc2[CRESIZ_LGL_DESC2-1] = 0;
         strcat(acTmp, pRec->LglDesc2);
      }
      updateLegal(pOutbuf, acTmp);
   }

   // FP - 1
   if (pRec->Fp[0] > '0')
   {
      *(pOutbuf+OFF_FIRE_PL) = ' ';
      *(pOutbuf+OFF_FIRE_PL+1) = pRec->Fp[0];
   }

   // FL - # of floors
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];
   if (pRec->Garage[0] == 'Y')
      *(pOutbuf+OFF_PARK_TYPE) = 'Z';

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // AC - 2

   // Heating - 3

   // Pool/Spa
   if (pRec->Pool[0] == 'Y' && pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'C';
   else if (pRec->Pool[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';
   else if (pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'S';

   // Zoning - 0

   // Neighborhood

   // Bldg Class
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
      acTmp[iTmp++] = pRec->BldgCls[0];
      acTmp[iTmp++] = '.';
      if (isdigit(pRec->BldgCls[1]))
         acTmp[iTmp++] = pRec->BldgCls[1];
      else if (isdigit(pRec->BldgCls[2]))
         acTmp[iTmp++] = pRec->BldgCls[2];
      else
         acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   return 0;
}

/********************************** Gle_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.  
 *
 * This has not been tested 04/27/2008 spn
 *
 ****************************************************************************/

int Gle_LoadRoll(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, lLienUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -1;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Gle_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01|UPDATE_SALES);
      if (!iRet)
      {
         if (fdLienExt)
         {
            iRet = PQ_MergeLien(acBuf, fdLienExt, GRP_CRES);
            if (!iRet)
               lLienUpd++;
         }

         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         iRetiredRec++;

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsgD("\nTotal output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total lien updated records: %u\n", lLienUpd);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   return 0;
}

/********************************** Gle_Load_LDR ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Gle_Load_LDR(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "001021004", 9))
      //   iRet=0;
#endif
      lLDRRecCount++;
      iRet = Gle_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01|UPDATE_SALES);
      if (!iRet)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      // Scan for bad character - LAS May 05
      while (iRet-- > 0)
         if (acRollRec[iRet] < ' ')
            acRollRec[iRet] = ' ';

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/******************************* Cres_ExtrSale ******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Gle_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024], *pTmp;
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice;
   double   dTmp;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Gle_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            pTmp = strchr(acTmp, '.');
            if (pTmp && !memcmp(pTmp, ".00", 3))
            {
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (int)dTmp);
            } else
            {
               lPrice = (long)(dTmp * SALE_FACTOR);
               if (lPrice < 100)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               else
                  sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
            }
            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Gle_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Gle_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   if (pOutFile)
      strcpy(acCSalFile, pOutFile);
   else
      sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");

   if (bAppend)
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acCSalFile, acCSalFile);
      iRet = -1;
   } else
   {
      // Rename files
      if (!_access(acCSalFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acCSalFile, acTmp);
      }
      iRet = rename(acBuf, acCSalFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acCSalFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", iRet);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/*********************************** loadGle ********************************
 *
 * Input files:
 *    redifile (899-bytes roll file)
 *
 *
 * Commands:
 *    -U    Monthly update
 *    -L    Lien update
 *    -Xl   Extract lien values
 *
 ****************************************************************************/

int loadGle(int iLoadFlag, int iSkip)
{
   int   iRet=0;

   iApnLen = myCounty.iApnLen;
   iRollLen = guessRecLen(acRollFile, iRollLen);

   //char *pOldSaleFile = "H:\\CO_PROD\\SGLE_CD\\raw\\GLE_sale_cum.dat";
   //iRet = convertSaleData(myCounty.acCntyCode, pOldSaleFile, CONV_SALE_REC2);

   /*
   char  acTmpFile[_MAX_PATH];
   strcpy(acRollFile, "G:\\CO_DATA\\GLE\\2005\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SGLE_CD\\raw\\Gle_Sale.2005");
   iRet = Gle_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\GLE\\2006\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SGLE_CD\\raw\\Gle_Sale.2006");
   iRet = Gle_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\GLE\\2007\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SGLE_CD\\raw\\Gle_Sale.2007");
   iRet = Gle_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\GLE\\2008\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SGLE_CD\\raw\\Gle_Sale.2008");
   iRet = Gle_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\GLE\\2009\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SGLE_CD\\raw\\Gle_Sale.2009");
   iRet = Gle_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\GLE\\2010\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SGLE_CD\\raw\\Gle_Sale.2010");
   iRet = Gle_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\GLE\\2011\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SGLE_CD\\raw\\Gle_Sale.2011");
   iRet = Gle_ExtrSale(acRollFile, acTmpFile, false);
   */
   // Extract sale1 from redifile and append to cum sale file
   if (iLoadFlag & EXTR_SALE)                // -Xs
      iRet = Gle_ExtrSale(acRollFile, NULL, true);

   // Load tax
   if (bLoadTax)
   {
      int iLRecLen = GetPrivateProfileInt(myCounty.acCntyCode, "LienRecSize", iRollLen, acIniFile);

      //iRet = Cres_Load_TaxRollInfo(true);
      iRet = Cres_Load_TaxBase(bTaxImport);
      if (!iRet)
         iRet = Cres_Load_TaxDelq(bTaxImport);
      if (!iRet)
         iRet = Cres_Load_TaxOwner(bTaxImport, iLRecLen);
      if (iRet < 0)
      {
         char sMailTo[64], sSubj[80], sBody[256];

         iRet = GetPrivateProfileString("Mail", "MailDeveloper", "", sMailTo, 256, acIniFile);
         if (iRet > 10)
         {
            sprintf(sSubj, "*** %s - Error loading tax file (%d)", myCounty.acCntyCode, iRet);
            sprintf(sBody, "Please review log file \"%s\" for more info", acLogFile);
            mySendMail(acIniFile, sSubj, sBody, sMailTo);
         }
      }
   }

   if (iLoadFlag & EXTR_LIEN)                // -Xl
      iRet = Cres_ExtrLien(myCounty.acCntyCode);

   if (iLoadFlag & LOAD_LIEN)                // -L 
      iRet = Gle_Load_LDR(myCounty.acCntyCode);
   else if (iLoadFlag & LOAD_UPDT)           // -U
      iRet = Gle_LoadRoll(myCounty.acCntyCode, iSkip);

   // May need to create special option to include all transactions of the same date
   //if (!iRet && (iLoadFlag & MERG_CSAL))
   //   iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE|DONT_CHK_ANY);

   return iRet;
}
