#ifndef _MERGETUO_H
#define _MERGETUO_H

#define TUO_VALUE_YEAR       0
#define TUO_VALUE_TRA        1
#define TUO_VALUE_LICNO      2
#define TUO_VALUE_LEGAL      3
#define TUO_VALUE_SADDR      4
#define TUO_VALUE_APN        5
#define TUO_VALUE_LAND       6
#define TUO_VALUE_IMPR       7
#define TUO_VALUE_OWNER1     8
#define TUO_VALUE_PPVAL      9
#define TUO_VALUE_OWNER2     10
#define TUO_VALUE_FIXTVAL    11
#define TUO_VALUE_FIXTTYPE   12
#define TUO_VALUE_MADDR      13
#define TUO_VALUE_EXEAMT     14
#define TUO_VALUE_EXETYPE    15
#define TUO_VALUE_CITYST     16
#define TUO_VALUE_NET        17
#define TUO_VALUE_ZIP        18
#define TUO_VALUE_BYVVAL     19
#define TUO_VALUE_COLS       20
// 2014
//#define TUO_VALUE_BYVCODE    19
//#define TUO_VALUE_BYVVAL     20
//#define TUO_VALUE_COLS       21

#define TUO_GR_PRSTAT        0
#define TUO_GR_PRDOC         1     
#define TUO_GR_PRSERV        2
#define TUO_GR_PRTYPE        3
#define TUO_GR_PRMNAME       4
#define TUO_GR_PRFOLDER      5           
#define TUO_GR_PRQUEUE       6
#define TUO_GR_DOCNUM        7        
#define TUO_GR_DOCTYPE       8
#define TUO_GR_DOCDATE       9
#define TUO_GR_GRANTOR       10
#define TUO_GR_GRANTEE       11
#define TUO_GR_BOOK          12
#define TUO_GR_PAGE          13
#define TUO_GR_APN           14
#define TUO_GR_DOCTAX        15
#define TUO_GR_COMMENTS      16


#define SOFF_SUBAREA         1-1
#define SOFF_APN             4-1
#define SOFF_SITUS           14-1
#define SOFF_ZONE            44-1
#define SOFF_USE             54-1
#define SOFF_LOTSQFT         60-1
#define SOFF_ACRES           69-1
#define SOFF_BUILDINGCLASS   79-1
#define SOFF_CONSTYR         84-1
#define SOFF_BEDROOMS        88-1
#define SOFF_BATHS           89-1
#define SOFF_TOTALROOMS      93-1
#define SOFF_BUILDINGSQFT    96-1
#define SOFF_TYPE            104-1     // E, M
#define SOFF_FIREPLACE       105-1
#define SOFF_COOLING         106-1
#define SOFF_STORIES         107-1
#define SOFF_MISCIMPROV      108-1
#define SOFF_PARKING         109-1
#define SOFF_POOL            110-1
#define SOFF_SPA             111-1
#define SOFF_HEATING         112-1
#define SOFF_LANDALLOC       113-1
#define SOFF_SELLER          121-1
#define SOFF_BUYER           150-1
#define SOFF_BOOKPAGE        179-1
#define SOFF_RECDATE         187-1     // mmddyyyy
#define SOFF_CONDITION       195-1
#define SOFF_LISTFLAG        196-1
#define SOFF_SALEPRICE       197-1
#define SOFF_ADJPRICE        206-1
#define SOFF_SALETYPE        215-1     // M, O
#define SOFF_VALUERATIO      216-1
#define SOFF_DAYSMARKET      222-1
#define SOFF_OVERALLSELL     225-1
#define SOFF_REMARK          232-1
#define SOFF_STAMPAMT        257-1
#define SOFF_TRAFLAG         264-1
#define SOFF_SALESLIST       265-1
#define SOFF_KEYDATE         266-1
#define SOFF_LIENSFLAG       274-1
#define SOFF_PROCESSDATE     275-1     // yyyymmdd or mmddyyyy
#define SOFF_GARGAGE         283-1
#define SOFF_PERSPROPFLAG    284-1


#define SSIZ_SUBAREA         3
#define SSIZ_APN             10
#define SSIZ_SITUS           30
#define SSIZ_ZONE            10
#define SSIZ_USE             6
#define SSIZ_LOTSQFT         9
#define SSIZ_ACRES           10
#define SSIZ_BLDGCLASS       5
#define SSIZ_CONSTYR         4
#define SSIZ_BEDROOMS        1
#define SSIZ_BATHS           4
#define SSIZ_TOTALROOMS      3
#define SSIZ_BLDGSQFT        8
#define SSIZ_TYPE            1
#define SSIZ_FIREPLACE       1
#define SSIZ_COOLING         1
#define SSIZ_STORIES         1
#define SSIZ_MISCIMPROV      1
#define SSIZ_PARKING         1
#define SSIZ_POOL            1
#define SSIZ_SPA             1
#define SSIZ_HEATING         1
#define SSIZ_LANDALLOC       8
#define SSIZ_SELLER          29
#define SSIZ_BUYER           29
#define SSIZ_BOOKPAGE        8
#define SSIZ_REC_DATE        8
#define SSIZ_CONDITION       1
#define SSIZ_LISTFLAG        1
#define SSIZ_SALEPRICE       9
#define SSIZ_ADJPRICE        9
#define SSIZ_SALETYPE        1
#define SSIZ_VALUERATIO      6
#define SSIZ_DAYSMARKET      3
#define SSIZ_OVERALLSELL     7
#define SSIZ_REMARK          25
#define SSIZ_STAMPAMT        7
#define SSIZ_TRAFLAG         1
#define SSIZ_SALESLIST       1
#define SSIZ_KEYDATE         8
#define SSIZ_LIENSFLAG       1
#define SSIZ_PROCESSDATE     8
#define SSIZ_GARAGE          1
#define SSIZ_PERSPROPFLAG    1

typedef struct _tTuoSale
{
   char  SubArea[SSIZ_SUBAREA];
   char  Apn[SSIZ_APN];
   char  Situs[SSIZ_SITUS];
   char  Zone[SSIZ_ZONE];
   char  Use[SSIZ_USE];
   char  LotSqft[SSIZ_LOTSQFT];
   char  Acres[SSIZ_ACRES];
   char  BuildingClass[SSIZ_BLDGCLASS];
   char  ConstYr[SSIZ_CONSTYR];
   char  BedRooms[SSIZ_BEDROOMS];
   char  Baths[SSIZ_BATHS];
   char  TotalRooms[SSIZ_TOTALROOMS];
   char  BuildingSqft[SSIZ_BLDGSQFT];
   char  Type;
   char  FirePlace;
   char  Cooling;
   char  Stories;
   char  MiscImprov;
   char  Parking;
   char  Pool;
   char  Spa;
   char  Heating;
   char  LandAlloc[SSIZ_LANDALLOC];
   char  Seller[SSIZ_BUYER];
   char  Buyer[SSIZ_BUYER];
   char  Rec_Doc[SSIZ_BOOKPAGE];
   char  Rec_Date[SSIZ_REC_DATE];
   char  Condition;
   char  ListFlag;
   char  SalePrice[SSIZ_SALEPRICE];
   char  AdjPrice[SSIZ_SALEPRICE];
   char  SaleType;
   char  ValueRatio[SSIZ_VALUERATIO];
   char  DaysMarket[SSIZ_DAYSMARKET];
   char  OverallSell[SSIZ_OVERALLSELL];
   char  Remark[SSIZ_REMARK];
   char  DocTax[SSIZ_STAMPAMT];
   char  TRAFlag;
   char  SalesList;
   char  KeyDate[SSIZ_KEYDATE];
   char  LiensFlag;
   char  ProcessDate[SSIZ_PROCESSDATE];
   char  Garage;
   char  PersPropFlag;
   char  Filler[16];
} TUO_SALE;

#define TOFF_APN               1-1
#define TOFF_BILLNO            11-1
#define TOFF_TOTALTAXAMT       17-1
#define TOFF_FIRSTTAXAMT       27-1
#define TOFF_SECTAXAMT         37-1
#define TOFF_SOLDTOSTATEDATE   47-1
#define TOFF_DEFAULTFLAG       55-1
#define TOFF_FIRSTPAYMENTDATE  57-1
#define TOFF_SECPAYMENTDATE    63-1

#define TSIZ_APN               10
#define TSIZ_BILLNO            6
#define TSIZ_TOTALTAXAMT       10
#define TSIZ_FIRSTTAXAMT       10
#define TSIZ_SECTAXAMT         10
#define TSIZ_SOLDTOSTATEDATE   8
#define TSIZ_DEFAULTFLAG       1
#define TSIZ_FIRSTPAYMENTDATE  6
#define TSIZ_SECPAYMENTDATE    6

// Cortacfile
typedef struct _tTuoTax
{  // 70-bytes
   char  Apn[TSIZ_APN];
   char  BillNo[TSIZ_BILLNO];
   char  TotalTaxAmt[TSIZ_TOTALTAXAMT];
   char  FirstTaxAmt[TSIZ_TOTALTAXAMT];
   char  SecTaxAmt[TSIZ_TOTALTAXAMT];
   // MMDDYYYY
   char  SoldToStateDate[TSIZ_SOLDTOSTATEDATE];
   char  DefaultFlag;
   char  Filler;
   // MMDDYY
   char  FirstPaymentDate[TSIZ_FIRSTPAYMENTDATE];
   char  SecPaymentDate[TSIZ_FIRSTPAYMENTDATE];
   char  CrLf[2];
} TUO_TAX;

// Suppfile
typedef struct _tTuoSup
{  // 72-bytes
   char  Apn[TSIZ_APN];
   char  BillNo[TSIZ_BILLNO];
   char  TotalTaxAmt[TSIZ_TOTALTAXAMT];
   char  FirstTaxAmt[TSIZ_TOTALTAXAMT];
   char  SecTaxAmt[TSIZ_TOTALTAXAMT];
   // MMDDYY
   char  SoldToStateDate[TSIZ_FIRSTPAYMENTDATE];
   char  DefaultFlag;
   // MMDDYY
   char  FirstPaymentDate[TSIZ_FIRSTPAYMENTDATE];
   char  SecPaymentDate[TSIZ_FIRSTPAYMENTDATE];
   char  UnknownDate[TSIZ_FIRSTPAYMENTDATE];
   char  Filler;
} TUO_SUP;

#define ESIZ_OWNER             52
#define ESIZ_PCTOWN            4
#define ESIZ_DOCNUM            12

// cddataetal
typedef struct _tTuoEtal
{  // 78-bytes
   char  Owner[ESIZ_OWNER];
   char  PctOwn[ESIZ_PCTOWN];
   char  DocNum[ESIZ_DOCNUM];
   char  Apn[SSIZ_APN];
} TUO_ETAL;


#define  OFF_TUOG_PRSTAT     1
#define  OFF_TUOG_PRDOC      7
#define  OFF_TUOG_PRSERV     12
#define  OFF_TUOG_PRTYPE     20
#define  OFF_TUOG_PRQUEUE    26
#define  OFF_TUOG_DOCNUM     36
#define  OFF_TUOG_DOCTYPE    52
#define  OFF_TUOG_DOCDATE    66
#define  OFF_TUOG_GRTOR      74
#define  OFF_TUOG_GRTEE      134
#define  OFF_TUOG_BOOK       228
#define  OFF_TUOG_PAGE       235
#define  OFF_TUOG_APN        242
#define  SIZ_TUOG_APN        10

#define  SIZ_GR_PRSTAT       6
#define  SIZ_GR_PRDOC        5
#define  SIZ_GR_PRSERV       8
#define  SIZ_GR_PRTYPE       6
#define  SIZ_GR_PRQUEUE      10
#define  SIZ_GR_DOCTYPE      14
#define  SIZ_GR_GRTOR        60
#define  SIZ_GR_GRTEE        94
#define  SIZ_GR_BOOK         7
#define  SIZ_GR_PAGE         7

typedef struct _tTuogDef
{  // RecSize=256
   char  PrStat[SIZ_GR_PRSTAT];
   char  PrDoc[SIZ_GR_PRDOC];
   char  PrServ[SIZ_GR_PRSERV];
   char  PrType[SIZ_GR_PRTYPE];
   char  PrQueue[SIZ_GR_PRQUEUE];
   char  DocNum[SIZ_GR_DOCNUM];
   char  DocType[SIZ_GR_DOCTYPE];
   char  DocDate[SIZ_GR_DOCDATE];
   char  Grantors[SIZ_GR_GRTOR];
   char  Grantees[SIZ_GR_GRTEE];
   char  Book[SIZ_GR_BOOK];
   char  Page[SIZ_GR_PAGE];
   char  APN[SIZ_TUOG_APN];
   char  filler[3];
   char  CRLF[2];
} TUOG_DEF;

IDX_TBL4 Tuo_Vesting[] =
{
   " ETUX",       "EU", 5, 2,
   " ET UX",      "EU", 6, 2,
   " ETAL",       "EA", 5, 2,
   " ET AL",      "EA", 6, 2,
   " TSTEE",      "TE", 6, 2,
   " SUCC TR",    "SU", 8, 2,    // Successor Trust
   " SUC TRS",    "SU", 8, 2,
   " TRUSTE",     "TE", 7, 2,
   " TR ",        "TR", 4, 2,
   " TRS",        "TR", 4, 2,
   " TSTES",      "TE", 6, 2,
   " CO-TR",      "TR", 6, 2,
   " CO TRS",     "TR", 7, 2,
   " COTRST",     "TR", 7, 2,
   " LIFE EST",   "LE", 9, 2,
   " M D",        "MD", 4, 2,
   " FAMILY TR",  "FM", 10,2,
   " FMLY TR",    "FM", 8, 2,
   "", "", 0, 0
};

// Use to translate GrGr's DocTitle to standard DocType
IDX_TBL5 TUO_DocTitle[] =
{  // DocTitle, Index, Non-sale, len1, len2
   "21", "13", 'N', 2, 2,  // Deed
   "91", "27", 'N', 2, 2,  // Trustees deed
   "14", "32", 'N', 2, 2,  // Bill of Sale
   "86", "67", 'N', 2, 2,  // Tax Deed
   "97", "8 ", 'Y', 2, 2,  // Agreement of Sale
   "22", "40", 'Y', 2, 2,  // Deed Easement
   "23", "13", 'N', 2, 2,  // Deed In Lieu - Short sale
   "10", "7 ", 'Y', 2, 2,  // Assignment
   "11", "7 ", 'Y', 2, 2,  // Assignment D/T
   "12", "7 ", 'Y', 2, 2,  // Assignment Lien
   "13", "19", 'Y', 2, 2,  // Agreement (all)
   "16", "19", 'Y', 2, 2,  // CNC tax default
   "25", "65", 'Y', 2, 2,  // D/T
   "26", "19", 'Y', 2, 2,  // D/T & ASGT
   "31", "46", 'Y', 2, 2,  // Lien
   "32", "44", 'Y', 2, 2,  // Lease
   "36", "46", 'Y', 2, 2,  // Lien - Mechanics
   "45", "52", 'Y', 2, 2,  // Notice of Default
   "52", "80", 'Y', 2, 2,  // Order Final Dist.
   "5",  "6 ", 'Y', 1, 2,  // Affidavit - death spouse
   "67", "19", 'Y', 2, 2,  // Release assmt lien
   "6",  "6 ", 'Y', 1, 2,  // Affidavit - death TR
   "72", "19", 'Y', 2, 2,  // Release lien
   "73", "19", 'Y', 2, 2,  // Release LIS Pendes
   "74", "19", 'Y', 2, 2,  // Release lien mechs
   "75", "19", 'Y', 2, 2,  // Release tax lien
   "76", "19", 'Y', 2, 2,  // Req Notice of Delinq
   "77", "19", 'Y', 2, 2,  // Req Notice of Default
   "7",  "6 ", 'Y', 1, 2,  // Affidavit - misc
   "83", "19", 'Y', 2, 2,  // Substitution of Trustee
   "84", "19", 'Y', 2, 2,  // Substitution of Trustee
   "86", "19", 'Y', 2, 2,  // Tax Deflt prop
   "88", "46", 'Y', 2, 2,  // Tax lien
   "92", "19", 'Y', 2, 2,  // Release - misc
   "95", "6 ", 'Y', 2, 2,  // Affidavit of death - misc
   "99", "19", 'Y', 2, 2,  // Misc docs
   "SU", "19", 'Y', 2, 2,  // Subrdn
   "","",' ',0,0
};

/*
XLAT_CODE Tuo_UseCode[]=
{
   "00","120",2,
   "01","120",2,
   "02","120",2,
   "03","120",2,
   "04","176",2,
   "05","176",2,
   "06","176",2,
   "07","835",2,
   "08","829",2,
   "10","101",2,
   "11","100",2,
   "12","115",2,
   "13","100",2,
   "14","100",2,
   "15","100",2,
   "16","107",2,
   "17","100",2,
   "18","100",2,
   "19","100",2,
   "20","175",2,
   "21","175",2,
   "22","175",2,
   "23","175",2,
   "24","175",2,
   "25","175",2,
   "30","175",2,
   "31","175",2,
   "32","175",2,
   "33","175",2,
   "34","175",2,
   "35","175",2,
   "40","175",2,
   "41","175",2,
   "42","175",2,
   "43","175",2,
   "44","175",2,
   "45","175",2,
   "50","522",2,
   "51","522",2,
   "52","522",2,
   "53","522",2,
   "54","522",2,
   "55","522",2,
   "56","522",2,
   "57","522",2,
   "58","522",2,
   "59","522",2,
   "60","536",2,
   "61","536",2,
   "62","536",2,
   "63","536",2,
   "64","536",2,
   "70","726",2,
   "71","727",2,
   "75","776",2,
   "77","793",2,
   "78","776",2,
   "79","776",2,
   "80","202",2,
   "81","219",2,
   "82","127",2,
   "83","114",2,
   "84","234",2,
   "85","200",2,
   "87","400",2,
   "89","115",2,
   "90","801",2,
   "91","801",2,
   "92","801",2,
   "94","801",2,
   "96","789",2,
   "97","801",2,
   "98","801",2,
   "99","783",2,
   "","851",0
};
*/
#endif
