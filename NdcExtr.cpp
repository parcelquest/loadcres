/******************************************************************************
 *
 * 11/14/2017 Modify NR_CreateSCSale() to add option to exclude Owner data (default=Y)
 * 02/08/2018 Add NR_FormatDnxApn() to support DNX.  Set ARCode='N' for NDC data.
 *            Modify NR_CreateSCSale() to format DocNum for DNX the same as old file.
 * 02/12/2018 Modify NR_CreateSCSale() to correct sale code based on Attom's definition.
 * 02/20/2018 Modify NR_CreateSCSale() to unzip file if it has ".gz" ext.
 * 03/05/2018 Add NR_FormatOrgApn() & NR_FormatSclApn().  Modify NR_CreateSCSale() to
 *            unzip if input file has .gz extension and to support ORG & SCL.
 * 03/11/2018 Fix bug in NR_CreateSCSale()
 * 03/15/2018 Update for new sale extract from NDC and standardize DocNum format YYYY99999 for SUT.
 * 03/20/2018 Bug fix in NR_CreateSCSale() that we forgot to update new infile after unzip.
 * 04/13/2018 Modify NR_CreateSCSale() to update owner info and change sort logic for output file. - ORG
 * 04/15/2018 Split seller if separated by '|' in NR_CreateSCSale().
 * 04/25/2018 Modify NR_CreateSCSale() to set DocType=1 when Doc_Type="HI" and Sale_Or_Transfer='S'
 *            Pickup owner info regardless of transaction type.
 * 05/22/2018 Add support for SFX.
 * 05/27/2018 Fix bug in NR_FormatSutApn()
 * 11/12/2018 Fix DocNum for bbbbpppp format records older than 19950201.
 * 01/07/2019 Modify NR_FormatSutDocNum() to match with DocNum in county roll file.
 * 01/09/2019 Fix bug in NR_FormatSutApn().
 * 02/22/2020 Add NR_FormatSbdApn(), NR_FormatSbdDocNum() & modify NR_CreateSCSale() to support SBD
 * 02/25/2020 Modify NR_CreateSCSale() to update ConfirmedSalePrice only if there no DocTax.
 *            Consider Trustee Deed as non-sale (SBD).
 * 03/02/2020 Fine tune NR_CreateSCSale()
 * 04/02/2020 Add NR_FormatLaxApn() & modify NR_CreateSCSale() to support LAX.
 *            Use default "ASH_File" in [Data] section of INI file.
 * 06/09/2020 Add NR_FormatColApn() & NR_FormatColDocNum()
 * 06/14/2020 Add NR_FormatEdxApn(), NR_FormatMadApn(), NR_FormatStdDocNum(), NR_FormatInyApn(), 
 *            NR_FormatInyDocNum(), NR_FormatMenApn(), NR_FormatSloApn(), and modify NR_CreateSCSale().
 *            
 *
 ******************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Recdef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "OutRecs.h"
#include "doSort.h"
#include "doZip.h"
#include "Prodlib.h"
#ifdef _LOADMB_
#include "MBExtrn.h"
#endif
#ifdef _LOADONE_
#include "LOExtrn.h"
#endif
#ifdef _LOADCRES_
#include "LCExtrn.h"
#endif

#include "NdcExtr.h"

/*****************************************************************************
 *
 * Format APN
 *
 *****************************************************************************/

int NR_FormatColApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;

   iTmp = strlen(pApn);
   if (iTmp == 12)
      strcpy(pFmtApn, pApn);
   else if (iTmp == 10)
      iTmp = sprintf(pFmtApn, "0%s0", pApn);
   else
      iTmp = 0;

   return iTmp;
}

int NR_FormatDnxApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;

   iTmp = strlen(pApn);
   if (iTmp == 12)
      strcpy(pFmtApn, pApn);
   else if (iTmp < 10)
      iTmp = sprintf(pFmtApn, "%.6s0%.2s000", pApn, pApn+6);   // Drop last 2 digits
   else
      iTmp = 0;

   return iTmp;
}

/*****************************************************************************
 *
 * 03106107   = 031061007000
 * 1236700230 = 123670023000
 * 0291600310 = 029160003000
 * Only take record with 12-byte APN.  Most other APN no longer exist in 2019 roll.
 *
 *****************************************************************************/
int NR_FormatEdxApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[32];

   strcpy(sApn, pApn);
   remChar(sApn, '-');
   iTmp = remChar(sApn, ' ');
   if (iTmp == 12)
      strcpy(pFmtApn, sApn);
   else
      iTmp = 0;

   return iTmp;
}

/*****************************************************************************
 *
 * 00105405   = 0010540500
 * 0101107    = 0010110700
 * 0010200602 = 0010200602

 *****************************************************************************/
int NR_FormatInyApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;

   iTmp = strlen(pApn);
   if (iTmp == 10)
      strcpy(pFmtApn, pApn);
   else if (iTmp == 8)
      iTmp = sprintf(pFmtApn, "%s00", pApn);
   else if (iTmp == 7)
      iTmp = sprintf(pFmtApn, "0%s00", pApn);
   else
      iTmp = 0;

   return iTmp;
}

int NR_FormatLaxApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[64];

   *pFmtApn = 0;
   strcpy(sApn, pApn);
   remCharEx(sApn, "- ");

   iTmp = strlen(sApn);
   if (*pApn > '9' || iTmp != iApnLen)
      iTmp = 0;
   else 
      strcpy(pFmtApn, sApn);

   return iTmp;
}

int NR_FormatMadApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[32];

   strcpy(sApn, pApn);
   remChar(sApn, '-');
   iTmp = remChar(sApn, ' ');
   if (iTmp == 9)
      iTmp = sprintf(pFmtApn, "%s000", pApn);   
   else if (iTmp == 12)
      strcpy(pFmtApn, sApn);
   else if (iTmp == 10 && sApn[9] == '0')
      iTmp = sprintf(pFmtApn, "%s00", sApn);   
   else
      iTmp = 0;

   return iTmp;
}

int NR_FormatMenApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;

   *pFmtApn = 0;
   iTmp = strlen(pApn);
   if (iTmp == iFmtLen)
      strcpy(pFmtApn, pApn);
   else
      iTmp = 0;

   return iTmp;
}

int NR_FormatOrgApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[64];

   *pFmtApn = 0;
   strcpy(sApn, pApn);
   remCharEx(sApn, "- ");

   iTmp = strlen(sApn);
   if (*pApn > '9' || iTmp != iApnLen)
      iTmp = 0;
   else 
      strcpy(pFmtApn, sApn);

   return iTmp;
}

/*****************************************************************************
 *
 * APN: 
 *    - 039930148
 *    - 0434581330000
 *    - 0621  172   06
 *    - 0000023913103
 *    - 0108-381-05
 *
 *****************************************************************************/

int NR_FormatSbdApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[64];

   *pFmtApn = 0;
   strcpy(sApn, pApn);
   remCharEx(sApn, "- ");

   iTmp = strlen(sApn);
   if (iTmp == iFmtLen)
   {
      if (!memcmp(sApn, "0000", 4))
         iTmp = sprintf(pFmtApn, "%s0000", (char *)&sApn[4]);
      else
         strcpy(pFmtApn, sApn);
   } else if (iTmp > iFmtLen)
   {
      memcpy(pFmtApn, sApn, iFmtLen);
      *(pFmtApn+iFmtLen) = 0;
   } else if (iTmp == 9)
   {
      iTmp = sprintf(pFmtApn, "%s0000", sApn);
   } else
      iTmp = 0;

   return iTmp;
}

int NR_FormatSclApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[64];

   *pFmtApn = 0;
   strcpy(sApn, pApn);
   remCharEx(sApn, "- /*\\");

   iTmp = strlen(sApn);
   if (iTmp == iFmtLen)
      strcpy(pFmtApn, sApn);
   else if (iTmp > iFmtLen)
   {
      memcpy(pFmtApn, sApn, iFmtLen);
      *(pFmtApn+iFmtLen) = 0;
   } else
      iTmp = 0;

   return iTmp;
}

int NR_FormatSfxApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[64], *pTmp;

   *pFmtApn = 0;

   if (bRawApn)
   {
      strcpy(sApn, pApn);
      blankRem(sApn);
      if (sApn[4] == ' ' || sApn[5] == ' ')
         strcpy(pFmtApn, sApn);
      else if (!(pTmp=strchr(sApn, ' ')))
      {
         if (sApn[4] > '9')
            sprintf(pFmtApn, "%.5s %s", sApn, &sApn[6]);
         else
            sprintf(pFmtApn, "%.4s %s", sApn, &sApn[5]);
      }
   } else
      strcpy(pFmtApn, pApn);

   iTmp = strlen(pFmtApn);

   return iTmp;
}

int NR_FormatSloApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   int   iTmp;
   char  sApn[64];

   *pFmtApn = 0;
   strcpy(sApn, pApn);
   iTmp = remChar(sApn, '-');

   if (iTmp == iFmtLen)
      strcpy(pFmtApn, pApn);
   else
      iTmp = 0;

   return iTmp;
}

int NR_FormatSolApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   char *p0, *p1, *p2;
   int   iTmp;

   if (p1 = strchr(pApn, '-'))
   {
      p0 = pApn;
      *p1++ = 0;
      p2 = strchr(p1, '-');
      *p2++ = 0;
      iTmp = sprintf(pFmtApn, "%.4d%.3d%.3d", atol(p0), atol(p1), atol(p2));
   } else if ((iTmp = strlen(pApn)) < iFmtLen)
   {
      iTmp = sprintf(pFmtApn, "%.*s%s", iFmtLen-iTmp, "00000000", pApn);
      LogMsg("*** Reformat APN: %s", pApn);
   } else
   {
      strcpy(pFmtApn, pApn);
      iTmp = strlen(pFmtApn);
   }

   return iTmp;
}

int NR_FormatSutApn(char *pApn, char *pFmtApn, int iFmtLen, bool bRawApn=false)
{
   char *p0, *p1, *p2;
   int   iTmp;

   if (p1 = strchr(pApn, '-'))
   {
      p0 = pApn;
      *p1++ = 0;
      p2 = strchr(p1, '-');
      *p2++ = 0;
      iTmp = sprintf(pFmtApn, "%.2d%.3d%.3d", atol(p0), atol(p1), atol(p2));
   } else
   {
      strcpy(pFmtApn, pApn);
      iTmp = strlen(pFmtApn);
   }

   return iTmp;
}

/******************************************************************************
 * How DocNum is being formatted?
 *
 * 0010 001      J18600120   20110523: J186120
 * 0011 007      G6090021    19961010: G060921
 * 0073 002B     0048510019  19931117: F485119
 * 0074 052      0047800956  20170720: K478956
 * 0123 020      0048420060  19931116: F484060
 * 0104 017      I586500231  20080521: I586532  ???
 * 0282 022      9K47100025  20170628: 
 * 0321 016      4711000700  20170629:
 *
 * Unable to format
 * 0104 061/0E38101049/19870709 
 *
 ******************************************************************************/

int NR_FormatSfxDocNum(char *pDocDate, char *pInDocNum, char *pOutDocNum)
{
   char sDocNum[32];
   int  iTmp=99;

   // Drop unrecorded
   if (!memcmp(pInDocNum, "0UNRE", 5))
      return iTmp;

   // Check DocNum
   if (isValidYMD(pDocDate, true))
   {
      if (*pInDocNum == '0')
      {
         if (!memcmp(pInDocNum, "000", 3))
         {
            while (*pInDocNum == '0') pInDocNum++;
            iTmp = sprintf(pOutDocNum, "%s", pInDocNum);
         } else if (!memcmp(pInDocNum, "00", 2))
         {
            iTmp = sprintf(pOutDocNum, "%.4s%s", pInDocNum+2, pInDocNum+7);
         } else if (!memcmp(pInDocNum+5, "00", 2))
         {
            iTmp = sprintf(sDocNum, "%.4s%s", pInDocNum+1, pInDocNum+7);
            if (iTmp == 6)
               iTmp = sprintf(pOutDocNum, "%c0%s", sDocNum[0], &sDocNum[1]);
            else
               strcpy(pOutDocNum, sDocNum);
         } else
         {
            while (*pInDocNum == '0') pInDocNum++;
            iTmp = sprintf(pOutDocNum, "%s", pInDocNum);
         }
      } else if (*pInDocNum > '9')
      {
         if (!memcmp(pInDocNum+4, "000", 3))
            iTmp = sprintf(pOutDocNum, "%.4s%s", pInDocNum, pInDocNum+7);
         else
            iTmp = sprintf(pOutDocNum, "%.5s%s", pInDocNum, pInDocNum+8);
      } else if (*pInDocNum > ' ')
         iTmp = sprintf(pOutDocNum, "%s", pInDocNum);
   }

   return iTmp;
}

/********************************* NR_FormatSutDocNum ******************************
 *
 * DocNum is in Book/Page format from 1901 till 19950131
 * Starting 19950201 it changed to 000YY99999 or 0000099999
 * 
 * Return 99 to keep DocNum unchanged
 ***********************************************************************************/

int NR_FormatSutDocNum(char *pDocDate, char *pInDocNum, char *pOutDocNum)
{
   int iDocNum, iTmp=99;

   // Due to various DocNum formats used before Y2K, we temporarily ignore them
   if (memcmp(pDocDate, "1990", 4) > 0 && isValidYMD(pDocDate, true))
   {
      iDocNum = atol(pInDocNum);
      if (iDocNum > 99999)
      {
         if (!memcmp(pDocDate+2, pInDocNum+3, 2))
         {
            iTmp = sprintf(pOutDocNum, "%.7s", pInDocNum+3);
         } else
         {
            // Format as bbbpppp
            iTmp = sprintf(pOutDocNum, "%.7d", iDocNum);
         }  
      } else
         iTmp = sprintf(pOutDocNum, "%.2s%.5d", pDocDate+2, iDocNum);
   } 

   return iTmp;
}

/********************************* NR_FormatSbdDocNum ******************************
 *
 * 1) pDocDate=20081117, pInDocNum=0000499807
 *    pOutDocNum=20080499807
 * 2) pDocDate=19750908, pInDocNum=0875800761 --> ignore
 * 3) pDocDate=19771106, pInDocNum=9360000001 --> ignore
 * 4) pDocDate=19560221, pInDocNum=0386200405 --> ignore
 * 5) pDocDate=19850712, pInDocNum=1676360003 --> ignore
 * 
 * Return 99 to keep DocNum unchanged
 ***********************************************************************************/

int NR_FormatSbdDocNum(char *pDocDate, char *pInDocNum, char *pOutDocNum)
{
   int iDocNum, iTmp=99;

   if (!memcmp(pInDocNum, "000", 3) && isValidYMD(pDocDate, true))
   {
      iDocNum = atol(pInDocNum);
      iTmp = sprintf(pOutDocNum, "%.4s%.7d", pDocDate, iDocNum);
   } 

   return iTmp;
}

/***********************************************************************************
 *
 * Only use transaction 2000 or later
 *
 ***********************************************************************************/

int NR_FormatColDocNum(char *pDocDate, char *pInDocNum, char *pOutDocNum)
{
   int iDocNum, iTmp=99;

   // Due to various DocNum formats used before Y2K, we temporarily ignore them
   if (*pDocDate == '2' && isValidYMD(pDocDate, true))
   {
      iDocNum = atol(pInDocNum);
      iTmp = sprintf(pOutDocNum, "%.4sR%d", pDocDate, iDocNum);
   } 

   return iTmp;
}

/***********************************************************************************
 *
 * Standard DocNum format yyyyR9999999
 *
 ***********************************************************************************/

int NR_FormatStdDocNum(char *pDocDate, char *pInDocNum, char *pOutDocNum, int iNumDigits=7)
{
   int iDocNum, iTmp=99;

   if (isValidYMD(pDocDate, true))
   {
      iDocNum = atol(pInDocNum);
      iTmp = sprintf(pOutDocNum, "%.4sR%.*d", pDocDate, iNumDigits, iDocNum);
   } 

   return iTmp;
}

/***********************************************************************************
 *
 *
 ***********************************************************************************/

int NR_FormatSolDocNum(char *pDocDate, char *pInDocNum, char *pOutDocNum)
{
   int iDocNum, iTmp=99;

   if (isValidYMD(pDocDate, true))
   {
      iDocNum = atol(pInDocNum);
      iTmp = sprintf(pOutDocNum, "%.4s%.8d", pDocDate, iDocNum);
   } 

   return iTmp;
}

/***********************************************************************************
 *
 * yyyy/nnnn
 *
 ***********************************************************************************/

int NR_FormatInyDocNum(char *pDocDate, char *pInDocNum, char *pOutDocNum)
{
   int iDocNum, iTmp=99;

   // Due to various DocNum formats used before Y2K, we temporarily ignore them
   if (*pDocDate == '2' && isValidYMD(pDocDate, true))
   {
      iDocNum = atol(pInDocNum);
      iTmp = sprintf(pOutDocNum, "%.4s/%.4d", pDocDate, iDocNum);
   } 

   return iTmp;
}

/***************************** NR_CreateSCSale *******************************
 *
 * Extract sale data from ???_NRSale.txt and output to ???_SALE.DAT
 * Drop all records without APN, DocNum or DocDate
 *
 * DOC_TYPE:  Indicates the type of document recorded (deed).  A DQ standard code 
 *            defining the type of the first document recorded in a transaction
 * DEED_TYPE: Indicates more detailed information, than is afforded by the field 
 *            SR_DOC_TYPE, regarding the type of document that was filed
 *
 * Input:  ???_NRSale.txt 
 * Output: ???_Sale.dat (SCSAL_REC format).  
 *
 * Return 0 if successful, Otherwise error
 *
 *****************************************************************************/

int NR_CreateSCSale(char *pCnty, char *pInFile, int iMinApnLen)
{
   char     acTmpFile[_MAX_PATH], acInFile[_MAX_PATH], *pTmp, *pTmp1;
   char     acTmp[256], acRec[2048], acBuf[2048], acDocNum[16], cSaleDelim;
   bool     bInclOwner, bDocNumOnly=false, bRaw;

   FILE      *fdOut, *fdIn;
   SCSAL_EXT *pSaleRec = (SCSAL_EXT *)acBuf;

   int      iTmp, iCnty;
   double   dTmp;
   long     lCnt=0, lPrice, lTmp;

   LogMsg0("Creating Sale export using %s", pInFile);

   // Check for zip file
   if ((pTmp = strstr(pInFile, ".gz")))
   {
      strcpy(acInFile, pInFile);
      pTmp = strrchr(acInFile, '.');
      *pTmp = 0;

      iTmp = chkFileDateTime(pInFile, acInFile);
      if (iTmp == 1 || iTmp == -2)  // Infile is newer or outfile doesn't exist
      {
         LogMsg("Unzip sale file: %s", pInFile);

         // Unzip input file if it is newer
         sprintf(acTmp, "C:\\Tools\\Gzip.exe -df %s", pInFile);
         try
         {
            system(acTmp);
         } catch (...)
         {
            LogMsg("***** Error running command: %s", acTmp);
            return -1;
         }
      } 
   } else
      strcpy(acInFile, pInFile);

   GetPrivateProfileString(pCnty, "SaleOwner", "Y", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bInclOwner = true;
   else
      bInclOwner = false;

   // Flag that indicates DocNum doesn't include Doc Year
   GetPrivateProfileString(pCnty, "DocNumOnly", "Y", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bDocNumOnly = true;
   else
      bDocNumOnly = false;

   GetPrivateProfileString(pCnty, "SaleDelim", "~", acTmp, _MAX_PATH, acIniFile);
   cSaleDelim = acTmp[0];

   iCnty = GetPrivateProfileInt(pCnty, "County", 0, acIniFile);

   if (!iCnty)
   {
      LogMsg("***** Not supported county: %s. Please contact Sony for instruction.", pCnty);
      return -1;
   }

   // Open Sales file
   LogMsg("Open Sales file %s", acInFile);
   fdIn = fopen(acInFile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Sales file: %s\n", acInFile);
      sprintf(acTmp, "*** %s - Error opening Sales file.  Errno=%d", pCnty, errno);
      sprintf(acRec, "Please review log file for more info on %s", acInFile);

      return -1;
   }

   // Skip header
   pTmp = fgets(acRec, 2048, fdIn);
   if (pTmp && (acRec[0] > '9' || acRec[0] < '0'))
      pTmp = fgets(acRec, 2048, fdIn);

   // Open Output file
   sprintf(acTmpFile, "%s\\%s\\%s_Sale.tmp", acTmpPath, pCnty, pCnty);

   LogMsg("Open output file %s", acTmpFile);
   fdOut = fopen(acTmpFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating sale output file: %s\n", acTmpFile);
      return -2;
   }

   lLastRecDate = 0;
   // Loop through record set
   while (!feof(fdIn))
   {  
      if (!(pTmp = fgets(acRec, 2048, fdIn)))
         break;

      // Parse input rec
      iTokens = ParseStringIQ(acRec, cSaleDelim, MAX_FLD_TOKEN, apTokens);
      if (iTokens <= NR_SALE_OR_TRANSFER)
      {
         LogMsg0("*** Error: bad sale record %.16s (#tokens=%d)", acRec, iTokens);
         continue;
      }

      // Collect data - Only take sale rec that has both docnum and docdate
      if (*apTokens[NR_DOC_NBR_FMT] == ' ' || *apTokens[NR_DATE_TRANSFER] == ' ')
         continue;

      // Ignore deed of trust
      if (!_memicmp(apTokens[NR_DOC_TYPE], "DEED OF TRUST", 9))
         continue;
      if (!strcmp(apTokens[NR_DEED_TYPE], "T") && *apTokens[NR_DOC_TYPE] < '0')
         continue;

      // Ignore transaction without APN
      if (*apTokens[NR_PARCEL_NBR_FMT] < '0')
	   {
         iTmp = remChar(apTokens[NR_PARCEL_NBR_RAW], '-');
         iTmp = remChar(apTokens[NR_PARCEL_NBR_RAW], ' ');
		   if (iTmp < iMinApnLen)
            continue;
         bRaw = true;
         strcpy(apTokens[NR_PARCEL_NBR_FMT], apTokens[NR_PARCEL_NBR_RAW]);
      } else
         bRaw = false;

      // Drop bad APN
      if (*apTokens[NR_PARCEL_NBR_FMT] > '9')
         continue;

	   // Reset output record
      memset((void *)&acBuf, ' ', sizeof(SCSAL_EXT));

      // APN
      if (iCnty == 6)
         iTmp = NR_FormatColApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 8)
         iTmp = NR_FormatDnxApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 9)
         iTmp = NR_FormatEdxApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 14)
         iTmp = NR_FormatInyApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 19)
         iTmp = NR_FormatLaxApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 20)
         iTmp = NR_FormatMadApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 23)
         iTmp = NR_FormatMenApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 30)
         iTmp = NR_FormatOrgApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 36)
         iTmp = NR_FormatSbdApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 38)
         iTmp = NR_FormatSfxApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen, bRaw);
      else if (iCnty == 40)
         iTmp = NR_FormatSloApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 43)
         iTmp = NR_FormatSclApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 48)
         iTmp = NR_FormatSolApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);
      else if (iCnty == 51)
         iTmp = NR_FormatSutApn(apTokens[NR_PARCEL_NBR_FMT], acTmp, iMinApnLen);

      if (!iTmp)
         continue;

      memcpy(pSaleRec->Apn, acTmp, iTmp);

      // Ext_Prop_ID
      memcpy(pSaleRec->OtherApn, apTokens[NR_EXT_PROPERTY_ID], strlen(apTokens[NR_EXT_PROPERTY_ID]));

#ifdef _DEBUG
      //if (!memcmp(pSaleRec->Apn, "010858207", 9))
      //   iTmp = 0;
#endif
      // Doc date
      memcpy(pSaleRec->DocDate, apTokens[NR_DATE_TRANSFER], 8);
      lTmp = atol(apTokens[NR_DATE_TRANSFER]);
      if (lTmp > lLastRecDate && lTmp < lToday)
         lLastRecDate = lTmp;

      // Docnum
      acDocNum[0] = 0;
      if (iCnty == 6)                      // COL
         iTmp = NR_FormatColDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum);
      else if (iCnty == 8)                 // DNX
         iTmp = sprintf(acDocNum, "%d", atol(apTokens[NR_DOC_NBR_FMT]));
      else if (iCnty == 9 || iCnty == 20)  // EDX, MAD
         iTmp = NR_FormatStdDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum, 7);
      else if (iCnty == 14)                // INY
         iTmp = NR_FormatInyDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum);
      else if (iCnty == 19)                // LAX
         iTmp = sprintf(acDocNum, "%.7d", atol(apTokens[NR_DOC_NBR_FMT]));
      else if (iCnty == 23)                // MEN
         iTmp = sprintf(acDocNum, "%.5d", atol(apTokens[NR_DOC_NBR_FMT]));
      else if (iCnty == 30 || iCnty == 43) // ORG, SCL
         iTmp = sprintf(acDocNum, "%.8d", atol(apTokens[NR_DOC_NBR_FMT]));
      else if (iCnty == 36)                // SBD
         iTmp = NR_FormatSbdDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum);
      else if (iCnty == 38)                // SFX
         iTmp = NR_FormatSfxDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum);
      else if (iCnty == 40)                // SLO
         iTmp = NR_FormatStdDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum, 6);
      else if (iCnty == 48)                // SFX
         iTmp = NR_FormatSolDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum);
      else if (iCnty == 51)                // SUT
      {
         iTmp = NR_FormatSutDocNum(apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], acDocNum);
         //if (iTmp == 10)
         //   LogMsg0("*** Bad DocNum or DocDate: APN=%.8s, DocDate=%.8s, DocNum=%s [%s]", pSaleRec->Apn, pSaleRec->DocDate, acDocNum, apTokens[NR_PARCEL_NBR_FMT]);
      } else                                 // SOL
         iTmp = sprintf(acDocNum, "%.4s%.8d", apTokens[NR_DATE_TRANSFER], atol(apTokens[NR_DOC_NBR_FMT]));

      // Bad DocNum
      if (!iTmp || iTmp > 12)
      {
         if (bDebug && *apTokens[NR_DOC_NBR_FMT] >= '0')
            LogMsg0("*** Drop DocDate or DocNum: DocDate=%s DocNum=%s [%s]", apTokens[NR_DATE_TRANSFER], apTokens[NR_DOC_NBR_FMT], apTokens[NR_PARCEL_NBR_FMT]);
         continue;
      }

      // Check for valid DocNum/DocDate
      if (!bDocNumOnly && memcmp(acDocNum, pSaleRec->DocDate, 4))
      {
         if (bDebug)
            LogMsg0("*** Bad DocNum or DocDate: DocDate=%.8s, DocNum=%s [%s]", pSaleRec->DocDate, acDocNum, apTokens[NR_PARCEL_NBR_FMT]);
         continue;
      }

#ifdef _DEBUG
      //if (!memcmp(pSaleRec->Apn, "00203728", 8))
      //   iTmp = 0;
#endif
      memcpy(pSaleRec->DocNum, acDocNum, iTmp);

      // Group sale?
      if (*apTokens[NR_MULT_APN_FLAG] == 'M' || *apTokens[NR_MULT_APN_FLAG] == 'Y' || *apTokens[NR_FULL_PART_CODE] == 'P')
         pSaleRec->MultiSale_Flg = 'Y';

      if (iCnty != 36)
      {
         // Confirmed sale price
         lPrice = atol(apTokens[NR_VAL_TRANSFER]);
         if (lPrice > 0)
         {
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            memcpy(pSaleRec->ConfirmedSalePrice, acTmp, iTmp);
            memcpy(pSaleRec->SalePrice, acTmp, iTmp);
         } else
            lPrice = 0;

         // Tax
         dTmp = atof(apTokens[NR_TAX_TRANSFER]);
         if (dTmp > 0.0)
         {
            // Save DocTax
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(pSaleRec->StampAmt, acTmp, iTmp);

            // Calculate sale price
            if (!lPrice)
            {
               lPrice = (long)(dTmp * SALE_FACTOR);
               iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               memcpy(pSaleRec->SalePrice, acTmp, iTmp);
            }
         }
      } else
      {
         // Tax
         dTmp = atof(apTokens[NR_TAX_TRANSFER]);
         if (dTmp > 0.0)
         {
            // Save DocTax
            iTmp = sprintf(acTmp, "%*.2f", SALE_SIZ_STAMPAMT, dTmp);
            memcpy(pSaleRec->StampAmt, acTmp, iTmp);

            // Calculate sale price
            lPrice = (long)(dTmp * SALE_FACTOR);
            iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            memcpy(pSaleRec->SalePrice, acTmp, iTmp);
         } else
         {
            // Actual transfer amount
            lPrice = atol(apTokens[NR_VAL_TRANSFER]);
            if (lPrice > 0)
            {
               iTmp = sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               memcpy(pSaleRec->ConfirmedSalePrice, acTmp, iTmp);
            } 
         }
      }

      // Doc code - accept following code only
      pSaleRec->NoneSale_Flg = 'N';
      if (*apTokens[NR_DEED_TYPE] == 'Q' || !_memicmp(apTokens[NR_DOC_TYPE], "QU", 2))
      {
         pSaleRec->DocType[0] = '4';               // QUITCLAIM DEED
         pSaleRec->NoneSale_Flg = 'Y';
      } else
      if (!_memicmp(apTokens[NR_DEED_TYPE], "AD", 2))
      {
         pSaleRec->DocType[0] = '6';               // AFFIDAVIT OF DEATH
         pSaleRec->NoneSale_Flg = 'Y';
      } else 
      if (!_memicmp(apTokens[NR_DEED_TYPE], "TX", 2))
      {
         if (!_memicmp(apTokens[NR_DOC_TYPE], "TR", 2))
            memcpy(pSaleRec->DocType, "27", 2);    // TRUSTEES DEED UPON SALE
         else
            memcpy(pSaleRec->DocType, "67", 2);    // TAX DEED
         pSaleRec->NoneSale_Flg = 'Y';
      } else 
      if (!_memicmp(apTokens[NR_DEED_TYPE], "TR", 2) && 
         (!_memicmp(apTokens[NR_DOC_TYPE], "TR", 2) || 
          !_memicmp(apTokens[NR_DOC_TYPE], "GR", 2) || 
          !_memicmp(apTokens[NR_DOC_TYPE], "HI", 2)))
      {
         memcpy(pSaleRec->DocType, "27", 2);       // TRUSTEES DEED UPON SALE

         // Special case for SBD since we use both county sale file and NDC sales
         if (!memcmp(pCnty, "SBD", 3))
            pSaleRec->NoneSale_Flg = 'Y';
      } else 
      if (!_memicmp(apTokens[NR_DEED_TYPE], "DL", 2) )
      {
         memcpy(pSaleRec->DocType, "78", 2);       // DEED IN LIEU OF FORCLOSURE
         if (*apTokens[NR_SALE_OR_TRANSFER] != 'S')
            pSaleRec->NoneSale_Flg = 'Y';
      } else 
      if (!_memicmp(apTokens[NR_DEED_TYPE], "GR", 2) || 
          !_memicmp(apTokens[NR_DOC_TYPE], "GR", 2) ||
          !strcmp(apTokens[NR_DEED_TYPE], "G"))
      {
         pSaleRec->DocType[0] = '1';               // GRANT DEED
      } else 
      if (!_memicmp(apTokens[NR_DOC_TYPE], "HI", 2))
      {
         if (lPrice > 0 || *apTokens[NR_SALE_OR_TRANSFER] == 'S')
            pSaleRec->DocType[0] = '1';            // High liability
      } else 
      if (!strcmp(apTokens[NR_DEED_TYPE], "U"))
      {
         memcpy(pSaleRec->DocType, "77", 2);       // ONE OF COMMITTEE, STRICT FORECLOSURE, SHERIFF, OR REDEMPTION DEEDS
      } else if (*apTokens[NR_DEED_TYPE] == 'W' || *apTokens[NR_DOC_TYPE] == 'W')
         memcpy(pSaleRec->DocType, "28", 2);       // WARRANTY DEED (similar to GRANT DEED but more protection)
      else
      {
         if (bDebug)
            LogMsg("*** Unknown DocType: Deed_Type= %s, DocType= %s, Doc_Nbr= %s, ext_property_id=%s", 
            apTokens[NR_DEED_TYPE], apTokens[NR_DOC_TYPE], apTokens[NR_DOC_NBR_FMT], apTokens[NR_EXT_PROPERTY_ID]);
         if (lPrice > 1000)
         {
            pSaleRec->DocType[0] = '1';            // If there is sale price, it's more likely a sale
         } else
            pSaleRec->NoneSale_Flg = 'Y';
      }

      // Sale code
      // C=Documentary Transfer Tax Calculated, 
      // F=Full consideration, 
      // N=Documentary transfer tax due = 0, 
      // P=Full consideration less liens/encumbrances, 
      // U=No SR_VAL_TRANSFER was keyed, because document was recorded as non-disclosed, 
      // <blank>=Unknown and Assumed to be a Full Consideration Transfer
      if (lPrice > 0)
      {
         switch (*apTokens[NR_FULL_PART_CODE])
         {
            case 'C':
            case 'F':
               pSaleRec->SaleCode[0] = 'F';
               break;
            case 'P':
               pSaleRec->SaleCode[0] = 'N';
               break;
         }
      }

#ifdef _DEBUG
      //if (!memcmp(pSaleRec->Apn, "2004001003", 10))
      //   iTmp = 0;
#endif
      if (bInclOwner)
      {
         // Seller
         if (_memicmp(apTokens[NR_SELLER], "UNKNOWN", 7) && _memicmp(apTokens[NR_SELLER], "OWNER", 5))
         {
            if (pTmp = strchr(apTokens[NR_SELLER], '|'))
            {
               *pTmp++ = 0;

               // Drop seller3 if present
               if (pTmp1 = strchr(pTmp, '|'))
                  *pTmp1 = 0;

               vmemcpy(pSaleRec->Seller2, pTmp, SALE_SIZ_SELLER);
            }
            vmemcpy(pSaleRec->Seller1, apTokens[NR_SELLER], SALE_SIZ_SELLER);
         }

         // Buyer
         if (_memicmp(apTokens[NR_BUYER], "UNKNOWN", 7) && _memicmp(apTokens[NR_BUYER], "OWNER", 5))
         {
            // Check to see if there is name2
            if (pTmp = strchr(apTokens[NR_BUYER], '|'))
            {
               *pTmp++ = 0;

               // Drop name3 if present
               if (pTmp1 = strchr(pTmp, '|'))
               {
                  *pTmp1 = 0;
                  pSaleRec->Etal = 'Y';
               }
               vmemcpy(pSaleRec->Name2, pTmp, SALE_SIZ_BUYER);
            }
            vmemcpy(pSaleRec->Name1, apTokens[NR_BUYER], SALE_SIZ_BUYER);
         }

         // Mail addr
         if (*apTokens[NR_MAIL_ADDR_RAW]> ' ' && _memicmp(apTokens[NR_SELLER], "UNKNOWN", 7))
         {
            if (*apTokens[NR_MAIL_UNIT_VAL] > ' ')
               sprintf(acTmp, "%s %s %s %s %s %s #%s", apTokens[NR_MAIL_HOUSE_NBR], apTokens[NR_MAIL_FRACTION], apTokens[NR_MAIL_DIR], 
                  apTokens[NR_MAIL_STREET_NAME], apTokens[NR_MAIL_SUF], apTokens[NR_MAIL_POST_DIR], apTokens[NR_MAIL_UNIT_VAL]);
            else
               sprintf(acTmp, "%s %s %s %s %s %s", apTokens[NR_MAIL_HOUSE_NBR], apTokens[NR_MAIL_FRACTION], apTokens[NR_MAIL_DIR], 
                  apTokens[NR_MAIL_STREET_NAME], apTokens[NR_MAIL_SUF], apTokens[NR_MAIL_POST_DIR]);
            iTmp = blankRem(acTmp);
            vmemcpy(pSaleRec->MailAdr1, acTmp, SALE_SIZ_M_ADR1, iTmp);

            sprintf(acTmp, "%s %s %s",  apTokens[NR_MAIL_CITY], apTokens[NR_MAIL_STATE], apTokens[NR_MAIL_ZIP]);
            iTmp = blankRem(acTmp);
            vmemcpy(pSaleRec->MailAdr2, acTmp, SALE_SIZ_M_ADR2, iTmp);
            vmemcpy(pSaleRec->MailZip, apTokens[NR_MAIL_ZIP], SALE_SIZ_M_ZIP);

            // Copy all elements of mailing
            vmemcpy(pSaleRec->M_StrNum, apTokens[NR_MAIL_HOUSE_NBR], SALE_SIZ_M_STRNUM);
            vmemcpy(pSaleRec->M_StrSub, apTokens[NR_MAIL_FRACTION], SALE_SIZ_M_STRSUB);
            vmemcpy(pSaleRec->M_PreDir, apTokens[NR_MAIL_DIR], SALE_SIZ_M_DIR);
            vmemcpy(pSaleRec->M_StrName, apTokens[NR_MAIL_STREET_NAME], SALE_SIZ_M_STREET);
            vmemcpy(pSaleRec->M_StrSfx, apTokens[NR_MAIL_SUF], SALE_SIZ_M_SUFF);
            vmemcpy(pSaleRec->M_PostDir, apTokens[NR_MAIL_POST_DIR], SALE_SIZ_M_DIR);
            vmemcpy(pSaleRec->M_UnitNo, apTokens[NR_MAIL_UNIT_VAL], SALE_SIZ_M_UNITNO);
            vmemcpy(pSaleRec->M_City, apTokens[NR_MAIL_CITY], SALE_SIZ_M_CITY);
            vmemcpy(pSaleRec->M_St, apTokens[NR_MAIL_STATE], SALE_SIZ_M_ST);
         }
      }

      pSaleRec->ARCode = 'N';
      pSaleRec->CRLF[0] = 10;
      pSaleRec->CRLF[1] = 0;
      fputs(acBuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   fclose(fdIn);
   fclose(fdOut);
   
   // Sort output file
   LogMsg("Total processed records: %u\n", lCnt);

   char acOutFile[_MAX_PATH], acAshTmpl[_MAX_PATH], acSlsFile[_MAX_PATH];

   GetIniString("Data", "ASH_File", acESalTmpl, acAshTmpl, _MAX_PATH, acIniFile);
   sprintf(acOutFile, acAshTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "dat");
   sprintf(acSlsFile, acAshTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "Sls");

   // Sort output file and dedup on APN asc, DocDate asc, Sale price desc, DocNum asc
   if (iCnty == 8 || iCnty == 23)
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B8192,1,34) ");
   else
      sprintf(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) OMIT(20,1,C,EQ,\" \",OR,31,1,C,EQ,\" \") F(TXT) DUPO(B8192,1,34) ");
   lTmp = sortFile(acTmpFile, acOutFile, acTmp);
   if (lTmp > 0)
   {
      if (!_access(acSlsFile, 0))
      {
         // Save old cumsale file
         sprintf(acTmpFile, acAshTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmpFile, 0))
            DeleteFile(acTmpFile);
         iTmp = rename(acSlsFile, acTmpFile);
      }

      // Rename output file to SLS
      LogMsg("Rename %s to %s", acOutFile, acSlsFile);
      iTmp = rename(acOutFile, acSlsFile);
      if (iTmp)
         LogMsg("***** Error renaming to %s", acSlsFile);
   } else
      iTmp = -2;

   LogMsg("Number of Sale records processed: %d.", lCnt);
   LogMsg("                          output: %d.", lTmp);
   LogMsg("         Latetest recording date: %d.", lLastRecDate);
   return iTmp;
}

