#define  SUP_MSG1                            0
#define  SUP_MSG2                            1
#define  SUP_MSG3                            2
#define  SUP_MSG4                            3
#define  SUP_MSG5                            4
#define  SUP_MSG6                            5
#define  SUP_MSG7                            6
#define  SUP_MSG8                            7
#define  SUP_MSG9                            8
#define  SUP_MSG10                           9
#define  SUP_DESCRIPTION                     10
#define  SUP_NAME1                           11
#define  SUP_NAME2                           12
#define  SUP_BILLNUMBER                      13
#define  SUP_ACREAGE                         14
#define  SUP_TRA                             15
#define  SUP_APN                             16
#define  SUP_ADDRESS1                        17
#define  SUP_ADDRESS2                        18
#define  SUP_CHANGEDATE                      19
#define  SUP_CONSTRDATE                      20
#define  SUP_NOTICEDATE                      21
#define  SUP_FISCALYEAR1                     22
#define  SUP_BILLDATE                        23
#define  SUP_ENTRYMONTH                      24
#define  SUP_FISCALYEAR2                     25
#define  SUP_LAND                            26
#define  SUP_LANDNEWBASEYEARVALUE            27
#define  SUP_LANDROLLYEARVALUE               28
#define  SUP_LANDPRIORSUPPLIMENTALVALUE      29
#define  SUP_LANDNETSUPPLIMENTALROLLVALUE    30
#define  SUP_IMPR                            31
#define  SUP_IMPRNEWBASEYEARVALUE            32
#define  SUP_IMPRROLLYEARVALUE               33
#define  SUP_IMPRPRIORSUPPLIMENTALVALUE      34
#define  SUP_IMPRNETSUPPLIMENTALROLLVALUE    35
#define  SUP_EXEM                            36
#define  SUP_EXEMNEWBASEYEARVALUE            37
#define  SUP_TOTAL                           38
#define  SUP_TOTALNEWBASEYEARVALUE           39
#define  SUP_TOTALROLLYEARVALUE              40
#define  SUP_TOTALPRIORSUPPLIMENTALVALUE     41
#define  SUP_TOTALNETSUPPLIMENTALROLLVALUE   42
#define  SUP_DESCRIPTION1                    43
#define  SUP_ASSESSEDVALUE1                  44
#define  SUP_TAXRATE1                        45
#define  SUP_FULLYEARTAX1                    46
#define  SUP_PRORATETAX1                     47
#define  SUP_DESCRIPTION2                    48
#define  SUP_ASSESSEDVALUE2                  49
#define  SUP_TAXRATE2                        50
#define  SUP_FULLYEARTAX2                    51
#define  SUP_PRORATETAX2                     52
#define  SUP_DESCRIPTION3                    53
#define  SUP_ASSESSEDVALUE3                  54
#define  SUP_TAXRATE3                        55
#define  SUP_FULLYEARTAX3                    56
#define  SUP_PRORATETAX3                     57
#define  SUP_DESCRIPTION4                    58
#define  SUP_ASSESSEDVALUE4                  59
#define  SUP_TAXRATE4                        60
#define  SUP_FULLYEARTAX4                    61
#define  SUP_PRORATETAX4                     62
#define  SUP_DESCRIPTION5                    63
#define  SUP_ASSESSEDVALUE5                  64
#define  SUP_TAXRATE5                        65
#define  SUP_FULLYEARTAX5                    66
#define  SUP_PRORATETAX5                     67
#define  SUP_DESCRIPTION6                    68
#define  SUP_ASSESSEDVALUE6                  69
#define  SUP_TAXRATE6                        70
#define  SUP_FULLYEARTAX6                    71
#define  SUP_PRORATETAX6                     72
#define  SUP_DESCRIPTION7                    73
#define  SUP_ASSESSEDVALUE7                  74
#define  SUP_TAXRATE7                        75
#define  SUP_FULLYEARTAX7                    76
#define  SUP_PRORATETAX7                     77
#define  SUP_DESCRIPTION8                    78
#define  SUP_ASSESSEDVALUE8                  79
#define  SUP_TAXRATE8                        80
#define  SUP_FULLYEARTAX8                    81
#define  SUP_PRORATETAX8                     82
#define  SUP_DESCRIPTION9                    83
#define  SUP_ASSESSEDVALUE9                  84
#define  SUP_TAXRATE9                        85
#define  SUP_FULLYEARTAX9                    86
#define  SUP_PRORATETAX9                     87
#define  SUP_DESCRIPTION10                   88
#define  SUP_ASSESSEDVALUE10                 89
#define  SUP_TAXRATE10                       90
#define  SUP_FULLYEARTAX10                   91
#define  SUP_PRORATETAX10                    92
#define  SUP_DESCRIPTION11                   93
#define  SUP_ASSESSEDVALUE11                 94
#define  SUP_TAXRATE11                       95
#define  SUP_FULLYEARTAX11                   96
#define  SUP_PRORATETAX11                    97
#define  SUP_DESCRIPTION12                   98
#define  SUP_ASSESSEDVALUE12                 99
#define  SUP_TAXRATE12                       100
#define  SUP_FULLYEARTAX12                   101
#define  SUP_PRORATETAX12                    102
#define  SUP_DESCRIPTION13                   103
#define  SUP_ASSESSEDVALUE13                 104
#define  SUP_TAXRATE13                       105
#define  SUP_FULLYEARTAX13                   106
#define  SUP_PRORATETAX13                    107
#define  SUP_DESCRIPTION14                   108
#define  SUP_ASSESSEDVALUE14                 109
#define  SUP_TAXRATE14                       110
#define  SUP_FULLYEARTAX14                   111
#define  SUP_PRORATETAX14                    112
#define  SUP_DESCRIPTION15                   113
#define  SUP_ASSESSEDVALUE15                 114
#define  SUP_TAXRATE15                       115
#define  SUP_FULLYEARTAX15                   116
#define  SUP_PRORATETAX15                    117
#define  SUP_PRORATIONFACTOR                 118
#define  SUP_FIRSTINSTDATE                   119
#define  SUP_SECONDINSTDATE                  120
#define  SUP_FIRSTINSTPAIDANDDATE            121
#define  SUP_FIRSTINSTAMOUNTDUE              122
#define  SUP_SECONDINSTPAIDANDDATE           123
#define  SUP_SECONDINSTAMOUNTDUE             124
#define  SUP_TOTALAMOUNTDUE                  125
#define  SUP_NAME1STUB1                      126
#define  SUP_NAME1STUB2                      127
#define  SUP_NAME2STUB1                      128
#define  SUP_NAME2STUB2                      129
#define  SUP_TOTALAMOUNTSTUB1                130
#define  SUP_TOTALAMOUNTSTUB2                131
#define  SUP_BILLNUMBERSTUB1                 132
#define  SUP_BILLNUMBERSTUB2                 133
#define  SUP_APNSTUB1                        134
#define  SUP_APNSTUB2                        135
#define  SUP_APNADDRESSCHGSTUB               136
#define  SUP_AMOUNTSTUB1                     137
#define  SUP_AMOUNTSTUB2                     138
#define  SUP_DATESTUB1                       139
#define  SUP_DATESTUB2                       140
#define  SUP_IFNOTPAIDSTUB1                  141
#define  SUP_IFNOTPAIDSTUB2                  142
#define  SUP_PENALTYSTUB1                    143
#define  SUP_PENALTYSTUB2                    144
#define  SUP_FIRSTINSTCURRENTOUTSTANDING     145
#define  SUP_FIRSTINSTDUEDATEPAID            146
#define  SUP_SECONDINSTCURRENTOUTSTANDING    147
#define  SUP_SECONDINSTDUEDATEPAID           148
#define  SUP_TOTALCURRENTOUTSTANDING         149
#define  SUP_JULYREDEMPTIONAMOUNT            150
#define  SUP_AUGUSTREDEMPTIONAMOUNT          151

#define  SEC_MSG1                            0
#define  SEC_MSG2                            1
#define  SEC_MSG3                            2
#define  SEC_MSG4                            3
#define  SEC_MSG5                            4
#define  SEC_MSG6                            5
#define  SEC_MSG7                            6
#define  SEC_MSG8                            7
#define  SEC_MSG9                            8
#define  SEC_MSG10                           9
#define  SEC_NAME1                           10
#define  SEC_DESCRIPTION                     11
#define  SEC_NAME2                           12
#define  SEC_SITUS1                          13
#define  SEC_STREETADDRESS                   14
#define  SEC_CITYSTATE                       15
#define  SEC_ZIP                             16
#define  SEC_BILLNUMBER                      17
#define  SEC_ACREAGE                         18
#define  SEC_TRA                             19
#define  SEC_APN                             20
#define  SEC_VALUEDESCRIPTION1               21
#define  SEC_VALUEAMOUNT1                    22
#define  SEC_VALUEDESCRIPTION2               23
#define  SEC_VALUEAMOUNT2                    24
#define  SEC_VALUEDESCRIPTION3               25
#define  SEC_VALUEAMOUNT3                    26
#define  SEC_VALUEDESCRIPTION4               27
#define  SEC_VALUEAMOUNT4                    28
#define  SEC_VALUEDESCRIPTION5               29
#define  SEC_VALUEAMOUNT5                    30
#define  SEC_VALUEDESCRIPTION6               31
#define  SEC_VALUEAMOUNT6                    32
#define  SEC_AGENCYPHONE1                    33
#define  SEC_AGENCYDESCRIPTION1              34
#define  SEC_VALUEAMOUNT7                    35
#define  SEC_TAXRATE                         36
#define  SEC_COUNTYTAX                       37
#define  SEC_SUBBOX1                         38
#define  SEC_AGENCYPHONE2                    39
#define  SEC_AGENCYDESCRIPTION2              40
#define  SEC_AGENCYVALUE2                    41
#define  SEC_AGENCYTAXRATE2                  42
#define  SEC_AGENCYTAX2                      43
#define  SEC_AGENCYPHONE3                    44
#define  SEC_AGENCYDESCRIPTION3              45
#define  SEC_AGENCYVALUE3                    46
#define  SEC_AGENCYTAXRATE3                  47
#define  SEC_AGENCYTAX3                      48
#define  SEC_AGENCYPHONE4                    49
#define  SEC_AGENCYDESCRIPTION4              50
#define  SEC_AGENCYVALUE4                    51
#define  SEC_AGENCYTAXRATE4                  52
#define  SEC_AGENCYTAX4                      53
#define  SEC_AGENCYPHONE5                    54
#define  SEC_AGENCYDESCRIPTION5              55
#define  SEC_AGENCYVALUE5                    56
#define  SEC_AGENCYTAXRATE5                  57
#define  SEC_AGENCYTAX5                      58
#define  SEC_AGENCYPHONE6                    59
#define  SEC_AGENCYDESCRIPTION6              60
#define  SEC_AGENCYVALUE6                    61
#define  SEC_AGENCYTAXRATE6                  62
#define  SEC_AGENCYTAX6                      63
#define  SEC_AGENCYPHONE7                    64
#define  SEC_AGENCYDESCRIPTION7              65
#define  SEC_AGENCYVALUE7                    66
#define  SEC_AGENCYTAXRATE7                  67
#define  SEC_AGENCYTAX7                      68
#define  SEC_AGENCYPHONE8                    69
#define  SEC_AGENCYDESCRIPTION8              70
#define  SEC_AGENCYVALUE8                    71
#define  SEC_AGENCYTAXRATE8                  72
#define  SEC_AGENCYTAX8                      73
#define  SEC_AGENCYPHONE9                    74
#define  SEC_AGENCYDESCRIPTION9              75
#define  SEC_AGENCYVALUE9                    76
#define  SEC_AGENCYTAXRATE9                  77
#define  SEC_AGENCYTAX9                      78
#define  SEC_AGENCYPHONE10                   79
#define  SEC_AGENCYDESCRIPTION10             80
#define  SEC_AGENCYVALUE10                   81
#define  SEC_AGENCYTAXRATE10                 82
#define  SEC_AGENCYTAX10                     83
#define  SEC_AGENCYPHONE11                   84
#define  SEC_AGENCYDESCRIPTION11             85
#define  SEC_AGENCYVALUE11                   86
#define  SEC_AGENCYTAXRATE11                 87
#define  SEC_AGENCYTAX11                     88
#define  SEC_AGENCYPHONE12                   89
#define  SEC_AGENCYDESCRIPTION12             90
#define  SEC_AGENCYVALUE12                   91
#define  SEC_AGENCYTAXRATE12                 92
#define  SEC_AGENCYTAX12                     93
#define  SEC_AGENCYPHONE13                   94
#define  SEC_AGENCYDESCRIPTION13             95
#define  SEC_AGENCYVALUE13                   96
#define  SEC_AGENCYTAXRATE13                 97
#define  SEC_AGENCYTAX13                     98
#define  SEC_AGENCYPHONE14                   99
#define  SEC_AGENCYDESCRIPTION14             100
#define  SEC_AGENCYVALUE14                   101
#define  SEC_AGENCYTAXRATE14                 102
#define  SEC_AGENCYTAX14                     103
#define  SEC_AGENCYPHONE15                   104
#define  SEC_AGENCYDESCRIPTION15             105
#define  SEC_AGENCYVALUE15                   106
#define  SEC_AGENCYTAXRATE15                 107
#define  SEC_AGENCYTAX15                     108
#define  SEC_AGENCYPHONE16                   109
#define  SEC_AGENCYDESCRIPTION16             110
#define  SEC_AGENCYVALUE16                   111
#define  SEC_AGENCYTAXRATE16                 112
#define  SEC_AGENCYTAX16                     113
#define  SEC_SUBBOX2                         114
#define  SEC_AGENCYPHONE17                   115
#define  SEC_AGENCYDESCRIPTION17             116
#define  SEC_AGENCYAMOUNT17                  117
#define  SEC_AGENCYPHONE18                   118
#define  SEC_AGENCYDESCRIPTION18             119
#define  SEC_AGENCYAMOUNT18                  120
#define  SEC_AGENCYPHONE19                   121
#define  SEC_AGENCYDESCRIPTION19             122
#define  SEC_AGENCYAMOUNT19                  123
#define  SEC_AGENCYPHONE20                   124
#define  SEC_AGENCYDESCRIPTION20             125
#define  SEC_AGENCYAMOUNT20                  126
#define  SEC_AGENCYPHONE21                   127
#define  SEC_AGENCYDESCRIPTION21             128
#define  SEC_AGENCYAMOUNT21                  129
#define  SEC_AGENCYPHONE22                   130
#define  SEC_AGENCYDESCRIPTION22             131
#define  SEC_AGENCYAMOUNT22                  132
#define  SEC_AGENCYPHONE23                   133
#define  SEC_AGENCYDESCRIPTION23             134
#define  SEC_AGENCYAMOUNT23                  135
#define  SEC_AGENCYPHONE24                   136
#define  SEC_AGENCYDESCRIPTION24             137
#define  SEC_AGENCYAMOUNT24                  138
#define  SEC_AGENCYPHONE25                   139
#define  SEC_AGENCYDESCRIPTION25             140
#define  SEC_AGENCYAMOUNT25                  141
#define  SEC_AGENCYPHONE26                   142
#define  SEC_AGENCYDESCRIPTION26             143
#define  SEC_AGENCYAMOUNT26                  144
#define  SEC_AGENCYPHONE27                   145
#define  SEC_AGENCYDESCRIPTION27             146
#define  SEC_AGENCYAMOUNT27                  147
#define  SEC_AGENCYPHONE28                   148
#define  SEC_AGENCYDESCRIPTION28             149
#define  SEC_AGENCYAMOUNT28                  150
#define  SEC_AGENCYPHONE29                   151
#define  SEC_AGENCYDESCRIPTION29             152
#define  SEC_AGENCYAMOUNT29                  153
#define  SEC_AGENCYPHONE30                   154
#define  SEC_AGENCYDESCRIPTION30             155
#define  SEC_AGENCYAMOUNT30                  156
#define  SEC_AGENCYPHONE31                   157
#define  SEC_AGENCYDESCRIPTION31             158
#define  SEC_AGENCYAMOUNT31                  159
#define  SEC_SUBBOX3                         160
#define  SEC_DUEBYDATE1                      161
#define  SEC_DUEBYAMOUNT1                    162
#define  SEC_DUEBYDATE2                      163
#define  SEC_DUEBYAMOUNT2                    164
#define  SEC_TOTALDUE                        165
#define  SEC_PAIDDATE1                       166
#define  SEC_PAIDDATE2                       167
#define  SEC_NAME1STUB1                      168
#define  SEC_NAME1STUB2                      169
#define  SEC_NAME2STUB1                      170
#define  SEC_NAME2STUB2                      171
#define  SEC_TAXAMTCORTACSTUB1               172
#define  SEC_TAXAMTCORTACSTUB2               173
#define  SEC_BILLNUMBERSTUB1                 174
#define  SEC_BILLNUMBERSTUB2                 175
#define  SEC_APNSTUB1                        176
#define  SEC_APNSTUB2                        177
#define  SEC_APNCHANGESTUB                   178
#define  SEC_NAME1ADDRESSCHGSTUB             179
#define  SEC_NAME2ADDRESSCHGSTUB             180
#define  SEC_DUEDATE1                        181
#define  SEC_DUEAMOUNT1                      182
#define  SEC_DUEDATE2                        183
#define  SEC_DUEAMOUNT2                      184
#define  SEC_IFNOTPAIDBY1                    185
#define  SEC_IFNOTPAIDBY2                    186
#define  SEC_PENALTY1                        187
#define  SEC_PENALTY2                        188
#define  SEC_BILLDATE                        189
#define  SEC_FIRSTINSTCURRENTOUTSTANDING     190
#define  SEC_FIRSTINSTDUEDATEORPAID          191
#define  SEC_SECONDINSTCURRENTOUTSTANDING    192
#define  SEC_SECONDINSTDUEDATEORPAID         193
#define  SEC_TOTALCURRENTOUTSTANDING         194
#define  SEC_JULYREDEMPTIONAMOUNT            195
#define  SEC_AUGUSTREDEMPTIONAMOUNT          196

// SRDATA.TXT
#define  SR_ACCTNUMBER                       0
#define  SR_APN                              1
#define  SR_SITUS                            2
#define  SR_LEGAL                            3
#define  SR_NAME1                            4
#define  SR_NAME2                            5
#define  SR_CONSTDATE                        6
#define  SR_OCDATE                           7
#define  SR_ROLLYEAR                         8        // Fiscal year the bill is enrolled and billed
#define  SR_TAXRATEYEAR                      9        // Year the bill should have been enrolled  usually this is same as Roll Year
                                                      // If different from Roll Year above, it would indicate an escape assessment 	(additional bill) or 
                                                      // a supplemental bill and is taxed at the tax rate in effect for that year.  Ex.  00 = 2000-01.  
                                                      // A supplemental bill will also have a Y in the supplemental flag field as below.

#define  SR_BILLDATE                        10        // If the bill date is between November and April, the bill is either:
                                                      // 1. supplemental (would also have a supplemental flag as below)
                                                      // 2. corrected bill in the current year (the roll year and tax year would be the same)
                                                      // 3. escape assessment for a prior year (roll year and tax rate year would be different) 
#define  SR_TAXAMT1                          11
#define  SR_TAXAMT2                          12
#define  SR_DUEDATE1                         13
#define  SR_DUEDATE2                         14
#define  SR_PAIDDATE1                        15
#define  SR_PAIDDATE2                        16
#define  SR_DEFAUTDATE                       17
#define  SR_PRCLTYPE                         18
#define  SR_ROLLCORR                         19
#define  SR_PRORATION                        20
#define  SR_PRORATED                         21

// SABSDATA.TXT
#define  ABS_ACCTNUMBER                      0
#define  ABS_APN                             1
#define  ABS_LEGAL                           2
#define  ABS_NAME1                           3
#define  ABS_NAME2                           4
#define  ABS_BILLDATE                        5        // If the bill date is between November and April, the bill is either:
                                                      // 1. supplemental (would also have a supplemental flag as below)
                                                      // 2. corrected bill in the current year (the roll year and tax year would be the same)
                                                      // 3. escape assessment for a prior year (roll year and tax rate year would be different) 

#define  ABS_ROLLYEAR                        6        // Fiscal year the bill is enrolled and billed   ex.  02 = 2002-03
#define  ABS_TAXRATEYEAR                     7        // Year the bill should have been enrolled  usually this is same as Roll Year
                                                      // If different from Roll Year above, it would indicate an escape assessment 	(additional bill) or 
                                                      // a supplemental bill and is taxed at the tax rate in effect for that year.  Ex.  00 = 2000-01.  
                                                      // A supplemental bill will also have a Y in the supplemental flag field as below.

#define  ABS_SUPPLIMENTALFLAG                8        // 'Y' indicates that the bill was a delinquent supplemental
#define  ABS_INSTALLMENTDEL                  9        // 2 = Second installment was delinquent, 3 = both installments were delinquent
#define  ABS_BASETAX                         10       // Total of delinquent tax and special assessments
#define  ABS_INTERESTRATE                    11
#define  ABS_PARCELTYPE                      12       // 10 = plain parcel with no installment plan
			                                             // 60 = bankruptcy filed
			                                             // 70 = Tax Collectors Power to Sell recorded eligible for sale
			                                             // 80 = Installment plan in good standing
			                                             // 81 = defaulted installment plan or partial payments applied
			                                             // 89 = Paid in full with multiple payments or by installment plan
			                                             // 99 = Paid in full with one payment

#define  ABS_DEFAULTDATE                     13       // Date the first year of taxes became defaulted
#define  ABS_PTSEFFECTIVEDATE                14       // Date the parcel became eligible for a tax sale
#define  ABS_REDEMPTIONDATE                  15       // Date payment in full was posted to our file
#define  ABS_REDEMPTIONJULY1                 16
#define  ABS_REDEMPTIONAUGUST1               17
#define  ABS_REDEMPTIONSEPTEMBER             18
#define  ABS_REDEMPTIONOCTOBER               19
#define  ABS_REDEMPTIONNOVEMBER              20
#define  ABS_REDEMPTIONDECEMBER              21
#define  ABS_REDEMPTIONJANUARY               22
#define  ABS_REDEMPTIONFEBRUARY              23
#define  ABS_REDEMPTIONMARCH                 24
#define  ABS_REDEMPTIONAPRIL                 25
#define  ABS_REDEMPTIONMAY                   26
#define  ABS_REDEMPTIONJUNE                  27
#define  ABS_REDEMPTIONJULY2                 28
#define  ABS_REDEMPTIONAUGUST2               29

// Cortacfile
#define TOFF_APN               1-1
#define TOFF_BILLNO            11-1
#define TOFF_TOTALTAXAMT       17-1
#define TOFF_FIRSTTAXAMT       27-1
#define TOFF_SECTAXAMT         37-1
#define TOFF_SOLDTOSTATEDATE   47-1
#define TOFF_DELQSTATUS        55-1
#define TOFF_FIRSTPAYMENTDATE  57-1
#define TOFF_SECPAYMENTDATE    63-1

#define TSIZ_APN               10
#define TSIZ_BILLNO            6
#define TSIZ_TOTALTAXAMT       10
#define TSIZ_FIRSTTAXAMT       10
#define TSIZ_SECTAXAMT         10
#define TSIZ_SOLDTOSTATEDATE   8
#define TSIZ_DELQSTATUS        2
#define TSIZ_FIRSTPAYMENTDATE  6
#define TSIZ_SECPAYMENTDATE    6

typedef struct _tCortac
{  // 70-bytes
   char  Apn[TSIZ_APN];
   char  BillNo[TSIZ_BILLNO];
   char  TotalTaxAmt[TSIZ_TOTALTAXAMT];
   char  FirstTaxAmt[TSIZ_TOTALTAXAMT];
   char  SecTaxAmt[TSIZ_TOTALTAXAMT];   
   char  SoldToStateDate[TSIZ_SOLDTOSTATEDATE];    // MMDDYYYY
   char  DelqStatus[TSIZ_DELQSTATUS];              // D, J, S, Y, E1, E2, E3, E4, E5
   char  FirstPaymentDate[TSIZ_FIRSTPAYMENTDATE];  // MMDDYY
   char  SecPaymentDate[TSIZ_FIRSTPAYMENTDATE];
   char  CrLf[2];
} CORTAC;

#define TSIZ_ACODE                     3
#define TSIZ_ANAME                     21
#define TSIZ_TAXRATE                   9
#define TSIZ_TAXAMT                    12
#define TSIZ_TRA                       6
#define TSIZ_PRCLTYPE                  2
#define TSIZ_SITUS                     30
#define TSIZ_S_STRNUM                  8
#define TSIZ_S_STRNAME                 22
#define TSIZ_S_CITY                    22
#define TSIZ_S_STATE                   3
#define TSIZ_S_ZIP                     5
#define TSIZ_ACREAGE                   10
#define TSIZ_AGENCIES                  15    // Number of agencies
#define TSIZ_ASSMNTVAL                 10
#define TSIZ_OWNERNAME                 29
#define TSIZ_M_ADDRESS                 29
#define TSIZ_M_ZIP                     10


typedef struct _tAgency
{
   char TaxCode[TSIZ_ACODE];           // 293
   char delim;                         // 296
   char Agency[TSIZ_ANAME];            // 297
   char TaxRate[TSIZ_TAXRATE];         // 318
   char TaxAmt[TSIZ_TAXAMT];           // 327
} AGENCY;

typedef struct _tGFGIS
{
   char  Apn[TSIZ_APN];                //    1
   char  TRA[TSIZ_TRA];                //   11
   char  BillNum[TSIZ_BILLNO];         //   17
   char  ParcelType[TSIZ_PRCLTYPE];    //   23
   char  Name1[TSIZ_OWNERNAME];        //   25
   char  Name2[TSIZ_OWNERNAME];        //   54
   char  M_Addr[TSIZ_M_ADDRESS];       //   83
   char  M_CitySt[TSIZ_M_ADDRESS];     //  112
   char  M_Zip[TSIZ_M_ZIP];            //  141
   char  S_StrNum[TSIZ_S_STRNUM];      //  151
   char  S_StrName[TSIZ_S_STRNAME];    //  159
   char  S_City[TSIZ_S_CITY];          //  181
   char  S_State[TSIZ_S_STATE];        //  203
   char  S_Zip[TSIZ_S_ZIP];            //  206
   char  Land[TSIZ_ASSMNTVAL];         //  211
   char  Impr[TSIZ_ASSMNTVAL];         //  221
   char  PPVal[TSIZ_ASSMNTVAL];        //  231
   char  Fixtr[TSIZ_ASSMNTVAL];        //  241
   char  ExeVal[TSIZ_ASSMNTVAL];       //  251
   char  NetVal[TSIZ_ASSMNTVAL];       //  261
   char  Acreage[TSIZ_ACREAGE];        //  271
   char  TotalTax[TSIZ_TAXAMT];        //  281
   AGENCY  asAgency[TSIZ_AGENCIES];    //  293
} GFGIS;
