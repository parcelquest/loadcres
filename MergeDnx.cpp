/*******************************************************************************
 *
 * DNX only allows us to use following fields in PQ4:
 *    - APN
 *    - TRA
 *    - Owners
 *    - Mail Addr
 *    - Situs Addr
 *    - Asmt Year
 *    - Doc#, Doc date
 *    - Acreage
 *    - Land, Impr values
 *    - Other values
 *    - Exemption value
 *    - 
 *
 * 11/27/2006 1.8.4     TCB Add OFF_S_ADDR_D and OFF_S_CTY_ST_D
 * 04/24/2008 1.10.5    Create loadDnx() and fix Dnx_CreateR01(). Also fix Dnx_MergeAdr()
 *                      to process situs and copy it to mail when mail is not available.
 *                      The update is now creating R01 from roll file and then merge 
 *                      lien data from extract file. Use HOE from current roll, not lien file.
 * 04/24/2008 1.10.5.2  Comment out code that copy situs to mail when mail is blank pending approval.
 * 06/10/2008 1.12.0    Add Dnx_ExtrSale() for future use.  Currently DNX doesn't want us
 *                      to publish sale price.
 * 07/01/2008 8.1.1     Add function Dnx_Load_LDR(), Dnx_ExtrLien() and Dnx_CreateLienRec()
 *                      Use Dnx_Load_LDR() to load LDR data only, no update.
 * 08/08/2008 8.2.1     Fix Dnx_LoadRoll() not to break when Dnx_CreateR01() return -1;
 * 01/21/2009 8.5.3     Standardize TRA as 6-digit number
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 06/22/2009 9.1.0     Remove Dnx_ExtrLien() and use Cres_ExtrLien() instead.
 * 08/04/2009 9.1.3     Populate other values.
 * 08/04/2009 9.1.4     Add DNX_Create_Ldr to accept new LDR input format 350-byte record.
 * 06/29/2010 10.0.0    DNX returns to old format 900-byte record.
 * 07/22/2011 11.1.3    Add S_HSENO.  Modify Dnx_MergeAdr() to clean up situs addr.
 * 10/19/2011 11.2.8    Modify Dnx_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      This is to create sale history only, not to populate PQ products.
 *                      Add -Xsi option, but cum sale file is only used for report, not to update R01.
 * 10/28/2011 11.2.8.2  Fix Dnx_ExtrSale() to ignore tax amt > 9999999.
 * 11/16/2012 12.3.0    Add Dnx_LoadRoll350() and Dnx_ExtrSale_R350() to support 350-byte roll format.
 * 03/01/2013 12.6.1    Automatically process different file versions based on record length.
 * 05/07/2013 12.6.3    Fix multiple owner names issue by adding Name2.
 * 05/08/2013 12.6.3.1  Add Dnx_LoadUpdt() to replace Dnx_LoadRoll350().  If input file has no situs,
 *                      bypass updating it.
 * 08/06/2013 13.2.5    Bug fixed in Dnx_R350_R01(). Modify Dnx_LoadUpdt() and check for return code 
 *                      from Dnx_R350_R01() before writing to file.
 * 09/29/2013 13.3.0    Modify Dnx_MergeOwner() and add Dnx_CleanName() to update Vesting.
 *                      Remove comma from owner name
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 02/13/2016 15.6.0    Modify Dnx_MergeOwner()to remove Name2 if it's the same as Name1.
 * 03/09/2016 15.9.1    Loading Tax data
 * 06/29/2016 16.0.0    Add comments.
 *
 *******************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Pq.h"
#include "doSort.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "MergeDnx.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "doSort.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "Tax.h"

static bool bHasSitus;

/********************************* Dnx_Fmt1Doc *******************************
 *
 *   1)  2042002  20678 = 0678 & 20020204
 *   2) 02092005 051040 = 1040 & 20050209
 *   3) 12312001  16636 = 6636 & 20011231
 *   4) 11231999 515416 = 515-416 & 19991123
 *   5) 4251994 423075  = 423-075 & 19940425
 *   6) 02092005051040  = 1040 & 20050209
 *   7) 10312003 904-688= 904-688 & 20031031
 *   8)  8102003 904573 = 904-573 & 20030710
 *
 *****************************************************************************/

void Dnx_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp, iDoc;
   char  *pTmp, *pDoc, acTmp[32];

   // Initialize output
   memset(pOutDoc, ' ', SIZ_SALE1_DOC);
   memset(pOutDate, ' ', SIZ_SALE1_DT);

   memcpy(acTmp, pRecDoc, 16);
   acTmp[16] = 0;
   pTmp = acTmp;

   if (*(pTmp+1) > ' ')
   {
      if (*pTmp == ' ')
         *pTmp = '0';
      pDoc = strchr(pTmp, ' ');
      if (pDoc)
         *pDoc++ = 0;
      iTmp = strlen(pTmp);
      if (iTmp == 8)
         sprintf(pOutDate, "%.4s%.4s", pTmp+4, pTmp);
      else if (iTmp == 7)
         sprintf(pOutDate, "%.4s0%.3s", pTmp+3, pTmp);
      else if (iTmp == 9)
         sprintf(pOutDate, "%.4s%.4s", pTmp+5, pTmp);
      else if (iTmp == 14)
      {
         sprintf(pOutDate, "%.4s%.4s", pTmp+4, pTmp);
         pDoc = pTmp+8;
      }

      iTmp = atoin(pOutDate, 4);
      iDoc = atol(pDoc);
      if (iDoc > 0)
      {
         if (pTmp = strchr(pDoc, '-'))
         {
            strcpy(pOutDoc, pDoc);
         } else
         {
            // This statement needs to be changed before year 2020
            if (iTmp >= 2000 && *pDoc <= '1')
               sprintf(pOutDoc, "%.4s", pDoc+2);
            else
               sprintf(pOutDoc, "%.3s-%.3s", pDoc, pDoc+3);
         }
         blankPad(pOutDoc, SIZ_SALE1_DOC);
      }
   }
}

void Dnx_FmtRecDoc_Lien(char *pOutbuf, char *pRecDoc1)
{
   char  acDoc[64], acDate[16], *pTmp;
   long  lDate;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "11212139", 8))
   //   pTmp = pRecDoc1;
#endif

   acDoc[0] = 0;
   acDate[0] = 0;

   pTmp = pRecDoc1;
   Dnx_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE1) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

   // Update last recording date
   lDate = atol(acDate);
   if (lDate > lLastRecDate && lDate < lToday)
      lLastRecDate = lDate;

}

void Dnx_FmtRecDoc(char *pOutbuf, char *pRecDoc1)
{
   char  acDoc[64], acDate[16], *pTmp;
   long  lDate;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "11212139", 8))
   //   pTmp = pRecDoc1;
#endif

   pTmp = pRecDoc1;
   Dnx_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE1) = ' ';           // Assessor-Recorder code

   //lTmp = atoin(acDate, 8);   
   //if (lTmp > lToday)                          // Check for valid date
   //{
   //   LogMsg0("*** WARNING: Bad last recording date %d at APN = %.14s", lTmp, pOutbuf);
   //   memset(acDate, ' ', SIZ_SALE1_DT);
   //}

   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

   // Update last recording date
   lDate = atol(acDate);
   if (lDate > lLastRecDate && lDate < lToday)
      lLastRecDate = lDate;

   // Advance to Rec #2
   pTmp += 16;
   Dnx_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE2) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);


   // Advance to Rec #3
   pTmp += 16;
   Dnx_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE3) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
}

/******************************** Dnx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 * Notes:
 *    - $ = continuation of Name1
 *    - % = Name2
 *
 *****************************************************************************/

void Dnx_MergeOwner1(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acName1[64];
   char  *pTmp, *pTmp1, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   memset(pOutbuf+OFF_XCAREOF, ' ', SIZ_XCAREOF);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "10102106", 8))
   //   iTmp = 0;
#endif

   // Initialize
   strncpy(acOwners, pNames, DNXSIZ_NAME_1+DNXSIZ_NAME_2);
   acOwners[CRESIZ_NAME_1+CRESIZ_NAME_2] = 0;
   // Point to name2
   pName2 = &acOwners[CRESIZ_NAME_1];

   // Check for name continuation
   if (*pName2 == '$')
   {
      if (!memcmp(pName2, "$ ETAL", 6) || !memcmp(pName2, "$ETAL", 5))
      {
         *pName2 = ' ';
         pName2 = NULL;
      } else if (!memcmp(pName2, "$ EATL", 6))
      {
         strcpy(pName2, " ETAL");
         pName2 = NULL;
      } else
      {
         *pName2 = 0;
         pName2 += 2;
      }
   } else if (*pName2 == '%')                // Check for name2
   {
      *pName2 = 0;
      pName2 += 2;
   } else if (!memcmp(pName2, "C/O", 3) )    // Check for Care of
   {
      /* 8/5/2013
      int iTmp1;
      if (*(pName2+4) == ' ')
         iTmp = 5;
      else
         iTmp = 4;

      iTmp1 = strlen(pName2+iTmp);
      if (iTmp1 > SIZ_CARE_OF)
         iTmp1 = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pName2+iTmp, iTmp1);
      */
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
      pName2 = NULL;
   } else if (!memcmp(pName2, "ETAL", 4) )   // Check for ETAL
   {
      pName2 = NULL;
   } else if (*pName2 > ' ' && *(pName2-1) == ' ' && *(pName2-2) == ' ')
   {
      // Separate Name1 and Name2
      *(pName2-1) = 0;
   } else if ((*pName2 > ' ' && *(pName2-1) == ' ' && *(pName2-2) > ' ') ||
      (*pName2 == ' ' && (*(pName2-1) > ' ' || *(pName2+1) == ' ') ) )
   {
      // Name1 continuation
      pName2 = NULL;
   } else
      pName2 = NULL;


   // Translate OR into AND
   if (pTmp = strstr(acOwners, " OR "))
   {
      *(pTmp+1) = '&';
      *(pTmp+2) = ' ';
   }

   remCharEx(acOwners, ",.'");
   blankRem(acOwners);
   pName1 = strcpy(acName1, acOwners);

   // Prepare Name1 for SwapName
   if (pTmp = strstr(pName1, "TRUSTEE"))
   {      
      if (!memcmp(pTmp-3, "CO-", 3))         // Remove CO-TRUSTEE
         *(pTmp-3) = 0;
      else
         *pTmp = 0;                          // Remove TRUSTEE
   }
   
   // Drop Estate of
   if ((pTmp = strstr(pName1, " ESTATE OF")) || (pTmp = strstr(pName1, " ETAL")))
   {
      *pTmp = 0;
   }

   // Keep last word in case written over into Name2
   if (pName2 && *pName2)
   {
      if (pTmp = strstr(pName2, "P.O."))
      {
         pName2 = NULL;
      } else if (*(pName2-1) > ' ' && *pName2 > ' ')
      {
         pTmp = pName2;

         // Word continuation
         while (*pTmp >= '0')
            pTmp++;

         if (*pTmp == ',')
            *pName2 = 0;
         else if (*(pTmp+2) == '.')
            *(pTmp+2) = 0;
         else
            *pTmp = 0;
      } else if (*(pName2+1) == '.')
      {
         // S.R. METHODIST CHURCH
         //*(pTmp+1) = 0;
         //iTmp = 0;
      } else  if (*pName2 > ' ')
      {
         remCharEx(pName2, ",.'");
      }
   }

   // Remove multiple spaces
   int iCommaCnt=0;
   pTmp = pName1;
   iTmp = 0;
   while (*pTmp)
   {
      if (*pTmp == ',')
      {
         iCommaCnt++;
         if (iCommaCnt > 2)            // Avoid bad name entry
         {
            *pTmp = 0;
            break;
         }
         *pTmp = ' ';
      }

      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Save data to append later
   pTmp = strstr(acTmp, " FAMILY TR");
   if (pTmp)
   {
      *pTmp = 0;
   } else
   {
      pTmp1 = strrchr(acTmp, ' ');
      if (pTmp1)
      {
         if (!memcmp(pTmp1, " LIVIN", 6))
         {
            *pTmp1 = 0;
         }
      } 
   }

   // Now parse owners
   splitOwner(acTmp, &myOwner);
   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   memcpy(pOutbuf+OFF_NAME1, acOwners, strlen(acOwners));
   if (pName2 && *pName2 > ' ')
   {
      myTrim(pName2, SIZ_NAME2);
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
   }
}

/******************************** Dnx_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Dnx_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Dnx_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Dnx_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acName1[64], acName2[64], acVesting1[8], acVesting2[8];
   char  *pTmp, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, DNXSIZ_NAME_1);
   blankRem(acName1, DNXSIZ_NAME_1);
   pName1 = acName1;
   acVesting1[0] = 0;

   // Point to name2
   memcpy(acName2, pNames+DNXSIZ_NAME_1, DNXSIZ_NAME_2);
   blankRem(acName2, DNXSIZ_NAME_2);
   pName2 = acName2;

   // Remove Name2 if it's the same as Name1
   if (!strcmp(pName1, pName2) || !strcmp(pName1, pName2+2))
      *pName2 = 0;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "11074101", 8))
   //   iTmp = 0;
#endif

   // Remove dot and single quote
   remCharEx(acName1, ".'");
   // Replace '/' with '&' in Name1
   if (pTmp = strchr(acName1, '/'))
   {
      *pTmp++ = 0;
      sprintf(acOwners, "%s & %s", acName1, pTmp);
      strcpy(acName1, acOwners);
   } 

   blankRem((char *)&acName1[0]);
   strcpy(acOwners, acName1);
   remCharEx(acName2, ".'");
   blankRem((char *)&acName2[0]);
   strcpy(acOwners, acName1);
   acVesting1[0] = acVesting2[0] = 0;
   findVesting(acName1, acVesting1);

   // Check owner2 
   if (!memcmp(pName2, "ETAL", 4) ||
              !memcmp(pName2, "$ETAL", 5) ||
              !memcmp(pName2, "$ EATL", 6) ||
              !memcmp(pName2, "$ ETAL", 6))
   {
      memcpy(pOutbuf+OFF_VEST, "EA", 2);
      *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      *pName2 = 0;
   } else if (*pName2 == '$')
   {
      // Process as two names
      pName2 += 2;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      // Merge if same last name and individual, not corp
      pName2 += 2;
      pTmp = strchr(pName1, ',');
      if (pTmp)
      {
         iTmp = pTmp - pName1;
         if (!memcmp(pName1, pName2, iTmp) &&    // Same last name
             !strchr(pName1, '&') &&             // Name1 is not a combo
             acVesting1[0] < ' ')                // and not a corp
         {
            // Combine Name1 & Name2 since they have the same last name
            // Make sure Name2 is individual name
            pTmp = findVesting(pName2, acVesting2);
            if (!pTmp)
            {
               MergeName(pName1, pName2, acOwners);
               pName2 = NULL;
            }
         }
      }
   } 

   if (pName2 && *pName2 > ' ')
   {
      iTmp = remChar(pName2, ',');
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      if (acVesting1[0] < '0' && acVesting2[0] < '0')
         pTmp = findVesting(pName2, acVesting2);
   }

   // Populate Name1
   if (*(pOutbuf+OFF_ETAL_FLG) == 'Y')
      sprintf(acName1, "%s ETAL", acOwners);
   else
      strcpy(acName1, acOwners);

   iTmp = remChar(acName1, ',');
   vmemcpy(pOutbuf+OFF_NAME1, acName1, SIZ_NAME1, iTmp);

   // Cleanup Name1 
   iTmp = Dnx_CleanName(acOwners, acTmp, acVesting1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acOwners, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));

      // Check EtAl
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   } else if (acVesting2[0] > ' ')
   {
      memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));

      // Check EtAl
      if (!memcmp(acVesting2, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // Now parse owners
   splitOwner(acOwners, &myOwner, 3);
   if (acVesting1[0] > ' ' && isVestChk(acVesting1))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

/********************************* Dnx_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Dnx_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   ADR_REC  sMailAdr, sSitusAdr;

   char     *pTmp, *pAddr1, acTmp[256], acAddr1[64], acAddr2[64], acUnit[8];
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
      blankRem(acAddr1, CRESIZ_ADDR_1);
      pAddr1 = acAddr1;

      iTmp = 0;
      if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
      {
         if (*pAddr1 == '%')
            pAddr1 += 2;
         else
            pAddr1 += 4;

         // Check for C/O name
         pTmp = strchr(pAddr1, ',');
         if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
         {
            *pTmp = 0;
            pAddr1 = pTmp + 1;
            if (*pAddr1 == ' ') 
               pAddr1++;
            iTmp = strlen(pAddr1);
            if (iTmp > SIZ_CARE_OF)
               iTmp = SIZ_CARE_OF;
            memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
         }
      }

      // Start processing
      memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
      iTmp = atoin(pRec->M_Zip, 5);
      if (iTmp > 10001)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
      } else
      {
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
      }
   
      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseAdr1(&sMailAdr, pAddr1);

      memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
      acAddr2[CRESIZ_ADDR_2] = 0;
      parseAdr2(&sMailAdr, myTrim(acAddr2));

      if (sMailAdr.lStrNum > 0)
         sprintf(acTmp, "%*d", SIZ_M_STRNUM, sMailAdr.lStrNum);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      blankPad(sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      blankPad(sMailAdr.State, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   }

   // Situs
   if (pRec->S_StrNo[0] >= '1' && pRec->S_StrName[0] >= '1')
   {
      removeSitus(pOutbuf);
      acUnit[0] = 0;

      // Check house number
      // 4600/10, 325/335, 2671&61, 555/65/
      // 1234 #A
      memcpy(acTmp, pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
      acTmp[CRESIZ_SITUS_STREETNO] = 0;
      if ((pTmp = strrchr(acTmp, '/')) && !isdigit(*(pTmp+1)))
         *pTmp = 0;     // Remove extra '/'

      if (pTmp = strchr(acTmp, ' '))
      {
         *pTmp++ = 0;
         if (*pTmp == '#')
            strcpy(acUnit, pTmp);
      }

      // Save original S_HSENO
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));

      // Format S_ADDR_D
      quoteRem(pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
      memcpy(acAddr2, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
      blankRem(acAddr2, CRESIZ_SITUS_STREETNAME);
      remChar(acAddr2, '.');
      iTmp = sprintf(acAddr1, "%s %s %s", acTmp, acAddr2, acUnit);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // Format StrNum for range search
      iStrNo = atol(acTmp);
      iTmp = sprintf(acTmp, "%*d", SIZ_S_STRNUM, iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      
      // Parse line 1
      parseAdr1S(&sSitusAdr, acAddr2);

      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
      else if (acUnit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, acUnit, strlen(acUnit));

      iTmp = 0;
      // Check City and Zip
      if (pRec->S_City[0] > ' ')
      {
         memcpy(acTmp, pRec->S_City, CRESIZ_SITUS_CITY);
         acTmp[CRESIZ_SITUS_CITY] = 0;
         parseAdr2(&sSitusAdr, myTrim(acTmp));

         // Get city code
         City2Code(sSitusAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
            memcpy(pOutbuf+OFF_S_ST, "CA", 2);

            iTmp = sprintf(acAddr2, "%s CA", sSitusAdr.City);
         }
         memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, strlen(sSitusAdr.Zip));
      } else if (!strcmp(sMailAdr.strName, sSitusAdr.strName) && sMailAdr.City[0] > ' ')
      {
         // Since no situs city avail., we use mailing is street name matched
         City2Code(sMailAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
            memcpy(pOutbuf+OFF_S_ST, "CA", 2);
            iTmp = sprintf(acAddr2, "%s CA", sMailAdr.City);
         }
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      }

      if (iTmp > 0)
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   }

   // Populate mailaddr from situs if situs avail and mail is not - pending decision
   //if (*(pOutbuf+OFF_M_STREET) == ' ' && *(pOutbuf+OFF_S_STREET) > '0')
   //   CopySitusToMailing(pOutbuf);
}

/******************************** Dnx_MergeAdr_R350 **************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Dnx_MergeAdr_R350(char *pOutbuf, char *pRollRec)
{
   ROLL_350 *pRec;
   ADR_REC  sMailAdr, sSitusAdr;

   char     *pTmp, *pAddr1, acTmp[256], acAddr1[256], acAddr2[256];
   int      iTmp;

   pRec = (ROLL_350 *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, DNXSIZ_ADDR_1);
      blankRem(acAddr1, DNXSIZ_ADDR_1);
      pAddr1 = acAddr1;

      iTmp = 0;
      if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
      {
         if (*pAddr1 == '%')
            pAddr1 += 2;
         else
            pAddr1 += 4;

         // Check for C/O name
         pTmp = strchr(pAddr1, ',');
         if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
         {
            *pTmp = 0;
            pAddr1 = pTmp + 1;
            if (*pAddr1 == ' ') 
               pAddr1++;
            iTmp = strlen(pAddr1);
            if (iTmp > SIZ_CARE_OF)
               iTmp = SIZ_CARE_OF;
            memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
         }
      }

      // Start processing
      iTmp = remChar(pAddr1, '.');
      memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, iTmp);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);

      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip+6, SIZ_M_ZIP4);
      
      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseAdr1(&sMailAdr, pAddr1);

      memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
      acAddr2[CRESIZ_ADDR_2] = 0;
      parseAdr2(&sMailAdr, myTrim(acAddr2));

      if (sMailAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%*d", SIZ_M_STRNUM, sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      blankPad(sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      blankPad(sMailAdr.State, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "10272309", 8) || !memcmp(pOutbuf, "10102119", 8))
   //   iTmp = 0;
#endif

   // Situs 
   // 15200 #7OCEAN VIEW DR         
   // 11909&13OCEAN VIEW DR
   // 10303   HWY. 101 N. HWY.
   // 231/241 SARINA RD
   // 1355/59 MOREHEAD RD
   // 2700    S. FRED HAIGHT DR
   // 6651&53 SOUTH BANK RD
   
   if (bHasSitus)
   {
      memset((void *)&sSitusAdr, 0, sizeof(ADR_REC));
      removeSitus(pOutbuf);

      memcpy(acAddr1, pRec->Situs1, DNXSIZ_SITUS1);
      acAddr1[DNXSIZ_SITUS1] = 0;

      // parseAdr1(&sSitusAdr, acAddr1);
      parseAdr1N(&sSitusAdr, &acAddr1[8]);

      // Parse HSENO
      memcpy(acTmp, acAddr1, 8);
      iTmp = blankRem(acTmp, 8);
      sSitusAdr.lStrNum = atol(acTmp);
      if (iTmp > 0)
      {
         if (pTmp = strchr(acTmp, ' '))
         {
            *pTmp++ = 0;
            memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));
            strcpy(sSitusAdr.strNum, acTmp);
            if (*pTmp == '#')
            {
               memcpy(pOutbuf+OFF_S_UNITNO, pTmp, strlen(pTmp));
               strcpy(sSitusAdr.Unit, pTmp);
            }
         } else
         {
            memcpy(pOutbuf+OFF_S_HSENO, acTmp, iTmp);
            strcpy(sSitusAdr.strNum, acTmp);
         }

         remChar(acAddr1, '.');
         sprintf(acAddr2, "%s %s %s", acTmp, &acAddr1[8], sSitusAdr.Unit);
         iTmp = blankRem(acAddr2);
         memcpy(pOutbuf+OFF_S_ADDR_D, acAddr2, iTmp);
      }


      // Save original S_HSENO
      if (sSitusAdr.lStrNum > 0)
      {
         sprintf(acTmp, "%*d", SIZ_S_STRNUM, sSitusAdr.lStrNum);
         memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_M_STRNUM);
      }

      if (sSitusAdr.strName[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));

         if (sSitusAdr.strSfx[0] > ' ')
         {
            Sfx2Code(sSitusAdr.strSfx, acTmp);
            memcpy(pOutbuf+OFF_S_SUFF, acTmp, SIZ_S_SUFF);
         }
      }

      memcpy(acAddr2, pRec->Situs2, DNXSIZ_SITUS2);
      //iTmp = remChar(acAddr2, '.');
      iTmp = blankRem(acAddr2, DNXSIZ_SITUS2);

      parseAdr2(&sSitusAdr, acAddr2);

      City2Code(sSitusAdr.City, acTmp);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
         memcpy(pOutbuf+OFF_S_ST, "CA", SIZ_S_ST);
         memcpy(pOutbuf+OFF_S_ZIP, sSitusAdr.Zip, strlen(sSitusAdr.Zip));     
      }

      if (sSitusAdr.City[0] > ' ')
      {
         iTmp = sprintf(acAddr2, "%s, CA %s", sSitusAdr.City, sSitusAdr.Zip);
         //iTmp = sprintf(acAddr2, "%s CA", sSitusAdr.City);
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************** CreateR01Rec *****************************
 *
 * InitFlg : 0=update, 1=create R01, 2=create Lien
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Dnx_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acTmp1[256], bStatus;
   long     lTmp; 
   int      iTmp, iRet;
   double   dTmp;

   pRec = (REDIFILE *)pRollRec;
   iRet = 0;
   bStatus = getParcStatus_DNX(pRec->ParcType);
   if (bStatus == 'X' || bStatus == 'R')
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pRec->APN, "10271259", 8))
   //   iRet = 0;
#endif
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "08DNXA", 5);

      // Create APN_D 
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Lien data - Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      /*
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lHpp = atoin(pRec->PPVal, CRESIZ_PP_VAL);
      lTmp = lHpp + lFixture;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      */
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTH_VALUE);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_OTH_VALUE);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_OTH_VALUE);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Parcel Status
   *(pOutbuf+OFF_STATUS) = bStatus;

   // TRA
   //memcpy(pOutbuf+OFF_TRA, (char *)&pRec->TRA[0], CRESIZ_TRA);
   lTmp = atoin(pRec->TRA, CRESIZ_TRA);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, CRESIZ_TRA);
   }

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
   } else
   {
      lTmp = 0;
      strcpy(acTmp, BLANK32);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

   // Lot Sqft
   if (lTmp > 0)
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
   memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

   // Recording info
   if (pRec->RecBook1[1] > ' ')
   {
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      Dnx_FmtRecDoc(pOutbuf, pRec->RecBook1);
   }

   // Owners
   Dnx_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address 
   Dnx_MergeAdr(pOutbuf, pRollRec);

   // HO Exempt - Exemp total
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      if (lTmp == 7000)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'
   } else
   {
      strcpy(acTmp, BLANK32);
      *(pOutbuf+OFF_HO_FL) = '2';         // 'N'
   }
   memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);

   // Following section are not allow to distribute yet.
   if (iLoadFlag & EXTR_CONF)
   {
      // UseCode
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

      // YrBlt & YrEff
      if (pRec->YrBlt[0] > '0')
         memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
      if (pRec->YrEff[0] > '0')
         memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);
   
      // Bldg Sqft
      lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
         memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
      }

      // Rooms
      iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
         memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
      }

      // Beds
      iTmp = pRec->Beds[0] & 0x0F;
      if (iTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
         memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
      }

      // Baths/Half Baths
      if (pRec->Baths[0] > '0')
      {
         *(pOutbuf+OFF_BATH_F) = ' ';
         *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
      }
      if (pRec->Baths[1] == '5' )
      {
         *(pOutbuf+OFF_BATH_H) = ' ';
         *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
      }

      // Legal Desc
      if (pRec->LglDesc1[0] > ' ')
      {
         memcpy(acTmp, pRec->LglDesc1, CRESIZ_LGL_DESC1);
         acTmp[CRESIZ_LGL_DESC1] = 0;
         if (pRec->LglDesc2[0] > ' ')
         {
            myTrim(acTmp);
            pRec->LglDesc2[CRESIZ_LGL_DESC2-1] = 0;
            strcat(acTmp, pRec->LglDesc2);
         }
         updateLegal(pOutbuf, acTmp);
      }
      
      // FL - # of floors 
      if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
      {
         sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
         memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
      }

      // #Units
      if (pRec->NumOfUnits[0] > '0' )
      {
         iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
         sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
         memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
      }

      // Garage - D or G : doesn't know the meaning
      //if (pRec->Garage[0] > ' ' )
      //   *(pOutbuf+OFF_PARK_TYPE) = pRec->Garage[0];

      // FP - 1
   
      // AC - 1

      // Heating

      // Pool /Spa

      // Zoning

      // Neighborhood

      // Type_Sqft - M, C
   }

   return iRet;
}

/********************************** CreateR01 ********************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Dnx_R350_R01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   ROLL_350 *pRec;
   char     acTmp[256], acTmp1[256], bStatus;
   int      iTmp, lTmp;
   double   dTmp;

   pRec = (ROLL_350 *)pRollRec;
   memcpy(acTmp, pRec->ParcType, DNXSIZ_PARCTYPE);
   acTmp[DNXSIZ_PARCTYPE] = 0;

   bStatus = getParcStatus_DNX(acTmp);
   if (bStatus != 'A')
      return -1;

#ifdef _DEBUG
   //if (!memcmp(pRec->APN, "10271259", 8))
   //   iRet = 0;
#endif
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->Apn, DNXSIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "08DNX", 5);

      // Create APN_D 
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Lien data - Land
      long lLand = atoin(pRec->LandVal, DNXSIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, DNXSIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      //long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      //long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      //long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lFixture;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
         }
         if (lFixture > 0)
         {
            sprintf(acTmp, "%d         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTH_VALUE);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   // Parcel Status
   *(pOutbuf+OFF_STATUS) = bStatus;

   // TRA
   lTmp = atoin(pRec->TRA, DNXSIZ_TRA);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%.6d", lTmp);
      memcpy(pOutbuf+OFF_TRA, acTmp, 6);
   }

   // Acres
   memcpy(acTmp, pRec->Acres, DNXSIZ_ACRES);
   acTmp[DNXSIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      // Lot Sqft
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Recording info 

   // Owners
   Dnx_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address 
   Dnx_MergeAdr_R350(pOutbuf, pRollRec);

   // HO Exempt - Exemp total
   *(pOutbuf+OFF_HO_FL) = '2';         // 'N'
   lTmp = atoin(pRec->ExVal, DNXSIZ_EX_VAL);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      if (lTmp == 7000)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   }

   // Following section are not allow to distribute yet.
   if (iLoadFlag & EXTR_CONF)
   {
      // UseCode
      memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, DNXSIZ_USE_CODE);

     // Legal Desc
      if (pRec->LglDesc[0] > ' ')
      {
         memcpy(acTmp, pRec->LglDesc, DNXSIZ_LGL_DESC);
         acTmp[DNXSIZ_LGL_DESC] = 0;
         updateLegal(pOutbuf, acTmp);
      }
   }

   return 0;
}

/********************************** Dnx_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 ****************************************************************************/

int Dnx_LoadRoll(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Dnx_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdLienExt)
            iRet = PQ_MergeLien(acBuf, fdLienExt);

         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

int Dnx_LoadRoll350(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   if (strstr(acRollFile, ".txt"))
      bHasSitus = false;
   else
      bHasSitus = true;

   // Sort roll file
   sprintf(acOutFile, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acBuf, "S(%d,%d,C,A) F(FIX,%d) ", DNXOFF_APN, DNXSIZ_APN, iRollLen);
   iRet = sortFile(acRollFile, acOutFile, acBuf);
   if (iRet < 100)
      return -1;

   // Open Roll file
   LogMsg("Open Roll file %s", acOutFile);
   fdRoll = fopen(acOutFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acOutFile);
      return -2;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return -3;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Dnx_R350_R01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdLienExt)
            iRet = PQ_MergeLien(acBuf, fdLienExt);

         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Dnx_LoadUpdt ****************************
 *
 * This function will either merge new data into old record or create new
 * record for new parcel.
 *
 ****************************************************************************/

int Dnx_LoadUpdt(int iSkip)
{
   char     acRec[MAX_RECSIZE], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   ROLL_350 *pRec;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0, lRollCnt=0;

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "S01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");

   // Use last out file for input if lien file missing
   if (_access(acRawFile, 0))
   {
      if (!_access(acOutFile, 0))
         rename(acOutFile, acRawFile);
      else
      {
         LogMsg("***** Input file missing: %s", acRawFile);
         return -1;
      }
   }

   // Sort roll file
   sprintf(acRec, "%s\\%s\\%s_Roll.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   if (strstr(acRollFile, ".txt"))
      bHasSitus = false;
   else
      bHasSitus = true;

   sprintf(acBuf, "S(%d,%d,C,A) F(FIX,%d) ", DNXOFF_APN, DNXSIZ_APN, iRollLen);
   iRet = sortFile(acRollFile, acRec, acBuf);
   if (iRet < 100)
      return -1;

   // Open Roll file
   LogMsg("Open Roll file %s", acRec);
   fdRoll = fopen(acRec, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRec);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Read first rec, skip blank rec as found 
   do 
   {
      iRet = fread(acRollRec, 1, iRollLen, fdRoll);
      lRollCnt++;
   } while (acRollRec[2] == ' ');

   bEof = (iRet==iRollLen ? false:true);
   pRec = (ROLL_350 *)&acRollRec[0];

   // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }
      iSkip = 0;

      if (!nBytesRead)
         break;

      NextRollRec:
#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "10170001", 8))
      //   iSkip=0;
#endif

      iTmp = memcmp(acBuf, pRec->Apn, iApnLen);
      if (!iTmp)
      {
         // Merge roll data
         iRet = Dnx_R350_R01(acBuf, acRollRec, iRollLen, UPDATE_R01);
         if (!iRet)
         {
            iRollUpd++;
            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         }

         // Read next roll record
         iRet = fread(acRollRec, 1, iRollLen, fdRoll);
         lRollCnt++;
         bEof = (iRet==iRollLen ? false:true);
      } else if (iTmp > 0)       // Roll not match, new roll record
      {
         if (bDebug)
            LogMsg0("New roll record: %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);

         // Create new R01 record
         iRet = Dnx_R350_R01(acRec, acRollRec, iRollLen, CREATE_R01);
         if (!iRet)
         {
            iNewRec++;

            // Write to file
            bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
            lCnt++;
         }

         // Read next record
         iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

         if (iRet != iRollLen)
            bEof = true;    // Signal to stop
         else
         {
            lRollCnt++;
            goto NextRollRec;
         }
      } else
      {
         // Record may be retired
         if (bDebug)
            LogMsg0("*** Roll not match (retired record?) : R01->%.*s < Roll->%.*s (%d)", iApnLen, acBuf, iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);
         iRetiredRec++;
         continue;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         bEof = true;
         break;
      }
   }

   // Do the rest of the file
   while (!bEof)
   {
      if (bDebug)
         LogMsg0("New roll record : %.*s (%d)", iApnLen, (char *)&acRollRec[CREOFF_APN], lCnt);

      // Create new R01 record
      iRet = Dnx_R350_R01(acRec, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Get next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

      if (iRet != iRollLen)
         bEof = true;    // Signal to stop
      else
         lRollCnt++;
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total new records:          %u", iNewRec);
   LogMsg("Total updated records:      %u", iRollUpd);
   LogMsg("Total retired records:      %u", iRetiredRec);

   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Total roll records input:   %u", lRollCnt);
   LogMsg("Last recording date:        %u", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return lRet;
}

/********************************** Dnx_Load_LDR ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Dnx_Load_LDR(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "001021004", 9))
      //   iRet=0;
#endif
      // 2009
      //iRet = Dnx_Create_Ldr(acBuf, acRollRec, iRollLen, CREATE_R01);
      // 2010
      iRet = Dnx_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         LogMsg0("+++ Drop retired record [%.20s]", acRollRec);
      lLDRRecCount++;

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt % 1000))
      {
         printf("\r%u", lCnt);
         //LogMsg("At %d: %.10s", lCnt, acBuf);
      }

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/******************************* Dnx_Load_LDR350 ****************************
 *
 * This function will create new R01 file using lien date roll.
 *
 ****************************************************************************/

int Dnx_Load_LDR350(int iSkip)
{
   HANDLE   fhOut;

   DWORD    nBytesWritten;
   BOOL     bRet;
   long     iRet, lRet=0, lCnt=0;
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];

   LogMsg("Loading LDR 350-byte record");
   bHasSitus = true;

   // Sort roll file
   sprintf(acOutFile, "%s\\%s\\%s_Lien.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
   sprintf(acBuf, "S(%d,%d,C,A) F(FIX,%d) ", DNXOFF_APN, DNXSIZ_APN, iRollLen);
   iRet = sortFile(acRollFile, acOutFile, acBuf);
   if (iRet < 100)
      return -1;

   // Open Roll file
   LogMsg("Open LDR file %s", acOutFile);
   fdRoll = fopen(acOutFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening LDR file: %s\n", acOutFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!feof(fdRoll))
   {
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      if (iRet != iRollLen)
         break;

      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Dnx_R350_R01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("Error occurs: %d\n", GetLastError());
            lRet = WRITE_ERR;
            break;
         }
         lCnt++;
      }

      if (!(++lLDRRecCount % 1000))
         printf("\r%u", lLDRRecCount);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   return 0;
}

/****************************** Dnx_CreateLienRec ***************************
 *
 * DO NOT REMOVE THIS FUNCTION.  WE MAY NEED IT FOR 2013 LDR
 *
 ****************************************************************************

void Dnx_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     acTmp[256];
   long     lTmp;
   LIENEXTR *pLien = (LIENEXTR *)pOutbuf;
   ROLL_350 *pRoll = (DNX_LDR *)pRollRec;

   // Clear output buffer
   memset((void *)pLien, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLien->acApn, pRoll->Apn, DNXSIZ_APN);

   // Assessment year
   memcpy(pLien->acYear, myCounty.acYearAssd, SIZ_LIEN_YEAR);

   // Land
   long lLand = atoin(pRoll->LandVal, DNXSIZ_LAND_VAL);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLien->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRoll->ImprVal, DNXSIZ_IMP_VAL);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLien->acImpr, acTmp, SIZ_IMPR);
   }

   // Fixture, Personal Properties
   long lPFixt = atoin(pRoll->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
   long lPers  = atoin(pRoll->PPVal, CRESIZ_PP_VAL);

   // Other value
   lTmp = lPFixt+lPers;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLien->acOther, acTmp, SIZ_LIEN_OTHERS);

      if (lPFixt > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPFixt);
         memcpy(pLien->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lTmp);
         memcpy(pLien->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLien->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
         memcpy(pLien->acRatio, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   lTmp = atoin(pRoll->ExVal, DNXSIZ_EX_VAL);
   if (!memcmp(pRoll->ExeCode1, "10", 2) && lTmp > 0)
      pLien->acHO[0] = '1';      // 'Y'
   else
      pLien->acHO[0] = '2';      // 'N'

   // Exemp total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pLien->acExAmt, acTmp, SIZ_EXE_TOTAL);

      memcpy(pLien->extra.Cres.Exe_Code1, pRoll->ExeCode1, CRESIZ_EXEMP_CODE);
      memcpy(pLien->extra.Cres.Exe_Code2, pRoll->ExeCode2, CRESIZ_EXEMP_CODE);
      memcpy(pLien->extra.Cres.Exe_Code3, pRoll->ExeCode3, CRESIZ_EXEMP_CODE);
      memcpy(pLien->extra.Cres.Exe_Code4, pRoll->ExeCode4, CRESIZ_EXEMP_CODE);
   }

   // TRA
   memcpy(pLien->acTRA, pRoll->TRA, CRESIZ_TRA);

   pLien->LF[0] = 10;
   pLien->LF[1] = 0;
}

/********************************* Dnx_ExtrLien *****************************
 *
 * DO NOT REMOVE THIS FUNCTION.  WE MAY NEED IT FOR 2013 LDR
 * Create lien extract file.
 *
 ****************************************************************************

int Dnx_ExtrLien(char *pCnty)
{
   char     acBuf[256], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH];

   int      iRet;
   BOOL     bEof;
   long     lCnt=0;
   FILE     *fdLien;

   // Open roll file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsgD("\nExtract lien data from Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Merge loop
   while (!bEof)
   {
      if (memcmp(acRollRec, "99", 2) < 0)
      {
         // Create new record
         Dnx_CreateLienRec(acBuf, acRollRec);
    
         // Write to output
         fputs(acBuf, fdLien);

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      }

      // Get next RollRec
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/******************************* Dnx_ExtrSale *******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Dnx_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024], *pTmp;
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   double   dTmp;
   long     lCnt=0, lNewRec=0, lPrice;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
      //if (!memcmp(acBuf, "10602158", 8))
      //   iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            if (dTmp > 9999999)
            {
               LogMsg("*** Questionable tax amount for APN: %.9s, StampAmt=%.2f", pSale->Apn, dTmp);
            } else
            {
               iRet = blankRem(acTmp);
               memcpy(pSale->StampAmt, acTmp, iRet);

               // Keep this check here until counties correct their roll
               pTmp = strchr(acTmp, '.');
               if (!pTmp || *(pTmp+1) == ' ' || dTmp > 100000.00)
               {
                  if (acTmp[0] == '0' || dTmp < 2000.00)
                  {
                     lPrice = (long)(dTmp * SALE_FACTOR);
                     if ((lPrice % 100) == 0)
                     {
                        sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                     } else
                     {
                        LogMsg0("??? Suspected stamp amount for APN: %.10s, StampAmt=%s", acBuf, acTmp);
                        memset(acTmp, ' ', SIZ_SALE1_AMT);
                     }
                  } else
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               } else
               {
                  if (dTmp > 9999.00 && ((long)dTmp % 100) == 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
                  } else
                  {
                     lPrice = (long)(dTmp * SALE_FACTOR);
                     if (lPrice > 0)
                     {
                        if (lPrice < 100 || (lPrice % 100) == 0)
                           sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                        else
                        {
                           LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt);
                           memset(acTmp, ' ', SIZ_SALE1_AMT);
                        }
                     }
                  }
               }
               memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
            }
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);

      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lNewRec);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

int Dnx_ExtrSale_R350(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024];
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0;
   boolean  bEof;

   ROLL_350  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (ROLL_350 *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
      //if (!memcmp(acBuf, "10602158", 8))
      //   iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->Apn, DNXSIZ_APN);
      Dnx_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, DNXSIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, DNXSIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->Apn);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lNewRec);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/****************************** Dnx_CreateLienRec ***************************
 *
 * Only output active records.
 *
 ****************************************************************************/

int Dnx_CreateLienRec(char *pOutbuf, char *pRollRec)
{
   char     bStatus, acTmp[256];
   long     lTmp;
   
   LIENEXTR *pLien = (LIENEXTR *)pOutbuf;
   ROLL_350 *pRoll = (ROLL_350 *)pRollRec;

   memcpy(acTmp, pRoll->ParcType, DNXSIZ_PARCTYPE);
   acTmp[DNXSIZ_PARCTYPE] = 0;

   bStatus = getParcStatus_DNX(acTmp);
   if (bStatus != 'A')
      return -1;

   // Clear output buffer
   memset((void *)pLien, ' ', sizeof(LIENEXTR));

   // Start copying data
   memcpy(pLien->acApn, pRoll->Apn, DNXSIZ_APN);

   // Prop 8

   // Assessment year
   memcpy(pLien->acYear, pRoll->RollYr, DNXSIZ_ROLL_YEAR);

   // Land
   long lLand = atoin(pRoll->LandVal, DNXSIZ_LAND_VAL);
   if (lLand > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      memcpy(pLien->acLand, acTmp, SIZ_LAND);
   }

   // Improve
   long lImpr = atoin(pRoll->ImprVal, DNXSIZ_IMP_VAL);
   if (lImpr > 0)
   {
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      memcpy(pLien->acImpr, acTmp, SIZ_IMPR);
   }

   // Fixture, Personal Properties
   // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
   long lFixtVal = atoin(pRoll->FixtVal, DNXSIZ_PERS_FIXT_VAL);
   long lPers  = atoin(pRoll->PPVal, DNXSIZ_PP_VAL);

   // Other value
   lTmp = lFixtVal+lPers;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_LIEN_OTHERS, lTmp);
      memcpy(pLien->acOther, acTmp, SIZ_LIEN_OTHERS);

      if (lFixtVal > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lFixtVal);
         memcpy(pLien->acME_Val, acTmp, SIZ_LIEN_FIXT);
      }
      if (lPers > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LIEN_FIXT, lPers);
         memcpy(pLien->acPP_Val, acTmp, SIZ_LIEN_FIXT);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      memcpy(pLien->acGross, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pLien->acRatio, acTmp, SIZ_RATIO);
      }
   }

   // HO Exempt
   lTmp = atoin(pRoll->ExVal, DNXSIZ_EX_VAL);
   if (lTmp == 7000)
      pLien->acHO[0] = '1';      // 'Y'
   else
      pLien->acHO[0] = '2';      // 'N'

   // Exemp total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pLien->acExAmt, acTmp, SIZ_EXE_TOTAL);

      memcpy(pLien->extra.Cres.Exe_Code1, pRoll->ExeCode1, DNXSIZ_EXEMP_CODE);
      memcpy(pLien->extra.Cres.Exe_Code2, pRoll->ExeCode2, DNXSIZ_EXEMP_CODE);
      memcpy(pLien->extra.Cres.Exe_Code3, pRoll->ExeCode3, DNXSIZ_EXEMP_CODE);
      memcpy(pLien->extra.Cres.Exe_Code4, pRoll->ExeCode4, DNXSIZ_EXEMP_CODE);
   }

   // TRA
   memcpy(pLien->acTRA, pRoll->TRA, DNXSIZ_TRA);

   pLien->LF[0] = 10;
   pLien->LF[1] = 0;

   return 0;
}

/****************************** Dnx_ExtrLien ********************************
 *
 * Create lien extract file.
 *
 ****************************************************************************/

int Dnx_ExtrLien(char *pCnty)
{
   char     acBuf[256], acRollRec[MAX_RECSIZE], acOutFile[_MAX_PATH];
   long     iRet, lCnt=0;
   FILE     *fdLien;

   LogMsg("Extract lien values");

   // Open roll file
   GetIniString(pCnty, "LienFile", "", acBuf, _MAX_PATH, acIniFile);
   LogMsgD("\nExtract lien data from Roll file %s", acBuf);
   fdRoll = fopen(acBuf, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acBuf);
      return -1;
   }

   // Open Output file
   sprintf(acOutFile, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acOutFile);
   fdLien = fopen(acOutFile, "w");
   if (fdLien == NULL)
   {
      LogMsg("***** Error creating lien extract file: %s\n", acOutFile);
      return -2;
   }

   // Merge loop
   while (!feof(fdRoll))
   {
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      if (iRet != iRollLen)
         break;

      // Create new record
      iRet = Dnx_CreateLienRec(acBuf, acRollRec);
      if (!iRet)
      {
         // Write to output
         fputs(acBuf, fdLien);

         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);
      } else
         LogMsg("Skip rec: %.50s", acRollRec);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLien)
      fclose(fdLien);

   LogMsgD("\nTotal lien records extracted:    %u", lCnt);

   return 0;
}

/*********************************** loadDnx ********************************
 *
 * Input files:
 *    redifile_nl       899-bytes roll file
 *
 * Problems: DNX only allows us to use following fields in PQ4:
 *    - APN
 *    - TRA
 *    - Owners
 *    - Mail Addr
 *    - Situs Addr
 *    - Asmt Year
 *    - Doc#, Doc date
 *    - Acreage
 *    - Land, Impr values
 *    - Other values
 *    - Exemption value
 *
 * Commands:
 *    -U    Monthly update
 *    -L    Lien update
 *    -Xl   Extract lien values
 *
 * Lien processing:  -L -Xl -Xc
 * Monthly update:   -U
 *
 ****************************************************************************/

int loadDnx(int iLoadFlag, int iSkip)
{
   int   iRet=0;

   iApnLen = myCounty.iApnLen;
   iRollLen = guessRecLen(acRollFile, iRollLen);

   if (bLoadTax)
   {
      TC_SetDateFmt(MMDDYY1);
      iRet = Load_TC(myCounty.acCntyCode, bTaxImport);
   }

   /* 10/18/2011
   char  acTmpFile[_MAX_PATH];
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\2005\\redifile_nl");
   strcpy(acTmpFile, "H:\\CO_PROD\\SDnx_CD\\raw\\Dnx_Sale.2005");
   iRet = Dnx_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\2006\\redifile_nl");
   strcpy(acTmpFile, "H:\\CO_PROD\\SDnx_CD\\raw\\Dnx_Sale.2006");
   iRet = Dnx_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\2007\\redifile_nl");
   strcpy(acTmpFile, "H:\\CO_PROD\\SDnx_CD\\raw\\Dnx_Sale.2007");
   iRet = Dnx_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\2008\\redifile_nl");
   strcpy(acTmpFile, "H:\\CO_PROD\\SDnx_CD\\raw\\Dnx_Sale.2008");
   iRet = Dnx_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\2009\\redifile_nl");
   strcpy(acTmpFile, "H:\\CO_PROD\\SDnx_CD\\raw\\Dnx_Sale.2009");
   iRet = Dnx_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\2010\\redifile_nl");
   strcpy(acTmpFile, "H:\\CO_PROD\\SDnx_CD\\raw\\Dnx_Sale.2010");
   iRet = Dnx_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\2011\\redifile_nl");
   strcpy(acTmpFile, "H:\\CO_PROD\\SDnx_CD\\raw\\Dnx_Sale.2011");
   iRet = Dnx_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Dnx\\redifile_nl");
   */
   // Extract sale1 from redifile and append to cum sale file
   if (iLoadFlag & EXTR_SALE)                            // -Xsi
   {                          
      if (iRollLen < 890)
         iRet = Dnx_ExtrSale_R350(acRollFile, NULL, true);
      else
         iRet = Dnx_ExtrSale(acRollFile, NULL, true);
   }

   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))                // -L 
   {
      // Resort lien roll to make sure it is sorted on APN
      /* 2009 LDR
      sprintf(acTmpFile, "%s\\%s\\Dnx_Lien.dat", acTmpPath, myCounty.acCntyCode);
      sprintf(acTmp, "S(13,9,C,A) F(FIX,%d)", iRollLen);
      iRet = sortFile(acRollFile, acTmpFile, acTmp);
      if (iRet > 0)
      {
         strcpy(acRollFile, acTmpFile);
         if (iLoadFlag & EXTR_LIEN)                // -Xl
            iRet = Dnx_ExtrLien(myCounty.acCntyCode);
      
         if (iLoadFlag & LOAD_LIEN)
         {
            iRet = Dnx_Load_LDR(myCounty.acCntyCode);

            // County request not to put sale price out
            if (!iRet)
               iRet = MergeCumSale1(iSkip, NULL, true, true);
         }
      } else
         iRet = -1;
      */
      /* 2010 LDR 
      if (iLoadFlag & EXTR_LIEN)                // -Xl
         iRet = Cres_ExtrLien(myCounty.acCntyCode);
      */

      // 2013 LDR 
      if (iLoadFlag & EXTR_LIEN)                // -Xl
      {
         if (iRollLen >= 899)
            // 2010
            iRet = Cres_ExtrLien(myCounty.acCntyCode);
         else if (iRollLen == 350)
            // 2013
            iRet = Dnx_ExtrLien(myCounty.acCntyCode);
      }

      if (iLoadFlag & LOAD_LIEN)
      {
         if (iRollLen >= 899)
            // 2010
            iRet = Dnx_Load_LDR(myCounty.acCntyCode);
         else if (iRollLen == 350)
            // 2013
            iRet = Dnx_Load_LDR350(iSkip);
         else
         {
            LogMsg("***** Invalid roll record size %d", iRollLen);
            iRet = -1;
         }
      }
   } else if (iLoadFlag & LOAD_UPDT)           // -U
   {
      // New 2012/11 format 
      if (iRollLen < 890)
         iRet = Dnx_LoadUpdt(iSkip);
      else
         // Standard CRES format
         iRet = Dnx_LoadRoll(iSkip);
   }

   // Fix TRA
   //if (bFixTRA)
   //   iRet = PQ_FixR01(myCounty.acCntyCode, PQ_FIX_TRA, 0, true);

   // DNX does not allow us to publish sale price online
   // But we can use it to generate report.  So, don't use it here.
   //if (!iRet)
   //   iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);

   // Apply DocNum & DocDate only
   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN)))
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCNOPRICE);

   return iRet;
}
