#ifndef  _MERGEZONING_H_
#define  _MERGEZONING_H_

// CCX_Zone.txt
#define  ZONE_ASMT      0
#define  ZONE_ACRES     1
#define  ZONE_TYPE      2
#define  ZONE_CODE      3

int MergeZoning(char *pCnty, int iSkip);

#endif
