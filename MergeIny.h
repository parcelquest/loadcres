#ifndef _MERGEINY_H
#define  _MERGEINY_H

/* Reformat by Sony on 3/7/2008 to fix misalignment */
#define COFF_APN                                   1   
#define COFF_BLDG_NBR                              19  
#define COFF_BLDG_TYPE_CODE                        22  
#define COFF_BLDG_TYPE_TEXT                        62  
#define COFF_BLDG_QUAL                             112  
#define COFF_FILLER2                               117  
#define COFF_STORY_DESC                            127  
#define COFF_AVG_BLDG_HEIGHT                       167  
#define COFF_DIST_BLDG                             172  
#define COFF_EXT_WALL_HEIGHT                       180  
#define COFF_BLDG_HEIGHT                           184  
#define COFF_BEDROOMS                              192  
#define COFF_ROOMS                                 195  
#define COFF_CONSTR_YEAR                           198  
#define COFF_EFF_AGE                               202  
#define COFF_FOUNDATION_TYPE                       205  
#define COFF_BLDG_TYPE                             245  
#define COFF_FILLER4                               246  
#define COFF_EXT_FRAME                             270  
#define COFF_EXT_BRICK                             271  
#define COFF_EXT_STUCCO                            272  
#define COFF_EXT_BLOCK                             273  
#define COFF_EXT_STEEL                             274  
#define COFF_EXT_ALUMINUM                          275  
#define COFF_EXT_STONE                             276  
#define COFF_EXT_OTHER                             277  
#define COFF_DECK                                  278  
#define COFF_DECK_LENGTH                           279  
#define COFF_DECK_WIDTH                            282  
#define COFF_RAILING_DECK                          285  
#define COFF_OPEN_PORCH                            286  
#define COFF_PORCH_SQFT                            287  
#define COFF_PORCH_FRAME                           295  
#define COFF_ENCLOSED_PORCH1                       296  
#define COFF_PORCH_BRICK                           297  
#define COFF_ENCLOSED_PORCH_FRAME                  298  
#define COFF_ROOF_TYPE_FLAT                        299  
#define COFF_ROOF_TYPE_GABLE                       300  
#define COFF_ROOF_TYPE_HIP                         301  
#define COFF_ROOF_TYPE_GAMBREL                     302  
#define COFF_ROOF_TYPE_MANSARD                     303  
#define COFF_ROOF_TYPE_CANOPY                      304  
#define COFF_ROOF_TYPE_OTHER                       305  
#define COFF_ROOF_MATERIAL_ASPHALT                 306  
#define COFF_ROOF_MATERIAL_WOOD                    307  
#define COFF_ROOF_MATERIAL_TILE                    308  
#define COFF_ROOF_MATERIAL_METAL                   309  
#define COFF_ROOF_MATERIAL_OTHER                   310  
#define COFF_FLOOR_CONST_WOOD                      311  
#define COFF_FLOOR_CONST_CONCRETE                  312  
#define COFF_FLOOR_CONST_REINF                     313  
#define COFF_FLOOR_CONST_METAL                     314  
#define COFF_FLOOR_CONST_OTHER                     315  
#define COFF_FLOOR_FINISH_TILE                     316  
#define COFF_FLOOR_FINISH_HARDWOOD                 317  
#define COFF_FLOOR_FINISH_SOFTWOOD                 318  
#define COFF_FLOOR_FINISH_CARPET                   319  
#define COFF_FLOOR_FINISH_VINYL                    320  
#define COFF_FLOOR_FINISH_MARBLE                   321  
#define COFF_FLOOR_FINISH_OTHER                    322  
#define COFF_INT_WALL_FINISH_DRYWALL               323  
#define COFF_INT_WALL_FINISH_PLASTER               324  
#define COFF_INT_WALL_FINISH_WOOD                  325  
#define COFF_INT_WALL_FINISH_OTHER                 326  
#define COFF_UNFIN_INTERIOR                        327  
#define COFF_PERCENT_UNFIN_INTERIOR                328  
#define COFF_UNFIN_INTERIOR_SQFT                   332  
#define COFF_FIN_OFFICE                            340  
#define COFF_PERCENT_FIN_OFFICE                    341  
#define COFF_FIN_OFFICE_SQFT                       345  
#define COFF_CEIL_TYPE_VAULTED                     353  
#define COFF_CEIL_TYPE_CATHEDRAL                   354  
#define COFF_CEIL_TYPE_COFFER                      355  
#define COFF_CEIL_TYPE_TRAY                        356  
#define COFF_CEIL_TYPE_GAMBREL                     357  
#define COFF_CEIL_TYPE_SUSPENDED                   358  
#define COFF_CEIL_TYPE_METAL                       359  
#define COFF_CEIL_TYPE_METALINSULATED              360  
#define COFF_CEIL_TYPE_OTHER                       361  
#define COFF_BATH1                                 362  
#define COFF_NBR_BATHR1                            363  
#define COFF_BATH2                                 366  
#define COFF_NBR_BATHR2                            367  
#define COFF_BATH3                                 370  
#define COFF_NBR_BATHR3                            371  
#define COFF_WHIRLPOOL                             374  
#define COFF_NBR_WHIRLPOOLS                        375  
#define COFF_SPA                                   378  
#define COFF_NBR_SPAS                              379  
#define COFF_SAUNA                                 382  
#define COFF_NBR_SAUNAS                            383  
#define COFF_WET_SPRINKLER                         386  
#define COFF_NBR_WET_SPRINKLER                     387  
#define COFF_DRY_SPRINKLER                         390  
#define COFF_NBR_DRY_SPRINKLER                     391  
#define COFF_OTHER_PLUMBING                        394  
#define COFF_NBR_OTHER_PLUMBING                    395  
#define COFF_STOVE_HEATING                         398  
#define COFF_GAS_HEATING                           399  
#define COFF_ELECTRIC_HEATING                      400  
#define COFF_STEAM_HEATING                         401  
#define COFF_HOT_WATER_HEATING                     402  
#define COFF_HEAT_PUMPTHERE_HEATING                403  
#define COFF_SPACE_HEATER_HEATING                  404  
#define COFF_CENTRAL_AIR                           405  
#define COFF_NBR_CENTRAL_AIR                       406  
#define COFF_OTHER_HEATING_OR_AC                   409  
#define COFF_BASEMENT                              410  
#define COFF_FULL_BASEMENT                         411  
#define COFF_FULL_BASEMENT_SQFT                    412  
#define COFF_PARTIAL_BASEMENT                      420  
#define COFF_PARTIAL_BASEMENT_SQFT                 421  
#define COFF_PERCENT_FINBASEMENT                   429  
#define COFF_LOWER_BASEMENT                        435  
#define COFF_LOWERBASEMENT_SQFT                    436  
#define COFF_PERCENT_LOWER_BASEMENT                444  
#define COFF_CRAWL_SPACE                           450  
#define COFF_CRAWL_SPACE_SQFT                      451  
#define COFF_ASEMENT_FLOOR_TYPE                    459  
#define COFF_NO_ATTIC                              469  
#define COFF_FIN_ATTIC                             470  
#define COFF_FIN_ATTIC_SQFT                        471  
#define COFF_UNFIN_ATTIC                           479  
#define COFF_UNFIN_ATTIC_SQFT                      480  
#define COFF_NO_ELEVATOR                           488  
#define COFF_FREIGHT_ELEVATOR                      489  
#define COFF_NBR_FREIGHT_ELEVATOR                  490  
#define COFF_PASSENGER_ELEVATOR                    493  
#define COFF_NBR_PASSENGER_ELEVATORS               494  
#define COFF_DETACHED_GARAGE                       497  
#define COFF_CAR_GARAGE1                           498  
#define COFF_CAR_GARAGE2                           499  
#define COFF_CAR_GARAGE3                           500  
#define COFF_CAR_GARAGE4                           501  
#define COFF_GARAGE_SQFT                           502  
#define COFF_GARAGE_BRICK                          510  
#define COFF_GARAGE_FRAME                          511  
#define COFF_GARAGE_OTHER                          512  
#define COFF_BLACK_TOP_AREA                        513  
#define COFF_FENCED_AREA                           514  
#define COFF_CONCRETE_AREA                         515  
#define COFF_GREENBELT_AREA                        516  
#define COFF_OTHER_IMPROV1                         517  
#define COFF_LOADING_DOCK                          518  
#define COFF_LOADING_DOCK_NBR                      519  
#define COFF_DOCK_OVERHEAD_DOORS_NBR               522  
#define COFF_SHED                                  525  
#define COFF_SHEDS_NBR                             526  
#define COFF_SHED_SQFT                             529  
#define COFF_IN_GROUND_POOL                        537  
#define COFF_IN_GROUND_POOL_NBR                    538  
#define COFF_IN_GROUND_POOL_SQFT                   541  
#define COFF_DIF_DETACHED_GARAGE                   549  
#define COFF_DETACHED_GARAGES_NBR                  550  
#define COFF_DETACHED_GARAGES_SQFT                 553  
#define COFF_CARS_DETACHED_GARAGES                 561  
#define COFF_GUEST_HOUSE                           563  
#define COFF_GUEST_HOUSE_NBR                       564  
#define COFF_GUEST_HOUSE_SQFT                      567  
#define COFF_STABLE                                575  
#define COFF_STABLE_SQFT                           576  
#define COFF_STABLE_NBR                            584  
#define COFF_STALLS_STABLE_NBR                     587  
#define COFF_WATER_STABLE                          590  
#define COFF_TENNIS_COURT                          591  
#define COFF_TENNIS_COURT_NBR                      592  
#define COFF_TENNIS_COURT_SQFT                     595  
#define COFF_OTHER_IMPROV2                         603  
#define COFF_OTHER_IMPROV_NBR                      604  
#define COFF_OTHER_IMPROV_SQFT                     607  
#define COFF_FIREPLACE                             615  
#define COFF_DESC_FIREPLACE                        616  
#define COFF_FIREPLACE_NBR                         656  
#define COFF_GROUND_BLDG_SQFT                      658  
#define COFF_USABLE_BLDG_SQFT                      666  
#define COFF_GROUND_BIGGEST_BLDG_SQFT              674  
#define COFF_DECK_SQFT                             684  
#define COFF_BLDG_COND_CLASS                       692  
#define COFF_PERCENT_FRAME                         707  
#define COFF_PERCENT_BRICK                         710  
#define COFF_PERCENT_STUCCO                        713  
#define COFF_PERCENT_BLOCK                         716  
#define COFF_PERCENT_STEEL                         719  
#define COFF_PERCENT_ALUMINUM                      722  
#define COFF_PERCENT_STONE                         725  
#define COFF_PERCENT_OTHER                         728  
#define COFF_VINYL                                 731  
#define COFF_PERCENT_VINYL                         732  
#define COFF_ENCLOSED_PORCH2                       735  
#define COFF_PORCHES_OPEN_FRAME_NBR                736  
#define COFF_PORCHES_OPEN_BRICK_NBR                744  
#define COFF_PORCHES_ENCL_FRAME_NBR                752  
#define COFF_PORCHES_ENCL_BRICK_NBR                760  
#define COFF_PORCHES_OPEN_BRICK_SQFT               768  
#define COFF_PORCHES_ENCL_FRAME_SQFT               776  
#define COFF_PORCHES_ENCL_BRICK_SQFT               784  
#define COFF_PERCENT_FLOOR_TILE                    792  
#define COFF_PERCENT_FLOOR_HARDWOOD                795  
#define COFF_PERCENT_FLOOR_SOFTWOOD                798  
#define COFF_PERCENT_FLOOR_CARPET                  801  
#define COFF_PERCENT_FLOOR_VINYL                   804  
#define COFF_PERCENT_FLOOR_MARBLE                  807  
#define COFF_PERCENT_FLOOR_OTHER                   810  
#define COFF_ROUGHED_IN_FIXTURE                    813  
#define COFF_ROUGHED_IN_FIXTURE_NBR                814  
#define COFF_FIREPLACE2_TYPE                       817  
#define COFF_FIREPLACE2_NBR                        857  
#define COFF_FIREPLACE2                            859  
#define COFF_DIF_CALC_BASE_COST                    860  
#define COFF_DIF_CALC_BASE_COST_AMT                861  
#define COFF_BLDG_ADDR_LINE1                       871  
#define COFF_BLDG_ADDR_LINE2                       971  
#define COFF_BLDG_ADDR_CITY                        1071 
#define COFF_BLDG_ADDR_STATE                       1116 
#define COFF_BLDG_ADDR_ZIP                         1118 
#define COFF_INFLUENCE_FACTOR                      1128 
#define COFF_BATH4                                 1139 
#define COFF_NBR_BATHR4                            1140 
#define COFF_BATH5                                 1143 
#define COFF_NBR_BATHR5                            1144 
#define COFF_BATH6                                 1147 
#define COFF_NBR_BATHR6                            1148 
#define COFF_APPLY_INFL_FACT_ALL_COSTS             1151 
#define COFF_CHANGE_TRACKING_NUMBER                1152 
#define COFF_REM_ECO_LIFE                          1158 
#define COFF_BUILD_CLASS                           1168
#define COFF_STYLE_FACTOR                          1208
#define COFF_UNITS                                 1219

#define CSIZ_APN                                   18
#define CSIZ_BLDG_NBR                              3
#define CSIZ_BLDG_TYPE_CODE                        40
#define CSIZ_BLDG_TYPE_TEXT                        50
#define CSIZ_BLDG_QUAL                             5
#define CSIZ_FILLER2                               10
#define CSIZ_STORY_DESC                            40
#define CSIZ_AVG_BLDG_HEIGHT                       5
#define CSIZ_DIST_BLDG                             8
#define CSIZ_EXT_WALL_HEIGHT                       4
#define CSIZ_BLDG_HEIGHT                           8
#define CSIZ_BEDROOMS                              3
#define CSIZ_ROOMS                                 3
#define CSIZ_CONSTR_YEAR                           4
#define CSIZ_EFF_AGE                               3
#define CSIZ_FOUNDATION_TYPE                       40
#define CSIZ_BLDG_TYPE                             1
#define CSIZ_FILLER4                               24
#define CSIZ_EXT_FRAME                             1
#define CSIZ_EXT_BRICK                             1
#define CSIZ_EXT_STUCCO                            1
#define CSIZ_EXT_BLOCK                             1
#define CSIZ_EXT_STEEL                             1
#define CSIZ_EXT_ALUMINUM                          1
#define CSIZ_EXT_STONE                             1
#define CSIZ_EXT_OTHER                             1
#define CSIZ_DECK                                  1
#define CSIZ_DECK_LENGTH                           3
#define CSIZ_DECK_WIDTH                            3
#define CSIZ_RAILING_DECK                          1
#define CSIZ_OPEN_PORCH                            1
#define CSIZ_PORCH_SQFT                            8
#define CSIZ_PORCH_FRAME                           1
#define CSIZ_ENCLOSED_PORCH1                       1
#define CSIZ_PORCH_BRICK                           1
#define CSIZ_ENCLOSED_PORCH_FRAME                  1
#define CSIZ_ROOF_TYPE_FLAT                        1
#define CSIZ_ROOF_TYPE_GABLE                       1
#define CSIZ_ROOF_TYPE_HIP                         1
#define CSIZ_ROOF_TYPE_GAMBREL                     1
#define CSIZ_ROOF_TYPE_MANSARD                     1
#define CSIZ_ROOF_TYPE_CANOPY                      1
#define CSIZ_ROOF_TYPE_OTHER                       1
#define CSIZ_ROOF_MATERIAL_ASPHALT                 1
#define CSIZ_ROOF_MATERIAL_WOOD                    1
#define CSIZ_ROOF_MATERIAL_TILE                    1
#define CSIZ_ROOF_MATERIAL_METAL                   1
#define CSIZ_ROOF_MATERIAL_OTHER                   1
#define CSIZ_FLOOR_CONST_WOOD                      1
#define CSIZ_FLOOR_CONST_CONCRETE                  1
#define CSIZ_FLOOR_CONST_REINF                     1
#define CSIZ_FLOOR_CONST_METAL                     1
#define CSIZ_FLOOR_CONST_OTHER                     1
#define CSIZ_FLOOR_FINISH_TILE                     1
#define CSIZ_FLOOR_FINISH_HARDWOOD                 1
#define CSIZ_FLOOR_FINISH_SOFTWOOD                 1
#define CSIZ_FLOOR_FINISH_CARPET                   1
#define CSIZ_FLOOR_FINISH_VINYL                    1
#define CSIZ_FLOOR_FINISH_MARBLE                   1
#define CSIZ_FLOOR_FINISH_OTHER                    1
#define CSIZ_INT_WALL_FINISH_DRYWALL               1
#define CSIZ_INT_WALL_FINISH_PLASTER               1
#define CSIZ_INT_WALL_FINISH_WOOD                  1
#define CSIZ_INT_WALL_FINISH_OTHER                 1
#define CSIZ_UNFIN_INTERIOR                        1
#define CSIZ_PERCENT_UNFIN_INTERIOR                4
#define CSIZ_UNFIN_INTERIOR_SQFT                   8
#define CSIZ_FIN_OFFICE                            1
#define CSIZ_PERCENT_FIN_OFFICE                    4
#define CSIZ_FIN_OFFICE_SQFT                       8
#define CSIZ_CEIL_TYPE_VAULTED                     1
#define CSIZ_CEIL_TYPE_CATHEDRAL                   1
#define CSIZ_CEIL_TYPE_COFFER                      1
#define CSIZ_CEIL_TYPE_TRAY                        1
#define CSIZ_CEIL_TYPE_GAMBREL                     1
#define CSIZ_CEIL_TYPE_SUSPENDED                   1
#define CSIZ_CEIL_TYPE_METAL                       1
#define CSIZ_CEIL_TYPE_METALINSULATED              1
#define CSIZ_CEIL_TYPE_OTHER                       1
#define CSIZ_BATH1                                 1
#define CSIZ_NBR_BATHR1                            3
#define CSIZ_BATH2                                 1
#define CSIZ_NBR_BATHR2                            3
#define CSIZ_BATH3                                 1
#define CSIZ_NBR_BATHR3                            3
#define CSIZ_WHIRLPOOL                             1
#define CSIZ_NBR_WHIRLPOOLS                        3
#define CSIZ_SPA                                   1
#define CSIZ_NBR_SPAS                              3
#define CSIZ_SAUNA                                 1
#define CSIZ_NBR_SAUNAS                            3
#define CSIZ_WET_SPRINKLER                         1
#define CSIZ_NBR_WET_SPRINKLER                     3
#define CSIZ_DRY_SPRINKLER                         1
#define CSIZ_NBR_DRY_SPRINKLER                     3
#define CSIZ_OTHER_PLUMBING                        1
#define CSIZ_NBR_OTHER_PLUMBING                    3
#define CSIZ_STOVE_HEATING                         1
#define CSIZ_GAS_HEATING                           1
#define CSIZ_ELECTRIC_HEATING                      1
#define CSIZ_STEAM_HEATING                         1
#define CSIZ_HOT_WATER_HEATING                     1
#define CSIZ_HEAT_PUMPTHERE_HEATING                1
#define CSIZ_SPACE_HEATER_HEATING                  1
#define CSIZ_CENTRAL_AIR                           1
#define CSIZ_NBR_CENTRAL_AIR                       3
#define CSIZ_OTHER_HEATING_OR_AC                   1
#define CSIZ_BASEMENT                              1
#define CSIZ_FULL_BASEMENT                         1
#define CSIZ_FULL_BASEMENT_SQFT                    8
#define CSIZ_PARTIAL_BASEMENT                      1
#define CSIZ_PARTIAL_BASEMENT_SQFT                 8
#define CSIZ_PERCENT_FINBASEMENT                   6
#define CSIZ_LOWER_BASEMENT                        1
#define CSIZ_LOWERBASEMENT_SQFT                    8
#define CSIZ_PERCENT_LOWER_BASEMENT                6
#define CSIZ_CRAWL_SPACE                           1
#define CSIZ_CRAWL_SPACE_SQFT                      8
#define CSIZ_ASEMENT_FLOOR_TYPE                    10
#define CSIZ_NO_ATTIC                              1
#define CSIZ_FIN_ATTIC                             1
#define CSIZ_FIN_ATTIC_SQFT                        8
#define CSIZ_UNFIN_ATTIC                           1
#define CSIZ_UNFIN_ATTIC_SQFT                      8
#define CSIZ_NO_ELEVATOR                           1
#define CSIZ_FREIGHT_ELEVATOR                      1
#define CSIZ_NBR_FREIGHT_ELEVATOR                  3
#define CSIZ_PASSENGER_ELEVATOR                    1
#define CSIZ_NBR_PASSENGER_ELEVATORS               3
#define CSIZ_DETACHED_GARAGE                       1
#define CSIZ_CAR_GARAGE1                           1
#define CSIZ_CAR_GARAGE2                           1
#define CSIZ_CAR_GARAGE3                           1
#define CSIZ_CAR_GARAGE4                           1
#define CSIZ_GARAGE_SQFT                           8
#define CSIZ_GARAGE_BRICK                          1
#define CSIZ_GARAGE_FRAME                          1
#define CSIZ_GARAGE_OTHER                          1
#define CSIZ_BLACK_TOP_AREA                        1
#define CSIZ_FENCED_AREA                           1
#define CSIZ_CONCRETE_AREA                         1
#define CSIZ_GREENBELT_AREA                        1
#define CSIZ_OTHER_IMPROV1                         1
#define CSIZ_LOADING_DOCK                          1
#define CSIZ_LOADING_DOCK_NBR                      3
#define CSIZ_DOCK_OVERHEAD_DOORS_NBR               3
#define CSIZ_SHED                                  1
#define CSIZ_SHEDS_NBR                             3
#define CSIZ_SHED_SQFT                             8
#define CSIZ_IN_GROUND_POOL                        1
#define CSIZ_IN_GROUND_POOL_NBR                    3
#define CSIZ_IN_GROUND_POOL_SQFT                   8
#define CSIZ_DIF_DETACHED_GARAGE                   1
#define CSIZ_DETACHED_GARAGES_NBR                  3
#define CSIZ_DETACHED_GARAGES_SQFT                 8
#define CSIZ_CARS_DETACHED_GARAGES                 2
#define CSIZ_GUEST_HOUSE                           1
#define CSIZ_GUEST_HOUSE_NBR                       3
#define CSIZ_GUEST_HOUSE_SQFT                      8
#define CSIZ_STABLE                                1
#define CSIZ_STABLE_SQFT                           8
#define CSIZ_STABLE_NBR                            3
#define CSIZ_STALLS_STABLE_NBR                     3
#define CSIZ_WATER_STABLE                          1
#define CSIZ_TENNIS_COURT                          1
#define CSIZ_TENNIS_COURT_NBR                      3
#define CSIZ_TENNIS_COURT_SQFT                     8
#define CSIZ_OTHER_IMPROV2                         1
#define CSIZ_OTHER_IMPROV_NBR                      3
#define CSIZ_OTHER_IMPROV_SQFT                     8
#define CSIZ_FIREPLACE                             1
#define CSIZ_DESC_FIREPLACE                        40
#define CSIZ_FIREPLACE_NBR                         2
#define CSIZ_GROUND_BLDG_SQFT                      8
#define CSIZ_USABLE_BLDG_SQFT                      8
#define CSIZ_GROUND_BIGGEST_BLDG_SQFT              10
#define CSIZ_DECK_SQFT                             8
#define CSIZ_BLDG_COND_CLASS                       15
#define CSIZ_PERCENT_FRAME                         3
#define CSIZ_PERCENT_BRICK                         3
#define CSIZ_PERCENT_STUCCO                        3
#define CSIZ_PERCENT_BLOCK                         3
#define CSIZ_PERCENT_STEEL                         3
#define CSIZ_PERCENT_ALUMINUM                      3
#define CSIZ_PERCENT_STONE                         3
#define CSIZ_PERCENT_OTHER                         3
#define CSIZ_VINYL                                 1
#define CSIZ_PERCENT_VINYL                         3
#define CSIZ_ENCLOSED_PORCH2                       1
#define CSIZ_PORCHES_OPEN_FRAME_NBR                8
#define CSIZ_PORCHES_OPEN_BRICK_NBR                8
#define CSIZ_PORCHES_ENCL_FRAME_NBR                8
#define CSIZ_PORCHES_ENCL_BRICK_NBR                8
#define CSIZ_PORCHES_OPEN_BRICK_SQFT               8
#define CSIZ_PORCHES_ENCL_FRAME_SQFT               8
#define CSIZ_PORCHES_ENCL_BRICK_SQFT               8
#define CSIZ_PERCENT_FLOOR_TILE                    3
#define CSIZ_PERCENT_FLOOR_HARDWOOD                3
#define CSIZ_PERCENT_FLOOR_SOFTWOOD                3
#define CSIZ_PERCENT_FLOOR_CARPET                  3
#define CSIZ_PERCENT_FLOOR_VINYL                   3
#define CSIZ_PERCENT_FLOOR_MARBLE                  3
#define CSIZ_PERCENT_FLOOR_OTHER                   3
#define CSIZ_ROUGHED_IN_FIXTURE                    1
#define CSIZ_ROUGHED_IN_FIXTURE_NBR                3
#define CSIZ_FIREPLACE2_TYPE                       40
#define CSIZ_FIREPLACE2_NBR                        2
#define CSIZ_FIREPLACE2                            1
#define CSIZ_DIF_CALC_BASE_COST                    1
#define CSIZ_DIF_CALC_BASE_COST_AMT                10
#define CSIZ_BLDG_ADDR_LINE1                       100
#define CSIZ_BLDG_ADDR_LINE2                       100
#define CSIZ_BLDG_ADDR_CITY                        45
#define CSIZ_BLDG_ADDR_STATE                       2
#define CSIZ_BLDG_ADDR_ZIP                         10
#define CSIZ_INFLUENCE_FACTOR                      11
#define CSIZ_BATH4                                 1
#define CSIZ_NBR_BATHR4                            3
#define CSIZ_BATH5                                 1
#define CSIZ_NBR_BATHR5                            3
#define CSIZ_BATH6                                 1
#define CSIZ_NBR_BATHR6                            3
#define CSIZ_APPLY_INFL_FACT_ALL_COSTS             1
#define CSIZ_CHANGE_TRACKING_NUMBER                6
#define CSIZ_REM_ECO_LIFE                          10
#define CSIZ_BUILD_CLASS                           40
#define CSIZ_STYLE_FACTOR                          11
#define CSIZ_UNITS                                 4


typedef struct _tInyChar
{
   char Apn[CSIZ_APN];
   char Bldg_Nbr[CSIZ_BLDG_NBR];
   char Bldg_Type_Code[CSIZ_BLDG_TYPE_CODE];
   char Bldg_Type_Text[CSIZ_BLDG_TYPE_TEXT];
   char Bldg_Qual[CSIZ_BLDG_QUAL];
   char Filler2[CSIZ_FILLER2];
   char StoryDesc[CSIZ_STORY_DESC];
   char Avg_Bldg_Height[CSIZ_AVG_BLDG_HEIGHT];
   char Dist_Bldg[CSIZ_DIST_BLDG];
   char Ext_Wall_Height[CSIZ_EXT_WALL_HEIGHT];
   char Bldg_Height[CSIZ_BLDG_HEIGHT];
   char Bedrooms[CSIZ_BEDROOMS];
   char Rooms[CSIZ_ROOMS];
   char Constr_Year[CSIZ_CONSTR_YEAR];
   char Eff_Age[CSIZ_EFF_AGE];
   char Foundation_Type[CSIZ_FOUNDATION_TYPE];
   char Bldg_Type[CSIZ_BLDG_TYPE];
   char Filler4[CSIZ_FILLER4];
   char Ext_Frame[CSIZ_EXT_FRAME];
   char Ext_Brick[CSIZ_EXT_BRICK];
   char Ext_Stucco[CSIZ_EXT_STUCCO];
   char Ext_Block[CSIZ_EXT_BLOCK];
   char Ext_Steel[CSIZ_EXT_STEEL];
   char Ext_Aluminum[CSIZ_EXT_ALUMINUM];
   char Ext_Stone[CSIZ_EXT_STONE];
   char Ext_Other[CSIZ_EXT_OTHER];
   char Deck[CSIZ_DECK];
   char Deck_Length[CSIZ_DECK_LENGTH];
   char Deck_Width[CSIZ_DECK_WIDTH];
   char Railing_Deck[CSIZ_RAILING_DECK];
   char Open_Porch[CSIZ_OPEN_PORCH];
   char Porch_Sqft[CSIZ_PORCH_SQFT];
   char Porch_Frame[CSIZ_PORCH_FRAME];
   char Enclosed_Porch1[CSIZ_ENCLOSED_PORCH1];
   char Porch_Brick[CSIZ_PORCH_BRICK];
   char Enclosed_Porch_Frame[CSIZ_ENCLOSED_PORCH_FRAME];
   char Roof_Type_Flat[CSIZ_ROOF_TYPE_FLAT];
   char Roof_Type_Gable[CSIZ_ROOF_TYPE_GABLE];
   char Roof_Type_Hip[CSIZ_ROOF_TYPE_HIP];
   char Roof_Type_Gambrel[CSIZ_ROOF_TYPE_GAMBREL];
   char Roof_Type_Mansard[CSIZ_ROOF_TYPE_MANSARD];
   char Roof_Type_Canopy[CSIZ_ROOF_TYPE_CANOPY];
   char Roof_Type_Other[CSIZ_ROOF_TYPE_OTHER];
   char Roof_Material_Asphalt[CSIZ_ROOF_MATERIAL_ASPHALT];
   char Roof_Material_Wood[CSIZ_ROOF_MATERIAL_WOOD];
   char Roof_Material_Tile[CSIZ_ROOF_MATERIAL_TILE];
   char Roof_Material_Metal[CSIZ_ROOF_MATERIAL_METAL];
   char Roof_Material_Other[CSIZ_ROOF_MATERIAL_OTHER];
   char Floor_Const_Wood[CSIZ_FLOOR_CONST_WOOD];
   char Floor_Const_Concrete[CSIZ_FLOOR_CONST_CONCRETE];
   char Floor_Const_Reinf[CSIZ_FLOOR_CONST_REINF];
   char Floor_Const_Metal[CSIZ_FLOOR_CONST_METAL];
   char Floor_Const_Other[CSIZ_FLOOR_CONST_OTHER];
   char Floor_Finish_Tile[CSIZ_FLOOR_FINISH_TILE];
   char Floor_Finish_Hardwood[CSIZ_FLOOR_FINISH_HARDWOOD];
   char Floor_Finish_SoftWood[CSIZ_FLOOR_FINISH_SOFTWOOD];
   char Floor_Finish_Carpet[CSIZ_FLOOR_FINISH_CARPET];
   char Floor_Finish_Vinyl[CSIZ_FLOOR_FINISH_VINYL];
   char Floor_Finish_Marble[CSIZ_FLOOR_FINISH_MARBLE];
   char Floor_Finish_Other[CSIZ_FLOOR_FINISH_OTHER];
   char Int_Wall_Finish_Drywall[CSIZ_INT_WALL_FINISH_DRYWALL];
   char Int_Wall_Finish_Plaster[CSIZ_INT_WALL_FINISH_PLASTER];
   char Int_Wall_Finish_Wood[CSIZ_INT_WALL_FINISH_WOOD];
   char Int_Wall_Finish_Other[CSIZ_INT_WALL_FINISH_OTHER];
   char Unfin_Interior[CSIZ_UNFIN_INTERIOR];
   char Percent_Unfin_Interior[CSIZ_PERCENT_UNFIN_INTERIOR];
   char Unfin_Interior_Sqft[CSIZ_UNFIN_INTERIOR_SQFT];
   char Fin_Office[CSIZ_FIN_OFFICE];
   char Percent_Fin_Office[CSIZ_PERCENT_FIN_OFFICE];
   char Fin_Office_Sqft[CSIZ_FIN_OFFICE_SQFT];
   char Ceil_Type_Vaulted[CSIZ_CEIL_TYPE_VAULTED];
   char Ceil_Type_Cathedral[CSIZ_CEIL_TYPE_CATHEDRAL];
   char Ceil_Type_Coffer[CSIZ_CEIL_TYPE_COFFER];
   char Ceil_Type_Tray[CSIZ_CEIL_TYPE_TRAY];
   char Ceil_Type_Gambrel[CSIZ_CEIL_TYPE_GAMBREL];
   char Ceil_Type_Suspended[CSIZ_CEIL_TYPE_SUSPENDED];
   char Ceil_Type_Metal[CSIZ_CEIL_TYPE_METAL];
   char Ceil_Type_MetalInsulated[CSIZ_CEIL_TYPE_METALINSULATED];
   char Ceil_Type_Other[CSIZ_CEIL_TYPE_OTHER];
   char Bath1[CSIZ_BATH1];                         // Full bath
   char Nbr_Bathr1[CSIZ_NBR_BATHR1];
   char Bath2[CSIZ_BATH1];                         // Half bath
   char Nbr_Bathr2[CSIZ_NBR_BATHR1];
   char Bath3[CSIZ_BATH1];
   char Nbr_Bathr3[CSIZ_NBR_BATHR1];
   char Whirlpool[CSIZ_WHIRLPOOL];
   char Nbr_Whirlpools[CSIZ_NBR_WHIRLPOOLS];
   char Spa[CSIZ_SPA];
   char Nbr_Spas[CSIZ_NBR_SPAS];
   char Sauna[CSIZ_SAUNA];
   char Nbr_Saunas[CSIZ_NBR_SAUNAS];
   char Wet_Sprinkler[CSIZ_WET_SPRINKLER];
   char Nbr_Wet_Sprinkler[CSIZ_NBR_WET_SPRINKLER];
   char Dry_Sprinkler[CSIZ_DRY_SPRINKLER];
   char Nbr_Dry_Sprinkler[CSIZ_NBR_DRY_SPRINKLER];
   char Other_Plumbing[CSIZ_OTHER_PLUMBING];
   char Nbr_Other_Plumbing[CSIZ_NBR_OTHER_PLUMBING];
   char Stove_Heating[CSIZ_STOVE_HEATING];
   char Gas_Heating[CSIZ_GAS_HEATING];
   char Electric_Heating[CSIZ_ELECTRIC_HEATING];
   char Steam_Heating[CSIZ_STEAM_HEATING];
   char Hot_Water_Heating[CSIZ_HOT_WATER_HEATING];
   char Heat_Pumpthere_Heating[CSIZ_HEAT_PUMPTHERE_HEATING];
   char Space_Heater_Heating[CSIZ_SPACE_HEATER_HEATING];
   char Central_Air[CSIZ_CENTRAL_AIR];
   char Nbr_Central_Air[CSIZ_NBR_CENTRAL_AIR];
   char Other_Heating_Or_AC[CSIZ_OTHER_HEATING_OR_AC];
   char Basement[CSIZ_BASEMENT];
   char Full_Basement[CSIZ_FULL_BASEMENT];
   char Full_Basement_Sqft[CSIZ_FULL_BASEMENT_SQFT];
   char Partial_Basement[CSIZ_PARTIAL_BASEMENT];
   char Partial_Basement_Sqft[CSIZ_PARTIAL_BASEMENT_SQFT];
   char Percent_Finbasement[CSIZ_PERCENT_FINBASEMENT];
   char Lower_Basement[CSIZ_LOWER_BASEMENT];
   char LowerBasement_Sqft[CSIZ_LOWERBASEMENT_SQFT];
   char Percent_Lower_Basement[CSIZ_PERCENT_LOWER_BASEMENT];
   char Crawl_Space[CSIZ_CRAWL_SPACE];
   char Crawl_Space_Sqft[CSIZ_CRAWL_SPACE_SQFT];
   char Asement_Floor_Type[CSIZ_ASEMENT_FLOOR_TYPE];
   char No_Attic[CSIZ_NO_ATTIC];
   char Fin_Attic[CSIZ_FIN_ATTIC];
   char Fin_Attic_Sqft[CSIZ_FIN_ATTIC_SQFT];
   char Unfin_Attic[CSIZ_UNFIN_ATTIC];
   char Unfin_Attic_Sqft[CSIZ_UNFIN_ATTIC_SQFT];
   char No_Elevator[CSIZ_NO_ELEVATOR];
   char Freight_Elevator[CSIZ_FREIGHT_ELEVATOR];
   char Nbr_Freight_Elevator[CSIZ_NBR_FREIGHT_ELEVATOR];
   char Passenger_Elevator[CSIZ_PASSENGER_ELEVATOR];
   char Nbr_Passenger_Elevators[CSIZ_NBR_PASSENGER_ELEVATORS];
   char Detached_Garage[CSIZ_DETACHED_GARAGE];
   char Car_Garage1[CSIZ_CAR_GARAGE1];
   char Car_Garage2[CSIZ_CAR_GARAGE2];
   char Car_Garage3[CSIZ_CAR_GARAGE3];
   char Car_Garage4[CSIZ_CAR_GARAGE4];
   char Garage_Sqft[CSIZ_GARAGE_SQFT];
   char Garage_Brick[CSIZ_GARAGE_BRICK];
   char Garage_Frame[CSIZ_GARAGE_FRAME];
   char Garage_Other[CSIZ_GARAGE_OTHER];
   char Black_Top_Area[CSIZ_BLACK_TOP_AREA];
   char Fenced_Area[CSIZ_FENCED_AREA];
   char Concrete_Area[CSIZ_CONCRETE_AREA];
   char Greenbelt_Area[CSIZ_GREENBELT_AREA];
   char Other_Improv1[CSIZ_OTHER_IMPROV1];
   char Loading_Dock[CSIZ_LOADING_DOCK];
   char Loading_Dock_Nbr[CSIZ_LOADING_DOCK_NBR];
   char Dock_Overhead_Doors_Nbr[CSIZ_DOCK_OVERHEAD_DOORS_NBR];
   char Shed[CSIZ_SHED];
   char Sheds_Nbr[CSIZ_SHEDS_NBR];
   char Shed_Sqft[CSIZ_SHED_SQFT];
   char In_Ground_Pool[CSIZ_IN_GROUND_POOL];
   char In_Ground_Pool_Nbr[CSIZ_IN_GROUND_POOL_NBR];
   char In_Ground_Pool_Sqft[CSIZ_IN_GROUND_POOL_SQFT];
   char Dif_Detached_Garage[CSIZ_DIF_DETACHED_GARAGE];
   char Detached_Garages_Nbr[CSIZ_DETACHED_GARAGES_NBR];
   char Detached_Garages_Sqft[CSIZ_DETACHED_GARAGES_SQFT];
   char Cars_Detached_Garages[CSIZ_CARS_DETACHED_GARAGES];
   char Guest_House[CSIZ_GUEST_HOUSE];
   char Guest_House_Nbr[CSIZ_GUEST_HOUSE_NBR];
   char Guest_House_Sqft[CSIZ_GUEST_HOUSE_SQFT];
   char Stable[CSIZ_STABLE];
   char Stable_Sqft[CSIZ_STABLE_SQFT];
   char Stable_Nbr[CSIZ_STABLE_NBR];
   char Stalls_Stable_Nbr[CSIZ_STALLS_STABLE_NBR];
   char Water_Stable[CSIZ_WATER_STABLE];
   char Tennis_Court[CSIZ_TENNIS_COURT];
   char Tennis_Court_Nbr[CSIZ_TENNIS_COURT_NBR];
   char Tennis_Court_Sqft[CSIZ_TENNIS_COURT_SQFT];
   char Other_Improv2[CSIZ_OTHER_IMPROV2];
   char Other_Improv_Nbr[CSIZ_OTHER_IMPROV_NBR];
   char Other_Improv_Sqft[CSIZ_OTHER_IMPROV_SQFT];
   char Fireplace[CSIZ_FIREPLACE];
   char Desc_Fireplace[CSIZ_DESC_FIREPLACE];
   char Fireplace_Nbr[CSIZ_FIREPLACE_NBR];
   char Ground_Bldg_Sqft[CSIZ_GROUND_BLDG_SQFT];
   char Usable_Bldg_Sqft[CSIZ_USABLE_BLDG_SQFT];
   char Ground_Biggest_Bldg_Sqft[CSIZ_GROUND_BIGGEST_BLDG_SQFT];
   char Deck_Sqft[CSIZ_DECK_SQFT];
   char Bldg_Cond_Class[CSIZ_BLDG_COND_CLASS];
   char Percent_Frame[CSIZ_PERCENT_FRAME];
   char Percent_Brick[CSIZ_PERCENT_BRICK];
   char Percent_Stucco[CSIZ_PERCENT_STUCCO];
   char Percent_Block[CSIZ_PERCENT_BLOCK];
   char Percent_Steel[CSIZ_PERCENT_STEEL];
   char Percent_Aluminum[CSIZ_PERCENT_ALUMINUM];
   char Percent_Stone[CSIZ_PERCENT_STONE];
   char Percent_Other[CSIZ_PERCENT_OTHER];
   char Vinyl[CSIZ_VINYL];
   char Percent_Vinyl[CSIZ_PERCENT_VINYL];
   char Enclosed_Porch2[CSIZ_ENCLOSED_PORCH2];
   char Porches_Open_Frame_Nbr[CSIZ_PORCHES_OPEN_FRAME_NBR];
   char Porches_Open_Brick_Nbr[CSIZ_PORCHES_OPEN_BRICK_NBR];
   char Porches_Encl_Frame_Nbr[CSIZ_PORCHES_ENCL_FRAME_NBR];
   char Porches_Encl_Brick_Nbr[CSIZ_PORCHES_ENCL_BRICK_NBR];
   char Porches_Open_Brick_Sqft[CSIZ_PORCHES_OPEN_BRICK_SQFT];
   char Porches_Encl_Frame_Sqft[CSIZ_PORCHES_ENCL_FRAME_SQFT];
   char Porches_Encl_Brick_Sqft[CSIZ_PORCHES_ENCL_BRICK_SQFT];
   char Percent_Floor_Tile[CSIZ_PERCENT_FLOOR_TILE];
   char Percent_Floor_Hardwood[CSIZ_PERCENT_FLOOR_HARDWOOD];
   char Percent_Floor_SoftWood[CSIZ_PERCENT_FLOOR_SOFTWOOD];
   char Percent_Floor_Carpet[CSIZ_PERCENT_FLOOR_CARPET];
   char Percent_Floor_Vinyl[CSIZ_PERCENT_FLOOR_VINYL];
   char Percent_Floor_Marble[CSIZ_PERCENT_FLOOR_MARBLE];
   char Percent_Floor_Other[CSIZ_PERCENT_FLOOR_OTHER];
   char Roughed_In_Fixture[CSIZ_ROUGHED_IN_FIXTURE];
   char Roughed_In_Fixture_Nbr[CSIZ_ROUGHED_IN_FIXTURE_NBR];
   char Fireplace2_Type[CSIZ_FIREPLACE2_TYPE];
   char Fireplace2_Nbr[CSIZ_FIREPLACE2_NBR];
   char Fireplace2[CSIZ_FIREPLACE2];
   char Dif_Calc_Base_Cost[CSIZ_DIF_CALC_BASE_COST];
   char Dif_Calc_Base_Cost_Amt[CSIZ_DIF_CALC_BASE_COST_AMT];
   char Bldg_Addr_Line1[CSIZ_BLDG_ADDR_LINE1];
   char Bldg_Addr_Line2[CSIZ_BLDG_ADDR_LINE2];
   char Bldg_Addr_City[CSIZ_BLDG_ADDR_CITY];
   char Bldg_Addr_State[CSIZ_BLDG_ADDR_STATE];
   char Bldg_Addr_Zip[CSIZ_BLDG_ADDR_ZIP];
   char Influence_Factor[CSIZ_INFLUENCE_FACTOR];
   char Bath4[CSIZ_BATH1];
   char Nbr_Bathr4[CSIZ_NBR_BATHR1];
   char Bath5[CSIZ_BATH1];
   char Nbr_Bathr5[CSIZ_NBR_BATHR1];
   char Bath6[CSIZ_BATH1];
   char Nbr_Bathr6[CSIZ_NBR_BATHR1];
   char Apply_Infl_Fact_All_Costs[CSIZ_APPLY_INFL_FACT_ALL_COSTS];
   char Change_Tracking_Number[CSIZ_CHANGE_TRACKING_NUMBER];
   char Remaining_Economic_Life[CSIZ_REM_ECO_LIFE];
	char Build_Class[CSIZ_BUILD_CLASS];
	char Style_Factor[CSIZ_STYLE_FACTOR];
	char Units[CSIZ_UNITS];
} INY_CHAR;


/*
#define SOFF_APN                       1-1
#define SOFF_FILLER1                   14-1
#define SOFF_DOCDATE                   19-1
#define SOFF_DOCTAX                    27-1
#define SOFF_DOCNUM                    38-1
#define SOFF_FILLER2                   47-1
#define SOFF_SELLER                    56-1
#define SOFF_FILLER3                   84-1
#define SOFF_DOCTYPE                   156-1
#define SOFF_FILLER4                   158-1
#define SOFF_DOCDESC                   161-1

#define SSIZ_APN                       13
#define SSIZ_FILLER1                   5
#define SSIZ_DOCDATE                   8
#define SSIZ_DOCTAX                    11
#define SSIZ_DOCNUM                    9
#define SSIZ_FILLER2                   9
#define SSIZ_SELLER                    28
#define SSIZ_FILLER3                   72
#define SSIZ_DOCTYPE                   2
#define SSIZ_FILLER4                   3
#define SSIZ_DOCDESC                   30


typedef struct _tInySale
{
   char  Apn[SSIZ_APN];
   char  Filler1[SSIZ_FILLER1];
   char  DocDate[SSIZ_DOCDATE];
   char  DocTax[SSIZ_DOCTAX];
   char  DocNum[SSIZ_DOCNUM];
   char  Filler2[SSIZ_FILLER2];
   char  Seller[SSIZ_SELLER];
   char  Filler3[SSIZ_FILLER3];
   char  DocType[SSIZ_DOCTYPE];
   char  Filler4[SSIZ_FILLER4];
   char  DocDesc[SSIZ_DOCDESC];
} INY_SALE;
*/

#define SOFF_APN                       1-1
#define SOFF_DOCDATE                   19-1
#define SOFF_DOCTAX                    27-1
#define SOFF_DOCNUM                    38-1
#define SOFF_SELLER                    56-1
#define SOFF_DOCTYPE                   156-1
#define SOFF_FILLER4                   158-1
#define SOFF_DOCDESC                   161-1
#define SOFF_MULTISALE                 191-1

#define SSIZ_APN                       18
#define SSIZ_DOCDATE                   8
#define SSIZ_DOCTAX                    11
#define SSIZ_DOCNUM                    18
#define SSIZ_SELLER                    100
#define SSIZ_DOCTYPE                   2
#define SSIZ_FILLER4                   3
#define SSIZ_DOCDESC                   30
#define SSIZ_MULTISALE                 1

typedef struct _tInySale
{
   char  Apn[SSIZ_APN];
   char  DocDate[SSIZ_DOCDATE];
   char  DocTax[SSIZ_DOCTAX];
   char  DocNum[SSIZ_DOCNUM];
   char  Seller[SSIZ_SELLER];
   char  DocType[SSIZ_DOCTYPE];
   char  Filler4[SSIZ_FILLER4];
   char  DocDesc[SSIZ_DOCDESC];
   char  MultiSale[SSIZ_MULTISALE];
   char  CRLF[2];
} INY_SALE;

#define TOFF_APN                       1-1
#define TOFF_BILLNUM                   11-1
#define TOFF_TOTALTAX                  17-1
#define TOFF_INSTAMT_1                 27-1
#define TOFF_INSTAMT_2                 37-1
#define TOFF_DEFAULTDT                 47-1
#define TOFF_DELQ_STATUS               55-1
#define TOFF_PAIDDT_1                  57-1
#define TOFF_PAIDDT_2                  63-1

#define TSIZ_APN                       10
#define TSIZ_BILLNUM                   6
#define TSIZ_TOTALTAX                  10
#define TSIZ_INSTAMT                   10
#define TSIZ_DEFAULTDT                 8
#define TSIZ_DELQ_STATUS               2
#define TSIZ_PAIDDT                    6

typedef struct _tCorTac
{
   char  Apn[TSIZ_APN];
   char  BillNum[TSIZ_BILLNUM];
   char  TotalTax[TSIZ_TOTALTAX];
   char  InstAmt1[TSIZ_INSTAMT];
   char  InstAmt2[TSIZ_INSTAMT];
   char  DefaultDt[TSIZ_DEFAULTDT];
   char  Delq_Status[TSIZ_DELQ_STATUS];
   char  PaidDt1[TSIZ_PAIDDT];
   char  PaidDt2[TSIZ_PAIDDT];
   char  CRLF[2];
} CORTAC;

// 2020 Secured Roll.csv
#define V_TAXYEAR                      0
#define V_APN                          1
#define V_EVENTTYPE                    2
#define V_EVENTDATE                    3
#define V_ROLLCASTE                    4
#define V_ASMTTYPE                     5
#define V_TRANTYPE                     6
#define V_CHANGEREASON                 7
#define V_LAND                         8
#define V_IMPROVEMENTS                 9
#define V_LIVINGIMPROVEMENTS           10
#define V_PERSONALPROPERTY             11
#define V_TRADEFIXTURES                12
#define V_ASSESSEDVALUE                13
#define V_HOMEOWNEREXEMPTIONS          14
#define V_ALLOTHEREXEMPTIONS           15
#define V_PENALTY                      16
#define V_PREVIOUSNETTAXABLE           17
#define V_CURRENTTAXABLEVALUE          18
#define V_DIFFERENCEINNETTAXABLE       19
#define V_ASMTEVENTID                  20
#define V_ASMTID                       21
#define V_ASMTTRANID                   22
#define V_FLDS                         23

// 2020 Annual Assessments with TAG.CSV
#define L_APN                          0
#define L_YEARFROM                     1
#define L_YEARTO                       2
#define L_PROPTYPE                     3
#define L_ASMTTYPE                     4
#define L_TRA                          5
#define L_PROPDESC                     6
#define L_UNKNOWN                      7
#define L_OWNERX                       8     // Owner name - last name first
#define L_OWNER                        9
#define L_MADDR1                       10
#define L_MADDR2                       11
#define L_SADDR1                       12
#define L_SCITY                        13
#define L_SZIP                         14
#define L_SITUS                        15
#define L_X1                           16
#define L_X2                           17
#define L_HOEXE                        18
#define L_EXE1                         19
#define L_EXE2                         20
#define L_EXE3                         21
#define L_LAND                         22
#define L_IMPR                         23
#define L_PPVAL                        24
#define L_FIXTURES                     25
#define L_LIVINGIMPR                   26
#define L_ASSDVAL                      27
#define L_TOTALEXE                     28
#define L_NETVAL                       29
#define L_ASMTTRANSID                  30
#define L_ASMTID                       31
#define L_ASMTEVENTID                  32
#define L_FLDS                         33

// AssessmentRollData.csv
#define R_PIN                          0
#define R_CLASSTYPE                    1
#define R_USECODEDESC                  2
#define R_DBA                          3
#define R_OWNER                        4
#define R_M_ADDRESS                    5
#define R_M_CITY                       6
#define R_M_STATE                      7
#define R_M_ZIPCODE                    8
#define R_S_STREETNUM                  9
#define R_S_STREETSUB                  10
#define R_S_STREETNAME                 11
#define R_S_CITY                       12
#define R_LEGAL                        13
#define R_TRA                          14
#define R_LOTTYPE                      15
#define R_ACREAGE                      16
#define R_ASSESSEE                     17
#define R_LAND                         18
#define R_IMPR                         19
#define R_LIVINGIMPR                   20
#define R_PPVALUE                      21
#define R_FIXTURES                     22
#define R_HOX                          23
#define R_VET_EXE                      24
#define R_DVET_EXE                     25
#define R_CHURCH_EXE                   26
#define R_REL_EXE                      27
#define R_CEMETERY_EXE                 28
#define R_PUBSCHOOL_EXE                29
#define R_PUBLIBRAY_EXE                30
#define R_PUBMUSEUM_EXE                31
#define R_WELCOLL_EXE                  32
#define R_WELHOSP_EXE                  33
#define R_WELPRISCH_EXE                34
#define R_WELCHARREL_EXE               35
#define R_OTHER_EXE                    36
#define R_FLDS                         37

// SaleData.csv
#define S_PIN                          0
#define S_GEO                          1
#define S_DOCMONTH                     2
#define S_DOCYEAR                      3
#define S_DOCNUM                       4
#define S_DOCDATE                      5
#define S_DOCTYPE                      6
#define S_SALEPRICE                    7
#define S_USECODE                      8
#define S_BASEYEAR                     9
#define S_S_LINE1                      10
#define S_S_LINE2                      11
#define S_ACRES                        12
#define S_BLDGAREA                     13
#define S_EFF_AGE                      14
#define S_CARPORT                      15
#define S_ATTACHED                     16
#define S_DETACHED                     17
#define S_POOL                         18
#define S_BEDROOMS                     19
#define S_BATH_H                       20
#define S_BATH_3Q                      21
#define S_BATH_F                       22
#define S_TOTALBATHS                   23
#define S_LAND                         24
#define S_STRUCTURE                    25
#define S_FLDS                         26

// CharacteristicsData.csv
#define C_PIN                          0
#define C_S_STRNUM                     1
#define C_S_STRNAME                    2
#define C_S_ZIPCODE                    3
#define C_S_CITYNAME                   4
#define C_EFFYRBLT                     5
#define C_STORIES                      6
#define C_ROOFTYPE                     7
#define C_GARAGETYPE                   8
#define C_GARAGEAREA                   9
#define C_CARPORTSIZE                  10
#define C_GARAGE2TYPE                  11
#define C_GARAGE2AREA                  12
#define C_CARPORT2SIZE                 13
#define C_HASFIREPLACE                 14
#define C_BATHSFULL                    15
#define C_BATHS3_4                     16
#define C_BATHSHALF                    17
#define C_TOTALBATHS                   18
#define C_HASCENTRALHEATING            19
#define C_HASCENTRALCOOLING            20
#define C_DESIGNTYPE                   21
#define C_CONSTRUCTIONTYPE             22
#define C_QUALITYCODE                  23
#define C_SHAPECODE                    24
#define C_LIVINGAREA                   25
#define C_ACTUALAREA                   26
#define C_HASPOOL                      27
#define C_BEDROOMCOUNT                 28
#define C_FLDS                         29

static XLAT_CODE  asRoofType[] =
{
   // Value, lookup code, value length
   "FLAT",     "F", 3,      // Flat
   "GABLE",    "G", 3,      // Gable
   "GAMBREL",  "N", 3,      // Gambrel
   "HIP",      "H", 3,      // Hip
   "MANSARD",  "M", 3,      // Mansard
   "OTHER",    "J", 3,      // Others
   "",   "",  0
};

IDX_TBL4 Iny_Vesting[] =
{
   " TRUST,",        "TR", 5, 2,
   " TRUST ",        "TR", 5, 2,
   " TR,",           "TR", 4, 2,
   " TRS ",          "TR", 4, 2,
   " TRST,",         "TR", 4, 2,
   " LIFE ESTATE,",  "LE", 9, 2,
   " LIFE EST,",     "LE", 9, 2,
   " ESTATE OF,",    "LE", 7, 2,
   " ESTATE OF",     "LE", 7, 2,
   " EST OF",        "LE", 7, 2,
   " M D,",          "MD", 4, 2,
   " MD,",           "MD", 4, 2,
   " MD INC,",       "MD", 4, 2,
   " RPT,",          "RP", 4, 2,
   " BENF",          "BE", 6, 2,
   " ETAL",          "EA", 5, 2,
   " ET AL",         "EA", 6, 2,
   " TRUSTEE",       "TE", 7, 2,
   " CSD",           "CS", 6, 2,
   " PROPERTY TRST", "PR", 10,2,
   "", "", 0, 0
};

// If not match Vesting table, check last word using this table
IDX_TBL4 Iny_VestingLW[] =
{
   " L EST",      "LE", 6, 2,
   " ESTATE",     "LE", 7, 2,
   " HWCP",       "CP", 5, 2,
   " HWJT",       "JT", 5, 2,
   " TR",         "TR", 3, 2,
   " CP",         "CP", 3, 2,
   " JT",         "JT", 3, 2,
   " TC",         "TC", 3, 2,
   "", "", 0, 0
};

IDX_TBL5 INY_DocCode[] =
{  // DocCode, Index, Non-sale, len1, len2
   "01", "1 ", 'N', 2, 2,     // GENERAL TRANSFER
   "02", "74", 'N', 2, 2,     // TO PERFECT/CLEAR TITLE
   "03", "57", 'N', 2, 2,     // PARTIAL INTEREST
   "04", "13", 'Y', 2, 2,     // DISTRIBUTE ESTATE
   "05", "74", 'Y', 2, 2,     // DEATH OF OWNER
   "06", "13", 'Y', 2, 2,     // PROP-60 BYV TRANSFER
   "07", "74", 'Y', 2, 2,     // OPTION
   "08", "11", 'N', 2, 2,     // CONTRACT OF SALE
   "09", "74", 'Y', 2, 2,     // COMPLETION CONTRACT
   "10", "74", 'Y', 2, 2,     // NEW TENANT
   "11", "74", 'Y', 2, 2,     // TENANT TO VACANT
   "19", "74", 'Y', 2, 2,     // CORP CONTROL
   "20", "74", 'Y', 2, 2,     // PARTNERSHIP
   "25", "67", 'N', 2, 2,     // TAX DEED
   "26", "77", 'N', 2, 2,     // FORECLOSURE/TRUSTEE SALE
   "27", "78", 'N', 2, 2,     // IN LIEU FORECLOSURE
   "28", "51", 'Y', 2, 2,     // NAME CHANGE
   "29", "44", 'Y', 2, 2,     // NEW/SUB-LEASE
   "30", "44", 'Y', 2, 2,     // LEASE ASSIGNMENT
   "31", "44", 'Y', 2, 2,     // LEASE ACTIVITY
   "32", "74", 'Y', 2, 2,     // END/NEW  ANTICIPATED TERM
   "33", "44", 'Y', 2, 2,     // MEMORANDUM OF LEASE
   "34", "74", 'Y', 2, 2,     // NEW ANTICIPATED TERM
   "35", "44", 'Y', 2, 2,     // LEASE /OPTION TO BUY
   "36", "44", 'Y', 2, 2,     // LEASE/OPT EXPIRES
   "37", "44", 'Y', 2, 2,     // TERMINATION LEASE
   "39", "74", 'Y', 2, 2,     // LSR INT W/ 35+ YR REMAIN
   "40", "74", 'Y', 2, 2,     // INTO LIFE ESTATE
   "41", "74", 'Y', 2, 2,     // DEATH LIFE TENANT- PROP 58
   "42", "74", 'Y', 2, 2,     // REVOCATION OF LIFE ESTATE
   "49", "6 ", 'Y', 2, 2,     // AFFIDAVIT DEATH
   "50", "13", 'Y', 2, 2,     // OTHER INTERESTS
   "51", "74", 'Y', 2, 2,     // MISC REC DOCS
   "60", "18", 'Y', 2, 2,     // INTERSPOUSAL
   "61", "18", 'Y', 2, 2,     // INTERSPOUSAL DIVORCE
   "62", "74", 'Y', 2, 2,     // DEATH- PROP58 GRANTED
   "63", "74", 'Y', 2, 2,     // DEATH OF SPOUSE
   "64", "13", 'Y', 2, 2,     // PARENT/CHILD TRANSFER
   "65", "13", 'Y', 2, 2,     // GRANDPARENT/GRANDCHILD
   "66", "74", 'Y', 2, 2,     // TAX POSTPONMENT
   "67", "74", 'Y', 2, 2,     // REALEASE TAX POSTPONE
   "70", "57", 'Y', 2, 2,     // NO CHANGE PROPORTIONAL INT
   "72", "3 ", 'Y', 2, 2,     // PARTITION TENANCY IN COMMON
   "74", "3 ", 'Y', 2, 2,     // TERMINATE ORIG JT INT
   "75", "3 ", 'Y', 2, 2,     // CREATE JT INCL ONE JT
   "76", "3 ", 'Y', 2, 2,     // TERMINATION JT TO ORIG
   "77", "74", 'Y', 2, 2,     // < 5% INTEREST AND < $10,000
   "80", "74", 'Y', 2, 2,     // INTO TRUST
   "81", "74", 'Y', 2, 2,     // INTO/OUT OF TRUST
   "82", "74", 'Y', 2, 2,     // OUT OF TRUST
   "83", "74", 'Y', 2, 2,     // CHANGE  TRUSTEES
   "85", "40", 'Y', 2, 2,     // CONSERVATION ESMT
   "86", "74", 'Y', 2, 2,     // SEPERATE ASSMT
   "87", "74", 'Y', 2, 2,     // NOTICE OF VIOLATION
   "88", "74", 'Y', 2, 2,     // ANNEXATION
   "89", "74", 'Y', 2, 2,     // ROAD ABANDONMENT
   "90", "74", 'Y', 2, 2,     // LOT LINE- DEED
   "91", "74", 'Y', 2, 2,     // PATENT
   "92", "74", 'Y', 2, 2,     // PARCEL/TRACT MAP/CONDO PLAN
   "93", "74", 'Y', 2, 2,     // CERTIFICATE OF COMPLIANCE
   "94", "74", 'Y', 2, 2,     // MH ON FOUNDATION
   "95", "74", 'Y', 2, 2,     // NEW SET UP
   "96", "74", 'Y', 2, 2,     // RECORD OF SURVEY
   "97", "40", 'Y', 2, 2,     // EASEMENTS
   "98", "74", 'Y', 2, 2,     // MERGER/COMBINATION
   "99", "74", 'Y', 2, 2,     // PARCEL SPLIT
   "","",0,0,0
};

#endif
