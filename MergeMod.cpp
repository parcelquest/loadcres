/*****************************************************************************
 * 
 * Commands:
 *    -U -Xsi -Mp   Monthly update
 *    -L -Xl -Ms    Lien update
 *
 * Revision:
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 07/09/2008 8.1.2     Add new function Mod_Load_LDR() and process LDR as provided
 *                      by the county.
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 08/04/2009 9.1.3     Populate other values.
 * 07/02/2010 10.1.1    Start using loadMod().
 * 06/28/2011 11.0.0    Modify Mod_MergeAdr() to populate S_ADDR_D, S_HSENO, S_ZIP
 *                      from city file if city name avail. This requires regular 
 *                      update of zipcode in MOD_CITY.N2CZ.
 * 07/29/2011 11.1.4    Clear situs before update in Mod_MergeAdr()
 * 10/26/2011 11.2.8.1  Add Mod_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      Modify Mod_Fmt1Doc() to correct DocNum. Use ApplyCumSale()
 *                      to update R01 file. Modify Mod_CreateR01() not to populate
 *                      sale info.  Let ApplyCumSale() populates it from Mod_Sale.sls.
 * 11/21/2011 11.3.10   Add option to load tax file.
 * 06/03/2013 12.6.4    Add -Mp option to include unassessed parcels
 *            12.6.4.3  Remove single quote from owner name.
 * 06/05/2013 12.6.5    Change APN extension for unassessed parcels from "00" to "11" 
 *                      to match with county.
 * 09/27/2013 13.3.0    Modify Mod_MergeOwner() and add Mod_CleanName() to update Vesting.
 *                      Also added additional chars data (awaiting xlat table).
 *                      Use removeNames() to clear owner names.
 * 09/29/2013 13.3.1    Remove comma from owner name
 * 10/02/2013 13.3.3    Update A/C, Pool, Fp, MiscImpr, and Heating (use code desc from TEH)
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 08/13/2014 14.0.3    Modify Mod_CreateR01() to add more translation code for HEAT.
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 *                      Also fix Mod_MergeAdr() when ADR1 is in NAME2 field.
 * 12/03/2015 15.2.1    Modify loadMod() to send email to developer when loading tax failed.
 * 08/18/2016 16.2.0    Add option to load Tax Agency from external text file.
 * 09/30/2016 16.3.1    Sort and dedup Public Parcels before processing to avoid duplicate entry.
 * 10/04/2016 16.3.2.1  See Mod_CreatePublR01() for comment (BO case)
 * 10/25/2016 16.5.1    Fix possible crashing bug in loadMod() by increase sTmp buffer.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "doSort.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Pq.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "SendMail.h"
#include "Tax.h"

// Unassessed
#define  MOD_UA_APN           0
#define  MOD_UA_NAME1         1
#define  MOD_UA_NAME2         2
#define  MOD_UA_LOCATION      3
#define  MOD_UA_ACRES         4
#define  MOD_UA_REM1          5
#define  MOD_UA_REM2          6
#define  MOD_UA_REM3          7

/********************************* Mod_Fmt1Doc *******************************
 *
 *   1) 0420486 05221996= 420-486 & 19960521
 *   2) 0001842 05012001=    1842 & 20010512
 *   3) 457293 09221999 = 457-293 & 19990922
 *   4) 000295007212000 =    2950 & 20000721
 *   5) 0000159 1112005 =     159 & 20050111
 *   6) PAR MAP 1-111
 *   7) 0456763 0907199
 *
 *****************************************************************************/

void Mod_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp;
   long  lTmp;
   char  acDate[16], acDoc[64], *pTmp, *pDate;
   bool  bMisAligned = false;

   pDate = pRecDoc+8;
   acDoc[0] = 0;

   // Input 12345678mmddyyyy
   // Output yyyymmdd123456789AB
   if (*pDate == ' ')
   {
      memset(pOutDate, ' ', 8);
      return;
   } else
   {
      if (*(pDate+7) == ' ')
      {  
         if (*(pDate-1) != ' ')
         {
            sprintf(acDate, "%.4s%.4s", pDate+3, pDate-1);  // Case #3 & #4
            if (*(pDate-2) == ' ')
               bMisAligned = true;
         } else 
            sprintf(acDate, "%.4s0%.3s", pDate+3, pDate);   // Case #5

      } else
         sprintf(acDate, "%.4s%.4s", pDate+4, pDate);

      // Replace 'O' with '0'
      for (iTmp = 0; iTmp < 8; iTmp++)
         if (acDate[iTmp] == 'O')
            acDate[iTmp] = '0';

      lTmp = atol(acDate);
      if (lTmp < 19000101)
      {
         lTmp = 0;
         memset(acDate, ' ', 8);
      }
   }
   memcpy(pOutDate, acDate, 8);

   pTmp = pRecDoc;
   if (*pTmp > ' ' && memcmp(pTmp, "0000000", 7))
   {
      if (lTmp > 20000000)          // Year 2000
      {
         iTmp = atoin(pTmp, 7);
         sprintf(acDoc, "%.7d           ", iTmp);
      } else
      {
         if (bMisAligned)
         {
            iTmp = atoin(pTmp, 3);
            sprintf(acDoc, "%d-%.3s          ", iTmp, pTmp+3);
         } else
         {
            iTmp = atoin(pTmp, 4);
            sprintf(acDoc, "%d-%.3s          ", iTmp, pTmp+4);
         }
      }
      acDoc[SIZ_SALE1_DOC] = 0;
      memcpy(pOutDoc, acDoc, SIZ_SALE1_DOC);
   } else
   {
      memset(pOutDoc, ' ', SIZ_SALE1_DOC);
   }
}

/********************************* Mod_FmtRecDoc *****************************
 *
 *   1) -01651  20040227= 01651 & 20040227: Fern displays doc# as -016 5100
 *   2) 0706061 19981201= This is not doc#, but recorder book 706, page 61.  
 *      Old format can be displayed as 706-061
 *
 *****************************************************************************/

void Mod_FmtRecDoc(char *pOutbuf, char *pRecDoc1)
{
   char  acDate[16], acDoc[64], *pTmp;
   bool  bMisAligned = false;

   pTmp = pRecDoc1;
   Mod_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE1) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

   // Update last recording date
   long lDate = atol(acDate);
   if (lDate > lLastRecDate && lDate < lToday)
      lLastRecDate = lDate;

   // Advance to Rec #2
   pTmp += 16;
   Mod_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE2) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);

   // Advance to Rec #3
   pTmp += 16;
   Mod_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE3) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
}

/******************************** Mod_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mod_MergeOwner1(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acTmp1[64], acSave[64];
   char  acName1[64], acName2[64], acCareOf[64], acVest[8];
   char  *pTmp, *pTmp1, *pTmp2, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);

   // Initialize
   // Point to name2
   pName2 = pNames+CRESIZ_NAME_1;
   acName2[0] = 0;
   acCareOf[0] = 0;
   acVest[0] = 0;

   // Remove '.'
   if (pTmp = strchr(pNames, '.'))
   {  
      while (*pTmp)
      {
         if (*pTmp == '.')
            *pTmp = ' ';
         pTmp++;
      }
   } 

   // Save name1
   strncpy(acName1, pNames, CRESIZ_NAME_1);
   acName1[CRESIZ_NAME_1] = 0;

   // Remove single quote and extra spaces
   remChar(acName1, 39);
   blankRem(acName1);

   // Check owner2 for # and %
   if (*pName2 == '#' )
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acName2, pName2+2);
   } else if (*pName2 == '%' )
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acCareOf, pName2);
   } else if (*pName2 == '&' )
   {  // Name2 is continuation of name1
      strcpy(acName1, pNames);
   } else if (!memcmp(pName2, "C/O", 3))
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acCareOf, pName2);
   } else if (!memcmp(pName2, "ATTN:", 5))
   {
      *pName2 = 0;
      strcpy(acName1, pNames);
      strcpy(acCareOf, pName2);
   } else if (*pName2 > ' ' && strchr(acName1, '&'))
   {
      if (strchr(pName2, ','))
         strcpy(acName2, pName2);
      else
         strcpy(acName1, pNames);
   } else if (!strchr(acName1, ',') && !strchr(pName2, ','))
   {  // HAMZA HOLDINGS LIMITED       PO BOX 236/FIRST ISLAND HOUSE
      if (memcmp(pName2, "PO BOX", 6) && !isdigit(*pName2))
         strcpy(acName1, pNames);
   } else if (strchr(pName2, ','))
   {
      strcpy(acName2, pName2);
   }

   // Check for vesting
   if (pTmp=strstr(acName2, " JT"))
   {
      *pTmp = 0;
      strcpy(acVest, "JT");
   } else if (pTmp=strstr(acName1, " JT"))
   {
      *pTmp = 0;
      strcpy(acVest, "JT");
   }
  
   myTrim(acName1);
   myTrim(acName2);

   // Remove things within parenthesis
   if (pTmp = strchr(acName1, '('))
   {  
      while (*pTmp)
      {
         if (*pTmp == ')')
         {
            *pTmp = ' ';
            break;
         } else
            *pTmp = ' ';
         pTmp++;
      }
   } 
   
   if (pTmp=strstr(acName1, " 1/"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " - "))
      *pTmp = 0;

   if (pTmp=strstr(acName1, " ETUX"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ET UX"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ET AL"))
      *pTmp = 0;

   if (pTmp=strstr(acName1, " TSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " SUCC  TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TSTES"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " COTRST"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " ESTATE OF"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " LIFE ESTATE"))
      *pTmp = 0;
   else if (pTmp=strstr(acName1, " M D"))
      *pTmp = 0;

   // Check on name2
   if (acName2[0])
   {
      if (pTmp1=strstr(acName2, " 1/"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " - "))
         *pTmp1 = 0;
      if (pTmp1=strstr(acName2, " ETUX"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ET UX"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ETAL"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ET AL"))
         *pTmp1 = 0;
      if (pTmp1=strstr(acName2, " TSTEE"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " SUCC  TR"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TRUSTE"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TR "))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TRS"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " TSTES"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " CO-TR"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " COTRST"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " ESTATE OF"))
         *pTmp1 = 0;
      else if (pTmp1=strstr(acName2, " LIFE ESTATE"))
         *pTmp1 = 0;
      else if (!memcmp(acName2, "PO BOX", 6))
         acName2[0] = 0;
   }

   // Process as two names if name2 has multiple names
   pTmp = strchr(acName1, '/');
   pTmp2 = strchr(acName2, '/');
   pTmp1 = strchr(acName2, '&');
   if ((pTmp && !isdigit(*(pTmp+1))) || (pTmp2 && !isdigit(*(pTmp2+1))) || pTmp1)
   {
      if (pTmp)
         *pTmp = '&';
      if (pTmp2)
         *pTmp2 = '&';

      // Check for vesting
      pTmp1 = strrchr(acName1, ' ');
      if (!acVest[0] && pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp)
         {
            strcpy(acVest, acTmp1);
            *pTmp1 = 0;
         }
      } 

      splitOwner(acName1, &myOwner,3);
      iTmp = strlen(myOwner.acName1);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);
      iTmp = strlen(myOwner.acSwapName);
      if (iTmp > SIZ_NAME_SWAP)
         iTmp = SIZ_NAME_SWAP;
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

      if (myOwner.acVest[0] > ' ' && !acVest[0])
         strcpy(acVest, myOwner.acVest);

      // Check for vesting
      pTmp1 = strrchr(acName2, ' ');
      if (!acVest[0] && pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp)
         {
            strcpy(acVest, acTmp1);
            *pTmp1 = 0;
         }
      } 
      splitOwner(acName2, &myOwner,3);
      iTmp = strlen(myOwner.acName1);
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName1, iTmp);

      if (acCareOf[0])
      {
         iTmp = strlen(acCareOf);
         if (iTmp > SIZ_CARE_OF)
            iTmp = SIZ_CARE_OF;
         memcpy(pOutbuf+OFF_CARE_OF, acCareOf, iTmp);
      }

      if (acVest[0])
      {
         iTmp = strlen(acVest);
         memcpy(pOutbuf+OFF_VEST, acVest, iTmp);
      }
      return;
   }

   // If Owner1 and Owner2 has the same last name, combine them
   if (acName2[0])
   {
      pTmp = strchr(acName1, ',');
      if (pTmp)
      {
         iTmp = pTmp - &acName1[0];
         if (!memcmp(acName1, acName2, iTmp))
         {
            MergeName(acName1, acName2, acName1);
            acName2[0] = 0;
         }
      }
   }

   // Now parse owners
   splitOwner(acName1, &myOwner, 3);
   iTmp = strlen(myOwner.acName1);
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, iTmp);
   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   if (!acVest[0] && myOwner.acVest[0] > ' ')
      strcpy(acVest, myOwner.acVest);

   if (acName2[0])
   {
      splitOwner(acName2, &myOwner, 3);
      iTmp = strlen(myOwner.acName1);
      if (iTmp > SIZ_NAME2)
         iTmp = SIZ_NAME2;
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName1, iTmp);
   }

   if (acCareOf[0])
   {
      /*
      iTmp = strlen(acCareOf);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, acCareOf, iTmp);
      */
      updateCareOf(pOutbuf, acCareOf, strlen(acCareOf));
   }
   if (acVest[0])
      memcpy(pOutbuf+OFF_VEST, acVest, strlen(acVest));
}

/******************************** Mod_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Mod_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Mod_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mod_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acName1[64], acName2[64], acVesting1[8], acVesting2[8];
   char  *pTmp, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   acName1[CRESIZ_NAME_1] = 0;
   pName1 = acName1;
   acVesting1[0] = 0;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   acName2[CRESIZ_NAME_2] = 0;
   pName2 = acName2;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0374421611", 10))
   //   iTmp = 0;
#endif

   // Remove dot and single quote
   remCharEx(acName1, ".'");
   blankRem((char *)&acName1[0]);
   strcpy(acOwners, acName1);
   remCharEx(acName2, ".'");
   blankRem((char *)&acName2[0]);

   // Check owner2 for # - drop them
   if (*pName2 <= ' ')
      *pName2 = 0;
   else if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '&' )
   {  // Keep them as two separate names
      pName2 += 2;
   } else if (*pName2 == '#')
   {
      pName2 += 2;
      pTmp = strchr(pName1, ',');
      if (pTmp)
      {
         iTmp = pTmp - pName1;
         if (!memcmp(pName1, pName2, iTmp))
         {
            // Combine Name1 & Name2 since they have the same last name
            MergeName(pName1, pName2, acOwners);
            pName2 = NULL;
         }
      }
   }

   if (pName2 && *pName2 > ' ')
   {
      remChar(pName2, ',');
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      // Find vesting in Name2 
      pTmp = findVesting(pName2, acVesting2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));

         // Check EtAl
         if (!memcmp(acVesting2, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } 
   }

   // Update Name1
   iTmp = remChar(acOwners, ',');
   if (acOwners[iTmp-1] == '&')
      acOwners[iTmp-1] = ' ';
   if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);

   // Cleanup Name1 
   iTmp = Mod_CleanName(acOwners, acTmp, acVesting1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acOwners, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));

      // Check Etal
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   } 

   // Now parse owners
   splitOwner(acOwners, &myOwner, 3);
   if (acVesting1[0] > ' ' && isVestChk(acVesting1))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/********************************* Mod_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Mod_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[256], acAddr2[128];
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pRec->M_Addr1, "     ", 5))
      return;

   memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
   blankRem(acAddr1, CRESIZ_ADDR_1);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ') 
            pAddr1++;
         iTmp = strlen(pAddr1);
         if (iTmp > SIZ_CARE_OF)
            iTmp = SIZ_CARE_OF;
         memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
      }
   }

   // Start processing
   memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
   iTmp = atoin(pRec->M_Zip, 5);
   if (iTmp > 10001)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   } else
   {
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
   }
   
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0130470111", 10))
   //   iTmp = 0;
#endif

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Check for Suite
   if (!memcmp(pAddr1, "SUITE ", 6) || !memcmp(pAddr1, "STE ", 4))
   {
      memcpy(acAddr1, pRec->Name2, CRESIZ_NAME_2+CRESIZ_ADDR_1);
      blankRem(acAddr1, CRESIZ_NAME_2+CRESIZ_ADDR_1);
      if (!memcmp(acAddr1, "C/O ", 4))
         pAddr1 = &acAddr1[4];
      else if (isdigit(acAddr1[0]))
         pAddr1 = &acAddr1[0];
   }
   parseAdr1_1(&sMailAdr, pAddr1);

   memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
   acAddr2[CRESIZ_ADDR_2] = 0;
   parseAdr2(&sMailAdr, myTrim(acAddr2));

   if (sMailAdr.lStrNum > 0)
   {
      sprintf(acTmp, "%*d", SIZ_M_STRNUM, sMailAdr.lStrNum);
      memcpy(pOutbuf+OFF_M_STRNUM, acTmp, strlen(acTmp));
      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
   } 
   blankPad(sMailAdr.strDir, SIZ_M_DIR);
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   blankPad(sMailAdr.strName, SIZ_M_STREET);
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
   blankPad(sMailAdr.Unit, SIZ_M_UNITNO);
   memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit, SIZ_M_UNITNO);
   blankPad(sMailAdr.City, SIZ_M_CITY);
   memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   blankPad(sMailAdr.State, SIZ_M_ST);
   memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

   // Situs 
   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      ADR_REC sSitusAdr;

      removeSitus(pOutbuf);
      memcpy(acTmp, pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
      iTmp = blankRem(acTmp, CRESIZ_SITUS_STREETNO);

      // Prevent situation likes 123 1/2 NORTH STREET.  We have to break
      // into strnum=123, strsub=1/2
      pTmp = strchr(acTmp, ' ');
      if (pTmp)
      {
         *pTmp++ = 0;
         memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, strlen(pTmp));
      }

      // Keep raw house number
      strcpy(acAddr1, acTmp);
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));

      // Format house number
      iTmp = sprintf(acTmp, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);

      // Copy street name
      memcpy(acTmp, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
      acTmp[CRESIZ_SITUS_STREETNAME] = 0;
      if (pTmp = strchr(acTmp, '/'))
         *pTmp = 0;
      if (pTmp = strstr(acTmp, " BVR"))
         *pTmp = 0;
      if (pTmp = strstr(acTmp, " MPH"))
         *pTmp = 0;
      if (pTmp = strstr(acTmp, " CP"))
         *pTmp = 0;
      if (pTmp = strstr(acTmp, " MRE"))
         *pTmp = 0;
   
      replStr(acTmp, "CO RD", " COUNTY ROAD ");
      blankRem(acTmp);
      strcat(acAddr1, " ");
      strcat(acAddr1, acTmp);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, strlen(acAddr1));

      parseAdr1S(&sSitusAdr, myTrim(acTmp));
      myTrim(sSitusAdr.strName);

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

      acTmp[0] = 0;
      if (pRec->S_City[0] >= 'A')
      {
         memcpy(acAddr1, pRec->S_City, CRESIZ_SITUS_CITY);
         myTrim(acAddr1, CRESIZ_SITUS_CITY);
         City2Code(acAddr1, acTmp, pOutbuf);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            memcpy(pOutbuf+OFF_S_ST, "CA", 2);
            if (City2Zip(acAddr1, acTmp))
            {
               memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
               iTmp = sprintf(acAddr2, "%s, CA %s", acAddr1, acTmp);
            } else
               iTmp = sprintf(acAddr2, "%s, CA", acAddr1, acTmp);
            memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
         }
      }
      
      if (acTmp[0] <= ' ')
      {
         myTrim(sMailAdr.strName);
         if (!strcmp(sMailAdr.strName, sSitusAdr.strName) && sMailAdr.City[0] > ' ')
         {  // // Assuming that no situs city and we have to use mail city
            myTrim(sMailAdr.City);
            City2Code(sMailAdr.City, acTmp);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ST, "CA", 2);
               iTmp = sprintf(acAddr2, "%s, CA %.5s", sMailAdr.City, pOutbuf+OFF_M_ZIP);
               memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
            }
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
         }
      }
   }
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Mod_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acTmp1[256], acCode[16];
   long     lTmp; 
   int      iTmp, iRet;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "25MOD", 5);

      // Create APN_D new record
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      /*
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lHpp = atoin(pRec->PPVal, CRESIZ_PP_VAL);
      lTmp = lHpp + lFixture;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      */
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTH_VALUE);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_OTH_VALUE);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_OTH_VALUE);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   iRet = 0;

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   //*(pOutbuf+OFF_STATUS) = getParcStatus_MOD(pRec->ParcType);
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"MOD");

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "002044111", 9))
   //   iTmp = 0;
#endif
   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, 2);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, 2, pOutbuf);

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
   } else
   {
      lTmp = 0;
      strcpy(acTmp, BLANK32);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

   // Lot Sqft
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   } else
   {
      lTmp = atoin(pRec->LotSqft, CRESIZ_LOT_SQFT);
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
         memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

         // Convert to acres
         lTmp = (long)((double)lTmp * 1000)/SQFT_PER_ACRE;
         sprintf(acTmp, "%*u", SIZ_LOT_ACRES, lTmp);
         memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
      }
   }

   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   /* Recording info
   if (pRec->RecBook1[0] > ' ')
   {
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      Mod_FmtRecDoc(pOutbuf, pRec->RecBook1);
   }
   */

   // Sale price or Stamp amount
   if (pRec->StampAmt[0] > '0' && pRec->StampAmt[0] <= '9')
   {
      memcpy(acTmp, pRec->StampAmt, 10);
      acTmp[10] = 0;
      //dTmp = atof(acTmp) / 1.1;
      //lTmp = (long)(dTmp * 1000);
      lTmp = (long)(atof(acTmp) * SALE_FACTOR);
      sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lTmp/100);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
   }

   // Owners
   memcpy(acTmp, pRec->Name1, CRESIZ_NAME_1+CRESIZ_NAME_2);
   acTmp[CRESIZ_NAME_1+CRESIZ_NAME_2] = 0;
   Mod_MergeOwner(pOutbuf, acTmp);

   // Mailing & Situs address 
   Mod_MergeAdr(pOutbuf, pRollRec);

   // Base year
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // HO Exempt
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (lTmp == 7000 || !memcmp(pRec->ExeCode1, "10", 2))
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   //lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (lTmp > 0)
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);


   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   // Translate .75 to a full bath, .5 or less to half bath
   if (pRec->Baths[1] >= '7')
   {
      iTmp = atoin(pOutbuf+OFF_BATH_F, 2);
      iTmp++;
      iTmp = sprintf(acTmp, "%2u", iTmp);
      memcpy(pOutbuf+OFF_BATH_F, acTmp, iTmp);
   } else if (pRec->Baths[1] >= '2')
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .25 or .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      memcpy(acTmp, pRec->LglDesc1, CRESIZ_LGL_DESC1);
      acTmp[CRESIZ_LGL_DESC1] = 0;
      if (pRec->LglDesc2[0] > ' ')
      {
         pRec->LglDesc2[CRESIZ_LGL_DESC2-1] = 0;
         strcat(acTmp, pRec->LglDesc2);
      }
      updateLegal(pOutbuf, acTmp);
   }
      
   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // FP - 0-5,B,F,I,M,S,W,Y
   if (isdigit(pRec->Fp[0]))
      *(pOutbuf+OFF_FIRE_PL) = pRec->Fp[0];
   else if (pRec->Fp[0] > ' ')
   {
      switch (pRec->Fp[0])
      {
         case 'I':   // Insert
         case 'F':   // Fireplace
         case 'S':   // Wood stove
            *(pOutbuf+OFF_FIRE_PL) = '1';
            break;
         default:
            LogMsg("XFp: %c", pRec->Fp[0]);
      }
   }

   // Misc Impr - 0-6,A,B,C,D,F,G,H,L,M,O,P,R,S,T,W,Y
   if (pRec->MiscImpr[0] > ' ')
      *(pOutbuf+OFF_MISC_IMPR) = pRec->MiscImpr[0];
      //LogMsg("XMisc: %c", pRec->MiscImpr[0]);

   // AC - 1,A,N,P,S,Y
   if (pRec->Ac[0] > ' ')
   {
      switch (pRec->Ac[0])
      {
         case 'A':   // Air cond
         case 'Y':   // Yes
            *(pOutbuf+OFF_AIR_COND) = 'U';   // Yes
            break;
         case 'P':   // Pump
            *(pOutbuf+OFF_AIR_COND) = 'H';   // Heat pump
            break;
         case 'S':   // Swamp 
            *(pOutbuf+OFF_AIR_COND) = 'E';   // Evaporative cooler
            break;
         case 'N':   // No 
            *(pOutbuf+OFF_AIR_COND) = 'N';  
            break;
         default:
            LogMsg("XAir: %c", pRec->Ac[0]);
      }
   }

   // Heating - 0,2,B,C,D,F,G,H,M,O,P,R,S,W
   // Heating:  B=baseboard  C=central  W=wall heater
   if (pRec->Heating[0] > ' ')
   {
      *(pOutbuf+OFF_HEAT) = pRec->Heating[0];
      switch (pRec->Heating[0])
      {
         case 'C':   // Central
            *(pOutbuf+OFF_HEAT) = 'Z';
            break;
         case 'B':   // Electric baseboard
         case 'R':
            *(pOutbuf+OFF_HEAT) = 'F';
            break;
         case 'F':   // Fireplace
            *(pOutbuf+OFF_HEAT) = 'R';
            break;
         case 'M':   // Monitor
            *(pOutbuf+OFF_HEAT) = 'V';
            break;
         case 'O':   // Other
            *(pOutbuf+OFF_HEAT) = 'X';
            break;
         case 'P':   // Heat pump
            *(pOutbuf+OFF_HEAT) = 'G';
            break;
         case 'S':   // Wood stove
            *(pOutbuf+OFF_HEAT) = 'S';
            break;
         case 'W':   // Wall
            *(pOutbuf+OFF_HEAT) = 'D';
            break;
         default:
            *(pOutbuf+OFF_HEAT) = 'X';
            LogMsg("XHeat %c", pRec->Heating[0]);
            break;
      }
   }

   // Pool/Spa
   if (pRec->Pool[0] > ' ' && pRec->Spa[0] > ' ')
      *(pOutbuf+OFF_POOL) = 'C';             // Combo
   else if (pRec->Spa[0] > ' ')
      *(pOutbuf+OFF_POOL) = 'S';             // Spa 
   else if (pRec->Pool[0] > ' ')
      *(pOutbuf+OFF_POOL) = 'P';             // Pool

   // Zoning - no data
   if (pRec->Zone[0] > '0')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, SIZ_ZONE);

   // *Neighborhood

   // *Type_Sqft - M, C

   // Building class
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
         acTmp[iTmp++] = pRec->BldgCls[0];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[1]))
            acTmp[iTmp++] = pRec->BldgCls[1];
         else if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else
            acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   return iRet;
}

/********************************** Mod_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 ****************************************************************************/

int Mod_LoadRoll(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, lLienUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }

   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Mod_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdLienExt)
         {
            iRet = PQ_MergeLien(acBuf, fdLienExt);
            if (!iRet)
               lLienUpd++;
         }

         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         iRetiredRec++;

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total lien updated records: %u\n", lLienUpd);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Mod_Load_LDR ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Mod_Load_LDR(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      lLDRRecCount++;
      iRet = Mod_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   // Check for NULL embeded in file
   iRet = replEmbededChar(acOutFile, NULL, 32, 32, -1, iRecLen);
   if (iRet < 0)
      LogMsg("***** Error opening %s for checking null", acOutFile);
   else if (iRet > 0)
      LogMsg("*** Ctrl character found in %s.  Please contact Sony", acOutFile);
 
   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/******************************* Mod_ExtrSale *******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Mod_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024], *pTmp;
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   double   dTmp;
   long     lCnt=0, lNewRec=0, lPrice;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            blankRem(acTmp);
            memcpy(pSale->StampAmt, acTmp, strlen(acTmp));
            // Keep this check here until counties correct their roll
            pTmp = strchr(acTmp, '.');
            if (!pTmp || *(pTmp+1) == ' ' || dTmp > 100000.00)
            {
               if (acTmp[0] == '0' || dTmp < 2000.00)
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if ((lPrice % 100) == 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                  } else
                  {
                     LogMsg0("??? Suspected stamp amount for APN: %.10s, StampAmt=%s", acBuf, acTmp);
                     memset(acTmp, ' ', SIZ_SALE1_AMT);
                  }
               } else
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
            } else
            {
               if (dTmp > 9999.00 && ((long)dTmp % 100) == 0)
               {
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               } else
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if (lPrice > 0)
                  {
                     if (lPrice < 100 || (lPrice % 100) == 0)
                        sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                     else
                     {
                        LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt);
                        memset(acTmp, ' ', SIZ_SALE1_AMT);
                     }
                  }
               }
            }
            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Mod_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", iRet);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/****************************** Mod_CreatePublR01 ****************************
 *
 * Create public record in R01 format
 *
 * Return 0 if successful, >0 if error
 * Note: weird problem occurs in calling updateLegal()
 * 1) At first it crashes every time calling updateLegal() even after rebuilt.
 * 2) After copying that function to local and rename it to Mod_updateLegal() and 
 *    use this new function, rebuild solution, everything works.
 * 2) Now call the original function updateLegal(), it works like it supposes to be.
 *
 *****************************************************************************/

int Mod_CreatePublR01(char *pOutbuf, char *pRollRec, bool bCreate)
{
   char     acTmp[256], acTmp1[256];
   int      iRet, iCnt;

   iRet = ParseStringNQ(pRollRec, '|', MAX_FLD_TOKEN, apTokens);
   if (iRet < MOD_UA_REM3)
   {
      LogMsg("***** Error: bad input record for APN=%s", apTokens[0]);
      return -1;
   }

   if (bCreate)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // Remove hyphen from APN
      strcpy(acTmp1, apTokens[MOD_UA_APN]);
      remChar(acTmp1, '-');
      strcat(acTmp1, "11");

      // Start copying data
      memcpy(pOutbuf, acTmp1, strlen(acTmp1));

      // Format APN
      iRet = formatApn(acTmp1, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iRet);

      // Create MapLink and output new record
      iRet = formatMapLink(acTmp1, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iRet);

      // Create index map link
      if (getIndexPage(acTmp, acTmp1, &myCounty))
         memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iRet);

      memcpy(pOutbuf+OFF_CO_NUM, "25MODA", 6);

      // Year assessed
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Entity
      if (*apTokens[MOD_UA_NAME1] > ' ')
      {
         if (!strcmp(apTokens[MOD_UA_NAME1], apTokens[MOD_UA_NAME2]))
            strcpy(acTmp, apTokens[MOD_UA_NAME1]);
         else
            sprintf(acTmp, "%s %s", apTokens[MOD_UA_NAME1], apTokens[MOD_UA_NAME2]);
         iCnt = remChar(acTmp, 39);        // Remove single quote
         iCnt = blankRem(acTmp);
         vmemcpy(pOutbuf+OFF_NAME1, acTmp, SIZ_NAME1, iCnt);
      }
   }

   long lSqFtLand=0;
   double dAcreAge = atof(apTokens[MOD_UA_ACRES]);
   if (dAcreAge > 0.0)
   {
      dAcreAge *= 1000.0;
      sprintf(acTmp, "%*d", SIZ_LOT_ACRES, (long)(dAcreAge));
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

      lSqFtLand = (long)((dAcreAge * SQFT_FACTOR_1000));
      sprintf(acTmp, "%*d", SIZ_LOT_SQFT, lSqFtLand);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

#ifdef _DEBUG
   //if (!memcmp(pRollRec, "002-110-02", 10))
   //   iRet = 0;
#endif

   // Legal
   if (*apTokens[MOD_UA_LOCATION] > ' ')
      updateLegal(pOutbuf+OFF_LEGAL, apTokens[MOD_UA_LOCATION]);
   //{
   //   removeLegal(pOutbuf);
   //   iCnt = blankRem(apTokens[MOD_UA_LOCATION]);
   //   if (iCnt > SIZ_LEGAL)
   //      iCnt = SIZ_LEGAL;
   //   memcpy(pOutbuf+OFF_LEGAL, apTokens[MOD_UA_LOCATION], iCnt);
   //}

   // Set public parcel flag
   *(pOutbuf+OFF_PUBL_FLG) = 'Y';

   // HO Exempt
   *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   return 0;
}

/********************************* Mod_MergePubl ****************************
 *
 * Merge public parcel file to roll file
 *
 ****************************************************************************/

int Mod_MergePubl(int iSkip)
{
   char     acRec[2048], acBuf[2048], acRollRec[2048];
   char     acRawFile[_MAX_PATH], acOutFile[_MAX_PATH], *pRoll;

   HANDLE   fhIn, fhOut;

   int      iRet, iTmp, iNewRec=0, iChgRec=0;
   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lRet=0, lCnt=0;

   // Preparing output file
   sprintf(acRawFile, acRawTmpl, "Mod", "Mod", "R01");
   sprintf(acOutFile, acRawTmpl, "Mod", "Mod", "TMP");

   // Make sure R01 file is avail.
   if (_access(acRawFile, 0))
   {
      LogMsg("***** Missing input file %s.  Please recheck!", acRawFile);
      return -1;
   }

   // Open unassessed file
   LogMsg("Open unassessed file %s", acPubParcelFile);
   fdRoll = fopen(acPubParcelFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening unassessed file: %s\n", acPubParcelFile);
      return -2;
   }

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   iNoMatch=iBadCity=iBadSuffix=0;

   // Skip header
   //pRoll = fgets(acRollRec, 512, fdRoll);
   // Get first rec
   pRoll = fgets(acRollRec, 512, fdRoll);

   // Merge loop
   while (pRoll &&  *pRoll < 'A')
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (iRecLen != nBytesRead)
         break;

      NextRollRec:
      iTmp = memcmp(&acBuf[OFF_APN_D], &acRollRec[0], 10);
      if (iTmp <= 0)
      {
         if (!iTmp)
         {
            LogMsg("*** Update existing unassessed record: %.10s", acBuf);
            iRet = Mod_CreatePublR01(acBuf, acRollRec, false);
            iChgRec++;

            // Get next record
            pRoll = fgets(acRollRec, 512, fdRoll);
         }

         // Write existing rec out
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else if (iTmp > 0)       // Insert new roll record
      {
         // Create new R01 record from unassessed record
         iRet = Mod_CreatePublR01(acRec, acRollRec, true);
         iNewRec++;

         bRet = WriteFile(fhOut, acRec, iRecLen, &nBytesWritten, NULL);
         if (!(++lCnt % 1000))
            printf("\r%u", lCnt);

         // Get next record
         pRoll = fgets(acRollRec, 512, fdRoll);
         if (!pRoll)
            break;
         else
            goto NextRollRec;
      }

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   while (bRet && (iRecLen == nBytesRead))
   {
      // Write existing rec out
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      lCnt++;

      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   DeleteFile(acRawFile);
   rename(acOutFile, acRawFile);

   printf("\n");
   LogMsgD("Total output records:       %u", lCnt);
   LogMsg("Total unassessed records:   %u", iNewRec);
   LogMsg("Records status changed  :   %u", iChgRec);

   lRecCnt = lCnt;

   return lRet;
}

/*********************************** loadMod ********************************
 *
 * Input files:
 *    redifile (899-bytes roll file)
 *
 *
 * Commands:
 *    -U    Monthly update
 *    -L    Lien update
 *    -Xl   Extract lien values
 *    -T    Load tax file
 *    -Mp   Load public parcels
 *
 ****************************************************************************/

int loadMod(int iLoadFlag, int iSkip)
{
   int   iRet;
   char  sTmp[1024];

   iApnLen = myCounty.iApnLen;
   iRet = guessRecLen(acRollFile, iRollLen);
   if (!iRet)
   {
      LogMsg("***** Roll file is not ready for production: %s", acRollFile);
      return -1;
   }
   iRollLen = iRet;

   if (bLoadTax)
   {
      // Load tax agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(sTmp);

      iRet = Cres_Load_TaxBase(bTaxImport);
      if (!iRet)
         iRet = Cres_Load_TaxDelq(bTaxImport);
      if (!iRet)
         iRet = Cres_Load_TaxOwner(bTaxImport);
      if (iRet < 0)
      {
         char sMailTo[64], sSubj[80], sBody[256];

         iRet = GetPrivateProfileString("Mail", "MailDeveloper", "", sMailTo, 256, acIniFile);
         if (iRet > 10)
         {
            sprintf(sSubj, "*** %s - Error loading tax file (%d)", myCounty.acCntyCode, iRet);
            sprintf(sBody, "Please review log file \"%s\" for more info", acLogFile);
            mySendMail(acIniFile, sSubj, sBody, sMailTo);
         }
      }
   }

   /*
   char  acTmpFile[_MAX_PATH];
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2004\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2004");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2005\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2005");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2006\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2006");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2007\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2007");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2008\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2008");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2009\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2009");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2010\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2010");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Mod\\2011\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SMod_CD\\raw\\Mod_Sale.2011");
   iRet = Mod_ExtrSale(acRollFile, acTmpFile, false);
   */
   // Extract sale1 from redifile and append to cum sale file
   if (iLoadFlag & EXTR_SALE)                // -Xs
   {
      iRet = Mod_ExtrSale(acRollFile, NULL, true);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
      iRet = FixSalePrice(acCSalFile, false);
   }

   if (iLoadFlag & EXTR_LIEN)                // -Xl
      iRet = Cres_ExtrLien(myCounty.acCntyCode);

   if (iLoadFlag & LOAD_LIEN)                // -L 
      iRet = Mod_Load_LDR(myCounty.acCntyCode);
   else if (iLoadFlag & LOAD_UPDT)           // -U
      iRet = Mod_LoadRoll(myCounty.acCntyCode, iSkip);

   // Merge public file
   if (!iRet && (iLoadFlag & MERG_PUBL))
   {
      GetIniString(myCounty.acCntyCode, "PubParcelFile", "", sTmp, _MAX_PATH, acIniFile);
      if (!_access(sTmp, 0))
      {
         // Sort and remove duplicate entries
         sprintf(acPubParcelFile, "%s\\%s\\%s_Publ.srt", acTmpPath, myCounty.acCntyCode, myCounty.acCntyCode);
         iRet = sortFile(sTmp, acPubParcelFile, "S(#1,C,A) DUPO(1,10)");
         if (iRet > 10)
         {
            LogMsgD("Merge public parcel file %s", acPubParcelFile);
            iRet = Mod_MergePubl(iSkip);
         }
      } else
         LogMsg("***** Public parcel file <%s> is missing (%d)", sTmp, _doserrno);
   }

   // County request not to put sale price out
   if (!iRet && (iLoadFlag & MERG_CSAL))
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);

   return iRet;
}
