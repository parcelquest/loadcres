#ifndef _MERGEDNX_H
#define  _MERGEDNX_H

#define  DNXOFF_PARCTYPE            1
#define  DNXOFF_ROLL_YEAR           3
#define  DNXOFF_TRA                 7
#define  DNXOFF_APN                 13
#define  DNXOFF_FILLER1             23
#define  DNXOFF_PPOP_STATEMENT      24
#define  DNXOFF_PPOP_PENALTY        25
#define  DNXOFF_CPI                 26
#define  DNXOFF_NOTIFY              29
#define  DNXOFF_EXE_CLAIM           30
#define  DNXOFF_USE_CODE            31
#define  DNXOFF_BASE_CODE           37
#define  DNXOFF_BOOK						39
#define  DNXOFF_NAME_1              55
#define  DNXOFF_NAME_2              84
#define  DNXOFF_ADDR_1              113
#define  DNXOFF_ADDR_2              142
#define  DNXOFF_LGL_DESC            171
#define  DNXOFF_ZIP                 202
#define  DNXOFF_TAXNUM	            212
#define  DNXOFF_TAXDATE             217
#define  DNXOFF_SITUS1     			223
#define  DNXOFF_SITUS2				   253
#define  DNXOFF_LAND_VAL            283
#define  DNXOFF_IMP_VAL             293
#define  DNXOFF_PP_VAL              303
#define  DNXOFF_EX_VAL              313
#define  DNXOFF_PERS_FIXT_VAL       323
#define  DNXOFF_ACRES               333
#define  DNXOFF_EXEMP_CODE1         343
#define  DNXOFF_EXEMP_CODE2         345
#define  DNXOFF_EXEMP_CODE3         347
#define  DNXOFF_EXEMP_CODE4         349

#define  DNXSIZ_PARCTYPE            2
#define  DNXSIZ_ROLL_YEAR           4
#define  DNXSIZ_TRA                 6
#define  DNXSIZ_APN                 10
#define  DNXSIZ_FILLER1             1
#define  DNXSIZ_PPOP_STATEMENT      1
#define  DNXSIZ_PPOP_PENALTY        1
#define  DNXSIZ_CPI                 3
#define  DNXSIZ_NOTIFY              1
#define  DNXSIZ_EXE_CLAIM           1
#define  DNXSIZ_USE_CODE            6
#define  DNXSIZ_BASE_CODE           2
#define  DNXSIZ_BOOK				      16
#define  DNXSIZ_NAME_1              29
#define  DNXSIZ_NAME_2              29
#define  DNXSIZ_ADDR_1              29
#define  DNXSIZ_ADDR_2              29
#define  DNXSIZ_LGL_DESC            31
#define  DNXSIZ_ZIP                 10
#define  DNXSIZ_TAXNUM	            5
#define  DNXSIZ_TAXDATE             6
#define  DNXSIZ_SITUS1     			30
#define  DNXSIZ_SITUS2				   30
#define  DNXSIZ_LAND_VAL            10
#define  DNXSIZ_IMP_VAL             10
#define  DNXSIZ_PP_VAL              10
#define  DNXSIZ_EX_VAL              10
#define  DNXSIZ_PERS_FIXT_VAL       10
#define  DNXSIZ_ACRES               10
#define  DNXSIZ_EXEMP_CODE          2


/***********************************************************************************
 *
 * Lien Redifile record layout
 *
 ***********************************************************************************/

typedef struct _tDnxRoll350
{
   char  ParcType[DNXSIZ_PARCTYPE];
   char  RollYr[DNXSIZ_ROLL_YEAR];
   char  TRA[DNXSIZ_TRA];
   char  Apn[DNXSIZ_APN];
   char  filler1[1];
   char	PPOP_Statement[DNXSIZ_PPOP_STATEMENT];
   char	PPOP_Penalty[DNXSIZ_PPOP_PENALTY];
   char	CPI[DNXSIZ_CPI];
   char	Notify[DNXSIZ_NOTIFY];
   char	Exe_Claim[DNXSIZ_EXE_CLAIM];
   char  UseCode[DNXSIZ_USE_CODE];
   char  BaseCode[DNXSIZ_BASE_CODE];
   char  RecBook1[DNXSIZ_BOOK]; 
   char  Name1[DNXSIZ_NAME_1];
   char  Name2[DNXSIZ_NAME_2];
   char  M_Addr1[DNXSIZ_ADDR_1];
   char  M_Addr2[DNXSIZ_ADDR_2];
   char  LglDesc[DNXSIZ_LGL_DESC];
   char  M_Zip[DNXSIZ_ZIP];
	char	DelqNum[DNXSIZ_TAXNUM];       // Tax default number
	char	DelqDate[DNXSIZ_TAXDATE];     // Tax default date
	char	Situs1[DNXSIZ_SITUS1];
	char	Situs2[DNXSIZ_SITUS2];
	char  LandVal[DNXSIZ_LAND_VAL];
	char  ImprVal[DNXSIZ_IMP_VAL];
	char  PPVal[DNXSIZ_PP_VAL];
	char  ExVal[DNXSIZ_EX_VAL];         // Exemption value
   char  FixtVal[DNXSIZ_PERS_FIXT_VAL];
   char  Acres[DNXSIZ_ACRES];

   char  ExeCode1[DNXSIZ_EXEMP_CODE];
   char  ExeCode2[DNXSIZ_EXEMP_CODE];
   char  ExeCode3[DNXSIZ_EXEMP_CODE];
   char  ExeCode4[DNXSIZ_EXEMP_CODE];
	
} ROLL_350;

#endif
