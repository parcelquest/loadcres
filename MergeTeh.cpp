/*****************************************************************************
 *
 * Revision:
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 04/24/2008 1.10.5.1  Use current HOE value instead of LDR data.
 * 04/27/2008 1.10.5.3  
 * 06/10/2008 1.12.0    Activate LoadTeh() and adding Teh_ExtrSale().
 * 07/22/2008 8.1.2     Add Teh_Load_LDR() to process 2008 LDR.
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 08/04/2009 9.1.3     Populate other values.
 * 02/23/2010 9.2.1     Fix CARE_OF bug and modify Teh_MergeOwner() to keep original 
 *                      owner name when possible.
 * 07/28/2010 10.3.0    Change logic to identify HO Exempt property in Teh_CreateR01().
 *                      We now have to check EXE_CD1 and ExVal.  If EXE_CD1='10' or
 *                      ExVal=7000, set HO_FLG to 1 (or Y).
 * 07/22/2011 11.1.2    Modify Teh_MergeAdr() to correct problem where StrName is a continuation
 *                      of StrNo field.  Add S_HSENO.
 * 10/17/2011 11.2.7.1  Modify Teh_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      Modify Teh_Fmt1Doc() to keep DocNum as is. Use ApplyCumSale()
 *                      to update R01 file. Modify Teh_CreateR01() not to populate
 *                      sale info.  Let ApplyCumSale() populates it from Teh_Sale.sls.
 *                      Remove Teh_FmtRecDoc().
 * 10/26/2011 11.2.8.1  Modify Teh_Fmt1Doc() to return immediately if sale date is not avail.
 * 11/07/2011 11.2.9    Initialize output in Teh_Fmt1Doc().
 * 05/17/2012 11.5.2    Add -T option to load Tax file.
 * 05/08/2013 12.6.3    Working on additional char fields Parking, A/C, Heat, Zoning
 *                      ... Wait for translation table.
 * 09/27/2013 13.3.0    Modify Teh_MergeOwner() and add Teh_CleanName() to update Vesting.
 *                      Also adding above char fields back in with known values.
 *                      Use removeNames() to clear owner names
 * 09/29/2013 13.3.1    Remove comma from owner name
 * 09/30/2013 13.3.2    Updating AC, Heating, and Pool/Spa.
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 04/14/2014 13.5.2    Add date verification in Teh_Fmt1Doc().
 * 04/15/2014 13.5.3    Add asCooling[] & as Heating[] and fix bug in Teh_CreateR01().
 * 04/16/2014 13.5.4    Update FirePlace code.
 * 05/28/2014 13.6.1    Modify Teh_ExtrSale() to include DocTax in cum sale file.
 *                      Add Teh_FixDocTax() to remove repeated DocTax and SalePrice.
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 *                      Also fix exception in Teh_MergeAdr().
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Dosort.h"
#include "Pq.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"

static XLAT_CODE  asCooling[] = 
{
   // Value, lookup code, value length
   "C" , "C", 1,               // Central
   "R" , "L", 1,               // Refrig. Wall unit
   "S" , "E", 1,               // Swamp or Evaporative cooler
   "A" , "X", 1,               // Other
   "D" , "X", 1,               // Other
   "E" , "X", 1,               // Other
   "F" , "X", 1,               // Other
   "P" , "X", 1,               // Other
   "V" , "X", 1,               // Other
   "W" , "X", 1,               // Other
   "Y" , "X", 1,               // Other
   "",   "",  0
};

static XLAT_CODE  asHeating[] = 
{
   // Value, lookup code, value length
   "C" , "Z", 1,               // Central
   "B" , "F", 1,               // Electric baseboard
   "W" , "D", 1,               // Wall heater
   "D" , "X", 1,               // Other
   "E" , "X", 1,               // Other
   "F" , "X", 1,               // Other
   "G" , "X", 1,               // Other
   "O" , "X", 1,               // Other
   "P" , "X", 1,               // Other
   "S" , "X", 1,               // Other
   "V" , "X", 1,               // Other
   "X" , "X", 1,               // Other
   "Y" , "X", 1,               // Other
   "c" , "Z", 1,               // Central
   "",   "",  0
};

/********************************* Teh_Fmt1Doc *******************************
 *
 *   1) -01651  20040227= 01651 & 20040227: Fern displays doc# as -016 5100
 *   2) 0706061 19981201= This is not doc#, but recorder book 706, page 61.
 *      Old format can be displayed as 706-061
 *
 *****************************************************************************/

void Teh_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp;
   char  acDate[16], acDoc[64], *pTmp, *pDate;
   bool  bMisAligned = false;

   pTmp = pRecDoc;
   pDate = pRecDoc+8;
   memset(pOutDoc, ' ', SIZ_SALE1_DOC);
   *(pOutDoc+SIZ_SALE1_DOC) = 0;
   acDoc[0] = 0;

   // Input 12345678yyyymmdd
   // Output yyyymmdd123456789AB
   memset(pOutDate, ' ', SIZ_SALE1_DT);
   *(pOutDate+SIZ_SALE1_DT) = 0;
   memset(acDate, ' ', SIZ_SALE1_DT);
   acDate[SIZ_SALE1_DT] = 0;
   if (*pDate == ' ')
   {
      return;
   } else if (*(pDate-1) == ' ' && *(pDate+7) == ' ')
   {
      memcpy(acDate, pDate, 8);
      myTrim(acDate);
      if ((iTmp = strlen(acDate)) == 4)      // Missing MMDD?
         strcat(acDate, "0101");             // 1942,1982,2001,2002,2000
      else if (iTmp == 6)
         strcat(acDate, "01");               // Missing DD?
      else                                   // 199304
         memset(acDate, ' ', 8);             // Unknown
      // 2000103,1990625,1990729,1978314,1990807,1990823
   } else if (*(pDate+7) == ' ')
   {
      bMisAligned = true;
      if (*(pDate+6) != ' ' && *(pDate-1) != ' ')
      {
         memcpy(acDate, pDate-1, 8);
      } else if (*(pDate+6) != ' ' && *(pDate-1) == ' ')
      {
         // 1971231
         memcpy(acDate, pDate, 7);
         acDate[7] = '0';
      } else if (*(pDate+5) != ' ' && *(pDate-2) != ' ')
      {
         memcpy(acDate, pDate-2, 8);
      } else if (!memcmp(pDate, "VC", 2))
      {
         memcpy(acDate, pTmp, 8);
         memcpy(acDoc, pDate, 8);
         memset((char*)&acDoc[8], ' ', 4);
      } else
      {  // VC
         memset(acDate, ' ', 8);
         bMisAligned = false;
      }
   } else
      memcpy(acDate, pDate, 8);

   if (acDate[0] != ' ' && !isValidYMD(acDate))
   {
      memset(acDate, ' ', 8);
      bMisAligned = false;
   }

   // Verify date
   iTmp = atol(acDate);
   if (iTmp > 19000101 && iTmp < lToday)
      memcpy(pOutDate, acDate, 8);

   if (acDoc[0] > ' ')
      memcpy(pOutDoc, acDoc, 7);
   else if (isdigit(*pTmp))
      memcpy(pOutDoc, pTmp, 7);
   else if (*pTmp == ' ' && isdigit(*(pTmp+1)))
      memcpy(pOutDoc, pTmp+1, 6);
   else if (*(pTmp+3) > ' ' && memcmp(pTmp, "0000000", 7))
   {
      if (*pTmp >= 'A')
         sprintf(pOutDoc, "%.8s    ", pTmp);
      else
      {
         iTmp  = atoin(pTmp, 7);
         if (iTmp > 0)
            sprintf(pOutDoc, "%.7d     ", iTmp);
         else
            memset(pOutDoc, ' ', SIZ_SALE1_DOC);
      }
   } else
   {
      memset(pOutDoc, ' ', SIZ_SALE1_DOC);
   }
}

/********************************* Teh_FmtRecDoc *****************************
 *
 *   1) -01651  20040227= 01651 & 20040227: Fern displays doc# as -016 5100
 *   2) 0706061 19981201= This is not doc#, but recorder book 706, page 61.
 *      Old format can be displayed as 706-061
 *
 *****************************************************************************

void Teh_FmtRecDoc(char *pOutbuf, char *pRollRec)
{
   long     lPrice, lDate;
   double   dTmp;
   char     acTmp[32], acDate[16], acDoc[64], *pTmp;
   bool     bMisAligned = false;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   pTmp = pRec->RecBook1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "01123034", 8))
   //   iTmp = 0;
#endif

   // Doc #1
   Teh_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
   {
     *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code

      memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

      // Update last recording date
      lDate = atol(acDate);
      if (lDate > lLastRecDate && lDate < lToday)
         lLastRecDate = lDate;

      memcpy(acTmp, pRec->StampAmt, CRESIZ_STMP_AMT);
      acTmp[CRESIZ_STMP_AMT] = 0;
      dTmp = atof(acTmp);
      if (dTmp > 0.0)
      {
         lPrice = (dTmp * SALE_FACTOR);
         if (lPrice < 100)
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         else
            sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }
   }

   // Advance to Rec #2
   pTmp += 16;
   Teh_Fmt1Doc(acDoc, acDate, pTmp);
   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);
   }

   // Advance to Rec #3
   pTmp += 16;
   Teh_Fmt1Doc(acDoc, acDate, pTmp);
   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
   }
}

/******************************** Teh_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Teh_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;
   int   iTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   if (pTmp=strstr(acTmp, " 1/"))
     *pTmp = 0;
   else if (pTmp=strstr(acTmp, " - "))
     *pTmp = 0;

   pTmp = &acTmp[0];
   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      pTmp++;

      // Remove known bad chars
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   blankRem((char *)&acTmp[0], iTmp);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Teh_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Teh_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acName1[64], acName2[64], acVesting[8];
   char  *pTmp, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   myTrim(acName1, CRESIZ_NAME_1);
   strcpy(acOwners, acName1);
   pName1 = acName1;
   acVesting[0] = 0;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   myTrim(acName2, CRESIZ_NAME_2);
   pName2 = _strupr(acName2);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001120201", 9))
   //   iTmp = 0;
#endif

   // Check owner2 for # - drop them
   if (*pName2 == ' ')
      *pName2 = 0;
   else if (*pName2 == '#' )
   {
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '&')
   {
      pName2 += 2;
      pTmp = strchr(pName1, ',');
      if (pTmp)
      {
         iTmp = pTmp - pName1;
         if (!memcmp(pName1, pName2, iTmp))
         {
            // Combine Name1 & Name2 since they have the same last name
            MergeName(pName1, pName2, acOwners);
            pName2 = NULL;
         }
      }
   }

   if (pName2 && *pName2 > ' ')
   {
      remChar(pName2, ',');
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      // Find vesting in Name2 
      pTmp = findVesting(pName2, acVesting);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

         // Check EtAl
         if (!memcmp(acVesting, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } 
   }

   // Remove multiple spaces
   iTmp = remChar(acOwners, ',');
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);

   // Cleanup Name1 
   iTmp = Teh_CleanName(acOwners, acTmp, acVesting);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acName1, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // Now parse owners
   splitOwner(acName1, &myOwner, 3);
   if (acVesting[0] > ' ' && isVestChk(acVesting))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   iTmp = strlen(myOwner.acSwapName);
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/********************************* Teh_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Teh_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   ADR_REC  sSitusAdr, sMailAdr;
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[128], acAddr2[128], acUnit[8], acChar;
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "004270201", 9))
   //   iTmp = 0;
#endif

   // Clear old Mailing
   removeMailing(pOutbuf, false);
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
      blankRem(acAddr1, CRESIZ_ADDR_1);
      pAddr1 = acAddr1;

      iTmp = 0;
      if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
      {
         if (*pAddr1 == '%')
            pAddr1 += 2;
         else
            pAddr1 += 4;

         // Check for C/O name
         pTmp = strchr(pAddr1, ',');
         if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
         {
            *pTmp = 0;
            pAddr1 = pTmp + 1;
            if (*pAddr1 == ' ') 
               pAddr1++;
            iTmp = strlen(pAddr1);
            if (iTmp > SIZ_CARE_OF)
               iTmp = SIZ_CARE_OF;
            memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
         }
      }

      // Start processing
      memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
      iTmp = atoin(pRec->M_Zip, 5);
      if (iTmp > 400)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
      } else
      {
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
      }

      // Parsing mail address
      parseMAdr1_1(&sMailAdr, pAddr1);

      memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
      acAddr2[CRESIZ_ADDR_2] = 0;
      parseMAdr2(&sMailAdr, myTrim(acAddr2));

      if (sMailAdr.lStrNum > 0)
      {
         if (pTmp = strstr(sMailAdr.strNum, "1/2"))
         {
            memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, SIZ_M_STR_SUB);
            *pTmp = 0;
            sMailAdr.lStrNum = atoi(sMailAdr.strNum);
            iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
            memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);
         } else 
         {
            iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
            memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);

            if (sMailAdr.strSub[0] > '0')
            {
               sprintf(acTmp, "%s  ", sMailAdr.strSub);
               memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
            } else
            {
               acChar = isChar(sMailAdr.strNum, SIZ_M_STRNUM);
               if (acChar > ' ')
               {
                  if (pTmp = strchr(sMailAdr.strNum, acChar))
                     memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, strlen(pTmp));
               }
            }
         }

      }
      blankPad(sMailAdr.strDir, SIZ_M_DIR);
      memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      blankPad(sMailAdr.strName, SIZ_M_STREET);
      memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
      memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      blankPad(sMailAdr.City, SIZ_M_CITY);
      memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      blankPad(sMailAdr.State, SIZ_M_ST);
      memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
      blankPad(sMailAdr.Unit,SIZ_M_UNITNO);
      memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit,SIZ_M_UNITNO);
   }

   // Situs
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0)
   {
      removeSitus(pOutbuf);
      acUnit[0] = 0;

      // Form Addr1
      memcpy(acAddr1, pRec->S_StrNo, CRESIZ_SITUS_STREETNO+CRESIZ_SITUS_STREETNAME);
      iTmp = blankRem(acAddr1, CRESIZ_SITUS_STREETNO+CRESIZ_SITUS_STREETNAME);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // Check StrNo - may contain portion of street name
      // 700 GIVE
      // 22673 B
      // 7730  HW
      memcpy(acTmp, pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
      iTmp = blankRem(acTmp, CRESIZ_SITUS_STREETNO);

      // Prevent situation likes 123 1/2 NORTH STREET.  We have to break
      // into strnum=123, strsub=1/2
      pTmp = strchr(acTmp, ' ');
      if (pTmp)
      {
         pTmp++;
         if (strchr(pTmp, '/'))
         {  // Fraction
            memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, strlen(pTmp));
            *pTmp = 0;
            if (pRec->S_StrName[0] == ' ')
               memcpy(acAddr1, &pRec->S_StrName[1], CRESIZ_SITUS_STREETNAME-1);
            else
               memcpy(acAddr1, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
            acAddr1[CRESIZ_SITUS_STREETNAME] = 0;
         } else if (pRec->S_StrNo[CRESIZ_SITUS_STREETNO-1] == ' ')
         {  // Unit#
            strcpy(acUnit, pTmp);
            *pTmp = 0;
            memcpy(acAddr1, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
            acAddr1[CRESIZ_SITUS_STREETNAME] = 0;
         } else
         {  // Any thing else
            *pTmp = 0;
            memcpy(acAddr2, pRec->S_StrNo, CRESIZ_SITUS_STREETNO+CRESIZ_SITUS_STREETNAME);
            iTmp = blankRem(acAddr2, CRESIZ_SITUS_STREETNO+CRESIZ_SITUS_STREETNAME);
            pTmp = strchr(acAddr2, ' ');
            strcpy(acAddr1, pTmp+1);
         }
         memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));
      } else
      {
         memcpy(pOutbuf+OFF_S_HSENO, acTmp, iTmp);
         memcpy(acAddr1, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
         acAddr1[CRESIZ_SITUS_STREETNAME] = 0;
      }

      iTmp = sprintf(acTmp, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);

      // Copy street name
      parseAdr1S(&sSitusAdr, acAddr1);

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
      else if (acUnit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, acUnit, strlen(acUnit));

      // Assuming that no situs city and we have to use mail city
      acAddr2[0] = 0;
      if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         City2Code(sMailAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            strcpy(acAddr2, sMailAdr.City);
         }
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      } else
      {
         // Condition: Valid city name, Ex_Val is 7000,
         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && (sMailAdr.lStrNum > 0) &&
             (iStrNo == sMailAdr.lStrNum) && (*(pOutbuf+OFF_HO_FL) == '1') )
         {
            // Get city code
            City2Code(sMailAdr.City, acTmp);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
               strcpy(acAddr2, sMailAdr.City);
            }
         }

      }

      if (acAddr2[0])
      {
         strcat(myTrim(acAddr2), " CA");
         iTmp = strlen(acAddr2);
         if (iTmp > SIZ_S_CTY_ST_D)
            iTmp = SIZ_S_CTY_ST_D;
         memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
      }
   }
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful
 *
 *****************************************************************************/

int Teh_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acCode[32], acTmp1[32], *pTmp;
   long     lTmp, iTmp;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Ignore retired records
   if (getParcStatus_TEH(pRollRec) == 'R')
      return 1;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "52TEH", 5);

      // Create APN_D 
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      /*
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lHpp = atoin(pRec->PPVal, CRESIZ_PP_VAL);
      long lPers = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      lTmp = lPers + lHpp + lFixture;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      */
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_OTH_VALUE);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_OTH_VALUE);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_OTH_VALUE);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_OTH_VALUE);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   //*(pOutbuf+OFF_STATUS) = getParcStatus_TEH(pRec->ParcType);
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"TEH");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // Exemption
   if (pRec->ExeCode1[0] > ' ')
   {
      memcpy(pOutbuf+OFF_EXE_CD1, pRec->ExeCode1, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD2, pRec->ExeCode2, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD3, pRec->ExeCode3, CRESIZ_EXEMP_CODE);
   }

   // HO Exempt
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (!memcmp(pRec->ExeCode1, "10", 2) || lTmp == 7000)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
   } else
   {
      lTmp = 0;
      strcpy(acTmp, BLANK32);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

   // Lot Sqft
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Recording info
   /* This is done in ApplyCumSale()
   if (pRec->RecBook1[3] > ' ')
   {
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      Teh_FmtRecDoc(pOutbuf, pRollRec);
   }
   */

   // Owners
   Teh_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address
   Teh_MergeAdr(pOutbuf, pRollRec);

   // Base year
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      pRec->LglDesc1[CRESIZ_LGL_DESC1+CRESIZ_LGL_DESC2] = 0;
      iTmp = updateLegal(pOutbuf, pRec->LglDesc1);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];
   else if (pRec->Garage[0] >= 'A')
   {
      switch (toupper(pRec->Garage[0]))
      {
         case 'A':   // Attached
         case 'C':   // Carport
         case 'D':   // Detached
            *(pOutbuf+OFF_PARK_TYPE) = pRec->Garage[0];
            break;
         case 'N':
         case 'Q':
         case 'S':
         case 'Y':
            *(pOutbuf+OFF_PARK_TYPE) = 'Z';
            break;
         default:
            LogMsg("XGarage: %c [%.10s]", pRec->Garage[0], pOutbuf);
            break;
      }
   }

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // FP - 0-5,?,A,C,D,E,F,G,N,P,R,S,W,Y
   // FirePlace:  G=Gas  P=Pellet W=Wood  Y=Yes has a fireplace
   if (pRec->Fp[0] > '0')
   {
      switch (pRec->Fp[0])
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         case 'G':
         case 'W':
         case 'Y':
            *(pOutbuf+OFF_FIRE_PL) = pRec->Fp[0];
            break;
         case 'P':
            *(pOutbuf+OFF_FIRE_PL) = 'S';
            break;
         default:
            *(pOutbuf+OFF_FIRE_PL) = ' ';
            LogMsg("XFp [%c] - %.12s", pRec->Fp[0], pOutbuf);
            break;
      }
   }

#ifdef _DEBUG
   //if (!memcmp(pRec->APN, "006360291", 9))
   //   iTmp = 0;
#endif
   // AC - 1,?,A,C,D,E,F,N,P,R,S,V,W,Y
   // Air Cond: C= Central Air  R= Refrig. Wall unit  S= Swamp cooler  ? = not sure
   if (pRec->Ac[0] > ' ')
   {
      pTmp = findXlatCode(pRec->Ac[0], &asCooling[0]);
      if (pTmp)
         *(pOutbuf+OFF_AIR_COND) = *pTmp;
      else
         LogMsg("XAir [%c] - %.12s", pRec->Ac[0], pOutbuf);
   } 

   // Heating - 1,2,3,8,9,?,B,C,D,E,F,G,O,P,S,V,W,X,Y
   // Heating:  B=baseboard  C=central  W=wall heater
   if (pRec->Heating[0] > ' ')
   {
      pTmp = findXlatCode(pRec->Heating[0], &asHeating[0]);
      if (pTmp)
         *(pOutbuf+OFF_HEAT) = *pTmp;
      else
         LogMsg("XHeat [%c] - %.12s", pRec->Heating[0], pOutbuf);
   } 

   // Pool/Spa - 1,A,D,F,P,Y
   if (pRec->Pool[0] == 'Y' && pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'C';
   else if (pRec->Pool[0] == 'Y' || pRec->Pool[0] == 'P')
      *(pOutbuf+OFF_POOL) = 'P';
   else if (pRec->Spa[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'S';
   else if (pRec->Spa[0] > ' ' || pRec->Pool[0] > ' ')
      LogMsg("XPool/Spa [%c/%c]", pRec->Pool[0], pRec->Spa[0]);

   // Zoning
   if (pRec->Zone[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, CRESIZ_ZONE);

   // Neighborhood

   // Type_Sqft - M, C

      // Bldg Class
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
         acTmp[iTmp++] = pRec->BldgCls[0];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[1]))
            acTmp[iTmp++] = pRec->BldgCls[1];
         else if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else
            acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   return 0;
}

/********************************** Teh_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 ****************************************************************************/

int Teh_LoadRoll(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, lLienUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Teh_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdLienExt)
         {
            iRet = PQ_MergeLien(acBuf, fdLienExt);
            if (!iRet)
               lLienUpd++;
         }

         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      } else
         iRetiredRec++;

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total lien updated records: %u\n", lLienUpd);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Teh_Load_LDR ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Teh_Load_LDR(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
#ifdef _DEBUG
      //if (!memcmp((char *)&acRollRec[CREOFF_APN], "001021004", 9))
      //   iRet=0;
#endif
      lLDRRecCount++;
      iRet = Teh_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      // Scan for bad character - LAS May 05
      while (iRet-- > 0)
         if (acRollRec[iRet] < ' ')
            acRollRec[iRet] = ' ';

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   // Check for NULL embeded in file
   iRet = replEmbededChar(acOutFile, NULL, 32, 32, -1, iRecLen);
   if (iRet < 0)
      LogMsg("***** Error opening %s for checking null", acOutFile);
   else if (iRet > 0)
      LogMsg("*** Ctrl character found in %s.  Please contact Sony", acOutFile);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/******************************* Cres_ExtrSale ******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Teh_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024];
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice;
   double   dTmp;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SIZ_SALE1_DT);
         memcpy(pSale->DocNum, acDoc, SIZ_SALE1_DOC);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            // Keep this check here until counties correct their roll
            if (!strchr(acTmp, '.'))
            {
               LogMsg0("??? Suspected stamp amount for APN: %.10s, StampAmt=%s", acBuf, acTmp);
               if (dTmp > 50000.00)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               else
                  memset(acTmp, ' ', SIZ_SALE1_AMT);
            } else
            {
               // Save DocTax
               iRet = sprintf(acTmp, "%.2f", dTmp);
               memcpy(pSale->StampAmt, acTmp, iRet);

               lPrice = (long)(dTmp * SALE_FACTOR);
               if (lPrice > 0)
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
               if (dTmp > 5000.00)
                  LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt);
            }
            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SIZ_SALE1_DT);
            memcpy(pSale->DocNum, acDoc, SIZ_SALE1_DOC);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Teh_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SIZ_SALE1_DT);
            memcpy(pSale->DocNum, acDoc, SIZ_SALE1_DOC);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lNewRec);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/******************************** FixDocTax **********************************
 *
 * Remove DocTax & SalePrice if it is carried over from prior sale.
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int Teh_FixDocTax(char *pInfile)
{
   char     acInbuf[1024], acLastRec[1024], acOutFile[_MAX_PATH], *pRec;
   long     lCnt=0, lClean=0, iTmp, lPrice, lLastPrice=0;
   FILE     *fdOut;

   SCSAL_REC *pInRec  = (SCSAL_REC *)&acInbuf[0];
   SCSAL_REC *pLastRec= (SCSAL_REC *)&acLastRec[0];

   if (_access(pInfile, 0))
   {
      LogMsg("***** FixCumSale(): Missing input file: %s", pInfile);
      return -1;
   }

   LogMsg("Fix DocTax and Sale Price for %s", pInfile);

   // Open input file
   LogMsg("Open input sale file %s", pInfile);
   fdSale = fopen(pInfile, "r");
   if (fdSale == NULL)
   {
      LogMsg("***** Error opening sale file: %s\n", pInfile);
      return -2;
   }

   // Open output file
   strcpy(acOutFile, pInfile);
   pRec = strrchr(acOutFile, '.');
   strcpy(pRec, ".out");
   LogMsg("Create output sale file %s", acOutFile);
   fdOut = fopen(acOutFile, "w");
   if (fdOut == NULL)
   {
      LogMsg("***** Error creating output sale file: %s\n", acOutFile);
      return -3;
   }

   // Convert loop
   while (!feof(fdSale))
   {
      if (!(pRec = fgets(acInbuf, 1024, fdSale)))
         break;

      // Remove bad seller name
      if (pInRec->Seller2[0] > ' ' && pInRec->Seller2[0] <= 'A')
         memset(pInRec->Seller2, ' ', SALE_SIZ_SELLER);

#ifdef _DEBUG
      //if (!memcmp(acInbuf, "0030851700", 9) || !memcmp(acInbuf, "0631011050", 9))
      //   iTmp = 0;
#endif
      lPrice = atoin(pInRec->SalePrice, SALE_SIZ_SALEPRICE);
      if (lPrice <= 1000)
      {
         lPrice = 0;
         memset(pInRec->SalePrice, ' ', SALE_SIZ_SALEPRICE);
      }

      if (!memcmp(pInRec->Apn, pLastRec->Apn, iApnLen))
      {
         if (pInRec->SalePrice[SALE_SIZ_SALEPRICE-2] > ' ' && 
             pInRec->Spc_Flg != SALE_FLG_CONFIRM &&
             lPrice == lLastPrice &&
             memcmp(pInRec->DocDate, pLastRec->DocDate, 8) >= 0)
         {
            if (bDebug)
               LogMsg("P&T %.10s: DocDate->%.8s DocNum->%.12s", pInRec->Apn, pInRec->DocDate, pInRec->DocNum);
            memset(pInRec->SalePrice,' ', SALE_SIZ_SALEPRICE);
            memset(pInRec->StampAmt, ' ', SALE_SIZ_STAMPAMT);
            memset(pInRec->Seller1,  ' ', SALE_SIZ_SELLER);
            pInRec->SaleCode[0] = ' ';
            lClean++;
         } else
         {
            strcpy(acLastRec, acInbuf);
            if (lPrice > 100)
               lLastPrice = lPrice;
         }
      } else
      {
         strcpy(acLastRec, acInbuf);
         lLastPrice = lPrice;
      }
      fputs(acInbuf, fdOut);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdSale)
      fclose(fdSale);
   if (fdOut)
      fclose(fdOut);

   if (lClean > 0)
   {
      // Save input file
      strcpy(acInbuf, pInfile);
      pRec = strrchr(acInbuf, '.');
      strcpy(pRec, ".org");
      if (!_access(acInbuf, 0))
         DeleteFile(acInbuf);

      // Save input file
      iTmp = rename(pInfile, acInbuf);

      // Rename output file
      iTmp = rename(acOutFile, pInfile);
   } else
      LogMsg("Good sale file, no record drop!");

   LogMsgD("\nTotal input records:     %u\n", lCnt);
   LogMsg("    records cleaned:     %u", lClean);

   return iTmp;
}

/*********************************** loadTeh ********************************
 *
 * Input files:
 *    redifile (899-bytes roll file)
 *
 *
 * Commands:
 *    -U -T   Monthly update
 *    -L      Lien update
 *    -Xl     Extract lien values
 *    -Xs     Extract/update cum sale
 *
 ****************************************************************************/

int loadTeh(int iLoadFlag, int iSkip)
{
   int   iRet=0;

   iApnLen = myCounty.iApnLen;
   iRollLen = guessRecLen(acRollFile, iRollLen);

   if (bLoadTax)
   {
      //iRet = Cres_Load_TaxRollInfo(true);
      iRet = Cres_Load_TaxBase(bTaxImport);
      if (!iRet)
         iRet = Cres_Load_TaxDelq(bTaxImport);
   }

   // Use SaleTmpl for cum sale file
   strcpy(acESalTmpl, acSaleTmpl);

   /*
   char  acTmpFile[_MAX_PATH];
   strcpy(acRollFile, "G:\\CO_DATA\\TEH\\2005\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\STeh_CD\\raw\\Teh_Sale.2005");
   iRet = Teh_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\TEH\\2006\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\STeh_CD\\raw\\Teh_Sale.2006");
   iRet = Teh_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\TEH\\2007\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\STeh_CD\\raw\\Teh_Sale.2007");
   iRet = Teh_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\TEH\\2008\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\STeh_CD\\raw\\Teh_Sale.2008");
   iRet = Teh_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\TEH\\2009\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\STeh_CD\\raw\\Teh_Sale.2009");
   iRet = Teh_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\TEH\\2010\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\STeh_CD\\raw\\Teh_Sale.2010");
   iRet = Teh_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\TEH\\2011\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\STeh_CD\\raw\\Teh_Sale.2011");
   iRet = Teh_ExtrSale(acRollFile, acTmpFile, false);
   */
   if (iLoadFlag & EXTR_SALE)                            // -Xs
   {
      iRet = Teh_ExtrSale(acRollFile, NULL, true);
      if (!iRet)
      {
         iLoadFlag |= MERG_CSAL;
       
         // Remove bad sale price
         iRet = Teh_FixDocTax(acCSalFile);
      }
   }

   // Extract lien
   if (iLoadFlag & EXTR_LIEN)                            // -Xl
      iRet = Cres_ExtrLien(myCounty.acCntyCode);

   if (iLoadFlag & LOAD_LIEN)                            // -L
      iRet = Teh_Load_LDR(myCounty.acCntyCode);
   else
      iRet = Teh_LoadRoll(myCounty.acCntyCode, iSkip);

   // County request not to put sale price out
   if (!iRet && (iLoadFlag & MERG_CSAL))
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);

   return iRet;
}
 