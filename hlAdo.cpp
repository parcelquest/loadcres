// hlAdo.cpp: implementation of the hlAdo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "hlAdo.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// ------------------------------------------------------------
// Helpfull functions
// ------------------------------------------------------------

inline void TESTHR(HRESULT x) {if FAILED(x) _com_issue_error(x);};

// return a string with the description of the ado error.
CString AdoError(_ConnectionPtr pConnection)
{
   // Print Provider Errors from Connection object.
   // pErr is a record object in the Connection's Error collection.
   ErrorPtr pErr = NULL;
	CString  s="";
   char     sTmp[256];

   if ((pConnection->Errors->Count) > 0)
   {
      long nCount = pConnection->Errors->Count;

      // Collection ranges from 0 to nCount -1.
      for (long i = 0; i < nCount; i++)
      {
         pErr = pConnection->Errors->GetItem(i);
         strcpy(sTmp, pErr->Description);
         s = s + sTmp;
         //s += pErr->Description;
      }
   } else 
      s="No Ado error.";
	return s;
}

// return a string with the description of the com error.
CString ComError(_com_error &e)
{
	CString s;
   _bstr_t bstrSource(e.Source());
   _bstr_t bstrDescription(e.Description());

   // Get Com errors.  
   s.Format("Error\nCode = %08lx\nCode meaning = %s\nSource = %s\nDescription = %s\n", e.Error(),e.ErrorMessage(),(LPCSTR) bstrSource,(LPCSTR) bstrDescription);
	return s;
}

// ------------------------------------------------------------

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

hlAdo::hlAdo(char* constr)
{
	IsOpen=false;
	DoesTrans=false;
	comm=NULL;
	// initialize the COM library. No problem if it is
	// already initialized.
	if(FAILED(CoInitializeEx(NULL,COINIT_MULTITHREADED|COINIT_SPEED_OVER_MEMORY))) 
	{
		MessageBox(NULL,"Can't initialize COM library.","Error:",MB_ICONHAND|MB_OK);
		return;
	}
	// Now create the Connection object.
	TESTHR(pConn.CreateInstance(__uuidof(Connection)));
	pConn->ConnectionString=constr;
	pConn->Open("","","",NULL);
	IsOpen=true;
}

hlAdo::hlAdo(CString &constr)
{
	hlAdo((char*)(LPCTSTR)constr);
}

hlAdo::hlAdo()
{
	IsOpen=false;
	DoesTrans=false;
	comm=NULL;
}

hlAdo::~hlAdo()
{
	if (DoesTrans) 
      RollbackTrans();

	if (comm) 
	{
		// if there was a command, we shall release the
		// created object.
		comm.Release();
	}
	// close the connection to the database
	if (IsOpen) 
	{
		pConn->Close();
		IsOpen=false;

	   pConn.Release();
	   // and uninitialize the COM library.
	   CoUninitialize();
   }
}

bool hlAdo::Connect(LPCTSTR constr)
{
	IsOpen=false;
	DoesTrans=false;
	comm=NULL;

   // initialize the COM library. No problem if it is
	// already initialized.
	if(FAILED(CoInitializeEx(NULL,COINIT_MULTITHREADED|COINIT_SPEED_OVER_MEMORY))) 
	{
		MessageBox(NULL,"Can't initialize COM library.","Error:",MB_ICONHAND|MB_OK);
   } else
   {
	   // Now create the Connection object.
	   TESTHR(pConn.CreateInstance(__uuidof(Connection)));
	   pConn->ConnectionString=constr;
	   pConn->Open("","","",NULL);
	   IsOpen=true;
   }

   return IsOpen;
}

void hlAdo::ExecuteCommand(char* sql, int iTimeOut)
{
	// execute a query that doesn't return any results.

	// if we didn't ExecuteCommand before,we shall create the command object
	// for the first time.
	if (comm==NULL) 
      TESTHR(comm.CreateInstance(__uuidof(Command)));

	comm->ActiveConnection = pConn;
   if (iTimeOut > 0)
      comm->CommandTimeout = iTimeOut;
	comm->CommandText = sql;
	comm->Execute(NULL, NULL, adCmdText);
}

void hlAdo::ExecuteCommand(CString &sql, int iTimeOut)
{
   ExecuteCommand((char*)(LPCTSTR)sql, iTimeOut);
}

hlAdoRs * hlAdo::Execute(char *sql)
{
	// execute a query that returns values
	_RecordsetPtr rsp=Execute_(sql);
	// on error return NULL. The Err has already been set
	if (rsp==NULL) 
      return NULL;

	// else just prepare the hlAdoRs object and return it.
	hlAdoRs *rs=new hlAdoRs(rsp,this);
	return rs;
}

hlAdoRs * hlAdo::Execute(CString &sql)
{
	return Execute((char*)(LPCTSTR)sql);
}

inline _ConnectionPtr hlAdo::GetConnection()
{
	// return the pointer to the Connection object
	return pConn;
}


_RecordsetPtr hlAdo::Execute_(char *sql)
{
	// execute a query that returns values
	_RecordsetPtr rsp;
	rsp=pConn->Execute(sql,NULL,0);
	return rsp;
}

_RecordsetPtr hlAdo::Execute_(CString &sql)
{
	return Execute_((char*)(LPCTSTR)sql);
}

// Transaction functions for hlAdo
void hlAdo::BeginTrans()
{
	pConn->BeginTrans();
	DoesTrans=true;
}

void hlAdo::CommitTrans()
{
	pConn->CommitTrans();
	DoesTrans=false;
}

void hlAdo::RollbackTrans()
{
	pConn->RollbackTrans();
	DoesTrans=false;
}
// End of transaction functions for hlAdo

// ------------------------------------------------------------
// The hlAdoRS implementation
// ------------------------------------------------------------

hlAdoRs::hlAdoRs(_RecordsetPtr rsp,hlAdo *_hlado)
{
	// initialize the hlAdoRs object. 
	rs=rsp;
	hlado=_hlado;
	// just_in is used to say that we shall do a
	// MoveFirst() in the first call to next()
	just_in=true;
}

hlAdoRs::~hlAdoRs()
{
	// done with the object,so release the recordset
	if (rs!=NULL) 
	{
		rs->Close();
		rs.Release();
      isOpened = false;
	}
}

bool hlAdoRs::next()
{
	// we'll move to the next row of our recordset.

	// if we are calling this for the first time, just
	// use the MoveFirst() function, otherwise call MoveNext()
	if (just_in)
	{
		just_in=false;
		if (rs->EndOfFile) return FALSE;
		rs->MoveFirst();
		return rs->EndOfFile==FALSE;
	}

	rs->MoveNext();
	return rs->EndOfFile==FALSE;
}

CString hlAdoRs::GetItem(char *ColumnName)
{
	// returns the data stored in the columnName at the
	// current row.
	_variant_t tmp;
	tmp=rs->Fields->GetItem(ColumnName)->Value;
	if (V_VT(&tmp)!=VT_NULL)
	{
		tmp.ChangeType(VT_BSTR);

		return (LPCTSTR)((_bstr_t)tmp);
	}
	return "";
}

CString hlAdoRs::GetItem(CString &ColumnName)
{
	return GetItem((char*)(LPCTSTR)ColumnName);
}

hlAdoRs::hlAdoRs(hlAdo &hl, char* sql)
{
	// initialize the hlAdoRs object. 
	rs=hl.Execute_(sql);
	hlado=&hl;
	// just_in is used to say that we shall do a
	// MoveFirst() in the first call to next()
	just_in=true;
   isOpened = true;
}

hlAdoRs::hlAdoRs(hlAdo &hl, CString &sql)
{
	hlAdoRs(hl,(char*)(LPCTSTR)sql);
}

hlAdoRs::hlAdoRs()
{
   isOpened = false;
}

bool hlAdoRs::Open(hlAdo &hl, char *sql)
{
	// initialize the hlAdoRs object. 
	rs=hl.Execute_(sql);
	hlado=&hl;
	// just_in is used to say that we shall do a
	// MoveFirst() in the first call to next()
	just_in=true;
   isOpened = true;
   return true;
}

void hlAdoRs::Close()
{
	// done with the object,so release the recordset
	if (rs!=NULL) 
	{
		rs->Close();
		rs.Release();
      rs = NULL;
      isOpened = false;
	}
}