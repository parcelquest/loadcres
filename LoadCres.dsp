# Microsoft Developer Studio Project File - Name="LoadCres" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=LoadCres - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "LoadCres.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "LoadCres.mak" CFG="LoadCres - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "LoadCres - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "LoadCres - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Cddata/LoadCrest.NET", SGNAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "LoadCres - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "_AFXDLL" /D "_WIN32_DCOM" /FR /Yu"stdafx.h" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 D:\CDData\Projects\ProdLib\ProdLib.lib D:\MISC\lib32\see32.lib D:\MISC\lib32\OTSW32.LIB kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "LoadCres - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "_AFXDLL" /D "_WIN32_DCOM" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 D:\CDData\Projects\ProdLib\ProdLib.lib D:\MISC\lib32\see32.lib D:\MISC\lib32\OTSW32.LIB kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /out:"LoadCres.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "LoadCres - Win32 Release"
# Name "LoadCres - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\doGrGr.cpp
# End Source File
# Begin Source File

SOURCE=.\doOwner.cpp
# End Source File
# Begin Source File

SOURCE=.\doSort.cpp
# End Source File
# Begin Source File

SOURCE=.\doZip.cpp
# End Source File
# Begin Source File

SOURCE=.\FormatApn.cpp
# End Source File
# Begin Source File

SOURCE=.\Getopt.cpp
# End Source File
# Begin Source File

SOURCE=.\hlAdo.cpp
# End Source File
# Begin Source File

SOURCE=.\LoadCres.cpp
# End Source File
# Begin Source File

SOURCE=.\LoadCres.rc
# End Source File
# Begin Source File

SOURCE=.\logs.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeAlp.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeDnx.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeGle.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeIny.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeLas.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeMod.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeSie.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeTeh.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeTri.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeTuo.cpp
# End Source File
# Begin Source File

SOURCE=.\PQ.cpp
# End Source File
# Begin Source File

SOURCE=.\R01.cpp
# End Source File
# Begin Source File

SOURCE=.\SaleRec.cpp
# End Source File
# Begin Source File

SOURCE=.\SendMail.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Tables.cpp
# End Source File
# Begin Source File

SOURCE=.\Tax.cpp
# End Source File
# Begin Source File

SOURCE=.\Update.cpp
# End Source File
# Begin Source File

SOURCE=.\UseCode.cpp
# End Source File
# Begin Source File

SOURCE=.\Utils.cpp
# End Source File
# Begin Source File

SOURCE=.\XlatTbls.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\CharRec.h
# End Source File
# Begin Source File

SOURCE=.\CountyInfo.h
# End Source File
# Begin Source File

SOURCE=.\Cres_TaxInfo.h
# End Source File
# Begin Source File

SOURCE=.\doGrGr.h
# End Source File
# Begin Source File

SOURCE=.\doOwner.h
# End Source File
# Begin Source File

SOURCE=.\doSort.h
# End Source File
# Begin Source File

SOURCE=.\doZip.h
# End Source File
# Begin Source File

SOURCE=.\FormatApn.h
# End Source File
# Begin Source File

SOURCE=.\getopt.h
# End Source File
# Begin Source File

SOURCE=.\hlAdo.h
# End Source File
# Begin Source File

SOURCE=.\LCExtrn.h
# End Source File
# Begin Source File

SOURCE=.\LoadCres.h
# End Source File
# Begin Source File

SOURCE=.\logs.h
# End Source File
# Begin Source File

SOURCE=.\MergeDnx.h
# End Source File
# Begin Source File

SOURCE=.\MergeIny.h
# End Source File
# Begin Source File

SOURCE=.\MergeTuo.h
# End Source File
# Begin Source File

SOURCE=.\OutRecs.h
# End Source File
# Begin Source File

SOURCE=.\PQ.h
# End Source File
# Begin Source File

SOURCE=.\ProdLib.h
# End Source File
# Begin Source File

SOURCE=.\R01.h
# End Source File
# Begin Source File

SOURCE=.\RecDef.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SaleRec.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\Tables.h
# End Source File
# Begin Source File

SOURCE=.\Tax.h
# End Source File
# Begin Source File

SOURCE=.\Update.h
# End Source File
# Begin Source File

SOURCE=.\UseCode.h
# End Source File
# Begin Source File

SOURCE=.\Utils.h
# End Source File
# Begin Source File

SOURCE=.\XlatTbls.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=".\DNX Notes.txt"
# End Source File
# Begin Source File

SOURCE=".\GLE Notes.txt"
# End Source File
# Begin Source File

SOURCE=".\INY Notes.txt"
# End Source File
# Begin Source File

SOURCE=.\Las_Notes.txt
# End Source File
# Begin Source File

SOURCE=.\LoadCres.ini
# End Source File
# Begin Source File

SOURCE=.\Mod_Notes.txt
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\Teh_Notes.txt
# End Source File
# Begin Source File

SOURCE=.\Tuo_Notes.txt
# End Source File
# End Target
# End Project
