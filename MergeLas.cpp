/*****************************************************************************
 *
 *
 * Revision:
 * 08/26/2005 1.3.2     Change Las_MergeAdr() to fix bug related to house#.  Also
 *                      use parseAdr1_1() instead of parseAdr1() to handle different
 *                      suffix spellings.
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 08/04/2009 9.1.3     Populate other values.
 * 02/23/2010 9.2.1     Fix CARE_OF bug and modify Las_MergeOwner() to keep original 
 *                      owner name when possible.
 * 06/30/2011 11.1.0    Save original StrNum in S_HSENO.
 * 07/22/2011 11.1.2    Clear out old situs before applying new one.
 * 10/26/2011 11.2.8.1  Add Las_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      Modify Las_Fmt1Doc() to correct DocNum. Use ApplyCumSale()
 *                      to update R01 file. Modify Las_CreateR01() not to populate
 *                      sale info.  Let ApplyCumSale() populates it from Las_Sale.sls.
 * 11/07/2011 11.2.10   Put the three transactions back on R01 file just as what are
 *                      on redifile even if there no sale date.  This is county request.
 * 07/26/2013 13.1.3    Modify Las_CreateR01() to add APN_D, YR_ASSD, BLDGCLS, BLDGQUAL.
 * 09/27/2013 13.3.0    Modify Las_MergeOwner() and add Las_CleanName() to update Vesting.
 *                      Also added additional chars data (awaiting xlat table).
 *                      Use removeNames() to clear owner names.
 * 09/29/2013 13.3.1    Fix Las_MergeOwner() and remove comma from owner name.
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 02/03/2014 13.5.1    Modify Las_CreateR01() to add PROP8 flag.
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 12/07/2015 15.3.0    Add option to load tax file.
 * 02/14/2016 15.6.0    Modify Las_MergeOwner()to remove Name2 if it's the same as Name1.
 * 07/25/2016 16.1.2    Fix CareOf in Las_MergeAdr()
 * 08/18/2016 16.2.0    Add option to load Tax Agency from external text file.
 * 12/05/2016 16.7.1    Modify Las_ExtrSale() to add Full/Partial flag
 * 11/17/2017 17.1.0    Call updateDelqFlag() to update Delq info in Tax_Base table
 * 02/05/2018 17.3.1    Modify Las_CreateR01() to add MultiApn flag.
 * 05/08/2018 17.7.0    Use new updateDelqFlag() to handle special case for LAS.
 * 06/30/2019 19.0.1    Modify Las_MergeAdr() to handle more special cases of mail adr.
 *                      Add -Ms back on but no sale price added.
 *                      Call ApplyCumSale() with SALE_USE_SCNOPRICE instead of SALE_USE_SCUPDXFR
 * 07/24/2019 19.1.0    Modify Las_MergeAdr() to remove CareOf before updating.
 * 04/27/2020 19.6.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 06/24/2020 20.0.0    Add -Mz 
 * 07/18/2020 20.1.1    Modify Las_MergeAdr() to verify direction before assign to S_DIR.
 * 07/21/2020 20.1.2    Modify Las_CreateR01() to clear out ZONE before apply new one.
 * 08/03/2020 20.1.3    Modify Las_MergeAdr() to handle special case of situs number.  When 
 *                      situs number is bad and it's partial matching with mailing, use mailing number.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "doSort.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Pq.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "SendMail.h"
#include "Tax.h"
#include "SqlExt.h"
#include "MergeZoning.h"

 /********************************* Las_Fmt1Doc *******************************
 *
 *   1) -01651  20040227= 01651 & 20040227: Fern displays doc# as -016 5100
 *   2) 0706061 19981201= This is not doc#, but recorder book 706, page 61.  
 *      Old format can be displayed as 706-061
 *
 *****************************************************************************/

void Las_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp;
   char  acDate[16], acDoc[64], *pTmp, *pDate;
   bool  bMisAligned = false;

   pTmp = pRecDoc;
   pDate = pRecDoc+8;
   acDoc[0] = 0;

   // Input 12345678yyyymmdd
   // Output yyyymmdd123456789AB
   acDate[8] = 0;
   if (*pDate == ' ')
      memset(acDate, ' ', 8);
   else if (*(pDate-1) == ' ' && *(pDate+7) == ' ')
   {
      memcpy(acDate, pDate, 8);
      myTrim(acDate);
      if ((iTmp = strlen(acDate)) == 4)      // Missing MMDD?
         strcat(acDate, "0101");             // 1942,1982,2001,2002,2000
      else if (iTmp == 6)
         strcat(acDate, "01");               // Missing DD?
      else                                   // 199304
         memset(acDate, ' ', 8);             // Unknown 
      // 2000103,1990625,1990729,1978314,1990807,1990823
   } else if (*(pDate+7) == ' ')
   {
      bMisAligned = true;
      if (*(pDate+6) != ' ' && *(pDate-1) != ' ')
      {
         memcpy(acDate, pDate-1, 8);          
      } else if (*(pDate+6) != ' ' && *(pDate-1) == ' ')
      {
         // 1971231
         memcpy(acDate, pDate, 7);          
         acDate[7] = '0';
      } else if (*(pDate+5) != ' ' && *(pDate-2) != ' ')
      {
         memcpy(acDate, pDate-2, 8);          
      } else if (!memcmp(pDate, "VC", 2))
      {
         memcpy(acDate, pTmp, 8);
         memcpy(acDoc, pDate, 8);
         memset((char*)&acDoc[8], ' ', 4);
      } else
      {  // VC
         memset(acDate, ' ', 8);
         bMisAligned = false;
      }
   } else
      memcpy(acDate, pDate, 8);

   if (acDate[0] > ' ' && !isValidYMD(acDate))
   {
      memset(acDate, ' ', 8);
      bMisAligned = false;
   }
   memcpy(pOutDate, acDate, 8);

   if (acDoc[0])
      memcpy(pOutDoc, acDoc, 12);
   else if (*pTmp > ' ' && memcmp(pTmp, "0000000", 7))
   {
      if (*pTmp == '-')
         sprintf(pOutDoc, "%.4s%.6s  ", acDate, pTmp);
      else
      {
         memcpy(acDoc, pTmp, 8);
         acDoc[8] = 0;
         pTmp = strchr(acDoc, ' ');
         if (pTmp)
            while (*pTmp)
               *pTmp++ = ' ';

         iTmp = atoin(acDoc, 8);
         if (iTmp > 1000)     // Min val 0001001
         {
            if (bMisAligned) 
               iTmp = 3;
            else
               iTmp = 4;

            if (acDoc[0] == '0')
               sprintf(pOutDoc, "%.4s-%.*s    ", acDoc, iTmp, (char *)&acDoc[4]);
            else
               sprintf(pOutDoc, "%.3s-%.*s    ", acDoc, iTmp+1, (char *)&acDoc[3]);
         } else
            sprintf(pOutDoc, "%.8s    ", acDoc);            
      }
   } else
   {
      memset(pOutDoc, ' ', SIZ_SALE1_DOC);
   }
}

/********************************* Las_FmtRecDoc *****************************
 *
 *   1) -01651  20040227= 01651 & 20040227: Fern displays doc# as -016 5100
 *   2) 0706061 19981201= This is not doc#, but recorder book 706, page 61.  
 *      Old format can be displayed as 706-061
 *
 *****************************************************************************/

void Las_FmtRecDoc(char *pOutbuf, char *pRecDoc1)
{
   char  acDate[16], acDoc[64], *pTmp;
   bool  bMisAligned = false;

   pTmp = pRecDoc1;
   Las_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE1) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

   // Update last recording date
   long lDate = atol(acDate);
   if (lDate > lLastRecDate && lDate < lToday)
      lLastRecDate = lDate;

   // Advance to Rec #2
   pTmp += 16;
   Las_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE2) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
   memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);

   // Advance to Rec #3
   pTmp += 16;
   Las_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE3) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
   memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
}

/******************************** Las_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************

void Las_MergeOwner1(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acTmp1[64], acSave[64], acName2[64];
   char  *pTmp, *pTmp1, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);

   // Initialize
   pName1 = pNames;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   acName2[CRESIZ_NAME_2] = 0;
   pName2 = acName2;
   pTmp1 = _strupr(acName2);

   // Check owner2 for # - drop them
   if (*pName2 == '#' )
   {
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (strchr(acName2, '&'))
   {
      // Process as two names
      memcpy(acOwners, pName1, CRESIZ_NAME_1);
      acOwners[CRESIZ_NAME_1] = 0;

      // Check for vesting
      pTmp1 = strrchr(myTrim(acOwners), ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp)
         {
            //strcpy(pOwners->acVest, acTmp1);
            memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
            *pTmp1 = 0;
         }
      } 
      if (pTmp = strstr(acOwners, " ALL AS"))
         *pTmp = 0;

      // Parse owner 1
      splitOwner(acOwners, &myOwner);
      memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, strlen(myOwner.acName1));
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, strlen(myOwner.acSwapName));

      // Process Name2
      if (*pName2 == '&')
         memcpy(acOwners, pName2+2, CRESIZ_NAME_1-2);
      else
         memcpy(acOwners, pName2, CRESIZ_NAME_1);
      acOwners[CRESIZ_NAME_1] = 0;

      // Check for vesting
      pTmp1 = strrchr(myTrim(acOwners), ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_LAS(acSave, acTmp1);
         if (*pTmp)
         {
            memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
            *pTmp1 = 0;
         }
      } 

      // Parse Owner 2
      splitOwner(acOwners, &myOwner);
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName1, strlen(myOwner.acName1));
      return;
   }

   // Remove multiple spaces
   memcpy(acOwners, pName1, CRESIZ_NAME_1+CRESIZ_NAME_2);
   acOwners[CRESIZ_NAME_1+CRESIZ_NAME_2] = 0;
   pTmp = acOwners;

   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Check for AKA - replace it with '&'
   pTmp = strstr(acTmp, " AKA ");
   if (pTmp)
   {
      *(pTmp+1) = '&';
      *(pTmp+2) = ' ';
      *(pTmp+3) = ' ';
   }

   // Save name1
   strcpy(acOwners, acTmp);

   // Blank out "SUC TRS" and "CO TRS"
   if (pTmp = strstr(acTmp, " SUC TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " CO TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " DVA"))
      *pTmp = 0;

  // Check for vesting
   pTmp1 = strrchr(acTmp, ' ');
   if (pTmp1)
   {
      strcpy(acSave, ++pTmp1);
      pTmp = getVestingCode_LAS(acSave, acTmp1);
      if (*pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
         *pTmp1 = 0;
      } else
      {
         if (pTmp1 = strstr(acTmp, " JT"))
         {
            memcpy(pOutbuf+OFF_VEST, "JT", 2);
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " TC"))
         {
            memcpy(pOutbuf+OFF_VEST, "TC", 2);
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " CP"))
         {
            memcpy(pOutbuf+OFF_VEST, "CP", 2);
            *pTmp1 = 0;
         }
      }
   } 

   // Save data to append later
   pTmp = strstr(acTmp, "FAM TRUST");
   if (pTmp)
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 1);
   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   if (strcmp(myOwner.acName1, myOwner.acName2))  
   {
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, strlen(myOwner.acName2));

      if (acSave[0])
         strcat(myOwner.acName1, acSave);
      memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, strlen(myOwner.acName1));
   } else
   {
      // Keep original name
      iTmp = strlen(acOwners);
      if (iTmp > SIZ_NAME1)
         iTmp = SIZ_NAME1;
      memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);
   }
}

/******************************** Las_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Las_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;
   int   iTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   if (pTmp=strstr(acTmp, " 1/"))
     *pTmp = 0;
   if (pTmp=strstr(acTmp, " - "))
     *pTmp = 0;
   if (pTmp=strchr(acTmp, '{'))
      *pTmp = 0;

   pTmp = &acTmp[0];
   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      pTmp++;

      // Remove known bad chars
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   blankRem((char *)&acTmp[0], iTmp);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Las_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Las_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acName1[64], acName2[64], acVesting[8];
   char  *pTmp, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   myTrim(acName1, CRESIZ_NAME_1);
   strcpy(acOwners, acName1);
   pName1 = acName1;
   acVesting[0] = 0;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   myTrim(acName2, CRESIZ_NAME_2);
   pName2 = _strupr(acName2);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1390604011", 9))
   //   iTmp = 0;
#endif

   // Check owner2 for # - drop them
   if (*pName2 == ' ')
      *pName2 = 0;
   else if (*pName2 == '#' )
   {
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '&')
   {
      pName2 += 2;
      // Remove Name2 if it's the same as Name1
      if (!strcmp(pName1, pName2))
         *pName2 = 0;
      else if (pTmp = strchr(pName1, ','))
      {
         iTmp = pTmp - pName1;
         if (!memcmp(pName1, pName2, iTmp))
         {
            // Combine Name1 & Name2 since they have the same last name
            MergeName(pName1, pName2, acOwners);
            pName2 = NULL;
         }
      }
   } 

   if (pName2 && *pName2 > ' ')
   {
      remChar(pName2, ',');
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      // Find vesting in Name2 
      pTmp = findVesting(pName2, acVesting);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

         // Check EtAl
         if (!memcmp(acVesting, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } 
   }

   // Remove multiple spaces
   iTmp = remChar(acOwners, ',');
   vmemcpy(pOutbuf+OFF_NAME1, acOwners, SIZ_NAME1, iTmp);

   // Cleanup Name1 
   iTmp = Las_CleanName(acOwners, acTmp, acVesting);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acName1, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // Now parse owners
   splitOwner(acName1, &myOwner, 3);
   if (acVesting[0] > ' ' && isVestChk(acVesting))
      memcpy(pOutbuf+OFF_NAME_SWAP, pOutbuf+OFF_NAME1, SIZ_NAME_SWAP);
   else
      vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);
}

/********************************* Las_MergeAdr ******************************
   % CARNS, P.O. BOX 182        ADIN, CA
   % B WAYMAN  171 COUNTRY CLUB COLUSA CA
   % LOCKLIN, 122 SHORELINE DR. PITTSBURG, CA
   %HERNANDEZ 69 ATHERTON AVE.  PITTSBURG, CA
   % 2550 LONGLEY LN            RENO NV
   % REECE, VERNON  P O BOX 221 STANDISH CA

   C/O ALBAUGH ALLEN P O BOX 218MCARTHUR CA
   C/O GOODWIN, R P O BOX 161   TOGIAK AK
   C/O 2517 SHAMROCK DR         SAN PABLO CA
   C/O 1787 PINION WAY          MORGAN HILL CA
   C/O 3421 108TH ST NW/BARILANIGIG HARBOR WA
   C/O WESTBY, 1814 BECKWITH LN LINCOLN CA
   C/O PADOVAN, BETH, MS #0222  RENO NV
   C/O WELLS, JOYCE 680 ELM ST  SUSANVILLE CA
   C/O HERMAN, JOSEPH           45 N TROPICANA CIRCLE
   44897 N E MACERO DR          EL MACERO CA
   RT 1 BOX 155                 WESTVILLE OK
   ST. RT. BOX 7                CHILCOOT, CA
   446-455 HONEY LAKE DR        HERLONG CA
   BOX 945                      HERLONG CA
   P O BOX 431                  HERLONG CA
   P.O. BOX 896                 HERLONG, CA
   2800 COTTAGE WAY SUITE W1834 SACRAMENTO CA
   6136 CO RD 12                ORLAND CA
   1280 N IRONWOOD DR LOT 4     APACHE JUNCTION AZ
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Las_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     *pTmp, *pAddr1, *pAddr2, acTmp[256], acAddr1[64], acAddr2[64], acCareOf[64];
   int      iTmp, iStrNo, iIdx;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing & CareOf
   removeMailing(pOutbuf, true);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1192706711", 8))
   //   iTmp = 0;
#endif
   
   // Check for blank address. If mail is blank, situs is always blank
   if (!memcmp(pRec->M_Addr1, "     ", 5))
      return;

   memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
   pAddr1 = myTrim(acAddr1, CRESIZ_ADDR_1);
   memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
   blankRem(acAddr2, CRESIZ_ADDR_2);
   pAddr2 = acAddr2;

   iTmp = 0;
   acCareOf[0] = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
      {
         // Set start of CareOf
         iIdx = 2;
         if (acAddr1[1] > ' ')
            iIdx = 1;

         // Assume addr1 starts here
         pAddr1 += iIdx;   

         if ((pTmp = strstr(pAddr1, "P O ")) || (pTmp = strstr(pAddr1, "P.O. ")))
         {
            // % REECE, VERNON  P O BOX 221
            pAddr1 = pTmp;
            *(pTmp-1) = 0;
            strcpy(acCareOf, &acAddr1[iIdx]);
            iTmp = remChar(acCareOf, ',');
            blankRem(acCareOf);
         } else if (isdigit(acAddr1[iIdx]))
         {
            // % 2550 LONGLEY LN
            pAddr1 = &acAddr1[iIdx];
         } else
         {
            // Replace comma with space
            iTmp = replChar(acAddr1, ',', ' ');
            // % B WAYMAN  171 COUNTRY CLUB
            // % LOCKLIN, 122 SHORELINE DR.
            if (pTmp = strstr(acAddr1, "  "))
            {
               pAddr1 = pTmp + 2;
               *pTmp = 0;
               strcpy(acCareOf, &acAddr1[iIdx]);
            } else if (pTmp = strchr(&acAddr1[iIdx], ' '))
            {
               if (isdigit(*(pTmp+1)))
               {
                  // %HERNANDEZ 69 ATHERTON AVE.
                  pAddr1 = pTmp+1;
                  *pTmp = 0;
                  strcpy(acCareOf, &acAddr1[iIdx]);
               } else
               {
                  pAddr1 = acAddr1;    // Can't parse?
                  LogMsg("*** Bad M_Street: %s APN=%.10s", pAddr1, pRec->APN);
               }
            } 
         }
      } else
      {
         // C/O
         iIdx = 4;
         pAddr1 += 4;
         if ((pTmp = strstr(pAddr1, "P O ")) || (pTmp = strstr(pAddr1, "P.O. ")) || (pTmp = strstr(pAddr1, "PO BOX")))
         {
            // C/O ALBAUGH ALLEN P O BOX 218
            pAddr1 = pTmp;
            *(pTmp-1) = 0;
            strcpy(acCareOf, &acAddr1[iIdx]);
            iTmp = remChar(acCareOf, ',');
            blankRem(acCareOf);
         } else if (isdigit(acAddr1[iIdx]))
         {
            // C/O 2517 SHAMROCK DR
            pAddr1 = &acAddr1[iIdx];
         } else
         {
            // Replace comma with space
            iTmp = replChar(acAddr1, ',', ' ');
            // C/O WELLS, JOYCE 680 ELM ST
            // C/O WESTBY, 1814 BECKWITH LN LINCOLN CA
            if ((pTmp = strstr(acAddr1, "  ")) && isdigit(*(pTmp+2)) )
            {
               pAddr1 = pTmp + 2;
               *pTmp = 0;
               strcpy(acCareOf, &acAddr1[iIdx]);
            } else if (pTmp = strchr(&acAddr1[iIdx], ' '))
            {
               if (isdigit(*(pTmp+1)))
               {
                  // %HERNANDEZ 69 ATHERTON AVE.
                  pAddr1 = pTmp+1;
                  *pTmp = 0;
                  strcpy(acCareOf, &acAddr1[iIdx]);
               } else if (isdigit(acAddr2[0]))
               {
                  pAddr1 = acAddr2;
                  pAddr2 = NULL;
                  strcpy(acCareOf, &acAddr1[iIdx]);
               } else if (pTmp = strstr(acAddr1, " MS02"))
               {
                  pAddr1 = pTmp+1;
                  *pTmp = 0;
                  strcpy(acCareOf, &acAddr1[iIdx]);
               } else
               {
                  pAddr1 = acAddr1;    // Can't parse?
                  LogMsg("*** Bad M_Street: %s APN=%.10s", pAddr1, pRec->APN);
               }
            } 
         }
      }

      // Check for C/O name
      if (acCareOf[0] >= 'A' && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         updateCareOf(pOutbuf, acCareOf, strlen(acCareOf));
      }
   }

   // Start processing
   memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
   iTmp = atoin(pRec->M_Zip, 5);
   if (iTmp > 10001)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   }

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseAdr1_1(&sMailAdr, pAddr1);

   if (pAddr2)
   {
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pAddr2, strlen(pAddr2));
      parseAdr2(&sMailAdr, myTrim(acAddr2));
   } else if (isdigit(pRec->M_Zip[0]))
   {
      //char acCity[64], acState[64], acCountry[64];
      // If City not available, lookup zipcode
      pRec->M_Zip[5] = 0;
      iTmp = locateCity(pRec->M_Zip, sMailAdr.City, sMailAdr.State, acTmp);
      if (iTmp == 1)
      {
         iTmp = sprintf(acAddr2, "%s, %s %s", sMailAdr.City, sMailAdr.State, pRec->M_Zip);
         memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);
      }
   } else if (pRec->M_Zip[0] >= 'A')
   {
      pRec->M_Zip4[3] = 0;
      iTmp = sprintf(acAddr2, "%s", pRec->M_Zip);
      memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, iTmp);
   }

   if (sMailAdr.lStrNum > 0)
   {
      // Remove '-' in strNum
      if (pTmp = strchr(sMailAdr.strNum, '-'))
      {
         *pTmp = 0;
         sprintf(acTmp, "%s%s    ", sMailAdr.strNum, pTmp+1);
         acTmp[SIZ_M_STRNUM] = '\0';
         *pTmp = '-';      // Put '-' back
         memcpy(pOutbuf+OFF_M_STRNUM, acTmp, SIZ_M_STRNUM);
      } else
         memcpy(pOutbuf+OFF_M_STRNUM, sMailAdr.strNum, strlen(sMailAdr.strNum));

      if (sMailAdr.strSub[0] > '0')
      {
         sprintf(acTmp, "%s  ", sMailAdr.strSub);
         memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
      }
   } 

   if (sMailAdr.strDir[0] > 'A')
      vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   if (sMailAdr.strName[0] > '0')
      vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   if (sMailAdr.strSfx[0] >= 'A')
      vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
   if (sMailAdr.City[0] >= 'A')
      vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   if (sMailAdr.State[0] >= 'A')
      vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

   // Situs 
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "1033031211", iApnLen))
   //   iStrNo=0;
#endif

   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      ADR_REC sSitusAdr;
      int   iIdx=0;

      removeSitus(pOutbuf);

      // Remove leading space
      memcpy(acTmp, pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
      myBTrim(acTmp, CRESIZ_SITUS_STREETNO);

      // Save original StrNum that may includes hyphen
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));

      // Prepare display field
      iTmp = sprintf(acAddr1, "%s %s", acTmp, myTrim(pRec->S_StrName, CRESIZ_SITUS_STREETNAME));
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // Prevent situation likes 123 1/2 NORTH STREET.  We have to break
      // into strnum=123, strsub=1/2
      if (pTmp = strchr(acTmp, ' '))
      {
         pTmp++;
         if (!memcmp(pTmp, "1/2", 3) || !memcmp(pTmp, "1\\2", 3))
            memcpy(pOutbuf+OFF_S_STR_SUB, "1/2", 3);
         else if (!memcmp(pTmp, "A/B", 3) || !memcmp(pTmp, "A&B", 3))
            memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, 3);
         else if (!strcmp(pTmp, "AB") || !strcmp(pTmp, "& B"))
            memcpy(pOutbuf+OFF_S_STR_SUB, "A&B", 3);
         else if (!memcmp(acTmp, sMailAdr.strNum, strlen(sMailAdr.strNum)))
         {
            // When situs number is bad and it's partial matching with mailing, use mailing number.
            strcpy(acTmp, sMailAdr.strNum);
            memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
            memcpy(pOutbuf+OFF_S_HSENO, acTmp, strlen(acTmp));
         } else
            LogMsg("??? StrNum: %s [%.10s]", acTmp, pOutbuf);
      }

      // Remove '-' in strNum
      if (pTmp = strchr(acTmp, '-'))
      {
         *pTmp = 0;
         sprintf(acAddr2, "%s%s ", acTmp, pTmp+1);
         acAddr2[SIZ_S_STRNUM] = '\0';
         *pTmp = '-';               // Put '-' back
         vmemcpy(pOutbuf+OFF_S_STRNUM, acAddr2, SIZ_S_STRNUM);
      } else
      {
         iTmp = sprintf(acTmp, "%d", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      }

      // Copy street name
      strcpy(acTmp, pRec->S_StrName);
      parseAdr1S(&sSitusAdr, acTmp);

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
      {
         if (isDir(sSitusAdr.strDir))
            memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
         else if (!memcmp(sSitusAdr.strDir, "NO", 2) || !memcmp(sSitusAdr.strDir, "SO", 2))
            *(pOutbuf+OFF_S_DIR) = sSitusAdr.strDir[0];
         else
            LogMsg("??? Invalid situs direction: %s", sSitusAdr.strDir);
      }

      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

      // Assuming that no situs city and we have to use mail city
      acAddr2[0] = 0;
      if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         City2Code(sMailAdr.City, acTmp, pOutbuf);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            memcpy(pOutbuf+OFF_S_ST, "CA", 2);
            strcpy(acAddr2, sMailAdr.City);
         } 
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      } else
      {
         // Condition: Valid city name, Ex_Val is 7000, 
         // Use_Code is N?,B1,A1,A2,C1,T1,R1,S1
         long lVal;
         lVal = atoin(pRec->ExVal, CRESIZ_EX_VAL);

         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && 
             (*(pOutbuf+OFF_HO_FL) == '1') )
         {
            // Get city code
            City2Code(sMailAdr.City, acTmp, pOutbuf);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ST, "CA", 2);
               memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
               strcpy(acAddr2, sMailAdr.City);
            }
         }            
      }

      if (acAddr2[0])
      {
         strcat(myTrim(acAddr2), " CA");
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      }
   }
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Las_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acTmp1[32], acCode[32];
   long     lTmp; 
   int      iTmp, iRet;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "18LAS", 5);

      // Create APN_D 
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, (LONGLONG)lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }

      // Exemption codes
      if (pRec->ExeCode1[0] > ' ')
      {
         memcpy(pOutbuf+OFF_EXE_CD1, pRec->ExeCode1, CRESIZ_EXEMP_CODE);
         memcpy(pOutbuf+OFF_EXE_CD2, pRec->ExeCode2, CRESIZ_EXEMP_CODE);
         memcpy(pOutbuf+OFF_EXE_CD3, pRec->ExeCode3, CRESIZ_EXEMP_CODE);
      }

      // HO Exempt
      lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
      if (!memcmp(pRec->ExeCode1, "10", 2) || lTmp == 7000)
         *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
      else
         *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

      // Exemp total
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
         memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
      }
   }

   iRet = 0;

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   //*(pOutbuf+OFF_STATUS) = getParcStatus_LAS(pRec->ParcType);
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"LAS");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
   } else
   {
      lTmp = 0;
      strcpy(acTmp, BLANK32);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

   // Lot Sqft
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Recording info
   if (pRec->RecBook1[0] > ' ')
   {
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      Las_FmtRecDoc(pOutbuf, pRec->RecBook1);

      // Check Full/Partial transfer
      if (pRec->LandBaseYr[3] == 'X')
         *(pOutbuf+OFF_MULTI_APN) = 'Y';
   }
   
   // Owners
   Las_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address 
   Las_MergeAdr(pOutbuf, pRollRec);

   // Base year
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      memcpy(acTmp, pRec->LglDesc1, CRESIZ_LGL_DESC1+CRESIZ_LGL_DESC2);
      acTmp[CRESIZ_LGL_DESC1+CRESIZ_LGL_DESC2] = 0;
      updateLegal(pOutbuf, acTmp);
   }
      
   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // FP - 1,2
   if (pRec->Fp[0] > ' ')
      *(pOutbuf+OFF_FIRE_PL) = pRec->Fp[0];
      //LogMsg("XFp: %c", pRec->Fp[0]);

   // AC - 1
   if (pRec->Ac[0] > ' ')
      LogMsg("XAir: %c", pRec->Ac[0]);

   // Heating - no data
   if (pRec->Heating[0] > ' ')
      LogMsg("XHeat: %c", pRec->Heating[0]);

   // Pool/Spa - 1
   if (pRec->Pool[0] > ' ')
      //*(pOutbuf+OFF_POOL) = 'P';
      LogMsg("XPool: %c", pRec->Pool[0]);

   // Zoning - no data
   memset(pOutbuf+OFF_ZONE, ' ', SIZ_ZONE);
   if (pRec->Zone[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, SIZ_ZONE);

   // Neighborhood

   // Type_Sqft - M, C

   // Bldg Class
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
         acTmp[iTmp++] = pRec->BldgCls[0];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[1]))
            acTmp[iTmp++] = pRec->BldgCls[1];
         else if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else
            acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   // Prop8
   iTmp = atoin(&pRec->ReappReason[0], 2);
   if (iTmp == 42)
      *(pOutbuf+OFF_PROP8_FLG) = 'Y';

   return iRet;
}

/********************************** Las_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 * This has not been tested 04/27/2008 spn
 *
 ****************************************************************************/

int Las_LoadRoll(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, lLienUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open lien extract file
   if (iLoadFlag & LOAD_UPDT)
   {
      sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
      LogMsg("Open lien extract file %s", acLienExt);
      fdLienExt = fopen(acLienExt, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
         return 2;
      }
   } else
      fdLienExt = NULL;

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

      iRet = Las_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         if (fdLienExt)
         {
            iRet = PQ_MergeLien(acBuf, fdLienExt);
            if (!iRet)
               lLienUpd++;
         }

         iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iRet >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
         else if (lLastRecDate < iRet)
            lLastRecDate = iRet;

         lLDRRecCount++;
         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      } else
         iRetiredRec++;

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lLDRRecCount);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total lien updated records: %u\n", lLienUpd);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lRecCnt = lLDRRecCount;
   printf("\nTotal output records: %u", lLDRRecCount);
   return 0;
}

/******************************* Las_ExtrSale *******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Las_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024], *pTmp;
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   double   dTmp;
   long     lCnt=0, lNewRec=0, lPrice;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            blankRem(acTmp);
            memcpy(pSale->StampAmt, acTmp, strlen(acTmp));
            // Keep this check here until counties correct their roll
            pTmp = strchr(acTmp, '.');
            if (!pTmp || *(pTmp+1) == ' ' || dTmp > 100000.00)
            {
               if (acTmp[0] == '0' || dTmp < 2000.00)
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if ((lPrice % 100) == 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                  } else
                  {
                     LogMsg0("??? Suspected stamp amount for APN: %.10s, StampAmt=%s", acBuf, acTmp);
                     memset(acTmp, ' ', SIZ_SALE1_AMT);
                  }
               } else
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
            } else
            {
               if (dTmp > 9999.00 && ((long)dTmp % 100) == 0)
               {
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               } else
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if (lPrice > 0)
                  {
                     if (lPrice < 100 || (lPrice % 100) == 0)
                        sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                     else
                     {
                        LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt);
                        memset(acTmp, ' ', SIZ_SALE1_AMT);
                     }
                  }
               }
            }
            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Check Full/Partial transfer
         if (pRoll->LandBaseYr[3] == 'X')
         {
            pSale->SaleCode[0] = 'P';
            pSale->MultiSale_Flg = 'Y';
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);

      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Las_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", iRet);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/*********************************** loadLas ********************************
 *
 * Input files:
 *    redifile (899-bytes roll file)
 *
 *
 * Commands:
 *    -U    Monthly update
 *    -L    Lien update
 *    -Xl   Extract lien values
 *
 ****************************************************************************/

int loadLas(int iLoadFlag, int iSkip)
{
   int   iRet=0;
   char  sTmp[_MAX_PATH];

   iApnLen = myCounty.iApnLen;
   iRollLen = guessRecLen(acRollFile, iRollLen);

   /* 10/18/2011
   char  acTmpFile[_MAX_PATH];
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\2005\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SLas_CD\\raw\\Las_Sale.2005");
   iRet = Las_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\2006\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SLas_CD\\raw\\Las_Sale.2006");
   iRet = Las_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\2007\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SLas_CD\\raw\\Las_Sale.2007");
   iRet = Las_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\2008\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SLas_CD\\raw\\Las_Sale.2008");
   iRet = Las_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\2009\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SLas_CD\\raw\\Las_Sale.2009");
   iRet = Las_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\2010\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SLas_CD\\raw\\Las_Sale.2010");
   iRet = Las_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\2011\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SLas_CD\\raw\\Las_Sale.2011");
   iRet = Las_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Las\\redifile");
   */

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   // Extract sale1 from redifile and append to cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xsi
   {
      iRet = Las_ExtrSale(acRollFile, NULL, true);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;
   }

   // Load tax
   if (iLoadTax)
   {
      int iLRecLen = GetPrivateProfileInt(myCounty.acCntyCode, "LienRecSize", iRollLen, acIniFile);

      // Load tax agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(sTmp);

      iRet = Cres_Load_TaxBase(bTaxImport);
      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Cres_Load_TaxDelq(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, false, true, 1);
      }

      if (iRet > 0)
         iRet = 0;

      if (!iLoadFlag)
         return iRet;
   }

   if (iLoadFlag & (LOAD_LIEN|EXTR_LIEN))          // -L or -Xl
      iRet = Cres_ExtrLien(myCounty.acCntyCode);

   if (!iRet && (iLoadFlag & (LOAD_UPDT|LOAD_LIEN)) )
      iRet = Las_LoadRoll(myCounty.acCntyCode, iSkip);

   // County request not to put sale price out
   if (!iRet && (iLoadFlag & MERG_CSAL))           // -Ms
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCNOPRICE);

   // Merge zoning
   if (!iRet && (iLoadFlag & MERG_ZONE) )          // -Mz
      iRet = MergeZoning(myCounty.acCntyCode, iSkip);

   return iRet;
}
