/*****************************************************************************
 *
 *
 * Revision:
 * 02/08/2006 1.0.0     First version by Tung Bui
 * 08/21/2007 1.8.16    Fixing situs problem and standardize sale updating. 
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 07/09/2008 8.1.2     Add new function Sie_Load_LDR() and process LDR as provided
 *                      by the county.
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 07/09/2009 9.1.2     Turn sale update back ON.
 * 08/04/2009 9.1.3     Populate other values.
 * 02/23/2010 9.2.1     Fix CARE_OF bug and modify Sie_MergeOwner() to keep original 
 *                      owner name when possible.
 * 07/28/2010 10.3.0    Change logic to identify HO Exempt property in Sie_CreateR01().
 *                      We now have to check EXE_CD1 and ExVal.  If EXE_CD1='10' or
 *                      ExVal=7000, set HO_FLG to 1 (or Y).
 * 07/14/2011 11.0.1    Add S_HSENO
 * 10/26/2011 11.2.8.1  Add Sie_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      Modify Sie_Fmt1Doc() to correct DocNum & DocDate. Use ApplyCumSale()
 *                      to update R01 file. Modify Sie_CreateR01() not to populate
 *                      sale info.  Let ApplyCumSale() populates it from Sie_Sale.sls.
 * 09/27/2013 13.3.0    Modify Sie_MergeOwner() and add Sie_CleanName() to update Vesting.
 *                      Also added Pool data.  Use removeNames() to clear owner names
 * 09/29/2013 13.3.1    Remove comma from owner name.
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 01/30/2014 13.5.0    Modify Sie_ExtrSale() to add Full/Partial flag. 
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 02/10/2015 14.6.1    Fix bug in Sie_ExtrSale()
 * 12/07/2015 15.3.0    Add option to load tax file.
 * 08/18/2016 16.2.0    Add option to load Tax Agency from external text file.
 * 11/17/2017 17.1.0    Call updateDelqFlag() to update Delq info in Tax_Base table
 * 12/11/2017 17.2.0.3  Modify Sie_Fmt1Doc() to fix DocNum format.
 * 05/08/2018 17.7.0    Use new updateDelqFlag() to handle special case for SIE.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "doSort.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Pq.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "SendMail.h"
#include "Tax.h"

/********************************* Sie_Fmt1Doc *******************************
 *
 * 123456  ==> 123456
 * 1234567 ==> 1234-567
 *
 *****************************************************************************/

void Sie_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp;
   char  acTmp[32], acDoc[64], *pDate;

   if (*pRecDoc >= '0' && *pRecDoc < '9')
   {
      if (*(pRecDoc+7) == ' ')
         pDate = pRecDoc+8;
      else
         pDate = pRecDoc+7;         // misalignment

      memcpy(acDoc, pRecDoc, 7);
      acDoc[7] = 0;
      myTrim(acDoc, 7);
      iTmp = strlen(acDoc);
      if (iTmp == 6)
         memset(&acDoc[6], ' ', SIZ_SALE1_DOC-6);
      else if (iTmp > 6)
      {
         sprintf(acTmp, "%.4s-%.3s    ", acDoc, &acDoc[4]);
         strcpy(acDoc, acTmp);
      } else
      {
         iTmp = atol(acDoc);
         sprintf(acTmp, "%.7d     ", iTmp);
         strcpy(acDoc, acTmp);
      }
      memcpy(pOutDoc, acDoc, SIZ_SALE1_DOC);
      if (*pDate == ' ' || !isValidMDY(pDate))
         memset(pOutDate, ' ', SIZ_SALE1_DT);
      else
         sprintf(pOutDate, "%.4s%.2s%.2s", pDate+4, pDate, pDate+2);
   } else
   {
      memset(pOutDate, ' ', SIZ_SALE1_DT);
      memset(pOutDoc, ' ', SIZ_SALE1_DOC);
   }
   *(pOutDate+SIZ_SALE1_DT) = 0;
   *(pOutDoc+SIZ_SALE1_DOC) = 0;
}

/********************************* Sie_FmtRecDoc *****************************
 *
 *
 *****************************************************************************/

void Sie_FmtRecDoc(char *pOutbuf, char *pRollRec)
{
   long     lPrice, lDate;
   double   dTmp;
   char     acTmp[32], acDate[16], acDoc[64], *pTmp, *pTmp1;
   bool     bMisAligned = false;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   pTmp = pRec->RecBook1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0010800050", 10))
   //   lPrice = 0;
#endif


   Sie_Fmt1Doc(acDoc, acDate, pTmp);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

      // Update last recording date
      lDate = atol(acDate);
      if (lDate > lLastRecDate && lDate < lToday)
         lLastRecDate = lDate;

      memcpy(acTmp, pRec->StampAmt, CRESIZ_STMP_AMT);
      acTmp[CRESIZ_STMP_AMT] = 0;
      dTmp = atof(acTmp);
      if (dTmp > 0.0)
      {
         pTmp1 = strchr(acTmp, '.');
         if (pTmp1)
         {
            lPrice = (long)(dTmp * SALE_FACTOR);
            if (lPrice < 100)
               sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            else
               sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
         } else if (dTmp > 1000.00)
         {
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
            LogMsg0("+++ Assummed sale price for APN: %.10s, Amt=%d, RecDate=%s", pOutbuf, (long)dTmp, acDate);
         } else
         {
            memset(acTmp, ' ', SIZ_SALE1_AMT);
            LogMsg0("??? Suspected stamp amount for APN: %.10s, Amt=%d, RecDate=%s", pOutbuf, (long)dTmp, acDate);
         }
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }
   }

   // Advance to Rec #2
   pTmp += 16;
   Sie_Fmt1Doc(acDoc, acDate, pTmp);
   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);
   }

   // Advance to Rec #3
   pTmp += 16;
   Sie_Fmt1Doc(acDoc, acDate, pTmp);
   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
   }
}

/******************************** Sie_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Sie_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;
   int   iTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   if (pTmp=strstr(acTmp, " 1/"))
     *pTmp = 0;
   if (pTmp=strstr(acTmp, " - "))
     *pTmp = 0;
   if (pTmp=strchr(acTmp, '{'))
      *pTmp = 0;

   pTmp = &acTmp[0];
   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      pTmp++;

      // Remove known bad chars
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   blankRem((char *)&acTmp[0], iTmp);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Sie_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sie_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acName1[64], acName2[64], acVesting[8];
   char  *pTmp, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   myTrim(acName1, CRESIZ_NAME_1);
   strcpy(acOwners, acName1);
   pName1 = acName1;
   acVesting[0] = 0;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   myTrim(acName2, CRESIZ_NAME_2);
   pName2 = _strupr(acName2);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "001120201", 9))
   //   iTmp = 0;
#endif

   // Check owner2 for # - drop them
   if (*pName2 == ' ')
      *pName2 = 0;
   else if (*pName2 == '#' )
   {
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '&')
   {
      pName2 += 2;
      pTmp = strchr(pName1, ',');
      if (pTmp)
      {
         iTmp = pTmp - pName1;
         if (!memcmp(pName1, pName2, iTmp))
         {
            // Combine Name1 & Name2 since they have the same last name
            MergeName(pName1, pName2, acOwners);
            pName2 = NULL;
         }
      }
   }

   if (pName2 && *pName2 > ' ')
   {
      remChar(pName2, ',');
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      // Find vesting in Name2 
      pTmp = findVesting(pName2, acVesting);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

         // Check EtAl
         if (!memcmp(acVesting, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } 
   }

   // Remove multiple spaces
   iTmp = remChar(acOwners, ',');
   if (iTmp > SIZ_NAME1)
      iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acOwners, iTmp);

   // Cleanup Name1 
   iTmp = Sie_CleanName(acOwners, acTmp, acVesting);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acName1, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

   // Now parse owners
   splitOwner(acName1, &myOwner, 3);
   if (acVesting[0] > ' ' && isVestChk(acVesting))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   iTmp = strlen(myOwner.acSwapName);
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/********************************* Sie_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Sie_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[64], acAddr2[64], acChar;
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pRec->M_Addr1, "     ", 5))
      return;

   memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
   blankRem(acAddr1, CRESIZ_ADDR_1);
   pAddr1 = acAddr1;

   iTmp = 0;
   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ') 
            pAddr1++;
         iTmp = strlen(pAddr1);
         if (iTmp > SIZ_CARE_OF)
            iTmp = SIZ_CARE_OF;
         memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
      }
   }

   // Start processing
   memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
   memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
   iTmp = atoin(pRec->M_Zip, 5);
   if (iTmp > 400)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   } else
   {
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
   }

   // Parsing mail address
   ADR_REC sMailAdr;
   memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
   parseMAdr1_1(&sMailAdr, pAddr1);

   memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
   acAddr2[CRESIZ_ADDR_2] = 0;
   parseMAdr2(&sMailAdr, myTrim(acAddr2));

   if (sMailAdr.lStrNum > 0)
   {
      if (pTmp = strstr(sMailAdr.strNum, "1/2"))
      {
         memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, SIZ_M_STR_SUB);
         *pTmp = 0;
         sMailAdr.lStrNum = atoi(sMailAdr.strNum);
         iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);
      } else 
      {
         iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);

         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         } else
         {
            acChar = isChar(sMailAdr.strNum, SIZ_M_STRNUM);
            if (acChar > ' ')
            {
               if (pTmp = strchr(sMailAdr.strNum, acChar))
                  memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, strlen(pTmp));
            }
         }
      }

   }
   blankPad(sMailAdr.strDir, SIZ_M_DIR);
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   blankPad(sMailAdr.strName, SIZ_M_STREET);
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
   blankPad(sMailAdr.City, SIZ_M_CITY);
   memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   blankPad(sMailAdr.State, SIZ_M_ST);
   memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0131101200", 10))
   //   iTmp = 0;
#endif

   // Situs
   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      ADR_REC sSitusAdr;
      char    acStrName[32];

      // Clear old Mailing
      removeSitus(pOutbuf);

      // Remove leading space
      memcpy(acTmp, pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
      iTmp = blankRem(acTmp, CRESIZ_SITUS_STREETNO);
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, iTmp);

      memcpy(acStrName, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
      blankRem(acStrName, CRESIZ_SITUS_STREETNAME);

      // Prepare display field
      iTmp = sprintf(acAddr1, "%s %s", acTmp, acStrName);
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // Prevent situation likes 123 1/2 NORTH STREET.  We have to break
      // into strnum=123, strsub=1/2 or A&B
      pTmp = strchr(acTmp, ' ');
      if (pTmp)
      {
         *pTmp++ = 0;
         if (*(pTmp+1) == '/' || *(pTmp+1) == '&')
            memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, strlen(pTmp));
         else if (*pTmp >= 'A' && isalpha(*(pTmp+1)))
         {
            if (!strcmp(pTmp, "HW") && acStrName[0] == 'Y')
               sprintf(acAddr1, "HIGHWA%s", acStrName);
            else
               sprintf(acAddr1, "%s %s", pTmp, acStrName);
            strcpy(acStrName, acAddr1);
         }
      }

      // Remove '-' in strNum
      if (pTmp = strchr(acTmp, '-'))
      {
         acChar = isChar(acTmp, SIZ_S_STRNUM);
         if (acChar == ' ')
            memcpy(pOutbuf+OFF_S_STRNUM, acTmp, strlen(acTmp));
         else
         {
            if (pTmp = strchr(acTmp, acChar))
            {
               iTmp = strlen(pTmp);
               if (iTmp > SIZ_S_STR_SUB) iTmp = SIZ_S_STR_SUB;
               memcpy(pOutbuf+OFF_S_STR_SUB, pTmp, iTmp);
               *pTmp = 0;
               sprintf(acAddr2, "%s      ", acTmp);
               acAddr2[SIZ_S_STRNUM] = '\0';
               memcpy(pOutbuf+OFF_S_STRNUM, acAddr2, SIZ_S_STRNUM);
            }
         }
      } else
      {
         sprintf(acTmp, "%d       ", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
      }
   
      // Parse street name
      parseAdr1S(&sSitusAdr, acStrName);

      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
      else
         memset(pOutbuf+OFF_S_UNITNO, ' ', SIZ_S_UNITNO);

#ifdef _DEBUG
      //if (!memcmp(pRec->NeighHood, "AGR", 3))
      //   LogMsg("APN=%.10s, NBH=%.3s", pRec->APN, pRec->NeighHood);
#endif

      // Situs City 
      if (pRec->S_City[0] >= 'A')
      {
         memcpy(acAddr2, pRec->S_City, CRESIZ_SITUS_CITY);
         myTrim(acAddr2, CRESIZ_SITUS_CITY);
         City2Code(acAddr2, acTmp, pOutbuf);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, 3);
            if (City2Zip(acAddr2, acTmp))
               memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
         }
      } else 
      {
         // Assuming that no situs city and we have to use mail city
         acAddr2[0] = 0;
         if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
         {
            City2Code(sMailAdr.City, acTmp, pOutbuf);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, 3);
               strcpy(acAddr2, sMailAdr.City);
            }
            memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
         } else if (pRec->NeighHood[1] >= 'A')   // Check neighborhood code
         {
            char acTmp1[4];

            memcpy(acTmp1, pRec->NeighHood, 3);
            acTmp1[3] = 0;
            Abbr2Code(acTmp1, acTmp, acAddr2, pOutbuf);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, 3);
               if (City2Zip(acAddr2, acTmp))
                  memcpy(pOutbuf+OFF_S_ZIP, acTmp, 5);
            }
         }
      }

      if (acAddr2[0])
      {
         strcat(myTrim(acAddr2), " CA");
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      }
   }
   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Sie_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acCode[32], acTmp1[32];
   long     lTmp;
   int      iTmp, iRet;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "46SIE", 5);

      // Create APN_D 
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      /*
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lHpp = atoin(pRec->PPVal, CRESIZ_PP_VAL);
      long lPers = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      lTmp = lPers + lHpp + lFixture;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);
      */
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   iRet = 0;

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"SIE");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // Exemption
   if (pRec->ExeCode1[0] > ' ')
   {
      memcpy(pOutbuf+OFF_EXE_CD1, pRec->ExeCode1, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD2, pRec->ExeCode2, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD3, pRec->ExeCode3, CRESIZ_EXEMP_CODE);
   }

   // HO Exempt
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (!memcmp(pRec->ExeCode1, "10", 2) || lTmp == 7000)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
   } else
   {
      lTmp = 0;
      strcpy(acTmp, BLANK32);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

   // Lot Sqft
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }

   // Recording info
   /* Sale is now updated via MergeCumSale()
   if (pRec->RecBook1[0] > ' ')
   {
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      Sie_FmtRecDoc(pOutbuf, pRollRec);
   }
   */

   // Base year
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // Owners
   Sie_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address
   Sie_MergeAdr(pOutbuf, pRollRec);

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      pRec->LglDesc1[CRESIZ_LGL_DESC1+CRESIZ_LGL_DESC2] = 0;
      iTmp = updateLegal(pOutbuf, pRec->LglDesc1);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0090200190", 10))
   //   iTmp = 0;
#endif
   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // FP - 1
   if (pRec->Fp[0] > ' ')
      LogMsg("XFp: %c", pRec->Fp[0]);

   // AC - 1
   if (pRec->Ac[0] > ' ')
      LogMsg("XAir: %c", pRec->Ac[0]);

   // Heating
   if (pRec->Heating[0] > ' ')
      LogMsg("XHeat: %c", pRec->Heating[0]);

   // Pool/Spa - Y
   if (pRec->Pool[0] == 'Y')
      *(pOutbuf+OFF_POOL) = 'P';
      //LogMsg("XPool: %c", pRec->Pool[0]);

   // Zoning
   if (pRec->Zone[0] > ' ')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, SIZ_ZONE);

   // Neighborhood

   // Type_Sqft - M, C
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
         acTmp[iTmp++] = pRec->BldgCls[0];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[1]))
            acTmp[iTmp++] = pRec->BldgCls[1];
         else if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else
            acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }
   return iRet;
}

/********************************* Sie_Load_Roll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 ****************************************************************************/

int Sie_Load_Roll(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;
   REDIFILE *pRec = (REDIFILE *)acRollRec;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

#ifdef _DEBUG
      //if (!memcmp(pRec->APN, "0121000010", 10) || acRollRec[0] == ' ')
      //   iRet = 0;
#endif
      iRet = Sie_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (iRet)
         break;

      if (fdLienExt)
         iRet = PQ_MergeLien(acBuf, fdLienExt);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/********************************** Sie_Load_LDR ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Sie_Load_LDR(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   iApnLen = myCounty.iApnLen;
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      lLDRRecCount++;
      iRet = Sie_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      // Scan for bad character - LAS May 05
      while (iRet-- > 0)
         if (acRollRec[iRet] < ' ')
            acRollRec[iRet] = ' ';

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   // Check for NULL embeded in file
   iRet = replEmbededChar(acOutFile, NULL, 32, 32, -1, iRecLen);
   if (iRet < 0)
      LogMsg("***** Error opening %s for checking null", acOutFile);
   else if (iRet > 0)
      LogMsg("*** Ctrl character found in %s.  Please contact Sony", acOutFile);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);
   
   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/******************************* Sie_ExtrSale *******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Sie_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024], *pTmp;
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   double   dTmp;
   long     lCnt=0, lNewRec=0, lPrice;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            blankRem(acTmp);
            memcpy(pSale->StampAmt, acTmp, strlen(acTmp));
            // Keep this check here until counties correct their roll
            pTmp = strchr(acTmp, '.');
            if (!pTmp || *(pTmp+1) == ' ' || dTmp > 100000.00)
            {
               if (acTmp[0] == '0' || dTmp < 2000.00)
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if ((lPrice % 100) == 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                  } else
                  {
                     LogMsg0("??? Suspected stamp amount for APN: %.10s, StampAmt=%s", acBuf, acTmp);
                     memset(acTmp, ' ', SIZ_SALE1_AMT);
                  }
               } else
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
            } else
            {
               if (dTmp > 9999.00 && ((long)dTmp % 100) == 0)
               {
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               } else
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if (lPrice > 0)
                  {
                     if (lPrice < 100 || (lPrice % 100) == 0)
                        sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                     else
                     {
                        LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt);
                        memset(acTmp, ' ', SIZ_SALE1_AMT);
                     }
                  }
               }
            }
            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Full/Partial
         iRet = atoin(pRoll->BaseCode, CRESIZ_BASE_CODE);
         switch (iRet)
         {
            case 21:
            case 22:
               pSale->SaleCode[0] = 'F';
               break;
            case 23:
            case 24:
               pSale->SaleCode[0] = 'P';
               break;
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;
            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);
            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Sie_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);
            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);
   LogMsg("Total input records:        %u", lCnt);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      LogMsg("Total sorted records:       %u", iRet);
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   LogMsg("Total new output records: %u\n", lNewRec);

   return iRet;
}

/*********************************** loadSie ********************************
 *
 * Input files:
 *    redifile          899/900-bytes roll file
 *
 * Problems:
 *
 * Processing:
 *
 * Commands:
 *    -U    Monthly update
 *    -L    Lien update 
 *    -Xl   Extract lien
 *
 ****************************************************************************/

int loadSie(int iLoadFlag, int iSkip)
{
   int   iRet;
   char  sTmp[_MAX_PATH];

   iApnLen = myCounty.iApnLen;
   iRet = guessRecLen(acRollFile, iRollLen);
   if (!iRet)
   {
      LogMsg("***** Roll file is not ready for production: %s", acRollFile);
      return -1;
   }
   iRollLen = iRet;

   // Use SaleTmpl for cum sale file
   strcpy(acESalTmpl, acSaleTmpl);
   /*
   char  acTmpFile[_MAX_PATH];
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\2005\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SSie_CD\\raw\\Sie_Sale.2005");
   iRet = Sie_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\2006\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SSie_CD\\raw\\Sie_Sale.2006");
   iRet = Sie_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\2007\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SSie_CD\\raw\\Sie_Sale.2007");
   iRet = Sie_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\2008\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SSie_CD\\raw\\Sie_Sale.2008");
   iRet = Sie_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\2009\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SSie_CD\\raw\\Sie_Sale.2009");
   iRet = Sie_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\2010\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SSie_CD\\raw\\Sie_Sale.2010");
   iRet = Sie_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\2011\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SSie_CD\\raw\\Sie_Sale.2011");
   iRet = Sie_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Sie\\redifile");
   */
   // Reformat DocNum to 7 digits
   // 20171211 iRet = FixCumSale(acCSalFile, SALE_FLD_DOCNUM+CNTY_SIE, false);

   // Extract sale1 from redifile and append to cum sale file
   if (iLoadFlag & EXTR_SALE)                // -Xsi
   {
      iRet = Sie_ExtrSale(acRollFile, NULL, true);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;

      iRet = FixSalePrice(acCSalFile, false);
   }

   // Loading tax
   if (iLoadTax)
   {
      int iLRecLen = GetPrivateProfileInt(myCounty.acCntyCode, "LienRecSize", iRollLen, acIniFile);

      // Load tax agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(sTmp);

      iRet = Cres_Load_TaxBase(bTaxImport);
      if (!iRet && lLastTaxFileDate > 0)
      {
         iRet = Cres_Load_TaxDelq(bTaxImport);
         if (!iRet)
            iRet = updateDelqFlag(myCounty.acCntyCode, true, true, 1);
      }

      if (iRet > 0)
         iRet = 0;

      if (!iLoadFlag)
         return iRet;
   }


   if (iLoadFlag & EXTR_LIEN)                // -Xl
      iRet = Cres_ExtrLien(myCounty.acCntyCode);

   if (iLoadFlag & LOAD_LIEN)                // -L 
      iRet = Sie_Load_LDR(myCounty.acCntyCode);
   else if (iLoadFlag & LOAD_UPDT)           // -U
      iRet = Sie_Load_Roll(myCounty.acCntyCode, iSkip);

   // Put this back when SIE fixed their sale data
   if (!iRet && (iLoadFlag & MERG_CSAL))
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR);
      //iRet = MergeCumSale1(iSkip);

   return iRet;
}
