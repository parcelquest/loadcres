/*****************************************************************************
 *
 * Notes: As of LDR 2011, TRI has no situs.
 *
 * Revision:
 * 02/01/2006  1.0.0    First version by Tung Bui
 * 03/21/2006  1.3.8.2  Fix PR #424
 *                      Changes to TRI processing (as per my discussions w/ Deanna and Sony):
 *                      - Verify active/inactive is not displaying.  Sample inactive parcel: 0150504000
 *                      - Populate DocNum even DocDate is not there.
 *                      - Don't use LDR values snapshot.  Use LDR values (including corrections) 
 *                        in the roll that she sends to us.  (Include the update of the exemption field 
 *                        and HO Exemp Y/N field.)
 *                      - Clear out any historical situs addresses.  The county will send us correct verified
 *                        situs addresses once they have them.  The few historical addresses that we are 
 *                        displaying are wrong.  (ex, parcel 0010511900 is displaying 211 Trinity Lake Blvd.  
 *                        It is actually Highway 3.)
 * 12/10/2007 1.8.24    Fix bug in Tri_Fmt1Doc() that causes program crashed. - spn
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 04/24/2008 1.10.5    Blank out USE_STD if usecode is "XX"
 * 04/24/2008 1.10.5.2  Fix Tri_MergeAdr() to process situs even when mail is not available.
 * 04/24/2008 1.10.5.3  Add loadTri() and Tri_LoadRoll() to make it independent from other counties.
 * 04/25/2008 1.10.5.3  If ExeCode is 10 but ExeAmt is 0, set HO Flag to 1 (N).
 *                      If MAdr is blank, go on to process situs.  Skip first record 
 *                      that has APN=000000.
 * 08/07/2008 8.1.2     Extract LDR values even though this might not be used any where since the 
 *                      county want us to display current values
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 08/04/2009 9.1.3     Populate other values.
 * 11/19/2009 9.1.7     TRI is now populating only FixtVal.  Other value fields are empty.
 * 02/23/2010 9.2.1     Fix CARE_OF bug.
 * 07/14/2011 11.0.1    Add S_HSENO
 * 07/22/2011 11.1.2    Clear out old situs before applying new one. Prepare for future.
 * 10/18/2011 11.2.8    Add Tri_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      Modify Tri_CreateR01() not to populate sale info.  Let ApplyCumSale() populates
 *                      R01 from Tri_Sale.sls. Remove Tri_FmtRecDoc().
 * 10/26/2011 11.2.8.1  Modify Tri_ExtrSale() to extract all three sales even sale1 is bad.
 * 01/04/2013 12.4.1    Modify Tri_ExtrSale() to keep all sales regardless of having sale date or not.
 *                      Call ApplyCumSale() with DONT_CHK_DOCDATE option.
 * 03/15/2013 12.6.5    Add verification for bad input file in Tri_ExtrSale();
 * 09/20/2013 13.2.7    Rewrite Tri_LoadRoll() and add INI option to use LDR value as needed. 
 *                      Add Tri_CleanName() to cleanup Name1 and extract Vesting.
 * 09/27/2013 13.3.0    Modify Teh_MergeOwner() and Teh_CleanName() to use external vesting table.
 *                      Use removeNames() to clear owner names
 * 10/14/2013 13.3.4    Use standard removeMailing() and removeSitus().
 * 11/22/2013 13.4.4    Add Tri_FmtDocLinks() to format DocLink with local images.
 *                      Reformat DocNum in Tri_Fmt1Doc() to standardize it to yyyy12345.
 * 08/16/2014 14.2.1    Add FP & POOL code to Tri_CreateR01().
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 03/13/2015 14.6.3    Modify Tri_MakeDocLink() to format DOCLINK starting with year.
 *                      Replace Tri_FmtDocLinks() with updateDocLinks().
 * 09/14/2015 15.1.2    Fix output record count in Tri_ExtrSale()
 * 02/14/2016 15.6.0    Modify TRI_MergeOwner() to fix SWAP_NAME.  Also remove '&' in Name1 and '#' in Name2
 * 04/14/2016 15.9.1    Add -T option to load tax file.  Still need Delq data.
 * 08/30/2016 16.2.2    Removed BLDG_SF & YRBLT per county request awaiting for county to fix their data.
 * 10/06/2016 16.4.0    Modify Tri_CreateR01() to set USE_STD="999" if UseCode="XX"
 * 11/17/2017 17.1.0    Call updateDelqFlag() to update Delq info in Tax_Base table
 * 05/07/2018 17.6.1    Fix bug in Tri_MergeOwner() that crashes when Name2 is empty.
 * 05/10/2018 17.8.0    Use Cres_Load_TaxDelq() to create Delq table. Turn off create delq in Cres_Load_Cortac().
 * 06/25/2018 17.9.0    Call LoadTaxCodeTable() to load tax code table.  This will fix the issue
 *                      of duplicate tax code (same tax code for different names).
 * 07/16/2019 19.1.0    Modify Tri_MergeAdr() to use Situs from REDIFILE.  Add Tri_Load_LDR() for new LDR file.
 * 12/08/2019 19.4.1    Change tax processing to use scraped file.
 * 04/27/2020 19.6.0    Remove -Ut and use -T to load both full & partial tax file with TC_LoadTax().
 * 07/18/2020 20.1.1    Modify Tri_MergeAdr() to verify direction before assign to S_DIR.
 * 
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Pq.h"
#include "doSort.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "SendMail.h"
#include "Cres_TaxInfo.h"
#include "Tax.h"
#include "MergeTri.h"

/********************************* Tri_FmtRecDoc *****************************
 *
 * Update both DocDate and DocNum if one of them has valid data.
 *
 *****************************************************************************/

void Tri_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   char  acDate[16], acDoc[SIZ_SALE1_DOC], sTmp1[64], *pTmp;
   char  acTmp[256], acMMDD[8];
   int   iTmp, iVal;

   if (*pRecDoc <= ' ')
   {
      *pOutDate = 0;
      *pOutDoc = 0;
      return;
   }

   // Use local var to make sure string is terminated by 0
   memcpy(acTmp, pRecDoc, 16);
   acTmp[16] = 0;

   memset(acDoc, ' ', SIZ_SALE1_DOC);
   if (pTmp = strchr(acTmp, '-'))
   {
      memcpy(acMMDD, pTmp+1, 4);
      memcpy(acDoc, acTmp, pTmp - &acTmp[0]);
   } else
   {
      memcpy(acMMDD, &acTmp[9], 4);
      memcpy(acDoc, acTmp, 9);
   }

   if (memcmp(acDoc, "00000000", 8) > 0)
   {
      iTmp = atoin(acDoc, 4);
      if (acDoc[0] == '9')
      {
         iVal = atoin(&acDoc[2], 6);
         sprintf(sTmp1, "19%.2s%.5d       ", acDoc, iVal);
      } else if (iTmp > 1996 && iTmp <= lToyear)
      {
         iVal = atoin(&acDoc[4], 6);
         sprintf(sTmp1, "%.4s%.5d       ", acDoc, iVal);
      } else if (!memcmp(acDoc, "09", 2))
      {
         iVal = atoin(&acDoc[3], 6);
         sprintf(sTmp1, "19%.2s%.5d       ", &acDoc[1], iVal);
      } else if (iTmp > 1990 && iTmp <= 1996)
      {
         iVal = atoin(&acDoc[4], 6);
         sprintf(sTmp1, "%.4s%.5d       ", acDoc, iVal);
      } else
         memcpy(sTmp1, acDoc, SIZ_SALE1_DOC);

      memcpy(pOutDoc, sTmp1, SIZ_SALE1_DOC);
   } else
      memset(pOutDoc, ' ', SIZ_SALE1_DOC);

   if (acMMDD[0] <= ' ')
   {
      memset(pOutDate, ' ', 8);
   } else 
   {
      memcpy(acDate, pRecDoc, 4);
      memcpy(acDate+4, acMMDD, 4);

      if (!isValidYMD(acDate))
         memset(pOutDate, ' ', 8);
      else
         memcpy(pOutDate, acDate, 8);
   }
}

void Tri_FmtRecDoc(char *pOutbuf, char *pRollRec)
{
   long     lPrice, lDate;
   char     acTmp[64], acDate[16], acDoc[64], *pTmp;
   bool     bMisAligned = false;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   pTmp = pRec->RecBook1;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "00101138", 8))
   //   iTmp = 0;
#endif

   memcpy(acTmp, pTmp, 16);
   acTmp[16] = 0;

   Tri_Fmt1Doc(acDoc, acDate, acTmp);

   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code

      memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
      memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
      memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

      // Update last recording date
      lDate = atol(acDate);
      if (lDate > lLastRecDate && lDate < lToday)
         lLastRecDate = lDate;

      // Sale price is stored in StampAmt location
      lPrice = atoin(pRec->StampAmt, CRESIZ_STMP_AMT);
      if (lPrice > 0)
      {
         sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
         memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
      }
   }

   // Advance to Rec #2
   pTmp += 16;
   memcpy(acTmp, pTmp, 16);
   acTmp[16] = 0;
   Tri_Fmt1Doc(acDoc, acDate, acTmp);
   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);
   }

   // Advance to Rec #3
   pTmp += 16;
   memcpy(acTmp, pTmp, 16);
   acTmp[16] = 0;
   Tri_Fmt1Doc(acDoc, acDate, acTmp);
   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
   }
}

/******************************** Tri_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Tri_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;
   int   iCnt;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   pTmp = strstr(acTmp, " AKA ");
   if (pTmp)
   {
      *(pTmp+1) = '&';
      *(pTmp+2) = ' ';
      *(pTmp+3) = ' ';
   }

   if ((pTmp=strstr(acTmp, " 1/")) || (pTmp=strstr(acTmp, " - ")) )
      *pTmp = 0;
   blankRem((char *)&acTmp[0]);

   iCnt = 0;
   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Tri_MergeOwner *****************************
 *
 * By request from the county, we keep Name1 and Name2 as is except when Name2
 * is CareOf, then we move it to the appropriate field.
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Tri_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acTmp[64], acName1[64], acName2[32], acVesting[8];
   char  *pTmp, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Copy Name1
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   iTmp = blankRem(acName1, CRESIZ_NAME_1);
   if (acName1[iTmp-1] == '&')
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp-1);
   else
      memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   pName2 = _strupr(myTrim(acName2, CRESIZ_NAME_2));
   iTmp = strlen(pName2);

   // Check owner2 for CareOf
   if ((*pName2 == '%') && iTmp > 2)
   {
      updateCareOf(pOutbuf, pName2, iTmp);
      *pName2 = 0;
   } else if ((pTmp = strstr(acName2, "C/O")) && iTmp > 4 )
   {
      updateCareOf(pOutbuf, pName2, iTmp);
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4) && iTmp > 5)
   {
      updateCareOf(pOutbuf, pName2, iTmp);
      *pName2 = 0;
   } else if (*pName2 == '#' && iTmp > 5)
      memcpy(pOutbuf+OFF_NAME2, &acName2[2], iTmp-2);
   else if (iTmp > 5)
      memcpy(pOutbuf+OFF_NAME2, acName2, iTmp);

   // Clean up name1 for swapped name
   //iTmp = Tri_CleanName_Old(acName1, acTmp, acVesting, CRESIZ_NAME_1);
   iTmp = Tri_CleanName(acName1, acTmp, acVesting, CRESIZ_NAME_1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acName1, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting, strlen(acVesting));

      // Check EtAl
      if (!memcmp(acVesting, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0201803200", 8))
   //   iTmp = 0;
#endif
   // Now parse owners
   splitOwner(acName1, &myOwner, 3);
   if (acVesting[0] > ' ' && isVestChk(acVesting))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }

   vmemcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, SIZ_NAME_SWAP);

}

/*
void Tri_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acTmp1[64], acSave[64], acName1[64], acName2[32];
   char  *pTmp, *pTmp1, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   memset(pOutbuf+OFF_NAME1, ' ', SIZ_NAME1+SIZ_NAME2);
   memset(pOutbuf+OFF_VEST, ' ', SIZ_VEST);
   memset(pOutbuf+OFF_CARE_OF, ' ', SIZ_CARE_OF);
   memset(pOutbuf+OFF_NAME_SWAP, ' ', SIZ_NAME_SWAP);
   memset(pOutbuf+OFF_NAME_TYPE1, ' ', SIZ_NAME_TYPE1);
   memset(pOutbuf+OFF_NAME_TYPE2, ' ', SIZ_NAME_TYPE2);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   acName1[CRESIZ_NAME_1] = 0;
   pName1 = acName1;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   acName2[CRESIZ_NAME_2] = 0;
   pName2 = acName2;
   pTmp1 = _strupr(acName2);

   // Check owner2 for # - drop them
   if (*pName2 == '#' )
   {
     memcpy(pOutbuf+OFF_NAME2, pName2+2, SIZ_NAME2);
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
     memcpy(pOutbuf+OFF_CARE_OF, pName2+2, SIZ_CARE_OF);
     *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      pTmp += 4;
      iTmp = strlen(pTmp);
      if (iTmp > SIZ_CARE_OF)
         iTmp = SIZ_CARE_OF;
      memcpy(pOutbuf+OFF_CARE_OF, pTmp, iTmp);
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      *pName2 = 0;
      if (acName2[4] == ' ')
         memcpy(pOutbuf+OFF_CARE_OF, pName2+5, SIZ_CARE_OF);
      else
         memcpy(pOutbuf+OFF_CARE_OF, pName2+6, SIZ_CARE_OF);
   } else if (strchr(acName2, '&'))
   {

      if (pTmp=strstr(pName1, " 1/"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " - "))
        *pTmp = 0;

      if (pTmp=strstr(pName1, " ETUX"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ET UX"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ETAL"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ET AL"))
        *pTmp = 0;

      if (pTmp=strstr(pName1, " TSTEE"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " SUCC  TR"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TRUSTE"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TRS"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TSTES"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TR "))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " CO-TR"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " COTRST"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ESTATE OF"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " LIFE ESTATE"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " M D"))
        *pTmp = 0;

      // Process as two names
      memcpy(acOwners, pName1, CRESIZ_NAME_1);
      acOwners[CRESIZ_NAME_1] = 0;

      // Check for vesting
      pTmp1 = strrchr(myTrim(acOwners), ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_TRI(acSave, acTmp1);
         if (*pTmp)
         {
            //strcpy(pOwners->acVest, acTmp1);
            memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
            *pTmp1 = 0;
         }
      }
      if (pTmp = strstr(acOwners, " ALL AS"))
         *pTmp = 0;

      // Parse owner 1
      splitOwner(acOwners, &myOwner);
      memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, strlen(myOwner.acName1));
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, strlen(myOwner.acSwapName));

      // Process Name2
      if (*pName2 == '&')
         memcpy(acOwners, pName2+2, CRESIZ_NAME_1-2);
      else
         memcpy(acOwners, pName2, CRESIZ_NAME_1);
      acOwners[CRESIZ_NAME_1] = 0;

      // Check for vesting
      pTmp1 = strrchr(myTrim(acOwners), ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_TRI(acSave, acTmp1);
         if (*pTmp)
         {
            memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
            *pTmp1 = 0;
         }
      }

      // Parse Owner 2
      splitOwner(acOwners, &myOwner);
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName1, strlen(myOwner.acName1));
      return;
   } else
      memcpy(pOutbuf+OFF_NAME2, acName2, strlen(acName2));

   // Remove multiple spaces
   memcpy(acOwners, pName1, CRESIZ_NAME_1);
   acOwners[CRESIZ_NAME_1] = 0;
   pTmp = acOwners;

   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;

   // Check for AKA - replace it with '&'
   pTmp = strstr(acTmp, " AKA ");
   if (pTmp)
   {
      *(pTmp+1) = '&';
      *(pTmp+2) = ' ';
      *(pTmp+3) = ' ';
   }


   if (pTmp=strstr(acTmp, " 1/"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " - "))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " ETUX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET UX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " TSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCC  TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " COTRST"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ESTATE OF"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " LIFE ESTATE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " L/E"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " M D"))
      *pTmp = 0;

   // Blank out "SUC TRS" and "CO TRS"
   if (pTmp = strstr(acTmp, " SUC TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " CO TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " DVA"))
      *pTmp = 0;

  // Check for vesting
   pTmp1 = strrchr(acTmp, ' ');
   if (pTmp1)
   {
      strcpy(acSave, ++pTmp1);
      pTmp = getVestingCode_TRI(acSave, acTmp1);
      if (*pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
         *pTmp1 = 0;
      } else
      {
         if (pTmp1 = strstr(acTmp, " JT"))
         {
            memcpy(pOutbuf+OFF_VEST, "JT", 2);
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " TC"))
         {
            memcpy(pOutbuf+OFF_VEST, "TC", 2);
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " CP"))
         {
            memcpy(pOutbuf+OFF_VEST, "CP", 2);
            *pTmp1 = 0;
         }
      }
   }

   // Save data to append later
   pTmp = strstr(acTmp, "FAM TRUST");
   if (pTmp)
   {
      strcpy(acSave, --pTmp);
      *pTmp = 0;
   } else
      acSave[0] = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 3);
   if (strcmp(myOwner.acName1, myOwner.acName2))
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName2, strlen(myOwner.acName2));

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   if (acSave[0])
      strcat(myOwner.acName1, acSave);
   memcpy(pOutbuf+OFF_NAME1, myOwner.acName1, strlen(myOwner.acName1));
}

/********************************* Tri_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Tri_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   ADR_REC  sMailAdr, sSitusAdr;
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[64], acAddr2[64], acChar;
   int      iTmp, iStrNo;

   pRec = (REDIFILE *)pRollRec;

   // Clear old Mailing
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (memcmp(pRec->M_Addr1, "     ", 5))
   {
      memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
      blankRem(acAddr1, CRESIZ_ADDR_1);
      pAddr1 = acAddr1;

      iTmp = 0;
      if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
      {
         if (*pAddr1 == '%')
            pAddr1 += 2;
         else
            pAddr1 += 4;

         // Check for C/O name
         pTmp = strchr(pAddr1, ',');
         if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
         {
            *pTmp = 0;
            pAddr1 = pTmp + 1;
            if (*pAddr1 == ' ') 
               pAddr1++;
            updateCareOf(pOutbuf, pAddr1, strlen(pAddr1));
         }
      }

      // Start processing
      memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
      memcpy(pOutbuf+OFF_M_CTY_ST_D, pRec->M_Addr2, CRESIZ_ADDR_2);
      iTmp = atoin(pRec->M_Zip, 5);
      if (iTmp > 400)
      {
         memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
      } else
      {
         memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
         memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
      }

      // Parsing mail address
      memset((void *)&sMailAdr, 0, sizeof(ADR_REC));
      parseAdr1_1(&sMailAdr, pAddr1);

      memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);
      acAddr2[CRESIZ_ADDR_2] = 0;
      parseAdr2(&sMailAdr, myTrim(acAddr2));

      if (sMailAdr.lStrNum > 0)
      {
         if (pTmp = strstr(sMailAdr.strNum, "1/2"))
         {
            memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, SIZ_M_STR_SUB);
            *pTmp = 0;
            sMailAdr.lStrNum = atoi(sMailAdr.strNum);
            iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
            memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);
         } else 
         {
            iTmp = sprintf(acTmp, "%d", sMailAdr.lStrNum);
            memcpy(pOutbuf+OFF_M_STRNUM, acTmp, iTmp);

            if (sMailAdr.strSub[0] > '0')
               memcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
            else
            {
               acChar = isChar(sMailAdr.strNum, SIZ_M_STRNUM);
               if (acChar > ' ')
               {
                  if (pTmp = strchr(sMailAdr.strNum, acChar))
                     memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, strlen(pTmp));
               }
            }
         }
      }

      if (sMailAdr.strSub[0] > '0')
         vmemcpy(pOutbuf+OFF_M_STR_SUB, sMailAdr.strSub, SIZ_M_STR_SUB);
      if (sMailAdr.strDir[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
      if (sMailAdr.strName[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
      if (sMailAdr.strSfx[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
      if (sMailAdr.City[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
      if (sMailAdr.State[0] > ' ')
         vmemcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   }

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "007120560", 8))
   //   iTmp = 0;
#endif
   // Situs
   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0 && pRec->S_StrName[0] >= ' ')
   {
      int   iIdx=0;

      removeSitus(pOutbuf);

      // Remove leading space
      iTmp = 0;
      while (pRec->S_StrNo[iTmp] == ' ')
         iTmp++;
      while (iTmp < CRESIZ_SITUS_STREETNO)
         acTmp[iIdx++] = pRec->S_StrNo[iTmp++];
      acTmp[iIdx] = 0;

      // Save original StrNum that may includes hyphen
      memcpy(pOutbuf+OFF_S_HSENO, acTmp, iIdx);

      // Prepare display field
      iTmp = sprintf(acAddr1, "%s %s", myTrim(acTmp), myTrim(pRec->S_StrName, CRESIZ_SITUS_STREETNAME));
      memcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, iTmp);

      // Prevent situation likes 123 1/2 NORTH STREET.  We have to break
      // into strnum=123, strsub=1/2
      pTmp = strchr(acTmp, ' ');
      if (pTmp)
      {
         *pTmp++ = 0;
         vmemcpy(pOutbuf+OFF_S_STR_SUB, pTmp, SIZ_S_STR_SUB);
      }

      sprintf(acTmp, "%d       ", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);

      // Copy street name
      strcpy(acTmp, pRec->S_StrName);
      parseAdr1S(&sSitusAdr, acTmp);

      if (sSitusAdr.strDir[0] == 'E' && !strcmp(sSitusAdr.strName, "SIDE"))
      {
         memcpy(pOutbuf+OFF_S_STREET, "EAST SIDE", 9);
      } else if (sSitusAdr.strDir[0] == 'W' && !strcmp(sSitusAdr.strName, "SIDE"))
      {
         memcpy(pOutbuf+OFF_S_STREET, "WEST SIDE", 9);
      } else if (sSitusAdr.strName[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
         if (sSitusAdr.strDir[0] > ' ' && isDir(sSitusAdr.strDir))
            memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      }
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));

      // Assuming that no situs city and we have to use mail city
      acAddr2[0] = 0;
      if (sMailAdr.lStrNum == sSitusAdr.lStrNum &&
         !strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && 
         sMailAdr.City[0] > ' ')
      {
         City2Code(sMailAdr.City, acTmp, pOutbuf);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            strcpy(acAddr2, sMailAdr.City);
         }
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      }
      else if (pRec->S_City[0] >= 'A')
      {
         char sCity[32];
         memcpy(sCity, pRec->S_City, CRESIZ_S_CITY);
         sCity[CRESIZ_S_CITY] = 0;
         City2Code(sCity, acTmp, pOutbuf);
         if (acTmp[0] > ' ')
         {
            vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
            strcpy(acAddr2, sCity);
         }

         if (pRec->S_City[25] == '9')
            memcpy(pOutbuf+OFF_S_ZIP, &pRec->S_City[25], SIZ_S_ZIP);
      }
      else if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
      {
         City2Code(sMailAdr.City, acTmp, pOutbuf);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            strcpy(acAddr2, sMailAdr.City);
         }
         memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
      } else
      {
         // Condition: Valid city name, Ex_Val is 7000,
         if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && (sMailAdr.lStrNum > 0) &&
             (iStrNo == sMailAdr.lStrNum) && (*(pOutbuf+OFF_HO_FL) == '1') )
         {
            // Get city code
            City2Code(sMailAdr.City, acTmp, pOutbuf);
            if (acTmp[0] > ' ')
            {
               memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
               memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
               strcpy(acAddr2, sMailAdr.City);
            }
         }

      }

      if (acAddr2[0])
      {
         strcat(myTrim(acAddr2), " CA");
         vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, SIZ_S_CTY_ST_D);
      }
      memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   }

   // Populate mailaddr from situs if situs avail and mail is not - pending decision
   //if (*(pOutbuf+OFF_M_STREET) == ' ' && *(pOutbuf+OFF_S_STREET) > '0')
   //   CopySitusToMailing(pOutbuf);
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Tri_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acCode[32], acTmp1[32];
   long     lTmp;
   int      iTmp;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "53TRI", 5);

      // Create APN_D 
      iTmp = formatApn(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

      // Other value
      // 20091119 - TRI is now populating only FixtVal
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PersFixtVal, LeaseVal, and PP_MH
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lFixture;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixture > 0)
         {
            sprintf(acTmp, "%d         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         } else
         {
            if (lPFixt > 0)
            {
               sprintf(acTmp, "%d         ", lPFixt);
               memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
            }
            if (lPP_MH > 0)
            {
               sprintf(acTmp, "%d         ", lPP_MH);
               memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
            }
            if (lLeaseVal > 0)
            {
               sprintf(acTmp, "%d         ", lLeaseVal);
               memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
            }
         }

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

      // Ratio
      if (lImpr > 0)
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
      else
         strcpy(acTmp, BLANK32);
      memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
   }

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS, pOutbuf+OFF_TIMBER, pOutbuf+OFF_AG_PRE, pRec->ParcType, "TRI");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   if (!memcmp(pRec->UseCode, "XX", 2))
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   else
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // HO Exempt
   if (!memcmp(pRec->ExeCode1, "10", 2) ||
       !memcmp(pRec->ExeCode2, "10", 2) ||
       !memcmp(pRec->ExeCode3, "10", 2) )
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (lTmp > 0)
   {
      if (lTmp == 7000)
         *(pOutbuf+OFF_HO_FL) = '1';            // Just to make sure, some time HO code is wrong
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Exemption
   if (pRec->ExeCode1[0] > ' ')
   {
      memcpy(pOutbuf+OFF_EXE_CD1, pRec->ExeCode1, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD2, pRec->ExeCode2, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD3, pRec->ExeCode3, CRESIZ_EXEMP_CODE);
   }

#ifdef _DEBUG
      //if (!memcmp(pRec->APN, "0143503500", 8))
      //   iTmp = 0;
#endif

   // Owners
   Tri_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address
   Tri_MergeAdr(pOutbuf, pRollRec);

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Lot Acres
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_SQFT,  ' ', SIZ_LOT_SQFT);
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
   }

   // Bldg Sqft - removed per county request 8/30/2016
   memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);
   memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT);
   //lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   //if (lTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
   //   memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   //}

   // Base year - removed per county request 8/30/2016
   //iTmp = atoin(pRec->YrBlt, 4);
   //if (iTmp > 1900)
   //   memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      pRec->LglDesc1[CRESIZ_LGL_DESC1+CRESIZ_LGL_DESC2] = 0;
      iTmp = updateLegal(pOutbuf, pRec->LglDesc1);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // FP - 0,1,2
   if (pRec->Fp[0] > ' ') 
   {
      if (isdigit(pRec->Fp[0]))
         *(pOutbuf+OFF_FIRE_PL) = pRec->Fp[0];
      else
         LogMsg("XFp: %c", pRec->Fp[0]);
   }

   // Misc Impr - No data
   if (pRec->MiscImpr[0] > ' ')
      LogMsg("XMisc: %c", pRec->MiscImpr[0]);

   // AC - No data
   if (pRec->Ac[0] > ' ')
      LogMsg("XAir: %c", pRec->Ac[0]);

   // Heating - No data
   if (pRec->Heating[0] > ' ')
      LogMsg("XHeat: %c", pRec->Heating[0]);

   // Pool - No data
   if (pRec->Pool[0] > ' ')
   {
      if (pRec->Pool[0] == 'Y')
         *(pOutbuf+OFF_POOL) = 'P';
      else
         LogMsg("XPool: %c", pRec->Pool[0]);
   }

   // Spa - No data
   if (pRec->Spa[0] > ' ')
      LogMsg("XSpa: %c", pRec->Spa[0]);

   // Zoning - no data
   if (pRec->Zone[0] > '0')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, SIZ_ZONE);

   // Neighborhood

   // Type_Sqft - M, C
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
         acTmp[iTmp++] = pRec->BldgCls[0];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[1]))
            acTmp[iTmp++] = pRec->BldgCls[1];
         else if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else
            acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   return 0;
}

/********************************* Tri_MergeSitus *****************************
 *
 * Merge situs using info from GFGIS.  To be implemented ...
 *
 ******************************************************************************/

//int Tri_MergeSitus(char *pOutbuf)
//{
//   static   char acRec[1024], *pRec=NULL;
//   char     acTmp[256], acCode[32], acAddr1[256], *pTmp;
//   long     lTmp;
//   int      iRet=0, iTmp;
//   GFGIS    *pSitus = (GFGIS *)&acRec[0];
//
//   // Get rec
//   if (!pRec)
//   {
//      // Get 1st record
//      pRec = fgets(acRec, 1024, fdSitus);
//   }
//
//   do
//   {
//      if (!pRec)
//      {
//         fclose(fdSitus);
//         fdSitus = NULL;
//         return 1;      // EOF
//      }
//
//      // Check APN
//      iTmp = memcmp(pOutbuf, pRec, iApnLen);
//      if (iTmp > 0)
//      {
//         if (bDebug)
//            LogMsg0("Skip Situs rec %.*s", iApnLen, pRec);
//         pRec = fgets(acRec, 1024, fdSitus);
//      }
//   } while (iTmp > 0);
//
//   // If not match, return
//   if (iTmp || pSitus->S_StrName[0] == ' ')
//      return 1;
//
//   // Clear old Situs
//   removeSitus(pOutbuf);
//
//   // Merge data
//   acAddr1[0] = 0;
//   lTmp = atoin(pSitus->S_StrNum, TSIZ_S_STRNUM);
//   if (lTmp > 0)
//   {
//      memset(pOutbuf+OFF_S_HSENO, ' ', SIZ_S_HSENO);
//      memcpy(pOutbuf+OFF_S_HSENO, pSitus->S_StrNum, TSIZ_S_STRNUM);
//
//      sprintf(acTmp, "%d       ", lTmp);
//      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);
//      pTmp = strchr(acTmp, ' ');
//      *(pTmp+1) = 0;
//      strcpy(acAddr1, acTmp);
//
//      //if (pTmp = strchr(apTokens[MB_SITUS_STRNUM], ' '))
//      //   memcpy(pOutbuf+OFF_S_STR_SUB, pTmp+1, strlen(pTmp+1));
//   }
//
//   char acStrSfx[16], acStrName[64];
//   //memcpy(acStrName, apTokens[MB_SITUS_STRNAME]);
//   //acStrSfx[0] = 0;
//   //if (*apTokens[MB_SITUS_STRTYPE] > ' ')
//   //{
//   //   // Fix specific spelling
//   //   if (!strcmp(apTokens[MB_SITUS_STRTYPE], "PA"))
//   //      strcpy(acStrSfx, "PATH");
//   //   else if (!strcmp(apTokens[MB_SITUS_STRTYPE], "GR"))
//   //      sprintf(acStrName, "%s GRADE", apTokens[MB_SITUS_STRNAME]);
//   //   else
//   //      strcpy(acStrSfx, apTokens[MB_SITUS_STRTYPE]);
//   //}
//   vmemcpy(pOutbuf+OFF_S_STREET, acStrName, SIZ_S_STREET);
//   strcat(acAddr1, acStrName);
//
//   // Encode suffix
//   //if (acStrSfx[0] > ' ')
//   //{
//   //   strcat(acAddr1, " ");
//   //   strcat(acAddr1, acStrSfx);
//   //   iTmp = GetSfxCodeX(acStrSfx, acTmp);
//   //   if (iTmp > 0)
//   //   {
//   //      sprintf(acCode, "%d", iTmp);
//   //   } else
//   //   {
//   //      LogMsg0("*** Invalid suffix: %s", apTokens[MB_SITUS_STRTYPE]);
//   //      iBadSuffix++;
//   //      acCode[0] = 0;
//   //   }
//   //   vmemcpy(pOutbuf+OFF_S_SUFF, acCode, SIZ_S_SUFF);
//   //}
//
//   //if (*apTokens[MB_SITUS_UNIT] > ' ')
//   //{
//   //   strcat(acAddr1, " ");
//   //   strcat(acAddr1, apTokens[MB_SITUS_UNIT]);
//   //   memcpy(pOutbuf+OFF_S_UNITNO, apTokens[MB_SITUS_UNIT], strlen(apTokens[MB_SITUS_UNIT]));
//   //}
//
//   //iTmp = blankRem(acAddr1, SIZ_S_ADDR_D);
//   //vmemcpy(pOutbuf+OFF_S_ADDR_D, acAddr1, SIZ_S_ADDR_D, iTmp);
//
//   //// Situs city
//   //if (*apTokens[MB_SITUS_COMMUNITY] > ' ')
//   //{
//   //   int iZip = atol(apTokens[MB_SITUS_ZIP]);
//
//   //   if (*apTokens[MB_SITUS_COMMUNITY] == 'E' && iZip == 95762)
//   //      iTmp = 0;
//
//   //   Abbr2Code(apTokens[MB_SITUS_COMMUNITY], acTmp, acAddr1, iZip, apTokens[MB_SITUS_ASMT]);   
//   //   vmemcpy(pOutbuf+OFF_S_CITY, acTmp, SIZ_S_CITY);
//   //   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
//   //   if (*apTokens[MB_SITUS_ZIP] > '0')
//   //      vmemcpy(pOutbuf+OFF_S_ZIP, apTokens[MB_SITUS_ZIP], SIZ_S_ZIP);
//
//   //   if (acAddr1[0] > ' ')
//   //      sprintf(acTmp, "%s, CA %s", myTrim(acAddr1), apTokens[MB_SITUS_ZIP]);
//   //   vmemcpy(pOutbuf+OFF_S_CTY_ST_D, acTmp, SIZ_S_CTY_ST_D);
//   //}
//
//   //lSitusMatch++;
//
//   // Get next record
//   pRec = fgets(acRec, 512, fdSitus);
//
//   return 0;
//}

/********************************** Tri_LoadRoll ****************************
 *
 * This function will create new R01 file.  
 *
 ****************************************************************************

int Tri_LoadRoll(char *pCnty, int iFirstRec)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   long     lCnt=0;

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   if (iFirstRec > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   // Check for test record
   if (!memcmp(&acRollRec[CREOFF_APN], "000000", 6))
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);

   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      iRet = Tri_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      lLDRRecCount++;
      if (!iRet)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         if (!bRet)
         {
            LogMsg("***** Error occurs: %d\n", GetLastError());
            iRet = WRITE_ERR;
            break;
         }
         lCnt++;
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);
   }

   if (iRet > 0)
      iRet = 0;

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u\n", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return iRet;
}

/******************************* Tri_ExtrSale *******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Tri_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024];
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   long     lCnt=0, lNewRec=0, lPrice;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {
      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0143503500", 10))
//         iRet = 0;
#endif

      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook1);

      if (acDate[0] > ' ' || acDoc[0] > ' ')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         lPrice = atol(acTmp);
         if (lPrice > 0)
         {
            sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > ' ' || acDoc[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Tri_Fmt1Doc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > ' ' || acDoc[0] > ' ')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      if (iRet < iRollLen)
         bEof = true;

      if (!(++lCnt % 1000))
      {
         printf("\r%u", lCnt);
         if (lCnt > 20000)
         {
            LogMsg("***** Input file is corrupted. Please check!");
            bEof = true;
            iRet = -999;
         }
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   if (iRet == -999)
      return iRet;

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,15,12,C,A,57,10,C,D) F(TXT) DUPO(B2000,1,34)");
   long lOutrecs = sortFile(acOutFile, acBuf, acTmp);
   if (lOutrecs <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lOutrecs);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/********************************** Tri_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 ****************************************************************************/

int Tri_LoadRoll(char *pCnty, int iSkip, int bUseLDR)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, lLienUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   if (bUseLDR)
   {
      // Open lien extract file
      sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
      LogMsg("Open lien extract file %s", acLienExt);
      fdLienExt = fopen(acLienExt, "r");
      if (fdLienExt == NULL)
      {
         LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
         return -2;
      }
   } else
      fdLienExt = NULL;

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

#ifdef _DEBUG
      //if (!memcmp(&acRollRec[CREOFF_APN], "0143503600", 10))
      //   iRet = 0;
#endif

      if (memcmp(&acRollRec[CREOFF_APN], "000000", 6))
      {
         iRet = Tri_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
         if (!iRet)
         {
            if (fdLienExt)
            {
               iRet = PQ_MergeLien(acBuf, fdLienExt);
               if (!iRet)
                  lLienUpd++;
            }

            iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iRet >= lToday)
               LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
            else if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            lRecCnt++;
         } else
            iRetiredRec++;
      } else
         LogMsg("*** Bad APN=%.10s at %d", &acRollRec[CREOFF_APN], lCnt);

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsgD("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lRecCnt);
   LogMsg("Total retired records:      %u", iRetiredRec);
   LogMsg("Total lien updated records: %u\n", lLienUpd);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   return 0;
}

/********************************* Tri_MergeLien *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Tri_MergeLien(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     acTmp[256], acCode[32], acTmp1[32];
   long     lTmp;
   int      iTmp;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Clear output buffer
   memset(pOutbuf, ' ', iRecLen);

   // APN
   memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
   memcpy(pOutbuf+OFF_CO_NUM, "53TRI", 5);

   // Create APN_D 
   iTmp = formatApn(pOutbuf, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

   // Create MapLink and output new record
   iTmp = formatMapLink(pOutbuf, acTmp, &myCounty);
   memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

   // Create index map link
   getIndexPage(acTmp, acTmp1, &myCounty);
   memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

   // Assessment year
   memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

   // Land
   long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
   if (lLand > 0)
      sprintf(acTmp, "%*d", SIZ_LAND, lLand);
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);

   // Improve
   long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
   if (lImpr > 0)
      sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);

   // Other value
   // 20091119 - TRI is now populating only FixtVal
   long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
   long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
   long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
   // FixtVal is the sum of PersFixtVal, LeaseVal, and PP_MH
   long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
   long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

   lTmp = lPers + lFixture;
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
      memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

      if (lFixture > 0)
      {
         sprintf(acTmp, "%d         ", lFixture);
         memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
      } else
      {
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }

      if (lPers > 0)
      {
         sprintf(acTmp, "%d         ", lPers);
         memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
      }
   }

   // Gross total
   lTmp += lLand+lImpr;
   if (lTmp > 0)
      sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);

   // Ratio
   if (lImpr > 0)
      sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
   else
      strcpy(acTmp, BLANK32);
   memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS, pOutbuf+OFF_TIMBER, pOutbuf+OFF_AG_PRE, pRec->ParcType, "TRI");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   if (!memcmp(pRec->UseCode, "XX", 2))
      memcpy(pOutbuf+OFF_USE_STD, USE_UNASGN, SIZ_USE_STD);
   else
      updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // HO Exempt
   if (!memcmp(pRec->ExeCode1, "10", 2) ||
       !memcmp(pRec->ExeCode2, "10", 2) ||
       !memcmp(pRec->ExeCode3, "10", 2) )
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (lTmp > 0)
   {
      if (lTmp == 7000)
         *(pOutbuf+OFF_HO_FL) = '1';            // Just to make sure, some time HO code is wrong
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   }

   // Exemption
   if (pRec->ExeCode1[0] > ' ')
   {
      memcpy(pOutbuf+OFF_EXE_CD1, pRec->ExeCode1, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD2, pRec->ExeCode2, CRESIZ_EXEMP_CODE);
      memcpy(pOutbuf+OFF_EXE_CD3, pRec->ExeCode3, CRESIZ_EXEMP_CODE);
   }

#ifdef _DEBUG
      //if (!memcmp(pRec->APN, "0143503500", 8))
      //   iTmp = 0;
#endif

   // Owners
   Tri_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address
   Tri_MergeAdr(pOutbuf, pRollRec);

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      // Lot Sqft
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);

      // Lot Acres
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
      memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);
   } else
   {
      memset(pOutbuf+OFF_LOT_SQFT,  ' ', SIZ_LOT_SQFT);
      memset(pOutbuf+OFF_LOT_ACRES, ' ', SIZ_LOT_ACRES);
   }

   // Bldg Sqft - removed per county request 8/30/2016
   memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);
   memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT);
   //lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   //if (lTmp > 0)
   //{
   //   sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
   //   memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   //}

   // Base year - removed per county request 8/30/2016
   //iTmp = atoin(pRec->YrBlt, 4);
   //if (iTmp > 1900)
   //   memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);

   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      pRec->LglDesc1[CRESIZ_LGL_DESC1+CRESIZ_LGL_DESC2] = 0;
      iTmp = updateLegal(pOutbuf, pRec->LglDesc1);
      if (iTmp > iMaxLegal)
         iMaxLegal = iTmp;
   }

   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // FP - 0,1,2
   if (pRec->Fp[0] > ' ') 
   {
      if (isdigit(pRec->Fp[0]))
         *(pOutbuf+OFF_FIRE_PL) = pRec->Fp[0];
      else
         LogMsg("XFp: %c", pRec->Fp[0]);
   }

   // Misc Impr - No data
   if (pRec->MiscImpr[0] > ' ')
      LogMsg("XMisc: %c", pRec->MiscImpr[0]);

   // AC - No data
   if (pRec->Ac[0] > ' ')
      LogMsg("XAir: %c", pRec->Ac[0]);

   // Heating - No data
   if (pRec->Heating[0] > ' ')
      LogMsg("XHeat: %c", pRec->Heating[0]);

   // Pool - No data
   if (pRec->Pool[0] > ' ')
   {
      if (pRec->Pool[0] == 'Y')
         *(pOutbuf+OFF_POOL) = 'P';
      else
         LogMsg("XPool: %c", pRec->Pool[0]);
   }

   // Spa - No data
   if (pRec->Spa[0] > ' ')
      LogMsg("XSpa: %c", pRec->Spa[0]);

   // Zoning - no data
   if (pRec->Zone[0] > '0')
      memcpy(pOutbuf+OFF_ZONE, pRec->Zone, SIZ_ZONE);

   // Neighborhood

   // Type_Sqft - M, C
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
         acTmp[iTmp++] = pRec->BldgCls[0];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[1]))
            acTmp[iTmp++] = pRec->BldgCls[1];
         else if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else
            acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }

   return 0;
}

/********************************** Tri_Load_LDR ****************************
 *
 * This function is create to load new LDR file for 2019.
 *
 ****************************************************************************/

int Tri_Load_LDR(int iFirstRec)
{
   char     acOutFile[_MAX_PATH], acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE], *pTmp;
   int      iRet, lRet=0, lCnt=0, lDrop=0;

   HANDLE   fhOut;
   DWORD    nBytesWritten;
   BOOL     bRet;

   LogMsg0("Loading LDR file.");

   // Open Roll file
   LogMsg("Open LDR file %s", acRollFile);
   fdRoll = fopen(acRollFile, "r");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return -2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   pTmp = fgets((char *)&acRollRec[0], 1024, fdRoll);
   if (*pTmp == ' ')
      pTmp = fgets((char *)&acRollRec[0], 1024, fdRoll);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (pTmp)
   {
      // Replace all TAB and NULL character in input record
      replCharEx(acRollRec, 31, 32, iRollLen);

#ifdef _DEBUG
      //if (!memcmp(&acRollRec[CREOFF_APN], "0143503600", 10))
      //   iRet = 0;
#endif

      if (acRollRec[CREOFF_APN] > ' ')
      {
         iRet = Tri_MergeLien(acBuf, acRollRec);
         if (!iRet)
         {
            iRet = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
            if (iRet >= lToday)
               LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iRet, lCnt, acBuf);
            else if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
            lRecCnt++;
         } else
            lDrop++;
      } else
         LogMsg("*** Bad APN=%.10s at %d", &acRollRec[CREOFF_APN], lCnt);

      // Read next roll record
      pTmp = fgets((char *)&acRollRec[0], 1024, fdRoll);

      if (!(lCnt++ % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total records processed:    %u", lCnt);
   LogMsg("Total output records:       %u", lRecCnt);
   LogMsg("Total bad records:          %u", lDrop);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);
   LogMsg("Last recording date:        %u\n", lLastRecDate);

   lLDRRecCount = lRecCnt;
   return 0;
}

/******************************* Tri_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Tri_MakeDocLink(LPSTR pDocLink, LPSTR pDoc, LPSTR pDate)
{
   int   iTmp, iDocNum;
   char  acTmp[256], acDocName[256];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      iTmp = atoin((char *)pDate, 4);
      iDocNum = atoin((char *)pDoc+4, 5);

      if (iTmp >= 1990 && iDocNum > 0)
      {
         sprintf(acDocName, "%.4s\\%.3s\\%.4s%.5d", pDoc, pDoc+4, pDoc, iDocNum);
         sprintf(acTmp, "%s\\%s.pdf", acDocPath, acDocName);
         if (!_access(acTmp, 0))
            strcpy(pDocLink, acDocName);
      }
   }
}

/******************************* Tuo_FmtDocLinks ****************************
 *
 * Format DocLinks as bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp,bbb/yyyybbbpppp
 * which are Doclink1, Doclink2, Doclink3, DocXferlink
 *
 ****************************************************************************/

int Tri_FmtDocLinks(int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRawFile[_MAX_PATH], acOutFile[_MAX_PATH],
            acDocLink[256], acDocLinks[256];

   HANDLE   fhIn, fhOut;

   DWORD    nBytesRead;
   DWORD    nBytesWritten;
   BOOL     bRet;
   long     lCnt=0, lLinkCnt=0;
   int      iTmp, iRet = 0;

   LogMsg("Format Doclinks ...");

   sprintf(acRawFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "R01");
   sprintf(acOutFile, acRawTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "T01");

   // Open Input file
   LogMsg("Open input file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return 3;
   }
   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acOutFile);
      return 4;
   }

   // Copy skip record
   memset(acBuf, ' ', MAX_RECSIZE);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (true)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRawFile, GetLastError());
         break;
      }

      if (!nBytesRead)
         break;

#ifdef _DEBUG
//      if (!memcmp(acBuf, "08529006", 8))
//         bRet = true;
#endif

      // Reformat doc#

      // Reset old links
      memset((char *)&acBuf[OFF_DOCLINKS], ' ', SIZ_DOCLINKS);
      acDocLinks[0] = 0;

      // Doc #1
      Tri_MakeDocLink(acDocLink, (char *)&acBuf[OFF_SALE1_DOC], (char *)&acBuf[OFF_SALE1_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Doc #2
      Tri_MakeDocLink(acDocLink, (char *)&acBuf[OFF_SALE2_DOC], (char *)&acBuf[OFF_SALE2_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Doc #3
      Tri_MakeDocLink(acDocLink, (char *)&acBuf[OFF_SALE3_DOC], (char *)&acBuf[OFF_SALE3_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }
      strcat(acDocLinks, ",");

      // Transfer Doc
      Tri_MakeDocLink(acDocLink, (char *)&acBuf[OFF_TRANSFER_DOC], (char *)&acBuf[OFF_TRANSFER_DT]);
      if (*acDocLink)
      {
         lLinkCnt++;
         strcat(acDocLinks, acDocLink);
      }

      // Update DocLinks
      iTmp = strlen(acDocLinks);
      if (iTmp > 10)
         memcpy((char *)&acBuf[OFF_DOCLINKS], acDocLinks, iTmp);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("***** Error occurs: %d\n", GetLastError());
         break;
      }
   }

   // Close files
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename out file
   if (lCnt > 10000)
   {
      if (remove(acRawFile))
      {
         LogMsg("***** Error removing file: %s (%d)", acRawFile, errno);
         iRet = errno;
      } else if (rename(acOutFile, acRawFile))
      {
         LogMsg("***** Error renaming temp file: %s --> %s (%d)", acOutFile, acRawFile, errno);
         iRet = errno;
      }
   }

   LogMsg("FmtDocLinks completed.  %d doclinks assigned.\n", lLinkCnt);
   return iRet;
}

/*********************************** loadTri ********************************
 *
 * Input files: redifile (899-bytes roll file)
 *
 * Problems:
 *
 * Note:    Always use current values.  Do not need to extract lien.
 *
 * Commands:
 *    -U -Xsi   Monthly update
 *    -L -Xsi   Lien update
 *
 ****************************************************************************/

int loadTri(int iLoadFlag, int iSkip)
{
   int   iRet;
   //char  acTmpFile[_MAX_PATH];

   iApnLen = myCounty.iApnLen;
   iRollLen = guessRecLen(acRollFile, iRollLen);

   // Load tax
   //if (iLoadTax)                                   // -T
   //{
   //   iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", acTmpFile, _MAX_PATH, acIniFile);
   //   if (iRet > 0)
   //      iRet = LoadTaxCodeTable(acTmpFile);

   //   // Load Base
   //   iRet = Cres_Load_Cortac(bTaxImport, false);
   //   if (!iRet && lLastTaxFileDate > 0)
   //   {
   //      iRet = Cres_Load_TaxDelq(bTaxImport);
   //      if (!iRet)
   //         iRet = updateDelqFlag(myCounty.acCntyCode);

   //      // Load Detail & Agency
   //      iRet = Cres_Load_GFGIS(bTaxImport, false);

   //      //if (!iRet)
   //      //   iRet = updateDelqFlag(myCounty.acCntyCode);
   //   }

   //   if (iRet > 0)
   //      iRet = 0;
   //}

   // Load tax
   if (iLoadTax == TAX_LOADING)                    // -T or -Lt
   {
      iRet = TC_LoadTax(myCounty.acCntyCode, bTaxImport);
   }

   if (!iLoadFlag)
      return iRet;

   // Use SaleTmpl for cum sale file
   strcpy(acESalTmpl, acSaleTmpl);

   // Extract sale1 from redifile and append to cum sale file
   if (iLoadFlag & EXTR_SALE)                      // -Xs
   {
      iRet = Tri_ExtrSale(acRollFile, NULL, true);
      if (iRet < 0)
         return iRet;
      iLoadFlag |= MERG_CSAL;
   }

   // Extract LDR values, but this might not be used anywhere since the county want us to display current values
   if (iLoadFlag & EXTR_LIEN)                      // -Xl
      iRet = Cres_ExtrLienLF(myCounty.acCntyCode); // 2019 record has LF

   // Always create new R01 file
   if (iLoadFlag & LOAD_LIEN)                      // -L
      iRet = Tri_Load_LDR(iSkip);
   else if (iLoadFlag & LOAD_UPDT)                 // -U
   {
      iRet = GetPrivateProfileInt(myCounty.acCntyCode, "UseLDR", 0, acIniFile);
      iRet = Tri_LoadRoll(myCounty.acCntyCode, iSkip, iRet);
   }
   // Update sale and populate DocNum even when there is no sale date
   if (!iRet && (iLoadFlag & MERG_CSAL))           // -Ms
   {
      char sTmp[32];

      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, CLEAR_OLD_SALE|DONT_CHK_DOCDATE);

      GetPrivateProfileString(myCounty.acCntyCode, "DocLink", "Y", sTmp, _MAX_PATH, acIniFile);

      // Format DocLinks - Doclinks are concatenate fields separated by comma 
     if (!iRet && sTmp[0] == 'Y')
         iRet = updateDocLinks(Tri_MakeDocLink, myCounty.acCntyCode, iSkip, 1);
         //iRet = Tri_FmtDocLinks(iSkip);
   }

   return iRet;
}
   