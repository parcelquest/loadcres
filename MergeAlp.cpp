/*****************************************************************************
 *
 * Notes:
 *    1) Adding characteristic data
 *    2) Adding sale price
 *    3) Add legal desc
 *
 * Problems:
 *    1) Clean up Name2, Keep Name 1 the same as input with minor cleanup
 *    2) Clean up situs address with multiple house number
 *
 * Revision:
 * 02/06/2006 1.0.0     First version by Tung Bui
 * 12/14/2006 1.8.5     Remove CHARS from R01 file. County said this data from
 *                      CREST is not reliable.
 * 05/02/2007 1.8.7     Modify to cope with problem that county sometimes sends
 *                      us data with LF at end of record.
 * 08/07/2007 1.8.13    Create cum sale file and update it every time the program run.
 *                      On next LDR, this cum sale will be used to update.
 *                      Extract lien values.
 * 08/13/2007 1.8.14    Create new function Alp_LoadRoll() to load roll file.
 *                      Lien values are now updated from extracted file.  And sale
 *                      data is merged via MergeCumSale1().  For big counties,
 *                      try to merge sales directly in ???_LoadRoll() to save time.
 * 12/11/2007 1.8.25    Fix Alp_LoadRoll() to include all parcels except retired ones
 *                      which ALP said they didn't extract for us any way.
 * 02/04/2008 1.10.3    Add support of Standard Usecode
 * 03/09/2008 1.10.4    Use standard function to update usecode
 * 05/01/2008 1.10.6    Adding Alp_Fmt2Doc() to support new DocNum/Date format.  This 
 *                      affects all recording after 08/2007.
 *                      ValueChg and NameChg are not reliable enough to be used as Rec. Date.
 *                      For now, just keep the year and leave month & date blank.
 * 07/16/2008 8.1.2     Add function Alp_Load_LDR() to load LDR data only, no update.
 * 03/24/2009 8.6       Format STORIES as 99.9.  We no longer multiply by 10
 * 07/09/2009 9.1.1     Modify Alp_CreateR01() to add other values.
 * 08/04/2009 9.1.3     Populate other values.
 * 07/01/2010 10.1.0    When update cumsale, update transfers also since it's in the roll.
 * 07/20/2011 11.0.2    Add S_HSENO. Modify Alp_MergeAdr() to take situs from both normal
 *                      location and S_CITY where full addr is stored.
 * 07/22/2011 11.1.2    Clear out old situs before applying new one.
 * 08/01/2011 11.1.4.1  Write CA to OFF_S_CTY_ST_D even when city not available.
 * 10/26/2011 11.2.8.1  Add Alp_ExtrSale() to extract sales from roll file to SCSAL_REC.
 *                      Add Alp_FmtDoc() to reformat DocNum & DocDate. Use ApplyCumSale()
 *                      to update R01 file. Modify Alp_CreateR01() not to populate
 *                      sale info.  Let ApplyCumSale() populates it from Alp_Sale.sls.
 * 07/05/2012 12.0.1    Remove single quote in street name in Alp_MergeAdr().
 * 10/02/2013 13.3.3    Rewrite Alp_MergeOwner() and update vesting
 * 10/14/2013 13.3.4    Remove '.' in owner names. Use standard removeMailing() and removeSitus().
 * 01/23/2014 13.5.0    Update Multi-APN, Sale Code (Full/Partial), and Pct xfer.
 * 10/29/2014 14.5.0    Increase bufsize for DocNum from 16 to 64 bytes to avoid problem.
 * 11/07/2016 16.6.0    Add -T option to load tax file.
 * 12/01/2016 16.7.0    Create empty Tax_Delq & Tax_Owner tables until we have data for it.
 * 05/04/2018 17.6.0    Use Cres_Load_Cortac() to load TaxBase and use Cres_Load_GFGIS() to load Detail tax bill.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "doSort.h"
#include "R01.h"
#include "Prodlib.h"
#include "RecDef.h"
#include "Tables.h"
#include "Pq.h"
#include "SaleRec.h"
#include "Logs.h"
#include "LoadCres.h"
#include "Utils.h"
#include "XlatTbls.h"
#include "doOwner.h"
#include "formatApn.h"
#include "UseCode.h"
#include "LCExtrn.h"
#include "Tax.h"

/********************************* Alp_FmtDoc ********************************
 *
 *   1) 
 *   2) 8200745   122293=8200745  19931222 ?
 *   3) 086250197 112497=86250197 19971124
 *   4) 
 *   5) 092269103  40203=92269103 20030402
 *   6) 085036096  22096=85036096 19961220
 *   7) 8300246   940131=8300246  19940131 ?
 *   8) 094591605 102405=94591605 20051024 (94-5916 20051024)
 *   9) 55021886  101086=55021886 19861010 (55-0218 19861010)
 *  10) 0926990  91803  =0926990  20030918 ?
 *  11) 2007000862081507=000862   20070815
 *  12) 2009000372 60209=000372   20090602
 *  13) 2011000342 60311=000342   20110603
 *
 *****************************************************************************/

void Alp_FmtDoc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp, iMonth, iDay, iYear, iCurYear;
   char  acTmp[32], acDate[16], acDoc[32], *pTmp;

   *pOutDoc = 0;
   *pOutDate = 0;

   // Make sure DocNum start with a digit
   if (!isdigit(*pRecDoc))
      return;

   acDoc[12] = 0;
   acDate[8] = 0;
   memset(acDoc, ' ', 12);
   memset(acDate, ' ', 8);
   memcpy(acTmp, pRecDoc, 16);
   myTrim(acTmp, 16);

   // Check for RecDate
   if (acTmp[11] >= '0')
   {
      pTmp  = &acTmp[10];
      iTmp = strlen(pTmp);
      if (iTmp == 5 && *pTmp != ' ')
         pTmp  = &acTmp[9];
      else if (iTmp == 4 && acTmp[9] != ' ')
         pTmp  = &acTmp[8];
      else if (iTmp != 6)
      {
         LogMsg("??? Invalid date: %s", acTmp);
         return;
      }

      iMonth= atoin(pTmp, 2);
      iDay  = atoin(pTmp+2, 2);
      iYear = atoin(pTmp+4, 2);
   } else
      return;                 // No date, skip it
               
   if (acTmp[0] == '2')
   {
      iTmp = atoin(&acTmp[4], 6);
      sprintf(acDoc, "%.6d      ", iTmp);
   } else if (acTmp[0] == '0')
   {
      iTmp = atoin(&acTmp[1], 6);
      sprintf(acDoc, "%.6d%.2d    ", iTmp, iYear);
   } else
   {
      if (pTmp = strchr(acTmp, ' '))
         *pTmp++ = 0;
      sprintf(acDoc, "%s          ", acTmp);
   }                                 

   // In most cases, date format is MMDDYY
   // but sometimes it is YYMMDD.  So check the first two for valid month
   if (iMonth > 12)
   {
      iTmp = iYear;
      iYear = iMonth;
      iMonth = iDay;
      iDay = iTmp;
   }

   if (iMonth && iDay)
   {
      iCurYear = getCurYear();
      if (iYear > iCurYear-2000)
         sprintf(acDate, "19%.2d%.2d%.2d", iYear, iMonth, iDay);
      else
         sprintf(acDate, "20%.2d%.2d%.2d", iYear, iMonth, iDay);
   }
   memcpy(pOutDate, acDate, 8);
   memcpy(pOutDoc, acDoc, 12);
}

/********************************* Alp_Fmt1Doc *******************************
 *
 *   1) 004350583      0=04350583
 *   2) 6344288        0=6344288
 *   3) 086250197 112497=86250197 19971124
 *   4) 000922561      0=00922561
 *   5) 092269103  40203=92269103 20030402
 *   6) 085036096  22096=85036096 19961220
 *   7) 8300246   940131=8300246  19940131
 *   8) 094591605 102405=94591605 20051024 (94-5916 20051024)
 *   9) 55021886  101086=55021886 19861010 (55-0218 19861010)
 *  10) 0926990  91803  =0926990  20030918
 *
 *****************************************************************************/

void Alp_Fmt1Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp, iMonth, iDay, iYear, iCurYear;
   char  acTmp[16], acTmp1[16], acDate[16], acDoc[64], *pTmp;

   acDoc[12] = 0;
   acDate[8] = 0;
   memset(acDoc, ' ', 12);
   memset(acDate, ' ', 8);

   memcpy(acTmp, pRecDoc, 16);
   myTrim(acTmp, 16);
   pTmp = strchr(acTmp, ' ');
   // Store Doc# in acTmp and DocDate in acTmp1
   if (pTmp)
   {
      *pTmp++ = 0;
      while (*pTmp == ' ')
         pTmp++;
      strcpy(acTmp1, pTmp);
   } else
      strcpy(acTmp1, "        ");

   if ((iTmp=strlen(acTmp)) > 8)
      memcpy(acDoc, (char *)&acTmp[1], 8);
   else if (iTmp > 3)
      memcpy(acDoc, acTmp, iTmp);

   memcpy(pOutDoc, acDoc, 12);

   if (memcmp(acTmp1, "  ", 2))
   {
      if ((iTmp=strlen(acTmp1)) == 5)
      {
         sprintf(acTmp, "0%s", acTmp1);
         pTmp = acTmp;
      } else if (iTmp > 5)
         pTmp = acTmp1;

      if (iTmp >= 5)
      {
         // In most cases, date format is MMDDYY
         // but sometimes it is YYMMDD.  So check the first two for valid month
         iMonth = atoin(pTmp, 2);
         if (iMonth > 0 && iMonth < 13)
         {
            iDay = atoin(pTmp+2, 2);
            iYear = atoin(pTmp+4, 2);
         } else
         {
            iYear = iMonth;
            iMonth = atoin(pTmp+2, 2);
            iDay = atoin(pTmp+4, 2);
         }

         if (iMonth && iDay)
         {
            iCurYear = getCurYear();
            if (iYear > iCurYear-2000)
               sprintf(acDate, "19%.2d%.2d%.2d", iYear, iMonth, iDay);
            else
               sprintf(acDate, "20%.2d%.2d%.2d", iYear, iMonth, iDay);
         }
      }
   }
   memcpy(pOutDate, acDate, 8);
}

/********************************* Alp_Fmt2Doc *******************************
 *
 * New change after 2007 LDR (11/01/2007)
 * DocNum            Value Chg   Name Chg 
 * 2007000862081507  10252006    03062006
 * 2007000868        04252008    11062007
 * 2007000905        04202007    09082005
 * 2007000920        07262007    11072007
 * 2007000932101007
 * 2007000933                    01152008
 * 2007001035        05202004    02142005
 * 2007001116        04222008    04222008
 *
 * Note: ValueChg and NameChg are not reliable enough to be used as Rec. Date.
 * For now, just keep the year and leave month & date blank.
 *
 *****************************************************************************/

void Alp_Fmt2Doc(char *pOutDoc, char *pOutDate, char *pRecDoc)
{
   int   iTmp, iMonth, iDay, iYear;
   char  acTmp[32], acDate[32], acDoc[32], *pTmp;
   memcpy(acTmp, pRecDoc, 16);
   myTrim(acTmp, 16);

   // Check for RecDate
   if (acTmp[11] >= '0')
   {
      pTmp  = &acTmp[10];
      iMonth= atoin(pTmp, 2);
      iDay  = atoin(pTmp+2, 2);
      iYear = 2000+atoin(pTmp+4, 2);
   } else
   {
      iMonth= 0;
      iDay  = 0;
      iYear = atoin(acTmp, 4);
   }

   iTmp = atoin(&acTmp[4], 6);
   sprintf(acDoc, "%d            ", iTmp);
   memcpy(pOutDoc, acDoc, 12);

   sprintf(acDate, "%d%.2d%.2d    ", iYear, iMonth, iDay);
   memcpy(pOutDate, acDate, 8);
}

/********************************* Alp_FmtRecDoc *****************************
 *
 *
 *****************************************************************************/

void Alp_FmtRecDoc(char *pOutbuf, char *pRollRec)
{
   long     lPrice, lDate, iTmp;
   double   dTmp;
   char     acTmp[32], acDate[16], acDoc[64], *pTmp;
   bool     bMisAligned = false;

   REDIFILE *pRec = (REDIFILE *)pRollRec;

   pTmp = pRec->RecBook1;

   iTmp = atoin(pTmp, 4);
   if (iTmp >= 2007 && iTmp < 2009)
      Alp_Fmt2Doc(acDoc, acDate, pTmp);
   else
      Alp_Fmt1Doc(acDoc, acDate, pTmp);

   // Update last recording date
   lDate = atol(acDate);
   if (lDate > lLastRecDate && lDate < lToday)
      lLastRecDate = lDate;

   if (acDoc[0] > ' ')
      *(pOutbuf+OFF_AR_CODE1) = 'A';           // Assessor-Recorder code
   else
      *(pOutbuf+OFF_AR_CODE1) = ' ';           // Assessor-Recorder code

   memcpy(pOutbuf+OFF_SALE1_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_TRANSFER_DT, acDate, SIZ_SALE1_DT);
   memcpy(pOutbuf+OFF_SALE1_DOC, acDoc, SIZ_SALE1_DOC);
   memcpy(pOutbuf+OFF_TRANSFER_DOC, acDoc, SIZ_SALE1_DOC);

   memcpy(acTmp, pRec->StampAmt, CRESIZ_STMP_AMT);
   acTmp[CRESIZ_STMP_AMT] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lPrice = (long)(dTmp * SALE_FACTOR);
      if (lPrice < 100)
         sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
      else
         sprintf(acTmp, "%*d00", SIZ_SALE1_AMT-2, lPrice/100);
      memcpy(pOutbuf+OFF_SALE1_AMT, acTmp, SIZ_SALE1_AMT);
   }

   // Advance to Rec #2
   pTmp += 16;
   if (iTmp >= 2007 && iTmp < 2009)
      Alp_Fmt2Doc(acDoc, acDate, pTmp);
   else
      Alp_Fmt1Doc(acDoc, acDate, pTmp);
   memcpy(pOutbuf+OFF_SALE2_DT, acDate, SIZ_SALE2_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE2) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE2_DOC, acDoc, SIZ_SALE2_DOC);
   }

   // Advance to Rec #3
   pTmp += 16;
   if (iTmp >= 2007 && iTmp < 2009)
      Alp_Fmt2Doc(acDoc, acDate, pTmp);
   else
      Alp_Fmt1Doc(acDoc, acDate, pTmp);
   memcpy(pOutbuf+OFF_SALE3_DT, acDate, SIZ_SALE3_DT);
   if (acDoc[0] > ' ')
   {
      *(pOutbuf+OFF_AR_CODE3) = 'A';           // Assessor-Recorder code
      memcpy(pOutbuf+OFF_SALE3_DOC, acDoc, SIZ_SALE3_DOC);
   }
}

/******************************** Alp_CleanName ******************************
 *
 * Return 99 if found a vesting
 *
 *****************************************************************************/

int Alp_CleanName(char *pSrcName, char *pDstName, char *pVesting, int iLen=0)
{
   char  acTmp[128], *pTmp;

   if (iLen > 0)
   {
      memcpy(acTmp, pSrcName, iLen);
      acTmp[iLen] = 0;
   } else
      strcpy(acTmp, pSrcName);

   pTmp = findVesting(acTmp, pVesting);
   if (pTmp)
      *pTmp = 0;
   strcpy(pDstName, acTmp);

   if (pTmp)
      return 99;
   else
      return 0;
}

/******************************** Alp_MergeOwner *****************************
 *
 * Merge Name1 & Name2 only for swap name. Keep Name1 & Name2 separate as provided
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Alp_MergeOwner(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acTmp[64], acName1[64], acName2[64], acVesting1[8], acVesting2[8];
   char  *pTmp, *pName1, *pName2, acOwners[64];

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   myTrim((char *)&acName1[0], CRESIZ_NAME_1);
   strcpy(acOwners, acName1);
   pName1 = acName1;
   acVesting1[0] = 0;

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   myTrim((char *)&acName2[0], CRESIZ_NAME_2);
   pName2 = acName2;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0411000230", 10) || !memcmp(pOutbuf, "0470100269", 10) )
   //   iTmp = 0;
#endif

   // Check owner2 for # and %
   if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, pName2, strlen(pName2));
      *pName2 = 0;
   } else if (*pName2 == '&')
   {
      // Process as two names
      pName2 += 2;
   } else if (*pName2 > ' ')
   {
      // Ignore name2 if it is duplicate of name1 last and first names
      if (!strcmp(pName1, pName2))
      {
         *pName2 = 0;
      } else if (!strchr(pName2, ' '))
      {
         // Continuation of name1
         iTmp = sprintf(acOwners, "%s %s", pName1, pName2);
         blankRem(acOwners, iTmp);
         strcpy(acName1, acOwners);
         *pName2 = 0;
      }
   }

   // Check name2
   if (*pName2 > ' ')
   {
      remChar(pName2, '.');
      memcpy(pOutbuf+OFF_NAME2, pName2, strlen(pName2));
      // Find vesting in Name2 
      pTmp = findVesting(pName2, acVesting2);
      if (pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acVesting2, strlen(acVesting2));

         // Check EtAl
         if (!memcmp(acVesting2, "EA", 2))
            *(pOutbuf+OFF_ETAL_FLG) = 'Y';
      } 
   }

   // Update Name1
   remChar(acName1, '.');
   iTmp = strlen(acName1);
   if (iTmp > SIZ_NAME1) iTmp = SIZ_NAME1;
   memcpy(pOutbuf+OFF_NAME1, acName1, iTmp);

   // Cleanup Name1 
   iTmp = Alp_CleanName(acName1, acTmp, acVesting1);
   if (iTmp == 99)
   {
      if (strchr(acTmp, ' '))
         strcpy(acOwners, acTmp);
      memcpy(pOutbuf+OFF_VEST, acVesting1, strlen(acVesting1));

      // Check Etal
      if (!memcmp(acVesting1, "EA", 2))
         *(pOutbuf+OFF_ETAL_FLG) = 'Y';
   } 

   // Now parse owners
   splitOwner(acOwners, &myOwner, 3);
   if (acVesting1[0] > ' ' && isVestChk(acVesting1))
   {
      memcpy(myOwner.acSwapName, pOutbuf+OFF_NAME1, SIZ_NAME1);
      myOwner.acSwapName[SIZ_NAME1] = 0;
   }
   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);
}

/******************************** Alp_MergeOwner *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Alp_MergeOwner1(char *pOutbuf, char *pNames)
{
   int   iTmp;
   char  acOwners[64], acTmp[64], acTmp1[64], acSave[64], acName1[64], acName2[32];
   char  *pTmp, *pTmp1, *pName1, *pName2;

   OWNER myOwner;

   // Clear output buffer if needed
   removeNames(pOutbuf);

   // Initialize
   memcpy(acName1, pNames, CRESIZ_NAME_1);
   acName1[CRESIZ_NAME_1] = 0;
   pName1 = myTrim(acName1);

   // Point to name2
   memcpy(acName2, pNames+CRESIZ_NAME_1, CRESIZ_NAME_2);
   acName2[CRESIZ_NAME_2] = 0;
   pName2 = myTrim(acName2);
   _strupr(acName2);

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0062330070", 10))
   //   iTmp = 0;
#endif

   // Check for duplicate name1 & name2
   pTmp = acName2;
   pTmp1 = strchr(acName2, ' ');
   if (pTmp1)
   {
      pTmp = strchr(pTmp1+1, ' ');
      if (pTmp) *pTmp = 0;
   }
   // Ignore name2 if it is duplicate of name1 last and first names
   if (!memcmp(acName1, acName2, strlen(acName2)) )
      acName2[0] = 0;
   else if (pTmp && !*pTmp) 
      *pTmp = ' ';     // Replace blank character that we removed earlier

   // Check owner2 for # - drop them
   if (*pName2 == '#' || !memcmp(pName2, "TRUST", 5))
   {
      *pName2 = 0;
   } else if (*pName2 == '%')
   {
      updateCareOf(pOutbuf, acName2, strlen(acName2));
      *pName2 = 0;
   } else if (pTmp = strstr(acName2, "C/O"))
   {
      updateCareOf(pOutbuf, acName2, strlen(acName2));
      *pName2 = 0;
   } else if (!memcmp(acName2, "ATTN", 4))
   {
      updateCareOf(pOutbuf, acName2, strlen(acName2));
      *pName2 = 0;
   } else if (strchr(acName2, '&'))
   {

      // Process as two names
      strcpy(acOwners, acName1);
      pName1 = acOwners;

      if (pTmp=strstr(pName1, " 1/"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " - "))
        *pTmp = 0;

      if (pTmp=strstr(pName1, " ETUX"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ET UX"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ETAL"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ET AL"))
        *pTmp = 0;

      if (pTmp=strstr(pName1, " TSTEE"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " SUCC  TR"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TRUSTE"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TRS"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TSTES"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " TR "))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " CO-TR"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " COTRST"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " ESTATE OF"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " LIFE ESTATE"))
        *pTmp = 0;
      else if (pTmp=strstr(pName1, " M D"))
        *pTmp = 0;

      // Check for vesting
      pTmp1 = strrchr(acOwners, ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_ALP(acSave, acTmp1);
         if (*pTmp)
         {
            //strcpy(pOwners->acVest, acTmp1);
            memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
            *pTmp1 = 0;
         }
      }
      if (pTmp = strstr(acOwners, " ALL AS"))
         *pTmp = 0;

      // Parse owner 1
      splitOwner(acOwners, &myOwner);
      memcpy(pOutbuf+OFF_NAME1, acName1, strlen(acName1));
      memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, strlen(myOwner.acSwapName));

      // Process Name2
      if (*pName2 == '&')
         memcpy(acOwners, pName2+2, CRESIZ_NAME_1-2);
      else
         memcpy(acOwners, pName2, CRESIZ_NAME_1);
      acOwners[CRESIZ_NAME_1] = 0;

      // Check for vesting
      pTmp1 = strrchr(myTrim(acOwners), ' ');
      if (pTmp1)
      {
         strcpy(acSave, ++pTmp1);
         pTmp = getVestingCode_ALP(acSave, acTmp1);
         if (*pTmp)
         {
            memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
            *pTmp1 = 0;
         }
      }

      // Parse Owner 2
      splitOwner(acOwners, &myOwner);
      memcpy(pOutbuf+OFF_NAME2, myOwner.acName1, strlen(myOwner.acName1));
      return;
   } else
   {
      pName2 = strchr(acName2, ' ');
      pTmp = acName2;
      if (pName2)
      {
         pTmp = strchr(pName2+1, ' ');
         if (pTmp) *pTmp = 0;
      }
      // Ignore name2 if it is duplicate of name1 last and first names
      if (memcmp(acName1, acName2, strlen(acName2)) )
      {
         if (pTmp && !*pTmp) *pTmp = ' ';
         memcpy(pOutbuf+OFF_NAME2, acName2, strlen(acName2));
      }
   }
   // Remove multiple spaces
//   memcpy(acOwners, pName1, CRESIZ_NAME_1+CRESIZ_NAME_2);
//   acOwners[CRESIZ_NAME_1+CRESIZ_NAME_2] = 0;
   pTmp = acName1;

   iTmp = 0;
   while (*pTmp)
   {
      acTmp[iTmp++] = *pTmp;
      if (*pTmp == ' ')
         while (*pTmp && *pTmp == ' ') pTmp++;
      else
         pTmp++;

      // Remove '.' too
      if (*pTmp == '.' || *pTmp == '\'')
         pTmp++;
   }
   if (acTmp[iTmp-1] == ' ')
      iTmp -= 1;
   acTmp[iTmp] = 0;
   strcpy(acName1, acTmp);

   // Check for AKA - replace it with '&'
   pTmp = strstr(acTmp, " AKA ");
   if (pTmp)
   {
      *(pTmp+1) = '&';
      *(pTmp+2) = ' ';
      *(pTmp+3) = ' ';
   }


   if (pTmp=strstr(acTmp, " 1/"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " - "))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " ETUX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET UX"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ETAL"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " ET AL"))
      *pTmp = 0;

   if (pTmp=strstr(acTmp, " TSTEE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " SUCC  TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRUSTE"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TRS"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TSTES"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " TR "))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " CO-TR"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " COTRST"))
      *pTmp = 0;
   else if (pTmp=strstr(acTmp, " M D"))
      *pTmp = 0;

   // Blank out "SUC TRS" and "CO TRS"
   if (pTmp = strstr(acTmp, " SUC TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " CO TRS"))
      *pTmp = 0;
   else if (pTmp = strstr(acTmp, " DVA"))
      *pTmp = 0;

  // Check for vesting
   pTmp1 = strrchr(acTmp, ' ');
   if (pTmp1)
   {
      strcpy(acSave, ++pTmp1);
      pTmp = getVestingCode_ALP(acSave, acTmp1);
      if (*pTmp)
      {
         memcpy(pOutbuf+OFF_VEST, acTmp1, strlen(acTmp1));
         *pTmp1 = 0;
      } else
      {
         if (pTmp1 = strstr(acTmp, " JT"))
         {
            memcpy(pOutbuf+OFF_VEST, "JT", 2);
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " TC"))
         {
            memcpy(pOutbuf+OFF_VEST, "TC", 2);
            *pTmp1 = 0;
         } else if (pTmp1 = strstr(acTmp, " CP"))
         {
            memcpy(pOutbuf+OFF_VEST, "CP", 2);
            *pTmp1 = 0;
         }
      }
   }

   // Save data to append later   
   if ((pTmp=strstr(acTmp, " FAM TRUST")) || 
       (pTmp=strstr(acTmp, " ESTATE OF")) ||
       (pTmp=strstr(acTmp, " LIFE ESTATE")) )
      *pTmp = 0;

   // Now parse owners
   splitOwner(acTmp, &myOwner, 3);

   iTmp = strlen(myOwner.acSwapName);
   if (iTmp > SIZ_NAME_SWAP)
      iTmp = SIZ_NAME_SWAP;
   memcpy(pOutbuf+OFF_NAME_SWAP, myOwner.acSwapName, iTmp);

   memcpy(pOutbuf+OFF_NAME1, acName1, strlen(acName1));
}

/********************************* Alp_MergeAdr ******************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

void Alp_MergeAdr(char *pOutbuf, char *pRollRec)
{
   REDIFILE *pRec;
   char     *pTmp, *pAddr1, acTmp[256], acAddr1[64], acAddr2[64], acChar;
   int      iTmp, iStrNo;

   pRec   = (REDIFILE *)pRollRec;
   pAddr1 = acAddr1;

   // Clear old addr
   removeMailing(pOutbuf, false);

   // Check for blank address
   if (!memcmp(pRec->M_Addr1, "     ", 5))
      return;

#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0030700010", 10))
   //   iTmp = 0;
#endif

   memcpy(acAddr1, pRec->M_Addr1, CRESIZ_ADDR_1);
   blankRem(acAddr1, CRESIZ_ADDR_1);

   // Test & Remove apos
   iTmp = remChar(acAddr1, 39);

   if (*pAddr1 == '%' || !_memicmp(pAddr1, "C/O", 3))
   {
      if (*pAddr1 == '%')
         pAddr1 += 2;
      else
         pAddr1 += 4;

      // Check for C/O name
      pTmp = strchr(pAddr1, ',');
      if (pTmp && !isdigit(*(pTmp-1)) && *(pOutbuf+OFF_CARE_OF) == ' ')
      {
         *pTmp = 0;
         pAddr1 = pTmp + 1;
         if (*pAddr1 == ' ') 
            pAddr1++;
         iTmp = strlen(pAddr1);
         if (iTmp > SIZ_CARE_OF)
            iTmp = SIZ_CARE_OF;
         memcpy(pOutbuf+OFF_CARE_OF, pAddr1, iTmp);
      }
   }

   // Start processing
   memcpy(pOutbuf+OFF_M_ADDR_D, pAddr1, strlen(pAddr1));
   memcpy(acAddr2, pRec->M_Addr2, CRESIZ_ADDR_2);

   // Standardize known spelling
   if (pTmp = strstr(acAddr2, " CALIF "))
      strcpy(pTmp, ", CA   ");

   blankRem(acAddr2, CRESIZ_ADDR_2);
   memcpy(pOutbuf+OFF_M_CTY_ST_D, acAddr2, strlen(acAddr2));

   iTmp = atoin(pRec->M_Zip, 5);
   if (iTmp > 400)
   {
      memcpy(pOutbuf+OFF_M_ZIP, pRec->M_Zip, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, pRec->M_Zip4, SIZ_M_ZIP4);
   } else
   {
      memcpy(pOutbuf+OFF_M_ZIP, BLANK32, SIZ_M_ZIP);
      memcpy(pOutbuf+OFF_M_ZIP4, BLANK32, SIZ_M_ZIP4);
   }

   // Parsing mail address
   ADR_REC sMailAdr;
   parseAdr1_1(&sMailAdr, pAddr1);
   parseAdr2(&sMailAdr, acAddr2);

   if (sMailAdr.lStrNum > 0)
   {
      if (pTmp = strstr(sMailAdr.strNum, "1/2"))
      {
         memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, SIZ_M_STR_SUB);
         *pTmp = 0;
         sMailAdr.lStrNum = atoi(sMailAdr.strNum);
         iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);
      } else 
      {
         iTmp = sprintf(acAddr2, "%d", sMailAdr.lStrNum);
         memcpy(pOutbuf+OFF_M_STRNUM, acAddr2, iTmp);

         if (sMailAdr.strSub[0] > '0')
         {
            sprintf(acTmp, "%s  ", sMailAdr.strSub);
            memcpy(pOutbuf+OFF_M_STR_SUB, acTmp, SIZ_M_STR_SUB);
         } else
         {
            acChar = isChar(sMailAdr.strNum, SIZ_M_STRNUM);
            if (acChar > ' ')
            {
               if (pTmp = strchr(sMailAdr.strNum, acChar))
                  memcpy(pOutbuf+OFF_M_STR_SUB, pTmp, strlen(pTmp));
            }
         }
      }

   }
   blankPad(sMailAdr.strDir, SIZ_M_DIR);
   memcpy(pOutbuf+OFF_M_DIR, sMailAdr.strDir, SIZ_M_DIR);
   blankPad(sMailAdr.strName, SIZ_M_STREET);
   memcpy(pOutbuf+OFF_M_STREET, sMailAdr.strName, SIZ_M_STREET);
   blankPad(sMailAdr.strSfx, SIZ_M_SUFF);
   memcpy(pOutbuf+OFF_M_SUFF, sMailAdr.strSfx, SIZ_M_SUFF);
   blankPad(sMailAdr.City, SIZ_M_CITY);
   memcpy(pOutbuf+OFF_M_CITY, sMailAdr.City, SIZ_M_CITY);
   blankPad(sMailAdr.State, SIZ_M_ST);
   memcpy(pOutbuf+OFF_M_ST, sMailAdr.State, SIZ_M_ST);
   blankPad(sMailAdr.Unit,SIZ_M_UNITNO);
   memcpy(pOutbuf+OFF_M_UNITNO, sMailAdr.Unit,SIZ_M_UNITNO);

   // Situs
   // ALP store situs address in city name field
   // 150,160,170,&180 DOUGLAS WAY
   // 14856,14866,14880 HIGHWAY 89
   // 190&200 DOUGLAS WAY
   // 130 & 140 DOUGLAS WAY
   // 105 MERK CREEK CT.
   // 9.5ACS TPZ
   // 115 ACS TPZ
   // 19750 / 19746 HWY. 89
   // 50% INTEREST 
   // (702)831-4900
   // 50 CREEKSIDE DRIVE # 37
   // 95 MULE-EAR RD.
   // 39 NO NAME ROAD
   // 60 HIGHWAY 88/89

   // Situs addr is stored in two different places: 
   ADR_REC sSitusAdr;
   iStrNo = atoin(pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
   if (iStrNo > 0)
   {
      // Clear old situs 
      removeSitus(pOutbuf);

      memcpy(pOutbuf+OFF_S_ADDR_D, pRec->S_City, CRESIZ_SITUS_CITY);

      iTmp = sprintf(acTmp, "%d", iStrNo);
      memcpy(pOutbuf+OFF_S_STRNUM, acTmp, iTmp);
      memcpy(pOutbuf+OFF_S_HSENO, pRec->S_StrNo, CRESIZ_SITUS_STREETNO);
      memcpy(acTmp, pRec->S_StrName, CRESIZ_SITUS_STREETNAME);
      acTmp[CRESIZ_SITUS_STREETNAME] = 0;
      parseAdr1C(&sSitusAdr, acTmp);

      if (sSitusAdr.strSub[0] > ' ')
         memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
      if (sSitusAdr.strDir[0] > ' ')
         memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
      if (sSitusAdr.strName[0] > ' ')
         memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
      if (sSitusAdr.strSfx[0] > ' ')
         memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
      if (sSitusAdr.Unit[0] > ' ')
         memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
   } else
   {
      memcpy(acAddr1, pRec->S_City, CRESIZ_SITUS_CITY);
      acAddr1[CRESIZ_SITUS_CITY] = 0;
      iStrNo = atoi(acAddr1);
      if (iStrNo > 0)
      {
         int   iIdx=0;
         char  *pTmp1;

         memcpy(pOutbuf+OFF_S_ADDR_D, pRec->S_City, CRESIZ_SITUS_CITY);

         sprintf(acTmp, "%d       ", iStrNo);
         memcpy(pOutbuf+OFF_S_STRNUM, acTmp, SIZ_S_STRNUM);

         while (acAddr1[iIdx] <= '9' && acAddr1[iIdx] >= ' ')
            iIdx++;
         iIdx--;
         if (iIdx > SIZ_S_HSENO)
            iIdx = SIZ_S_HSENO;
         memcpy(pOutbuf+OFF_S_HSENO, acAddr1, iIdx);

         // In case there is more than one strNum, keep first one only
         if (strchr(acAddr1, ',') || strchr(acAddr1, '&') )
         {
            pTmp = strrchr(acAddr1, '&');
            if (pTmp)
            {
               // Looking for last number
               pTmp1 = strchr(pTmp+2, ' ');
               if (pTmp1)
                  sprintf(acAddr1, "%d%s", iStrNo, pTmp1);
            } else if (pTmp=strrchr(acAddr1, ','))
            {
               // Looking for last number
               pTmp1 = strchr(pTmp+2, ' ');
               if (pTmp1)
                  sprintf(acAddr1, "%d%s", iStrNo, pTmp1);
            }
         }
         parseAdr1C(&sSitusAdr, acAddr1);

         if (sSitusAdr.strSub[0] > ' ')
            memcpy(pOutbuf+OFF_S_STR_SUB, sSitusAdr.strSub, strlen(sSitusAdr.strSub));
         if (sSitusAdr.strDir[0] > ' ')
            memcpy(pOutbuf+OFF_S_DIR, sSitusAdr.strDir, strlen(sSitusAdr.strDir));
         if (sSitusAdr.strName[0] > ' ')
            memcpy(pOutbuf+OFF_S_STREET, sSitusAdr.strName, strlen(sSitusAdr.strName));
         if (sSitusAdr.strSfx[0] > ' ')
            memcpy(pOutbuf+OFF_S_SUFF, sSitusAdr.strSfx, strlen(sSitusAdr.strSfx));
         if (sSitusAdr.Unit[0] > ' ')
            memcpy(pOutbuf+OFF_S_UNITNO, sSitusAdr.Unit, strlen(sSitusAdr.Unit));
      }
   }

   // Assuming that no situs city and we have to use mail city
   acAddr2[0] = 0;
   if (!strcmp(myTrim(sMailAdr.strName), myTrim(sSitusAdr.strName)) && sMailAdr.City[0] > ' ')
   {
      City2Code(sMailAdr.City, acTmp);
      if (acTmp[0] > ' ')
      {
         memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
         strcpy(acAddr2, sMailAdr.City);
      }
      memcpy(pOutbuf+OFF_S_ZIP, pOutbuf+OFF_M_ZIP, SIZ_S_ZIP+SIZ_S_ZIP4);
   } else
   {
      // Condition: Valid city name, Ex_Val is 7000,
      if ((sMailAdr.City[0] >= 'A') && (pRec->M_Zip[0] > '8') && (sMailAdr.lStrNum > 0) &&
          (iStrNo == sMailAdr.lStrNum) && (*(pOutbuf+OFF_HO_FL) == '1') )
      {
         // Get city code
         City2Code(sMailAdr.City, acTmp);
         if (acTmp[0] > ' ')
         {
            memcpy(pOutbuf+OFF_S_CITY, acTmp, strlen(acTmp));
            memcpy(pOutbuf+OFF_S_ZIP, pRec->M_Zip, 5);
            strcpy(acAddr2, sMailAdr.City);
         }
      }
   }

   memcpy(pOutbuf+OFF_S_ST, "CA", 2);
   if (acAddr2[0])
   {
      strcat(myTrim(acAddr2), " CA");
      iTmp = strlen(acAddr2);
      if (iTmp > SIZ_S_CTY_ST_D)
         iTmp = SIZ_S_CTY_ST_D;
      memcpy(pOutbuf+OFF_S_CTY_ST_D, acAddr2, iTmp);
   }
}

/******************************** Alp_CreateGrGr *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

int Alp_CreateGrGr(char *pCnty)
{
   return 0;
}

/********************************** CreateR01Rec *****************************
 *
 * Return 0 if successful, >0 if error
 *
 *****************************************************************************/

#define  _NO_CHARS
int Alp_CreateR01(char *pOutbuf, char *pRollRec, int iLen, int iFlag)
{
   REDIFILE *pRec;
   char     acTmp[256], acTmp1[256];
   long     lTmp;
   int      iRet, iTmp;
   double   dTmp;

   // Init input
   pRec = (REDIFILE *)pRollRec;

   // Init output buffer
   if (iFlag & CREATE_R01)
   {
      // Clear output buffer
      memset(pOutbuf, ' ', iRecLen);

      // APN
      memcpy(pOutbuf+OFF_APN_S, pRec->APN, CRESIZ_APN);
      memcpy(pOutbuf+OFF_CO_NUM, "02ALP", 5);

      // Create APN_D new record
      iTmp = formatApn(pRec->APN, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_APN_D, acTmp, iTmp);

      // Create MapLink and output new record
      iTmp = formatMapLink(pRec->APN, acTmp, &myCounty);
      memcpy(pOutbuf+OFF_MAPLINK, acTmp, iTmp);

      // Create index map link
      getIndexPage(acTmp, acTmp1, &myCounty);
      memcpy(pOutbuf+OFF_IMAPLINK, acTmp1, iTmp);

      // Assessment year
      memcpy(pOutbuf+OFF_YR_ASSD, myCounty.acYearAssd, 4);

      // Land
      long lLand = atoin(pRec->LandVal, CRESIZ_LAND_VAL);
      if (lLand > 0)
      {
         sprintf(acTmp, "%*d", SIZ_LAND, lLand);
         memcpy(pOutbuf+OFF_LAND, acTmp, SIZ_LAND);
      }

      // Improve
      long lImpr = atoin(pRec->ImprVal, CRESIZ_IMP_VAL);
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_IMPR, lImpr);
         memcpy(pOutbuf+OFF_IMPR, acTmp, SIZ_IMPR);
      }

      // FixtVal is the sum of PersFixtVal, LeaseVal, and MobileVal
      /*
      long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);
      lTmp = lPers + lFixture;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lFixture > 0)
         {
            sprintf(acTmp, "%d         ", lFixture);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
      }
      */
      long lLeaseVal = atoin(pRec->LeaseVal, CRESIZ_LEASE_VAL);
      long lPFixt = atoin(pRec->PersFixtVal, CRESIZ_PERS_FIXT_VAL);
      long lPP_MH = atoin(pRec->MobileVal, CRESIZ_MOBILE_VAL);
      // FixtVal is the sum of PFixt, LeaseVal, and PP_MH
      //long lFixture = atoin(pRec->FixtVal, CRESIZ_FIXT_VAL);
      long lPers  = atoin(pRec->PPVal, CRESIZ_PP_VAL);

      lTmp = lPers + lLeaseVal + lPFixt + lPP_MH;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_OTHER, lTmp);
         memcpy(pOutbuf+OFF_OTHER, acTmp, SIZ_OTHER);

         if (lPers > 0)
         {
            sprintf(acTmp, "%d         ", lPers);
            memcpy(pOutbuf+OFF_PERSPROP, acTmp, SIZ_PERSPROP);
         }
         if (lPFixt > 0)
         {
            sprintf(acTmp, "%d         ", lPFixt);
            memcpy(pOutbuf+OFF_FIXTR, acTmp, SIZ_FIXTR);
         }
         if (lPP_MH > 0)
         {
            sprintf(acTmp, "%d         ", lPP_MH);
            memcpy(pOutbuf+OFF_PP_MH, acTmp, SIZ_PP_MH);
         }
         if (lLeaseVal > 0)
         {
            sprintf(acTmp, "%d         ", lLeaseVal);
            memcpy(pOutbuf+OFF_GR_IMPR, acTmp, SIZ_GR_IMPR);
         }
      }

      // Gross total
      lTmp += lLand+lImpr;
      if (lTmp > 0)
      {
         sprintf(acTmp, "%*d", SIZ_GROSS, lTmp);
         memcpy(pOutbuf+OFF_GROSS, acTmp, SIZ_GROSS);
      }

      // Ratio
      if (lImpr > 0)
      {
         sprintf(acTmp, "%*d", SIZ_RATIO, lImpr*100/(lLand+lImpr));
         memcpy(pOutbuf+OFF_RATIO, acTmp, SIZ_RATIO);
      }
   }

   iRet = 0;

   // TRA
   memcpy(pOutbuf+OFF_TRA, pRec->TRA, CRESIZ_TRA);

   // Parcel Status
   pRec->ParcType[CRESIZ_PARCTYPE] = 0;
   getParcType(pOutbuf+OFF_STATUS,pOutbuf+OFF_TIMBER,pOutbuf+OFF_AG_PRE,pRec->ParcType,"ALP");

   // UseCode
   memcpy(pOutbuf+OFF_USE_CO, pRec->UseCode, CRESIZ_USE_CODE);

   // Std Usecode
   updateStdUse(pOutbuf+OFF_USE_STD, pRec->UseCode, CRESIZ_USE_CODE, pOutbuf);

   // Acres
   memcpy(acTmp, pRec->Acres, CRESIZ_ACRES);
   acTmp[CRESIZ_ACRES] = 0;
   dTmp = atof(acTmp);
   if (dTmp > 0.0)
   {
      lTmp = (long)(dTmp * SQFT_PER_ACRE);
      dTmp *= 1000;
      sprintf(acTmp, "%*u", SIZ_LOT_ACRES, (long)dTmp);
   } else
   {
      lTmp = 0;
      strcpy(acTmp, BLANK32);
   }
   memcpy(pOutbuf+OFF_LOT_ACRES, acTmp, SIZ_LOT_ACRES);

   // Lot Sqft
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_LOT_SQFT, lTmp);
      memcpy(pOutbuf+OFF_LOT_SQFT, acTmp, SIZ_LOT_SQFT);
   }

#ifdef _NO_CHARS   
   memset(pOutbuf+OFF_BLDG_SF, ' ', SIZ_BLDG_SF);
#else
   // Bldg Sqft
   lTmp = atoin(pRec->StruSqft, CRESIZ_STRUCT_SQFT);
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_BLDG_SF, lTmp);
      memcpy(pOutbuf+OFF_BLDG_SF, acTmp, SIZ_BLDG_SF);
   }
#endif
   
#ifdef _DEBUG
   //if (!memcmp(pOutbuf, "0022300380", 10))
   //   iTmp = 0;
#endif
   // Recording info
   /*
   lTmp = atoin(pRec->RecBook1, 8);
   if (pRec->RecBook1[0] > ' ' || lTmp > 0)
   {
      memset(pOutbuf+OFF_SALE1_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_TRANSFER_DT, ' ', SIZ_SALE1_DT);
      memset(pOutbuf+OFF_SALE1_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_TRANSFER_DOC, ' ', SIZ_SALE1_DOC);
      memset(pOutbuf+OFF_SALE2_DT, ' ', SIZ_SALE2_DT);
      memset(pOutbuf+OFF_SALE2_DOC, ' ', SIZ_SALE2_DOC);
      memset(pOutbuf+OFF_SALE3_DT, ' ', SIZ_SALE3_DT);
      memset(pOutbuf+OFF_SALE3_DOC, ' ', SIZ_SALE3_DOC);
      Alp_FmtRecDoc(pOutbuf, pRollRec);
   }
   */

   // Owners
   Alp_MergeOwner(pOutbuf, pRec->Name1);

   // Mailing & Situs address
   Alp_MergeAdr(pOutbuf, pRollRec);

#ifdef _NO_CHARS   
   memset(pOutbuf+OFF_YR_BLT, ' ', SIZ_YR_BLT);
   memset(pOutbuf+OFF_YR_EFF, ' ', SIZ_YR_BLT);
#else

   // Base year
   iTmp = atoin(pRec->YrBlt, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_BLT, pRec->YrBlt, CRESIZ_YR_BUILT);
   iTmp = atoin(pRec->YrEff, 4);
   if (iTmp > 1900)
      memcpy(pOutbuf+OFF_YR_EFF, pRec->YrEff, CRESIZ_EFF_YR);
#endif

   // HO Exempt
   lTmp = atoin(pRec->ExVal, CRESIZ_EX_VAL);
   if (!memcmp(pRec->ExeCode1, "10", 2) && lTmp > 0)
      *(pOutbuf+OFF_HO_FL) = '1';      // 'Y'
   else
      *(pOutbuf+OFF_HO_FL) = '2';      // 'N'

   // Exemp total
   if (lTmp > 0)
   {
      sprintf(acTmp, "%*d", SIZ_EXE_TOTAL, lTmp);
      memcpy(pOutbuf+OFF_EXE_TOTAL, acTmp, SIZ_EXE_TOTAL);
   } else
      memset(pOutbuf+OFF_EXE_TOTAL, ' ', SIZ_EXE_TOTAL);

#ifdef _NO_CHARS   
   memset(pOutbuf+OFF_BEDS, ' ', SIZ_BEDS+SIZ_BATH_F+SIZ_BATH_H+SIZ_ROOMS);
#else
   // Rooms
   iTmp = atoin(pRec->Rooms, SIZ_ROOMS);
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_ROOMS, iTmp);
      memcpy(pOutbuf+OFF_ROOMS, acTmp, SIZ_ROOMS);
   }

   // Beds
   iTmp = pRec->Beds[0] & 0x0F;
   if (iTmp > 0)
   {
      sprintf(acTmp, "%*u", SIZ_BEDS, iTmp);
      memcpy(pOutbuf+OFF_BEDS, acTmp, SIZ_BEDS);
   }

   // Baths/Half Baths
   if (pRec->Baths[0] > '0')
   {
      *(pOutbuf+OFF_BATH_F) = ' ';
      *(pOutbuf+OFF_BATH_F+1) = pRec->Baths[0];
   }
   if (pRec->Baths[1] == '5' )
   {
      *(pOutbuf+OFF_BATH_H) = ' ';
      *(pOutbuf+OFF_BATH_H+1) = '1';        // Translate .5 to one half bath
   }
#endif

   // Legal Desc
   if (pRec->LglDesc1[0] > ' ')
   {
      memcpy(acTmp, pRec->LglDesc1, CRESIZ_LGL_DESC1);
      acTmp[CRESIZ_LGL_DESC1] = 0;
      if (pRec->LglDesc2[0] > ' ')
      {
         myTrim(acTmp);
         pRec->LglDesc2[CRESIZ_LGL_DESC2-1] = 0;
         strcat(acTmp, pRec->LglDesc2);
      }

      updateLegal(pOutbuf, acTmp);
   }

#ifdef _NO_CHARS   
   memset(pOutbuf+OFF_STORIES, ' ', SIZ_STORIES);
   memset(pOutbuf+OFF_PARK_SPACE, ' ', SIZ_PARK_SPACE);
   memset(pOutbuf+OFF_UNITS, ' ', SIZ_UNITS);
   memset(pOutbuf+OFF_BLDG_CLASS, ' ', SIZ_BLDG_CLASS+SIZ_BLDG_QUAL);
#else
   
   // FL - # of floors  or stories
   if (pRec->Fl[0] > '0' && pRec->Fl[0] <= '9')
   {
      sprintf(acTmp, "%c.0 ", pRec->Fl[0]);
      memcpy(pOutbuf+OFF_STORIES, acTmp, 4);
   }

   // Garage
   if (pRec->Garage[0] > '0' && pRec->Garage[0] <= '9')
      *(pOutbuf+OFF_PARK_SPACE) = pRec->Garage[0];

   // #Units
   if (pRec->NumOfUnits[0] > '0' )
   {
      iTmp = atoin(pRec->NumOfUnits, CRESIZ_UNITS);
      sprintf(acTmp, "%*u", SIZ_UNITS, iTmp);
      memcpy(pOutbuf+OFF_UNITS, acTmp, SIZ_UNITS);
   }

   // Bldg Class
   acTmp[0] = 0;
   iTmp = 0;
   if (pRec->BldgCls[0] >= 'A')
   {
      *(pOutbuf+OFF_BLDG_CLASS) = pRec->BldgCls[0];
      if (isdigit(pRec->BldgCls[1]))
      {
         acTmp[iTmp++] = pRec->BldgCls[1];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else if (isdigit(pRec->BldgCls[3]))
            acTmp[iTmp++] = pRec->BldgCls[3];
         else
            acTmp[iTmp++] = '0';
      }
   } else if (isdigit(pRec->BldgCls[0]))
   {
         acTmp[iTmp++] = pRec->BldgCls[0];
         acTmp[iTmp++] = '.';
         if (isdigit(pRec->BldgCls[1]))
            acTmp[iTmp++] = pRec->BldgCls[1];
         else if (isdigit(pRec->BldgCls[2]))
            acTmp[iTmp++] = pRec->BldgCls[2];
         else
            acTmp[iTmp++] = '0';
   }
   acTmp[iTmp] = 0;

   // Bldg Quality
   if (iTmp > 0)
   {
      iTmp = Value2Code(acTmp, acCode, NULL);
      blankPad(acCode, SIZ_BLDG_QUAL);
      memcpy(pOutbuf+OFF_BLDG_QUAL, acCode, SIZ_BLDG_QUAL);
   }
#endif

   // FP - 1

   // AC - 1

   // Heating

   // Pool /Spa

   // Zoning

   // Neighborhood

   // Type_Sqft - M, C

   return iRet;
}

/********************************** Alp_LoadRoll ****************************
 *
 * This function will create new record using roll file.  Then merge lien data
 * from lien extract file.
 *
 ****************************************************************************/

int Alp_LoadRoll(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acLienExt[_MAX_PATH];

   HANDLE   fhOut;

   int      iRet, iTmp, iRollUpd=0, iNewRec=0, iRetiredRec=0;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open lien extract file
   sprintf(acLienExt, acLienTmpl, pCnty, pCnty);
   LogMsg("Open lien extract file %s", acLienExt);
   fdLienExt = fopen(acLienExt, "r");
   if (fdLienExt == NULL)
   {
      LogMsg("***** Error opening lien extract file: %s\n", acLienExt);
      return 2;
   }

   // Open Output file
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");
   if (!_access(acOutFile, 0))
   {
      sprintf(acBuf, acRawTmpl, pCnty, pCnty, "S01");
      if (_access(acBuf, 0))
         rename(acOutFile, acBuf);
   }

   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   if (iSkip > 0)
   {
      memset(acBuf, '9', iRecLen);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      iSkip = 0;
      if (getParcStatus_ALP(acRollRec) != 'R')
      {
         iRet = Alp_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
         if (iRet)
            break;

         if (fdLienExt)
            iRet = PQ_MergeLien(acBuf, fdLienExt);

         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      } else
         iRetiredRec++;

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdLienExt)
      fclose(fdLienExt);
   if (fhOut)
      CloseHandle(fhOut);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("      retired records:      %u", iRetiredRec);
   LogMsg("      bad-city records:     %u", iBadCity);
   LogMsg("      bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u\n", lCnt);
   return 0;
}

/********************************** Alp_Load_LDR ****************************
 *
 * This function will create new record using lien date roll.
 *
 ****************************************************************************/

int Alp_Load_LDR(char *pCnty)
{
   char     acBuf[MAX_RECSIZE], acRollRec[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acTmp[256];

   HANDLE   fhOut;
   FILE     *fdRoll;

   int      iRet, iTmp;
   DWORD    nBytesWritten;
   BOOL     bRet, bEof;
   long     lRet=0, lCnt=0;

   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "R01");

   // Open Roll file
   LogMsg("Open Roll file %s", acRollFile);
   fdRoll = fopen(acRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", acRollFile);
      return 2;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return 4;
   }

   // Write first record
   memset(acBuf, '9', iRecLen);
   bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);

   // Init buffer
   memset(acBuf, ' ', iRecLen);
   iNoMatch=iBadCity=iBadSuffix=0;

   // Merge loop
   while (!bEof)
   {
      lLDRRecCount++;
      iRet = Alp_CreateR01(acBuf, acRollRec, iRollLen, CREATE_R01);
      if (!iRet)
      {
         // Get last recording date
         iTmp = atoin((char *)&acBuf[OFF_TRANSFER_DT], SIZ_TRANSFER_DT);
         if (iTmp >= lToday)
            LogMsg("*** WARNING: Bad last recording date %d at record# %d -->%.14s", iTmp, lCnt, acBuf);
         else if (lLastRecDate < iTmp)
            lLastRecDate = iTmp;

         bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
         lCnt++;
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      // Scan for bad character - LAS May 05
      while (iRet-- > 0)
         if (acRollRec[iRet] < ' ')
            acRollRec[iRet] = ' ';

      if (!(lCnt % 1000))
         printf("\r%u", lCnt);

      if (!bRet)
      {
         LogMsg("Error occurs: %d\n", GetLastError());
         lRet = WRITE_ERR;
         break;
      }
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fhOut)
      CloseHandle(fhOut);

   // Create flag file
   if (bEof)
   {
      // Only create flag when template is defined
      if (acFlgTmpl[0] >= 'C')
      {
         sprintf(acTmp, acFlgTmpl, pCnty, pCnty);
         fdRoll = fopen(acTmp, "w");
         fclose(fdRoll);
      }
   }

   LogMsg("Total input records:        %u", lLDRRecCount);
   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Total bad-city records:     %u", iBadCity);
   LogMsg("Total bad-suffix records:   %u", iBadSuffix);

   lRecCnt = lCnt;
   printf("\nTotal output records: %u", lCnt);
   return 0;
}

/******************************* Alp_ExtrSale *******************************
 *
 * Extract all sale from redifile and update ???_sale.sls. 
 * Return 0 if successful.
 *
 ****************************************************************************/

int Alp_ExtrSale(char *pRollFile, char *pOutFile, bool bAppend)
{
   char     acBuf[1024], acRollRec[1024], *pTmp;
   char     acOutFile[_MAX_PATH], acTmp[256], acDate[16], acDoc[64];

   int      iRet;
   double   dTmp;
   long     lCnt=0, lNewRec=0, lPrice;
   boolean  bEof;

   REDIFILE  *pRoll;
   SCSAL_REC *pSale;

   LogMsg("\nExtract sales from roll file %s", pRollFile);

   // Open Roll file
   LogMsg("Open Roll file %s", pRollFile);
   fdRoll = fopen(pRollFile, "rb");
   if (fdRoll == NULL)
   {
      LogMsg("***** Error opening roll file: %s\n", pRollFile);
      return -2;
   }

   // Check output file
   if (pOutFile)
      strcpy(acOutFile, pOutFile);
   else 
      sprintf(acOutFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "tmp");

   // Open old cum sale file
   LogMsg("Create sale file %s", acOutFile);
   fdCSale = fopen(acOutFile, "w");
   if (fdCSale == NULL)
   {
      LogMsg("***** Error opening cum sale file: %s\n", acOutFile);
      return -2;
   }

   // Get first RollRec
   iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
   bEof = (iRet==iRollLen ? false:true);
   pRoll= (REDIFILE *)acRollRec;
   pSale= (SCSAL_REC *)acBuf;
   pSale->CRLF[0] = 10;
   pSale->CRLF[1] = 0;

   // Extract loop
   while (!bEof)
   {

#ifdef _DEBUG
//      if (!memcmp(acBuf, "0010113600", 10))
//         iRet = 0;
#endif

      memset(acBuf, ' ', sizeof(SCSAL_REC)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Alp_FmtDoc(acDoc, acDate, pRoll->RecBook1);
      if (acDate[0] > '0')
      {
         memcpy(pSale->Name1, pRoll->Name1, CRESIZ_NAME_1);
         memcpy(pSale->Name2, pRoll->Name2, CRESIZ_NAME_2);
         memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
         memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

         memcpy(acTmp, pRoll->StampAmt, CRESIZ_STMP_AMT);
         acTmp[CRESIZ_STMP_AMT] = 0;
         remChar(acTmp, ',');
         dTmp = atof(acTmp);
         if (dTmp > 0.0)
         {
            blankRem(acTmp);
            memcpy(pSale->StampAmt, acTmp, strlen(acTmp));
            // Keep this check here until counties correct their roll
            pTmp = strchr(acTmp, '.');
            if (!pTmp || *(pTmp+1) == ' ' || dTmp > 100000.00)
            {
               if (acTmp[0] == '0' || dTmp < 2000.00)
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if ((lPrice % 100) == 0)
                  {
                     sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                  } else
                  {
                     LogMsg0("??? Suspected stamp amount for APN: %.10s, StampAmt=%s", acBuf, acTmp);
                     memset(acTmp, ' ', SIZ_SALE1_AMT);
                  }
               } else
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
            } else
            {
               if (dTmp > 9999.00 && ((long)dTmp % 100) == 0)
               {
                  sprintf(acTmp, "%*d", SIZ_SALE1_AMT, (long)dTmp);
               } else
               {
                  lPrice = (long)(dTmp * SALE_FACTOR);
                  if (lPrice > 0)
                  {
                     if (lPrice < 100 || (lPrice % 100) == 0)
                        sprintf(acTmp, "%*d", SIZ_SALE1_AMT, lPrice);
                     else
                     {
                        LogMsg0("*** Questionable sale price on APN=%.10s, SalePrice= %d, StampAmt=%.*s", acBuf, lPrice, CRESIZ_STMP_AMT, pRoll->StampAmt);
                        memset(acTmp, ' ', SIZ_SALE1_AMT);
                     }
                  }
               }
            }
            memcpy(pSale->SalePrice, acTmp, SIZ_SALE1_AMT);
         }

         // Multi-APN?
         if (!memcmp(pRoll->ReappCode, "TM", 2))
            pSale->MultiSale_Flg = 'Y';

         // Full/Partial
         if (!memcmp(pRoll->ReappPct, "100", 3))
         {
            pSale->SaleCode[0] = 'F';
            memcpy(pSale->PctXfer, "100", 3);
         } else
         {
            iRet = atoin(pRoll->ReappPct, CRESIZ_REAPP_PCT);
            if (iRet > 0)
            {
               pSale->SaleCode[0] = 'P';
               iRet = sprintf(acTmp, "%d", (iRet+5)/10);
               memcpy(pSale->PctXfer, acTmp, iRet);
            }
         }

         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            if (lLastRecDate < iRet)
               lLastRecDate = iRet;

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(1) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 2
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Alp_FmtDoc(acDoc, acDate, pRoll->RecBook2);
      if (acDate[0] > '0')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(2) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Sale 3
      memset(acBuf, ' ', sizeof(SALE_REC1)-2);
      memcpy(pSale->Apn, pRoll->APN, CRESIZ_APN);
      Alp_FmtDoc(acDoc, acDate, pRoll->RecBook3);
      if (acDate[0] > '0')
      {
         // Save last recording date
         iRet = atoin(acDate, 8);
         if (iRet < lToday)
         {
            memcpy(pSale->DocDate, acDate, SALE_SIZ_DOCDATE);
            memcpy(pSale->DocNum, acDoc, SALE_SIZ_DOCNUM);

            // Write output
            fputs(acBuf, fdCSale);
            lNewRec++;
         } else
            LogMsg("*** Invalid sale date(3) %d on parcel %.10s", iRet, pRoll->APN);
      }

      // Read next roll record
      iRet = fread((char *)&acRollRec[0], 1, iRollLen, fdRoll);
      bEof = (iRet==iRollLen ? false:true);

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdRoll)
      fclose(fdRoll);
   if (fdCSale)
      fclose(fdCSale);

   // Sort cum sale and remove duplicate
   sprintf(acCSalFile, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sls");
   if (bAppend && !_access(acCSalFile, 0))
   {
      strcat(acOutFile, "+");
      strcat(acOutFile, acCSalFile);
   }

   sprintf(acBuf, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "srt");
   strcpy(acTmp, "S(1,14,C,A,27,8,C,A,57,10,C,D,15,12,C,A) F(TXT) DUPO(B2000,1,34)");
   iRet = sortFile(acOutFile, acBuf, acTmp);
   if (iRet <= 0)
   {
      LogMsg("***** Error sorting %s to %s", acOutFile, acBuf);
      iRet = -1;
   } else
   {
      if (pOutFile)
         strcpy(acOutFile, pOutFile);
      else
         strcpy(acOutFile, acCSalFile);

      // Rename files
      if (!_access(acOutFile, 0))
      {
         sprintf(acTmp, acESalTmpl, myCounty.acCntyCode, myCounty.acCntyCode, "sav");
         if (!_access(acTmp, 0))
            remove(acTmp);
         iRet = rename(acOutFile, acTmp);
      }
      iRet = rename(acBuf, acOutFile);
      if (iRet)
         LogMsg("*** Unable to rename %s to %s", acBuf, acOutFile);
   }

   LogMsg("Total input records:        %u", lCnt);
   LogMsg("Total output records:       %u", lNewRec);
   LogMsg("Last sale date from roll:   %u", lLastRecDate);
   printf("\nTotal output records: %u\n", lNewRec);

   return iRet;
}

/*********************************** loadAlp ********************************
 *
 * Input files:
 *    redifile          899/900-bytes roll file
 *
 * Problems:
 *
 * Processing:
 *
 * Commands:
 *    -Monthly update: -CALP -U -Xsi -O
 *    -LDR           : -CALP -L -Xl -Xs -O
 *
 *
 ****************************************************************************/

int loadAlp(int iLoadFlag, int iSkip)
{
   int   iRet;

   iApnLen = myCounty.iApnLen;
   iRet = guessRecLen(acRollFile, iRollLen);
   if (!iRet)
   {
      LogMsg("***** Roll file is not ready for production: %s", acRollFile);
      return -1;
   }
   iRollLen = iRet;

   // Load tax file
   if (iLoadTax)           // -T
   {
      char  sTmp[_MAX_PATH];

      // Load tax agency table
      iRet = GetIniString(myCounty.acCntyCode, "TaxDist", "", sTmp, _MAX_PATH, acIniFile);
      if (iRet > 0)
         iRet = LoadTaxCodeTable(sTmp);

      iRet = Cres_Load_Cortac(bTaxImport, true);
      if (!iRet && lLastTaxFileDate > 0)
      {
         // Create import files for Items, & Agency
         iRet = Cres_Load_GFGIS(bTaxImport, false);

         // Create empty tables 
         doTaxPrep(myCounty.acCntyCode, TAX_OWNER);
      }

      if (iRet > 0)
         iRet = 0;
   }

   if (!iLoadFlag)
      return iRet;

   /*
   char  acTmpFile[_MAX_PATH];
   strcpy(acRollFile, "G:\\CO_DATA\\Alp\\2005\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SAlp_CD\\raw\\Alp_Sale.2005");
   iRet = Alp_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Alp\\2006\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SAlp_CD\\raw\\Alp_Sale.2006");
   iRet = Alp_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Alp\\2007\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SAlp_CD\\raw\\Alp_Sale.2007");
   iRet = Alp_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Alp\\2008\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SAlp_CD\\raw\\Alp_Sale.2008");
   iRet = Alp_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Alp\\2009\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SAlp_CD\\raw\\Alp_Sale.2009");
   iRet = Alp_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Alp\\2010\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SAlp_CD\\raw\\Alp_Sale.2010");
   iRet = Alp_ExtrSale(acRollFile, acTmpFile, false);
   strcpy(acRollFile, "G:\\CO_DATA\\Alp\\2011\\redifile");
   strcpy(acTmpFile, "H:\\CO_PROD\\SAlp_CD\\raw\\Alp_Sale.2011");
   iRet = Alp_ExtrSale(acRollFile, acTmpFile, false);
   */


   // Extract sale1 from redifile and append to cum sale file
   if (iLoadFlag & EXTR_SALE)                // -Xs
   {
      iRet = Alp_ExtrSale(acRollFile, NULL, true);
      if (!iRet)
         iLoadFlag |= MERG_CSAL;

      iRet = FixSalePrice(acCSalFile, false);
   }

   if (iLoadFlag & EXTR_LIEN)                // -Xl
      iRet = Cres_ExtrLien(myCounty.acCntyCode);

   if (iLoadFlag & LOAD_LIEN)                // -L 
      iRet = Alp_Load_LDR(myCounty.acCntyCode);
   else if (iLoadFlag & LOAD_UPDT)           // -U
      iRet = Alp_LoadRoll(myCounty.acCntyCode, iSkip);

   if (!iRet && (iLoadFlag & MERG_CSAL))
      iRet = ApplyCumSale(iSkip, acCSalFile, false, SALE_USE_SCUPDXFR, true);

   return iRet;
}
