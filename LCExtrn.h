#ifndef _myLoadExtrn_
#define _myLoadExtrn_

extern   COUNTY_INFO myCounty;
extern	FILE  *fdRoll, *fdLienExt, *fdCSale;
extern	char  acRawTmpl[], acFlgTmpl[], acGrGrTmpl[], acESalTmpl[], acTmpPath[];
extern	char  acRollFile[], acLogFile[], acIniFile[], acCSalFile[], acPubParcelFile[];
extern	char  acTaxFile[], acSaleFile[], acCharFile[], acLienTmpl[];
extern	int   iRecLen, iAsrRecLen, iRollLen, iApnLen, iSaleLen, iTaxLen, iCharLen, iTokens;
extern	int   iSkip, iNoMatch, iLoadFlag, iLoadTax, iMaxLegal, iMaxChgAllowed;
extern	long  lRecCnt, lAssrRecCnt, lLastRecDate, lLastGrGrDate, lToday, lLienDate, lLDRRecCount, lToyear, lLienYear, lTaxYear;
extern	long  lCharSkip, lSaleSkip, lTaxSkip, lSaleMatch, lTaxMatch, lCharMatch, lLastTaxFileDate;
extern	bool  bEnCode, bUseSfxXlat, bCopySales, bOverwriteLogfile, bDebug, bFixTRA,
               bSaleImport, bGrgrImport, bTaxImport, bMixSale;

// INY
extern   FILE  *fdSale, *fdChar;
extern   char  acRollSale[], acCChrFile[];

// TUO
extern   char  acToday[], acEGrGrTmpl[], acDocPath[], acSaleTmpl[], sTaxOutTmpl[], cDelim;
extern   FILE  *fdTax;
extern	char  *apTokens[MAX_FLD_TOKEN], acValueFile[];

// TRI
extern   FILE  *fdSitus;
#endif