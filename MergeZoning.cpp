/*****************************************************************************
 *
 * This file is prepared for CCX and counties that use zoning data from Denise.
 * All data must be preprocessed by LoadZoning program before calling MergeZoning().
 *
 * 03/25/2020  Modify MergeZoning() to clear zoning before applying new one to avoid any left over.
 * 04/07/2020  Modify MergeZoning() log msg if missing zoning file
 * 06/24/2020  Add support for LoadCres
 * 07/20/2020  Modify MergeZoneRec() to populate PQZoning only.  Leave old Zoning field as county supplied.
 * 08/13/2020  Modify MergeZoning() to use zoning in [Data] section of INI file if not defined in county section.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "CountyInfo.h"
#include "Prodlib.h"
#include "R01.h"
#include "Tables.h"
#include "Logs.h"
#include "Utils.h"
#include "doSort.h"
#if defined(AFX_STDAFX_H__LOADONE_EFA4_45F7_9334_A6196A45181B__INCLUDED_)
   #include "LOExtrn.h"
#endif
#if defined(AFX_STDAFX_H__LOADMB_EFA4_45F7_9334_A6196A45181B__INCLUDED_)
   #include "MBExtrn.h"
#endif
#if defined(AFX_STDAFX_H__LOADCRES_500C_46D1_99BD_BD199B02CBC1__INCLUDED_)
   #include "LCExtrn.h"
#endif
#include "MergeZoning.h"


FILE  *fdZone;
long  lZoneSkip, lZoneMatch;

/********************************* MergeZoneRec ******************************
 *
 * Merge Zoning from ???_Zone.txt
 * This function will overwrite zoning from county roll 
 *
 * Return 0 if successful, otherwise error.
 *
 *****************************************************************************/

int MergeZoneRec(char *pOutbuf)
{
   static   char acRec[1024], *pRec=NULL;
   char     *apItems[16], *pNextZone;
   int      iRet=0, iTmp;

   // Get rec
   if (!pRec)
   {
      // Get first rec
      pRec = fgets(acRec, 1024, fdZone);
   }

   do
   {
      if (!pRec)
      {
         fclose(fdZone);
         fdZone = NULL;
         return 1;      // EOF
      }

      iTmp = memcmp(pOutbuf, pRec, iApnLen);
      if (iTmp > 0)
      {
         if (bDebug)
            LogMsg0("Skip Zoning rec %.*s", iApnLen, pRec);
         pRec = fgets(acRec, 1024, fdZone);
         lZoneSkip++;
      }
   } while (iTmp > 0);

   // If not match, return
   if (iTmp)
      return 1;

   iRet = ParseStringIQ(acRec, '|', 16, apItems);
   if (iRet < ZONE_CODE+1)
   {
      if (*pRec == 13 || *pRec == 10)
      {
         fclose(fdZone);
         fdZone = NULL;
         iRet = 1;      // EOF
      } else
      {
         LogMsg("***** Error: bad input roll record for APN=%s", apItems[0]);
         iRet = -1;
      }

      return iRet;
   }

   lZoneMatch++;
   pNextZone = NULL;

   // Zoning
   if (*apItems[ZONE_CODE] > ' ')
   {
      char *pTmp;

      iTmp = iTrim(apItems[ZONE_CODE]);
      vmemcpy(pOutbuf+OFF_ZONE_X1, apItems[ZONE_CODE], SIZ_ZONE_X1, iTmp);
      if (iTmp > SIZ_ZONE_X1)
      {
         pNextZone = apItems[ZONE_CODE]+SIZ_ZONE_X1;
         iTmp = strlen(pNextZone);
         vmemcpy(pOutbuf+OFF_ZONE_X2, pNextZone, SIZ_ZONE_X2);
         if (iTmp > SIZ_ZONE_X2)
            vmemcpy(pOutbuf+OFF_ZONE_X3, pNextZone+SIZ_ZONE_X2, SIZ_ZONE_X3);
      }
   }

   // Get next record
   pRec = fgets(acRec, 1024, fdZone);

   return 0;
}

/******************************** MergeZoning *******************************
 *
 * Merge zoning data
 *
 ****************************************************************************/

int MergeZoning(char *pCnty, int iSkip)
{
   char     acBuf[MAX_RECSIZE], acTmp[_MAX_PATH];
   char     acOutFile[_MAX_PATH], acRawFile[_MAX_PATH];

   HANDLE   fhIn, fhOut;
   DWORD    nBytesRead, nBytesWritten;
   BOOL     bRet;
   int      iRet;
   long     lCnt=0;

   LogMsg0("Merge Zoning");

   // Setup file names
   sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "R01");
   sprintf(acOutFile, acRawTmpl, pCnty, pCnty, "TMP");

   if (_access(acRawFile, 0))
      sprintf(acRawFile, acRawTmpl, pCnty, pCnty, "S01");

   // Open Zoning file
   iRet = GetIniString(pCnty, "Zoning", "", acBuf, _MAX_PATH, acIniFile);
   if (!iRet)
   {
      iRet = GetIniString("Data", "Zoning", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acBuf, acTmp, pCnty, pCnty);
   }
   if (acBuf[0] > ' ')
   {
      // Sort on ASMT
      sprintf(acTmp, "%s\\%s\\%s_Zoning.srt", acTmpPath, pCnty, pCnty);
      iRet = sortFile(acBuf, acTmp, "S(#1,C,A)");

      LogMsg("Open Zoning file %s", acTmp);
      fdZone = fopen(acTmp, "r");
      if (fdZone == NULL)
      {
         LogMsg("***** Error opening Zoning file: %s\n", acTmp);
         return -2;
      }
   } else
   {
      LogMsg("*** Missing zoning file.  Merge zoning ignored.  Please check INI file");
      return 0;
   }

   // Open R01 file
   LogMsg("Open R01 file %s", acRawFile);
   fhIn = CreateFile(acRawFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening input file: %s\n", acRawFile);
      return -3;
   }

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   fhOut = CreateFile(acOutFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

   if (fhOut == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error opening output file: %s\n", acOutFile);
      return -4;
   }

   // Copy skip record
   bRet = true;
   memset(acBuf, ' ', iRecLen);
   while (iSkip-- > 0)
   {
      ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
   }

   // Merge loop
   while (bRet)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

      // Check for EOF
      if (!bRet)
      {
         LogMsg("Error reading input file %s (%f)", acRawFile, GetLastError());
         iRet = -1;
         break;
      }

      // EOF ?
      if (iRecLen != nBytesRead)
         break;

      // Remove old data
      removeZoning(acBuf);

      // Merge Zoning
      if (fdZone)
         iRet = MergeZoneRec(acBuf);

      bRet = WriteFile(fhOut, acBuf, iRecLen, &nBytesWritten, NULL);
      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   printf("\n");

   // Close files
   if (fdZone)
      fclose(fdZone);
   if (fhOut)
      CloseHandle(fhOut);
   if (fhIn)
      CloseHandle(fhIn);

   // Rename output file
   sprintf(acTmp, acRawTmpl, pCnty, pCnty, "Z01");
   if (!_access(acTmp, 0))
      remove(acTmp);
   LogMsg("Rename %s to %s", acRawFile, acTmp);
   iRet = rename(acRawFile, acTmp);
   LogMsg("Rename %s to %s", acOutFile, acRawFile);
   iRet = rename(acOutFile, acRawFile);

   LogMsg("Total output records:       %u", lCnt);
   LogMsg("Number of Zone matched:     %u", lZoneMatch);
   LogMsg("Number of Zone skiped:      %u", lZoneSkip);

   return iRet;
}

