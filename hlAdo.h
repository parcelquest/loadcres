
#if !defined(AFX_hlAdo_H__FE44AF27_DA4F_4787_A6EC_9CE68FFA219E__INCLUDED_)
#define AFX_hlAdo_H__FE44AF27_DA4F_4787_A6EC_9CE68FFA219E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#import "C:\Program Files\Common Files\System\ADO\msado15.dll" \
    no_namespace rename("EOF", "EndOfFile")
#include <ole2.h>
#include <objbase.h>

class hlAdoRs;

#define AdoCatch(e)		catch(_com_error &e)

// The high-level Ado class...
class hlAdo  
{
private:
   _ConnectionPtr pConn;
   _CommandPtr comm;
public:
   // is the connection Open? 
   bool IsOpen,
   // do we do a transaction? We need this cause if there
   // is an error in the transaction and we "AdoCatch" it,then
   // in order not to crush we shall do a Rollback.
      DoesTrans;
public:
   // Transaction functions
   void RollbackTrans();
   void CommitTrans();
   void BeginTrans();
   // Mostly used internally to create an hlAdoRs
   _RecordsetPtr Execute_(CString & sql);
   _RecordsetPtr Execute_(char * sql);
   // constructor. Pass the connection string.
   hlAdo(CString &constr);
   hlAdo(char* constr);
   hlAdo(void);
   bool Connect(LPCTSTR constr);


   // destructor
   ~hlAdo();

   // return the conection pointer
   _ConnectionPtr GetConnection();
   // execute a query which returns data, i.e. select
   hlAdoRs * Execute(CString &sql);
   hlAdoRs * Execute(char* sql);
   // execute a query which doesn't return data i.e. update
   // defaule timeout to 10 hours
   void ExecuteCommand(CString &sql, int iTimeOut=36000);
   void ExecuteCommand(char* sql, int iTimeOut=36000);
};

// The high-level Recordset class.
class hlAdoRs
{
private:
   _RecordsetPtr rs;
   hlAdo *hlado;
   bool just_in;

public:
   bool isOpened;

   // the recommended constructor. You shall have already
   // made the connection to the DB with an hlAdo object.
   hlAdoRs(hlAdo &hl,CString &sql);
   hlAdoRs(hlAdo &hl,char *sql);
   hlAdoRs();
   void Close();
   bool Open(hlAdo &hl, char *sql);

   // get an item of a row.
   CString GetItem(CString &ColumnName);
   CString GetItem(char *ColumnName);
   // move to the next row.
   bool next();

   // the constructor used by hlAdo->Execute to create this
   // object.
   hlAdoRs(_RecordsetPtr rsp,hlAdo *hlado);

   // destruction
   ~hlAdoRs();
};

// the help functions...
void TESTHR(HRESULT x);
CString AdoError(_ConnectionPtr pConnection);
CString ComError(_com_error &e);

#endif // !defined(AFX_hlAdo_H__FE44AF27_DA4F_4787_A6EC_9CE68FFA219E__INCLUDED_)

